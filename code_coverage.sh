#!/bin/bash

FILE=./integraal/integraal-all/target/site/jacoco-aggregate/index.html

echo -n "Code coverage is "

grep -o -E "Total[^%]*<td class=\"ctr2\">[[:digit:]]*\s?%</td>" $FILE | grep -o -E "[[:digit:]]*\s?%"
