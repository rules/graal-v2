# InteGraal

## Apps built on top of InteGraal

- [CLI](https://gitlab.inria.fr/api/v4/projects/34878/jobs/artifacts/develop/raw/integraal/integraal-api/target/integraal-cli.jar?job=integraal-build): a command line interface with handling of an execution environment
- [Commander](https://gitlab.inria.fr/api/v4/projects/34878/jobs/artifacts/develop/raw/integraal/integraal-api/target/integraal-commander.jar?job=integraal-build): a command line tool usefull for integrating InteGraal in shell scripts

## Installation

The current release of InteGraal is [available on Maven](https://central.sonatype.com/artifact/fr.lirmm.graphik/integraal). Please check the website for the last version.

InteGraal requires Java version >= 21.

To use the library, you can either use the [jar files automatically built for each module](https://gitlab.inria.fr/api/v4/projects/34878/jobs/artifacts/develop/download?job=integraal-build) (note that integraal-all contains all the modules) or add the required modules as maven dependencies (such as in the example below for the module integraal-model).

```xml
<dependency>
    <groupId>fr.lirmm.graphik</groupId>
    <artifactId>integraal-model</artifactId>
    <version>1.1.0</version>
</dependency>
```

To clone Integraal's repository, you can use
```sh
git clone https://gitlab.inria.fr/rules/integraal.git
```

Available source modules include :
- [integraal-api](https://gitlab.inria.fr/rules/integraal/-/wikis/InteGraal-Commander) : support for high level user interface
- integraal-model: essential data models for facts and rules
- [integraal-backward-chaining](https://gitlab.inria.fr/rules/integraal/-/wikis/User-Documentation/Backward-Chaining) : support for reasoning with rewriting approaches
- integraal-forgetting : support for the process of forgetting ; remove some rules from the rulebase
- [integraal-forward-chaining](https://gitlab.inria.fr/rules/integraal/-/wikis/User-Documentation/Forward-Chaining) : support for reasoning with saturation approaches
- integraal-graal-ruleset-analysis : import of graal's rule analysis module (for internal use mostly)
- integraal-grd : support for graph of rule dependencies
- [integraal-io](https://gitlab.inria.fr/rules/integraal/-/wikis/User-Documentation/IO/Description) : support for import and export of data from DLGP and RDF files
- [integraal-query-evaluation](https://gitlab.inria.fr/rules/integraal/-/wikis/User-Documentation/Query-Evaluation) : support for evaluating queries on a factbase
- integraal-redundancy : support for detection and removal of dependencies between rules 
- [integraal-storage](https://gitlab.inria.fr/rules/integraal/-/wikis/User-Documentation/Storage) : support for facts storages handled by InteGraal
- integraal-unifiers : support for unifier computing
- integraal-util : support for various small help functions
- [integraal-views](https://gitlab.inria.fr/rules/integraal/-/wikis/User-Documentation/Storage) : support for facts access using relational views

On dedicated branches, modules exist for additional features such as using the Tatooine mediator or Corese storages. Please note that those features will require some additional dependencies that are neither distributed with InteGraal nor available with Maven.

## Documentation

- The [project `integraal-examples`](https://gitlab.inria.fr/rules/integraal-examples) includes examples for the main features of the library as well as some of the concret cases in which the library have been used
    - Please note that some of the use cases may require some identification information to access the datasources.
- The [InteGraal Wiki](https://gitlab.inria.fr/rules/integraal/-/wikis/home) is available online, with detailed information about InteGraal usage and high level description of the main modules with code examples for a quick use.
- The [javadoc](https://rules.gitlabpages.inria.fr/integraal/) is available in each package and [online](https://rules.gitlabpages.inria.fr/integraal/).
- If you need more information about the library or encounter any problem, please contact [Florent Tornil](mailto:florent.tornil@inria.fr) or [Federico Ulliana](mailto:federico.ulliana@inria.fr).

## Development

- If you want to help us extend and improve InteGraal you are welcome and we thank you in advance for your work with us.

## Contact

If you need more information about the library or encounter any problem, please contact [Florent Tornil](mailto:florent.tornil@inria.fr) or [Federico Ulliana](mailto:federico.ulliana@inria.fr).

If you encounter some unexpected behaviour, feel free to fill an issue on this Git and we will try our best to help you.

## License

Copyright 2022 INRIA, LIRMM, CNRS, Univ. Montpellier, INRAe

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
