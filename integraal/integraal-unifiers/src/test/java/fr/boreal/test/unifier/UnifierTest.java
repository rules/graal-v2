package fr.boreal.test.unifier;

import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.partition.TermPartition;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.impl.FORuleImpl;
import fr.boreal.unifier.QueryUnifier;
import fr.boreal.unifier.QueryUnifierAlgorithm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runners.Parameterized.Parameters;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class UnifierTest {

	static final QueryUnifierAlgorithm algo = new QueryUnifierAlgorithm();

	static final Predicate p = SameObjectPredicateFactory.instance().createOrGetPredicate("p", 2);
	static final Predicate q = SameObjectPredicateFactory.instance().createOrGetPredicate("q", 2);
	static final Predicate r = SameObjectPredicateFactory.instance().createOrGetPredicate("r", 2);
	static final Predicate s = SameObjectPredicateFactory.instance().createOrGetPredicate("s", 1);

	static final Variable u = SameObjectTermFactory.instance().createOrGetVariable("u");
	static final Variable v = SameObjectTermFactory.instance().createOrGetVariable("v");
	static final Variable w = SameObjectTermFactory.instance().createOrGetVariable("w");
	static final Variable x = SameObjectTermFactory.instance().createOrGetVariable("x");
	static final Variable y = SameObjectTermFactory.instance().createOrGetVariable("y");
	static final Variable z = SameObjectTermFactory.instance().createOrGetVariable("z");

	static final Atom qxy = new AtomImpl(q, x, y);

	static final Atom pxy = new AtomImpl(p, x, y);
	static final Atom pxz = new AtomImpl(p, x, z);
	static final Atom puv = new AtomImpl(p, u, v);
	static final Atom pwv = new AtomImpl(p, w, v);

	static final Atom rwu = new AtomImpl(r, w, u);

	static final Atom sx = new AtomImpl(s, x);
	static final Atom qvu = new AtomImpl(q, v, u);

	// q(x, y) -> p(x, y)
	static final FORule Rqxy_pxy = new FORuleImpl(qxy, pxy);

	// q(x, y) -> p(x, z)
	static final FORule Rqxy_pxz = new FORuleImpl(qxy, pxz);

	// ?() :- p(u, v), p(w, v), r(w, u)
	static final FOQuery<?> Qpuv_pwv_rwv = FOQueryFactory.instance().createOrGetQuery(
			FOFormulaFactory.instance().createOrGetConjunction(puv, pwv, rwu),
			List.of(), 
			null);

	// s(X) -> q(X, Y)
	static final FORule Rsx_pxz = new FORuleImpl(sx, qxy);

	// ?(u) :- q(v, u)
	static final FOQuery<?> Qu_qvu = FOQueryFactory.instance().createOrGetQuery(
			qvu,
			List.of(u), 
			null);

	@Parameters
	static Stream<Arguments> getMostGeneralSinglePieceUnifiersData() {
		return Stream.of(
				Arguments.of(Qpuv_pwv_rwv, Rqxy_pxy, Set.of(
						Set.of(Set.of(u, x), Set.of(v, y)),
						Set.of(Set.of(w, x), Set.of(v, y)))),
				Arguments.of(Qpuv_pwv_rwv, Rqxy_pxz, Set.of(
						Set.of(Set.of(u, w, x), Set.of(v, z)))),
				Arguments.of(Qu_qvu, Rsx_pxz, Set.of())
				);
	}

	@DisplayName("Test MostGeneralSinglePieceUnifiers computing")
	@ParameterizedTest(name = "{index}: unif({0}, {1}) = {2}")
	@MethodSource("getMostGeneralSinglePieceUnifiersData")
	void getMostGeneralSinglePieceUnifiersTest(FOQuery<?> query, FORule rule, Collection<Collection<Set<Term>>> expected_partitions) {
		Collection<QueryUnifier> unifiers = algo.getMostGeneralSinglePieceUnifiers(query, rule);

		Collection<TermPartition> computeds = unifiers.stream().map(QueryUnifier::getPartition).collect(Collectors.toSet());
		Collection<TermPartition> expecteds = expected_partitions.stream().map(TermPartition::new).collect(Collectors.toSet());

		Assertions.assertTrue(expecteds.containsAll(computeds));
		Assertions.assertTrue(computeds.containsAll(expecteds));
	}

}
