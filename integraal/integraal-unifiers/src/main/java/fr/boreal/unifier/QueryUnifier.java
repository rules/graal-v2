/**
 *
 */
package fr.boreal.unifier;

import com.google.common.collect.Sets;
import fr.boreal.model.formula.FOFormulas;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.partition.TermPartition;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.rule.api.FORule;
import fr.lirmm.boreal.util.FOQueries;

import java.util.Optional;
import java.util.Set;

/**
 * A unifier of a query Q with a rule R is a triple u(Q', H', P)
 * where Q' is a subset of Q
 * H' is a subset of  head(R)
 * P is a partition of the terms of Q' and H'
 * <br/>
 * It describes how to unify a piece of a query with a part of a head
 * FORule in order to rewrite the fact according to the FORule
 *
 * @author Guillaume Pérution-Kihli
 * @author Florent Tornil
 */
public interface QueryUnifier {

	/**
	 * Not used yet
	 * @return the query unified by this unifier
	 */
	FOQuery<?> getQuery();

	/**
	 * Not used yet
	 * @return the FORule where the unifier applies
	 */
	FORule getRule();

	/**
	 * @return the part of the query that is unified by this unifier
	 */
	FOFormula getUnifiedQueryPart();

	/**
	 * @return the partition that unify the piece and a part of the head FORule
	 */
	TermPartition getPartition();

	/**
	 * Not used yet
	 * @return the initial FORules (before aggregation) where the unifier apply
	 */
	Set<FORule> getInitialFORules();

	/**
	 * @param f a fact
	 * @return the image of a given fact by the substitution associated to this unifier
	 */
	FOFormula getImageOf(FOFormula f);

	/**
	 * @return the substitution associated to this unifier
	 */
	Substitution getAssociatedSubstitution();

	/**
	 * Creates a new unifier corresponding to the aggregation of this unifier and the given one
	 * Also assure that the unifiers are on a different set of variables, renaming the given one if necessary
	 * Return the aggregated unifier if possible or an empty optional if the unifiers are not compatible
	 * @param u unifier to aggregate
	 * @return the aggregation of the given unifier and the receiving unifier
	 */
	Optional<QueryUnifier> safeAggregate(QueryUnifier u);

	/**
	 * Creates a new unifier corresponding to the aggregation of this unifier and the given one
	 * @param u unifier to aggregate
	 * @return the aggregation of the given unifier and the receiving unifier
	 */
	QueryUnifier aggregate(QueryUnifier u);

	/**
	 * If the pieces of the two unifiers have atom in common the unifiers are not compatible
	 * Otherwise check if the two partition of the unifiers are possible to join
	 * @param u unifier to check compatibility with
	 * @return true iff this unifier is compatible with the given unifier
	 */
	boolean isCompatible(QueryUnifier u);

	/**
	 * Rewrite the query with the unifier
	 * @param query to rewrite
	 * @return the given query rewritten by the given unifier
	 */
	default FOQuery<?> apply(FOQuery<?> query) {

		FORule rule = this.getRule();
		FOFormula piece = this.getUnifiedQueryPart();
		Substitution renaming = this.getAssociatedSubstitution();

		// Part of the new query coming from the body of the rule, renamed by the unifier
		FOFormula from_unified_rule = FOFormulas.createImageWith(rule.getBody(), renaming);

		// Part of the new query left from the initial one
		// Computed as the query minus the unified piece
		FOQuery<?> renamed_query = FOQueries.createImageWith(query, renaming);
		FOFormula renamed_piece = FOFormulas.createImageWith(piece, renaming);
		Set<Atom> left_from_initial_query = Sets.difference(renamed_query.getFormula().asAtomSet(), renamed_piece.asAtomSet());

		// The new query conditions is the union of the rule part and the initial query that is not unified
		Set<Atom> new_query_conditions = Sets.union(left_from_initial_query, from_unified_rule.asAtomSet());
		FOFormula new_query_formula = FOFormulaFactory.instance().createOrGetConjunction(new_query_conditions.toArray(new Atom[0]));

		return FOQueryFactory.instance().createOrGetQuery(
				new_query_formula,
				renamed_query.getAnswerVariables(),
				renamed_query.getVariableEqualities());
	}


}
