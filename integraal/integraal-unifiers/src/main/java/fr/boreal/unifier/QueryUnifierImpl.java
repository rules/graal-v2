package fr.boreal.unifier;

import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import fr.boreal.model.formula.FOFormulas;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.partition.TermPartition;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.impl.FORuleImpl;
import fr.lirmm.boreal.util.Rules;

/**
 * Basic implementation for the query unifier
 */
public class QueryUnifierImpl implements QueryUnifier {

	/**
	 * Aggregation of all the FORule that are unified with the query
	 */
	private final FORule rule;

	/**
	 * Set of all rules unified with the query
	 */
	private final Set<FORule> initialFORules;

	/**
	 * Query that is unified with the FORule head
	 */
	private final FOQuery<?> query;

	/**
	 * Part of the query that is unified
	 */
	private final FOFormula unifiedQueryPart;

	/**
	 * Partition that unify the piece of the query and a part of the head FORule
	 */
	private final TermPartition partition;

	/**
	 * Substitution associated to this unifier
	 * Uses the substitution associated to the partition using variable names from the query when possible
	 * This variable is used as a cache
	 */
	private Substitution associatedSubstitution;

	/**
	 * Creates a new QueryUnifier using the given parameters
	 * @param query the query that is unified
	 * @param unifiedQueryPart the part of the query that is unified
	 * @param partition the partition of the terms of the rule and the query
	 * @param rule the rule that is unified
	 */
	public QueryUnifierImpl(
			FOQuery<?> query,
			FOFormula unifiedQueryPart, 
			TermPartition partition, 
			FORule rule) {
		this(query, unifiedQueryPart, partition, rule, Set.of(rule));
	}

	/**
	 * Creates a new QueryUnifier using the given parameters
	 * @param query the query that is unified
	 * @param unifiedQueryPart the part of the query that is unified
	 * @param partition the partition of the terms of the rule and the query
	 * @param rule the rule that is unified
	 * @param initialFORules the initial rules that created the rule
	 */
	public QueryUnifierImpl(
			FOQuery<?> query,
			FOFormula unifiedQueryPart, 
			TermPartition partition, 
			FORule rule,
			Set<FORule> initialFORules) {
		this.rule = rule;
		this.query = query;
		this.unifiedQueryPart = unifiedQueryPart;
		this.partition = partition;
		this.initialFORules = initialFORules;
	}

	@Override
	public FORule getRule() {
		return this.rule;
	}

	@Override
	public FOFormula getUnifiedQueryPart() {
		return this.unifiedQueryPart;
	}

	@Override
	public FOQuery<?> getQuery() {
		return query;
	}

	@Override
	public TermPartition getPartition() {
		return partition;
	}

	@Override
	public Set<FORule> getInitialFORules() {
		return initialFORules;
	}

	@Override
	public Substitution getAssociatedSubstitution() {
		if (associatedSubstitution == null) {
			associatedSubstitution = partition.getAssociatedSubstitution(query).orElseThrow();
		}
		return new SubstitutionImpl(associatedSubstitution);
	}

	@Override
	public FOFormula getImageOf(FOFormula f) {
		FOFormula atomset = null;
		Substitution s = this.getAssociatedSubstitution();
		if (s != null) {
			atomset = FOFormulas.createImageWith(f, s);
		}
		return atomset;
	}

	@Override
	public Optional<QueryUnifier> safeAggregate(QueryUnifier u) {
		// Unifiers should be on the same query
		if(!this.getQuery().equals(u.getQuery())) {
			return Optional.empty();
		}

		// But disjoint parts of this query
		var thisPiece = this.getUnifiedQueryPart().asAtomSet();
		for (Atom a : u.getUnifiedQueryPart().asAtomSet()) {
			if (thisPiece.contains(a)) {
				return Optional.empty();
			}
		}

		FORule uRule = u.getRule();

		Set<Variable> ruleVariables = new HashSet<>();
		ruleVariables.addAll(uRule.getBody().getVariables());
		ruleVariables.addAll(uRule.getHead().getVariables());

		Substitution freshRenamer = new SubstitutionImpl();
		for(Variable v : ruleVariables) {
			freshRenamer.add(v, SameObjectTermFactory.instance().createOrGetFreshVariable());
		}

		FORule safeURule = Rules.createImageWith(uRule, freshRenamer);
		TermPartition safeUPartition = u.getPartition().createImageWith(freshRenamer);
		QueryUnifier safeU = new QueryUnifierImpl(
				u.getQuery(),
				u.getUnifiedQueryPart(),
				safeUPartition,
				safeURule);

		// After the renaming, we can check the second condition for unifier compatibility :
		// the join of the partitions is admissible
		TermPartition part = new TermPartition(this.getPartition());
		part.join(safeU.getPartition());
		if(part.getAssociatedSubstitution(null).isEmpty()) {
			return Optional.empty();
		} else {
			return Optional.of(this.aggregate(safeU));
		}
	}

	@Override
	public QueryUnifier aggregate(QueryUnifier other) {
		// we create a part that is the conjunction of the two parts
		FOFormula parts = FOFormulaFactory.instance()
				.createOrGetConjunction(
						this.getUnifiedQueryPart(),
						other.getUnifiedQueryPart());

		// we create a FORule that is the aggregation of the two FORules
		FOFormula b = FOFormulaFactory.instance()
				.createOrGetConjunction(
						this.getRule().getBody(),
						other.getRule().getBody());
		FOFormula h = FOFormulaFactory.instance()
				.createOrGetConjunction(
						this.getRule().getHead(),
						other.getRule().getHead());
		FORule aggregated_rule = new FORuleImpl(b, h);

		// we create the partition which is the join of the two partitions
		TermPartition partition = new TermPartition(this.getPartition());
		partition.join(other.getPartition());

		// We store the initial rules
		Set<FORule> rules = new HashSet<>(this.getInitialFORules());
		rules.addAll(other.getInitialFORules());

		return new QueryUnifierImpl(this.getQuery(), parts, partition, aggregated_rule, rules);
	}

	@Override
    public boolean isCompatible(QueryUnifier other) {
		var thisPiece = this.getUnifiedQueryPart().asAtomSet();
		for (Atom a : other.getUnifiedQueryPart().asAtomSet()) {
			if (thisPiece.contains(a)) {
				return false;
			}
		}
		var part = new TermPartition(this.getPartition());
		part.join(other.getPartition());		

		return part.getAssociatedSubstitution(null).isPresent();
	}

	@Override
	public String toString() {
		try {
			return "(QueryUnifier |  " + unifiedQueryPart + " <=> " + rule.getHead() + " | = " + partition + ")";
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(query, unifiedQueryPart, partition, rule, initialFORules);
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (!(o instanceof QueryUnifier other)) {
			return false;
		}

        return this.getQuery().equals(other.getQuery()) &&
				this.getUnifiedQueryPart().equals(other.getUnifiedQueryPart()) &&
				this.getPartition().equals(other.getPartition()) &&
				this.getRule().equals(other.getRule()) &&
				this.getInitialFORules().equals(other.getInitialFORules());
	}
}
