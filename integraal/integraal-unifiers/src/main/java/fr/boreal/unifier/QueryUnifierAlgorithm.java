package fr.boreal.unifier;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.partition.TermPartition;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.ruleCompilation.NoRuleCompilation;
import fr.boreal.model.ruleCompilation.api.RuleCompilation;

/**
 * Computes the most general single piece unifiers
 * This uses Mélanie König's thesis algorithms
 * <br/>
 * These algorithms also take into account a compilation of the rules
 */
public class QueryUnifierAlgorithm {

	private final RuleCompilation compilation;

	/**
	 * Default constructor.
	 * Uses no compilation
	 */
	public QueryUnifierAlgorithm() {
		this(NoRuleCompilation.instance());
	}

	/**
	 * Constructor with compilation
	 * @param compilation the compilation to use
	 */
	public QueryUnifierAlgorithm(RuleCompilation compilation) {
		this.compilation = compilation;
	}

	/**
	 * Algorithm 2 in Mélanie König's thesis
	 * Computes the single piece most general unifiers of the given query with the given rule
	 * The rule and the query must have a disjoint set of variables
	 * @param q the query
	 * @param r the rule
	 * @return all the most general single piece unifiers of the query with the rule
	 */
	public Collection<QueryUnifier> getMostGeneralSinglePieceUnifiers(FOQuery<?> q, FORule r) {
		Set<QueryUnifier> u = new HashSet<>();
		Set<QueryUnifier> apu = computeAtomicPreUnifiers(q, r);
		while (!apu.isEmpty()) {
			QueryUnifier qu = apu.iterator().next();
			apu.remove(qu);
			u.addAll(extend(q, qu, apu));
		}

		return u;
	}

	/////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////

	// Algorithm 3 in Mélanie König's thesis
	private Set<QueryUnifier> computeAtomicPreUnifiers(FOQuery<?> q, FORule r) {
		Set<QueryUnifier> atomicQueryUnifiers = new HashSet<>();
		Collection<Atom> queryAtoms = q.getFormula().asAtomSet();
		for (Atom b : queryAtoms) {
			for (Atom a : r.getHead().asAtomSet()) {
				Set<TermPartition> partitions = this.compilation.getUnifications(a, b);
				for(TermPartition tp : partitions) {
					if (tp.isValid(r, q)) {
						atomicQueryUnifiers.add(new QueryUnifierImpl(q, b, tp, r));
					}
				}
			}
		}
		return atomicQueryUnifiers;
	}

	// Algorithm 4 in Mélanie König's thesis
	private Set<QueryUnifier> extend(FOQuery<?> rewrittenQuery, QueryUnifier queryUnifier, Set<QueryUnifier> atomicQueryUnifiers) {
		Set<Atom> difference = new HashSet<>(rewrittenQuery.getFormula().asAtomSet());
		difference.removeAll(queryUnifier.getUnifiedQueryPart().asAtomSet());

		Set<Variable> differenceVariables = difference.stream()
				.flatMap(a -> a.getVariables().stream())
				.collect(Collectors.toSet());

		Set<Variable> separatingVariables = rewrittenQuery.getFormula().asAtomSet().stream()
				.flatMap(a -> a.getVariables().stream())
				.filter(differenceVariables::contains)
				.collect(Collectors.toSet());

		Set<Variable> separatingStickyVariables = queryUnifier.getPartition().getSeparatingStickyVariables(
				separatingVariables, 
				queryUnifier.getRule());

		if (separatingStickyVariables.isEmpty()) {
			return Set.of(queryUnifier);
		} else {
			Set<QueryUnifier> res = new HashSet<>();
			Set<Atom> Qext = difference.stream()
					.filter(a -> !queryUnifier.getPartition().getSeparatingStickyVariables(a.getVariables(), queryUnifier.getRule()).isEmpty())
					.collect(Collectors.toSet());
			for (QueryUnifier uExt: extend_aux(queryUnifier, Qext, atomicQueryUnifiers)) {
				res.addAll(extend(rewrittenQuery, uExt, atomicQueryUnifiers));
			}
			return res;
		}
	}

	// Algorithm 5 in Mélanie König's thesis
	private Set<QueryUnifier> extend_aux(QueryUnifier queryUnifier, Set<Atom> Qext, Set<QueryUnifier> atomicQueryUnifiers) {
		if (Qext.isEmpty()) {
			return Set.of(queryUnifier);
		} else {
			Set<QueryUnifier> ext = new HashSet<>();
			Atom a = Qext.iterator().next();

			for (QueryUnifier pu : atomicQueryUnifiers) {
				if (pu.getUnifiedQueryPart().asAtomSet().iterator().next().equals(a)) {
					TermPartition tp = new TermPartition(queryUnifier.getPartition());
					tp.join(pu.getPartition());
					if (tp.isValid(pu.getRule())) {
						var newQext = new HashSet<>(Qext);
						newQext.remove(a);
						ext.addAll(extend_aux(queryUnifierUnion(queryUnifier, pu), newQext, atomicQueryUnifiers));
					}
				}
			}

			return ext;
		}
	}

	private QueryUnifier queryUnifierUnion(QueryUnifier qu1, QueryUnifier qu2) {
		Set<Atom> unifiedParts = new HashSet<>(qu1.getUnifiedQueryPart().asAtomSet());
		unifiedParts.addAll(qu2.getUnifiedQueryPart().asAtomSet());

		TermPartition unifiedPartition = new TermPartition(qu1.getPartition());
		unifiedPartition.join(qu2.getPartition());

		return new QueryUnifierImpl(
				qu1.getQuery(), 
				FOFormulaFactory.instance().createOrGetConjunction(unifiedParts.toArray(new Atom[0])),
				unifiedPartition, 
				qu1.getRule(), 
				qu1.getInitialFORules());
	}
}
