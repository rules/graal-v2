package fr.boreal.forward_chaining.chase.rule_applier.trigger_computer;

import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.FOQuery;

import java.util.Iterator;

/**
 * Computes the triggers from a rule's body on a factbase
 */
public interface TriggerComputer {

	/**
	 * Initialize the trigger computer for the given chase
	 * @param c the chase object
	 */
    void init(Chase c);

	/**
	 * Computes and returns the triggers of the body on the factbase
	 * @param body the body of the rule to evaluate
	 * @param fb the factbase on which to evaluate the rule
	 * @return the triggers of the rule on the factbase
	 */
	Iterator<Substitution> compute(FOQuery<?> body, FactBase fb);


	/**
	 * Defautl method to describe the rule scheduler
	 */
	default String describe(){return this.getClass().getSimpleName();}
}
