package fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.facts_handler;

import java.util.Collection;
import java.util.stream.Collectors;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBase;

/**
 * Keep the new facts and delay the addition in the factbase
 */
public class DelegatedApplication implements FactsHandler {

	@Override
	public FOFormula add(FOFormula new_facts, FactBase fb) {		
		Collection<FOFormula> atoms_to_add = new_facts.asAtomSet().stream()
				.filter(a -> !fb.contains(a))
				.collect(Collectors.toSet());

		if(atoms_to_add.isEmpty()) {
			return null;
		}
		return FOFormulaFactory.instance().createOrGetConjunction(atoms_to_add);
	}


}
