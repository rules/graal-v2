package fr.boreal.forward_chaining.chase.treatment;

import fr.boreal.core.CoreProcessor;
import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.model.logicalElements.api.Atom;

import java.util.List;

/**
 * Treatment that computes the core of the current fact base
 *
 * @author Guillaume Pérution-Kihli
 */
public class ComputeCore implements EndTreatment, Pretreatment {
    private Chase chase;
    final private CoreProcessor processor;

    public ComputeCore(CoreProcessor processor)
    {
        this.processor = processor;
    }

    @Override
    public void init(Chase c) {
        this.chase = c;
    }

    @Override
    public void apply() {
        this.processor.computeCore(this.chase.getFactBase());

        List<Atom> toRemove = this.chase.getLastStepResults()
                .created_facts()
                .stream()
                .flatMap(f -> f.asAtomSet().stream())
                .filter(a -> !this.chase.getFactBase().contains(a))
                .toList();

        this.chase.getLastStepResults().created_facts().removeAll(toRemove);
    }
}
