package fr.boreal.forward_chaining.chase;

import java.util.Collection;

import fr.boreal.forward_chaining.chase.description.ChaseDescription;
import fr.boreal.forward_chaining.chase.description.IChaseDescription;
import fr.boreal.forward_chaining.chase.halting_condition.HaltingCondition;
import fr.boreal.forward_chaining.chase.rule_applier.RuleApplier;
import fr.boreal.forward_chaining.chase.rule_scheduler.RuleScheduler;
import fr.boreal.forward_chaining.chase.treatment.EndTreatment;
import fr.boreal.forward_chaining.chase.treatment.Pretreatment;
import fr.boreal.forward_chaining.chase.treatment.Treatment;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.rule.api.FORule;

/**
 * Default implementation of the chase algorithm
 */
public class ChaseImpl implements Chase {

    private final FactBase fb;
    private RuleBase rb;

    private final RuleScheduler rule_scheduler;
    private final RuleApplier rule_applier;
    private RuleApplicationStepResult last_step_result;

    private final Collection<HaltingCondition> halting_conditions;

    private final Collection<Pretreatment> global_pretreatments;
    private final Collection<Pretreatment> step_pretreatments;

    private final Collection<EndTreatment> global_end_treatments;
    private final Collection<EndTreatment> end_of_step_treatments;

    private int step_number = 0;

    /////////////////////////////////////////////////
    // Constructors
    /////////////////////////////////////////////////

    /**
     * Use {@link ChaseBuilder} to create a chase
     *
     * @param fb                     the factbase
     * @param rb                     the rulebase
     * @param rule_scheduler         the rule scheduler
     * @param rule_applier           the rule applier
     * @param halting_conditions     the halting conditions
     * @param global_pretreatments   the global pre-treatments
     * @param step_pretreatments     the step pre-treatments
     * @param global_end_treatments  the global end treatments
     * @param end_of_step_treatments the end of step treatments
     */
    protected ChaseImpl(FactBase fb, RuleBase rb,
                        RuleScheduler rule_scheduler, RuleApplier rule_applier,
                        Collection<HaltingCondition> halting_conditions,
                        Collection<Pretreatment> global_pretreatments,
                        Collection<Pretreatment> step_pretreatments,
                        Collection<EndTreatment> global_end_treatments,
                        Collection<EndTreatment> end_of_step_treatments) {
        this.fb = fb;
        this.rb = rb;
        this.rule_scheduler = rule_scheduler;
        this.rule_applier = rule_applier;
        this.halting_conditions = halting_conditions;
        this.global_pretreatments = global_pretreatments;
        this.step_pretreatments = step_pretreatments;
        this.global_end_treatments = global_end_treatments;
        this.end_of_step_treatments = end_of_step_treatments;

        this.last_step_result = new RuleApplicationStepResult(null, null);
    }

    /////////////////////////////////////////////////
    // Public methods

    /// //////////////////////////////////////////////

    @Override
    public boolean hasNextStep() {
        return this.halting_conditions.stream().allMatch(HaltingCondition::check);
    }

    @Override
    public void nextStep() {
        ++step_number;
        Collection<FORule> rules_to_apply = this.rule_scheduler.getRulesToApply(this.last_step_result.applied_rules());
        this.last_step_result = this.rule_applier.apply(rules_to_apply, this.fb);
    }

    @Override
    public void applyGlobalPretreatments() {
        this.initAll();
        this.global_pretreatments.forEach(Treatment::apply);
    }

    @Override
    public void applyPretreatments() {
        this.step_pretreatments.forEach(Treatment::apply);
    }

    @Override
    public void applyEndOfStepTreatments() {
        this.end_of_step_treatments.forEach(Treatment::apply);
    }

    @Override
    public void applyGlobalEndTreatments() {
        this.global_end_treatments.forEach(Treatment::apply);
    }

    /////////////////////////////////////////////////
    // Getter

    /// //////////////////////////////////////////////

    @Override
    public FactBase getFactBase() {
        return this.fb;
    }

    @Override
    public RuleBase getRuleBase() {
        return this.rb;
    }

    @Override
    public void setRuleBase(RuleBase rb) {
        this.rb = rb;
        this.rule_scheduler.init(rb);
    }

    @Override
    public RuleApplicationStepResult getLastStepResults() {
        return this.last_step_result;
    }

    @Override
    public RuleScheduler getRuleScheduler() {
        return this.rule_scheduler;
    }

    @Override
    public int getStepCount() {
        return this.step_number;
    }

    @Override
    public String toString() {
        return "Steps : " + this.step_number +
               "\nStep result :\n" + this.last_step_result +
               "\nFacts :\n" + this.fb.toString();
    }

    public IChaseDescription getDescription() {
        return new ChaseDescription(
                fb,
                rb,
                rule_scheduler,
                rule_applier,
                last_step_result,
                step_number,
                halting_conditions,
                global_pretreatments,
                step_pretreatments,
                global_end_treatments,
                end_of_step_treatments
        );
    }
    @Override
    public String describe() {
        StringBuilder sb = new StringBuilder();
        sb.append("ChaseImpl\n");
        sb.append("├── FactBase (size): ").append(fb.size()).append("\n");
        sb.append("├── RuleBase (size): ").append(rb.getRules().size()).append("\n");
        sb.append("├── RuleScheduler: ").append(rule_scheduler.describe()).append("\n");
        sb.append("├── RuleApplier: ").append(rule_applier.describe()).append("\n");
        sb.append("├── LastStepResult: ").append(last_step_result.describe()).append("\n");
        sb.append("├── StepNumber: ").append(step_number).append("\n");

        sb.append("├── HaltingConditions:\n");
        for (HaltingCondition hc : halting_conditions) {
            sb.append("│   ├── ").append(hc.describe()).append("\n");
        }

        if (global_pretreatments.isEmpty()) {
            sb.append("├── No Global Pretreatments\n");
        } else {
            sb.append("├── GlobalPretreatments:\n");
            for (Pretreatment p : global_pretreatments) {
                sb.append("│   ├── ").append(p.describe()).append("\n");
            }
        }

        if (step_pretreatments.isEmpty()) {
            sb.append("├── No Step Pretreatment\n");
        } else {
            sb.append("├── StepPretreatments:\n");
            for (Pretreatment p : step_pretreatments) {
                sb.append("│   ├── ").append(p.describe()).append("\n");
            }
        }

        if (global_pretreatments.isEmpty()) {
            sb.append("├── No Global End Treatment\n");
        } else {
            sb.append("├── GlobalEndTreatments:\n");
            for (EndTreatment et : global_end_treatments) {
                sb.append("│   ├── ").append(et.describe()).append("\n");
            }
        }

        if (end_of_step_treatments.isEmpty()) {
            sb.append("└── No End Of Step Treatment\n");
        } else {
            sb.append("└── EndOfStepTreatments:\n");
            for (EndTreatment et : end_of_step_treatments) {
                sb.append("    ├── ").append(et.describe()).append("\n");
            }
        }

        return sb.toString();
    }

    /////////////////////////////////////////////////
    // Private methods

    /// //////////////////////////////////////////////

    private void initAll() {
        this.initGlobalPretreatments();
        this.initPretreatments();
        this.initRuleApplier();
        this.initRuleScheduler();
        this.initEndOfStepTreatments();
        this.initGlobalEndTreatments();
        this.initHaltingConditions();
    }

    private void initGlobalPretreatments() {
        this.global_pretreatments.forEach(treatment -> treatment.init(this));
    }

    private void initRuleApplier() {
        this.rule_applier.init(this);
    }

    private void initRuleScheduler() {
        this.rule_scheduler.init(this.rb);
    }

    private void initPretreatments() {
        this.step_pretreatments.forEach(treatment -> treatment.init(this));
    }

    private void initEndOfStepTreatments() {
        this.end_of_step_treatments.forEach(treatment -> treatment.init(this));
    }

    private void initGlobalEndTreatments() {
        this.global_end_treatments.forEach(treatment -> treatment.init(this));
    }

    private void initHaltingConditions() {
        this.halting_conditions.forEach(condition -> condition.init(this));
    }


}
