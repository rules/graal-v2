package fr.boreal.forward_chaining.chase.rule_applier;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import fr.boreal.forward_chaining.chase.RuleApplicationStepResult;
import fr.boreal.forward_chaining.chase.rule_applier.body_to_query_transformer.BodyToQueryTransformer;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.TriggerApplier;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_checker.TriggerChecker;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_computer.TriggerComputer;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.rule.api.FORule;

/**
 * The parallel rule application applies the rules by
 * * Computing the homomorphisms from the body of the rules to the factbase
 * * Checking if the homomorphism is applicable
 * * Applying the homomorphism to the head of the rule and
 * * Adding the resulting atoms to a temporary factbase that will need to be merged into the factbase at the end of the step
 */
public class ParallelTriggerRuleApplier extends AbstractRuleApplier {

	/**
	 * Constructor
	 * 
	 * @param transformer the BodyToQueryTransformer
	 * @param computer the TriggerComputer
	 * @param checker the TriggerChecker
	 * @param applier the TriggerApplier
	 */
	public ParallelTriggerRuleApplier(BodyToQueryTransformer transformer, TriggerComputer computer, TriggerChecker checker, TriggerApplier applier) {
		super(transformer, computer, checker, applier);
	}

	@Override
	public RuleApplicationStepResult apply(Collection<FORule> rules, FactBase fb) {
		Map<FOQuery<?>, Collection<FORule>> rules_by_body = this.groupRulesByBodyQuery(rules);
		Collection<FORule> applied_rules = new HashSet<>();
		Collection<Atom> created_facts = new HashSet<>();

		// Evaluate all rule bodies and store the substitution
		for(FOQuery<?> body : rules_by_body.keySet()) {
			Iterator<Substitution> res;
			try {
				res = this.computer.compute(body, fb);
			} catch (Exception e) {
				throw new RuntimeException(
						String.format("[%s::apply] Error during the application of the rules - " +
										"error while computing the homomorphisms for the body: %s",
								this.getClass(), body),
						e);
			}

			while(true) {
				try {
					if (!res.hasNext()) { // An exception could occur in "hasNext" if this iterator is lazy
						break;
					}
				} catch (Exception e) {
					throw new RuntimeException(
							String.format("[%s::apply] Error during the application of the rules - " +
											"error while computing the homomorphisms for the body: %s",
									this.getClass(), body),
							e);
				}
				Substitution h = res.next();

				for(FORule rule : rules_by_body.get(body)) {
					boolean toApply;
					try {
						toApply = this.checker.check(rule, h, fb);
					} catch (Exception e) {
						throw new RuntimeException(
								String.format("[%s::apply] Error during the application of the rules - " +
												"error while checking the rule %s with homomorphism: %s",
										this.getClass(), rule, h),
								e);
					}

					if(toApply) {
						FOFormula application_facts;

						try {
							application_facts = this.applier.apply(rule, h, fb);
						} catch (Exception e) {
							Thread.currentThread().interrupt();
							throw new RuntimeException(
									String.format("[%s::apply] Error during the application of the rules - " +
													"error while applying the rule %s with homomorphism: %s",
											this.getClass(), rule, h),
									e);
						}

						if(application_facts != null) {
							created_facts.addAll(application_facts.asAtomSet());
							applied_rules.add(rule);
						}
					}
				}
			}
		}
		return new RuleApplicationStepResult(applied_rules, created_facts);
	}

}
