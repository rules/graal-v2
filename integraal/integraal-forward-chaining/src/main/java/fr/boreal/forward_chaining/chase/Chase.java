package fr.boreal.forward_chaining.chase;

import fr.boreal.forward_chaining.api.ForwardChainingAlgorithm;
import fr.boreal.forward_chaining.chase.description.ChaseDescription;
import fr.boreal.forward_chaining.chase.description.IChaseDescription;
import fr.boreal.forward_chaining.chase.rule_scheduler.RuleScheduler;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;

/**
 * The Chase is a way to saturate a {@link FactBase} according to rules
 * <p>
 * The work done for the modeling and implementation of the Chase is mostly part of Guillaume Pérution-Kihli internship's work (2020)
 */
public interface Chase extends ForwardChainingAlgorithm {

    /**
     * @return true iff this chase has a next step
     */
    boolean hasNextStep();

    /**
     * Apply the next step of this chase
     */
    void nextStep();

    /**
     * Apply global pretreatments
     * These are treatments to do once at the beginning of the chase
     */
    void applyGlobalPretreatments();

    /**
     * Apply pretreatments
     * These are treatments to do at the beginning of each step of the chase
     */
    void applyPretreatments();

    /**
     * Apply end of step treatments
     * These are treatments to do at the end of each step of the chase
     */
    void applyEndOfStepTreatments();

    /**
     * Apply global end treatments
     * These are treatments to do once at the end of the chase
     */
    void applyGlobalEndTreatments();

    @Override
    default void execute() {
        this.applyGlobalPretreatments();
        while (this.hasNextStep()) {
            this.applyPretreatments();
            this.nextStep();
            this.applyEndOfStepTreatments();
        }
        this.applyGlobalEndTreatments();
    }

    /////////////////////////////////
    // Used for halting conditions //
    /////////////////////////////////

    /**
     * @return the factbase of this chase
     */
    FactBase getFactBase();

    /**
     * @return the rulebase of this chase
     */
    RuleBase getRuleBase();

    /**
     * Sets the rulebase of this chase
     *
     * @param rb the new rulebase
     */
    void setRuleBase(RuleBase rb);

    /**
     * @return the result of the last step of this chase
     */
    RuleApplicationStepResult getLastStepResults();

    /**
     * @return the rule scheduler of this chase
     */
    RuleScheduler getRuleScheduler();

    /**
     * @return the number of the current step
     */
    int getStepCount();

    /**
     * Returns a description of the configuration of the chase
     */
    IChaseDescription getDescription();

    /**
     * Returns a String description of the configuration of the chase
     */
    default String describe() {
        return this.getDescription().toJson();
    }
}
