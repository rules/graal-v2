package fr.boreal.forward_chaining.chase.rule_applier.body_to_query_transformer;

import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.rule.api.FORule;

/**
 * Transforms a rule body into a query
 * 
 * The properties of the query answer variables are dependent on the implementation
 * 
 * @author Florent Tornil
 *
 */
public interface BodyToQueryTransformer {
	
	/**
	 * Transforms the given rule's body into the corresponding query
	 * @param rule rule to transform
	 * @return the query corresponding to the given rule's body
	 */
	FOQuery<?> transform(FORule rule);


	/**
	 * Defautl method to describe the rule scheduler
	 */
	default String describe(){return this.getClass().getSimpleName();}

}
