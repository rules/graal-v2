package fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.renamer;

import java.util.HashMap;
import java.util.Map;

import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.rule.api.FORule;

/**
 * Namer created with all the variables of the rule's body
 */
public class BodySkolem implements TriggerRenamer {
	
	private final Map<FORule,Map<Substitution,Map<Variable,Term>>> existentials_names = new HashMap<>();
	
	private final TermFactory tf;
	
	/**
	 * @param tf the term factory
	 */
	public BodySkolem(TermFactory tf) {
		this.tf = tf;
	}

	@Override
	public Substitution renameExitentials(FORule rule, final Substitution substitution) {
		
		if (!existentials_names.containsKey(rule)) {
			existentials_names.put(rule, new HashMap<>());
		}
		
		if (!existentials_names.get(rule).containsKey(substitution)) {
			existentials_names.get(rule).put(substitution, new HashMap<>());
		}
		
		Substitution renamed = new SubstitutionImpl();
		for (Variable v : rule.getExistentials()) {
			Map<Variable, Term> skolem_names = existentials_names.get(rule).get(substitution);
			if (!skolem_names.containsKey(v)) {
				skolem_names.put(v, this.tf.createOrGetFreshVariable());
			}
			renamed.add(v, skolem_names.get(v));
		}
		
		for(Variable v : substitution.keys()) {
			renamed.remove(v);
		}
		
		return renamed.merged(substitution).get();
	}

}
