package fr.boreal.forward_chaining.chase.rule_scheduler;

import java.util.Collection;

import fr.boreal.configuration.parameters.IGParameter;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.configuration.keywords.InteGraalKeywords;

/**
 * Schedules all the rules for every step
 */
public class NaiveScheduler implements RuleScheduler {
	
	private RuleBase rb;
	
	/**
	 * @param rb the rulebase
	 */
	public NaiveScheduler(RuleBase rb) {
		this.init(rb);
	}
	
	@Override
	public void init(RuleBase rb) {
		this.rb = rb;
	}

	@Override
	public Collection<FORule> getRulesToApply(Collection<FORule> last_applied_rules) {
		return this.rb.getRules();
	}

	public String describe(){
		return getCorrespondingParameter().value().toString();
	}

	public IGParameter<InteGraalKeywords,?> getCorrespondingParameter(){
		return new IGParameter<>(InteGraalKeywords.SCHEDULER,InteGraalKeywords.Algorithms.Parameters.Chase.Scheduler.NAIVE_SCHEDULER);
	}

}
