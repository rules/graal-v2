package fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.facts_handler;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;

/**
 * Adds the created facts to the factbase
 */
public class DirectApplication implements FactsHandler {

	@Override
	public FOFormula add(FOFormula new_facts, FactBase fb) {
		boolean added_something = fb.add(new_facts);		
		return added_something ? new_facts : null;
	}

}
