package fr.boreal.forward_chaining.chase.rule_applier.trigger_checker;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.rule.api.FORule;

/**
 * Accept the trigger if it is the first time, checking only the variables from the frontier of the rule.
 * This is the criteria for the semi-oblivious chase
 */
public class SemiObliviousChecker implements TriggerChecker {

	final Map<FORule, Set<Substitution>> alreadyTreated = new HashMap<>();

	@Override
	public boolean check(FORule rule, Substitution substitution, FactBase fb) {
		
		Substitution frontier_limited = substitution.limitedTo(rule.getFrontier());
		Set<Substitution> rule_substitutions = this.alreadyTreated.get(rule);
		if(rule_substitutions == null) {
			rule_substitutions = new HashSet<>();
		} else if(rule_substitutions.contains(frontier_limited)) {
			return false;
		}
		
		rule_substitutions.add(frontier_limited);
		this.alreadyTreated.put(rule, rule_substitutions);
		return true;
	}

}
