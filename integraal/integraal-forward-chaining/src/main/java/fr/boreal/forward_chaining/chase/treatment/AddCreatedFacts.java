package fr.boreal.forward_chaining.chase.treatment;

import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;

import java.util.Collection;

/**
 * When applied, this treatment adds the facts created at the previous step to the factbase
 * This is used for parallel application of rules.
 */
public class AddCreatedFacts implements EndTreatment {

	private Chase c;

	@Override
	public void apply() {
		FactBase fb = c.getFactBase();
		Collection<Atom> new_facts = c.getLastStepResults().created_facts();
		new_facts.forEach(fb::add);
	}

	@Override
	public void init(Chase c) {
		this.c = c;		
	}

}
