package fr.boreal.forward_chaining.chase.treatment;

import fr.boreal.forward_chaining.chase.Chase;

/**
 * Apply a given treatment
 */
public interface Treatment {
	
	/**
	 * Initialize the treatment for the given chase
	 * @param c the chase object
	 */
    void init(Chase c);
	
	/**
	 * Applies the treatment
	 */
    void apply();

	/**
	 * Default method to describe the rule scheduler
	 */
	default String describe(){return this.getClass().getSimpleName();}

}
