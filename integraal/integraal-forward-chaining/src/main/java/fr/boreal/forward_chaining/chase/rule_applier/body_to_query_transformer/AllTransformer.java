package fr.boreal.forward_chaining.chase.rule_applier.body_to_query_transformer;

import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.rule.api.FORule;

/**
 * Transforms a rule's body into a query using all variables as answer variables
 * 
 * @author Florent Tornil
 *
 */
public class AllTransformer implements BodyToQueryTransformer {

	@Override
	public FOQuery<?> transform(FORule rule) {
		return FOQueryFactory.instance().createOrGetQuery(rule.getBody(), null, null);
	}

}
