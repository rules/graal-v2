package fr.boreal.forward_chaining.chase;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * This object represents the result of a step of the chase
 * It contains the applied rules and the created facts
 */
public class RuleApplicationStepResult {

    private final Collection<FORule> applied_rules;
    private final Collection<Atom> created_facts;

    private FactBase created_facts_factbase = null;

    /**
     * Constructor with rules and facts
     *
     * @param applied_rules rules applied at the step
     * @param created_facts facts created at the step
     */
    public RuleApplicationStepResult(Collection<FORule> applied_rules, Collection<Atom> created_facts) {
        this.applied_rules = applied_rules;
        this.created_facts = created_facts;
    }

    /**
     * @return the rules applied at the step
     */
    public Collection<FORule> applied_rules() {
        return this.applied_rules;
    }

    /**
     * @return the facts created at the step
     */
    public Collection<Atom> created_facts() {
        return this.created_facts;
    }

    /**
     * @return the facts created at the step seen as a FactBase with indexes, kept in cache
     */
    public FactBase created_facts_as_factbase() {
        if (this.created_facts_factbase == null) {
            this.created_facts_factbase = new SimpleInMemoryGraphStore(
                    this.created_facts.stream()
                            .flatMap(facts -> facts.asAtomSet().stream())
                            .collect(Collectors.toSet()));
        }
        return this.created_facts_factbase;
    }


    /**
     * Defautl method to describe the rule scheduler
     */
    public String describe() {
        if (applied_rules == null) {
            return "No rule applied yet.";
        } else {
            return "Applied " + applied_rules.size() + " rules in the last step ; inferred " + created_facts.size() + " atoms.";
        }
    }
}
