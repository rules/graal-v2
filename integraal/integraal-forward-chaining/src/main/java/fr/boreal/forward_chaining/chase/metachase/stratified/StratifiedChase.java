package fr.boreal.forward_chaining.chase.metachase.stratified;

import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.forward_chaining.chase.ChaseBuilder;
import fr.boreal.forward_chaining.chase.RuleApplicationStepResult;
import fr.boreal.forward_chaining.chase.description.ChaseDescription;
import fr.boreal.forward_chaining.chase.description.IChaseDescription;
import fr.boreal.forward_chaining.chase.description.StratifiedChaseDescription;
import fr.boreal.forward_chaining.chase.halting_condition.HaltingCondition;
import fr.boreal.forward_chaining.chase.rule_scheduler.RuleScheduler;
import fr.boreal.forward_chaining.chase.treatment.EndTreatment;
import fr.boreal.forward_chaining.chase.treatment.Pretreatment;
import fr.boreal.forward_chaining.chase.treatment.Treatment;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class StratifiedChase implements Chase {
    private final FactBase fb;
    private List<RuleBase> strata;

    private final ChaseBuilder chaseBuilder;
    private Chase chase;

    private final Collection<HaltingCondition> halting_conditions;

    private final Collection<Pretreatment> global_pretreatments;
    private final Collection<Pretreatment> step_pretreatments;

    private final Collection<EndTreatment> global_end_treatments;
    private final Collection<EndTreatment> end_of_step_treatments;

    private int step_number = 0;

    public StratifiedChase(ChaseBuilder builder, FactBase fb,
                           List<RuleBase> strata,
                           Collection<HaltingCondition> halting_conditions,
                           Collection<Pretreatment> global_pretreatments,
                           Collection<Pretreatment> step_pretreatments,
                           Collection<EndTreatment> global_end_treatments,
                           Collection<EndTreatment> end_of_step_treatments) {
        this.fb = fb;
        this.strata = strata;

        this.chaseBuilder = builder;
        this.chaseBuilder.setFactBase(fb);

        this.halting_conditions = halting_conditions;
        this.global_pretreatments = global_pretreatments;
        this.step_pretreatments = step_pretreatments;
        this.global_end_treatments = global_end_treatments;
        this.end_of_step_treatments = end_of_step_treatments;
    }

    @Override
    public boolean hasNextStep() {
        return this.step_number < this.strata.size() && this.halting_conditions.stream().allMatch(HaltingCondition::check);
    }

    @Override
    public void nextStep() {
        Optional<Chase> optChase = this.chaseBuilder
                .setRuleBase(this.strata.get(this.step_number))
                .build();
        if (optChase.isPresent()) {
            this.chase = optChase.get();
            this.chase.execute();
            ++this.step_number;
        } else {
            throw new RuntimeException("The chase builder used in the stratified chase cannot build a chase instance.");
        }
    }

    @Override
    public void applyGlobalPretreatments() {
        this.initAll();
        this.global_pretreatments.forEach(Treatment::apply);
    }

    @Override
    public void applyPretreatments() {
        this.step_pretreatments.forEach(Treatment::apply);
    }

    @Override
    public void applyEndOfStepTreatments() {
        this.end_of_step_treatments.forEach(Treatment::apply);
    }

    @Override
    public void applyGlobalEndTreatments() {
        this.global_end_treatments.forEach(Treatment::apply);
    }

    @Override
    public FactBase getFactBase() {
        return this.fb;
    }

    @Override
    public RuleBase getRuleBase() {
        return this.strata.get(this.step_number);
    }

    @Override
    public void setRuleBase(RuleBase rb) {
        throw new UnsupportedOperationException("Setting a new rule base is not supported for StratifiedChase.");
    }

    @Override
    public RuleApplicationStepResult getLastStepResults() {
        return this.chase.getLastStepResults();
    }

    @Override
    public RuleScheduler getRuleScheduler() {
        return this.chase.getRuleScheduler();
    }

    @Override
    public int getStepCount() {
        return this.step_number;
    }

    @Override
    public String toString() {
        return "Steps : " + this.step_number +
               "\nFacts :\n" + this.fb.toString();
    }

    private void initAll() {
        this.initGlobalPretreatments();
        this.initPretreatments();
        this.initEndOfStepTreatments();
        this.initGlobalEndTreatments();
        this.initHaltingConditions();
    }

    private void initGlobalPretreatments() {
        this.global_pretreatments.forEach(treatment -> treatment.init(this));
    }

    private void initPretreatments() {
        this.step_pretreatments.forEach(treatment -> treatment.init(this));
    }

    private void initEndOfStepTreatments() {
        this.end_of_step_treatments.forEach(treatment -> treatment.init(this));
    }

    private void initGlobalEndTreatments() {
        this.global_end_treatments.forEach(treatment -> treatment.init(this));
    }

    private void initHaltingConditions() {
        this.halting_conditions.forEach(condition -> condition.init(this));
    }

    public IChaseDescription getDescription() {
        return new StratifiedChaseDescription(
                fb,
                strata,
                chase,
                chaseBuilder,
                halting_conditions,
                global_pretreatments,
                step_pretreatments,
                global_end_treatments,
                end_of_step_treatments,
                step_number
        );
    }

}
