package fr.boreal.forward_chaining.chase.treatment;

import fr.boreal.forward_chaining.chase.Chase;

/**
 * When applied, this treatment prints informations about the chase, the current step and the factbase
 */
public class Debug implements EndTreatment {

	private Chase c;
	
	private long time;
	
	@Override
	public void apply() {
		System.out.println("---");
		System.out.println("Step : " + c.getStepCount());
		System.out.println("Atoms : " + c.getFactBase().size());
		System.out.println("Added atoms : " + c.getLastStepResults().created_facts().size());
		System.out.println("Time : " + (System.currentTimeMillis() - time));
		System.out.println("Last step rules : " + c.getLastStepResults().applied_rules().size());
		this.time = System.currentTimeMillis();
	}

	@Override
	public void init(Chase c) {
		this.c = c;
		this.time = System.currentTimeMillis();
	}

}
