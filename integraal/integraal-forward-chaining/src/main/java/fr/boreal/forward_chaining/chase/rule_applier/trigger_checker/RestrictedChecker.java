package fr.boreal.forward_chaining.chase.rule_applier.trigger_checker;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;

import java.util.Set;

/**
 * Accept the trigger if it cannot be extended with the head of the rule
 * This is the criteria for the restricted chase
 */
public class RestrictedChecker implements TriggerChecker {

	private final FOQueryEvaluator<FOFormula> evaluator;

	/**
	 * Default constructor using the generic query evaluator
	 */
	public RestrictedChecker() {
		this(GenericFOQueryEvaluator.defaultInstance());
	}

	/**
	 * Constructor using the given query evaluator
	 * @param evaluator the query evaluator to use
	 */
	public RestrictedChecker(FOQueryEvaluator<FOFormula> evaluator) {
		this.evaluator = evaluator;
	}

	@Override
	public boolean check(FORule rule, Substitution substitution, FactBase fb) {
		FOQuery<FOFormula> query = FOQueryFactory.instance().createOrGetQuery(rule.getHead(), Set.of());
		return !this.evaluator.existHomomorphism(query, fb, substitution);
	}
}
