package fr.boreal.forward_chaining.chase.rule_applier;

import fr.boreal.forward_chaining.chase.RuleApplicationStepResult;
import fr.boreal.forward_chaining.chase.rule_applier.body_to_query_transformer.BodyToQueryTransformer;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.TriggerApplier;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_checker.TriggerChecker;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_computer.TriggerComputer;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.rule.api.FORule;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * The breadth first rule application applies the rules by
 * * Computing the homomorphisms from the body of the rules to the factbase
 * * Checking if the homomorphism is applicable
 * * Applying the homomorphism to the head of the rule and
 * * Adding the resulting atoms to the factbase
 */
public class BreadthFirstTriggerRuleApplier extends AbstractRuleApplier {

	/**
	 * Constructor
	 * 
	 * @param transformer the BodyToQueryTransformer
	 * @param computer the TriggerComputer
	 * @param checker the TriggerChecker
	 * @param applier the TriggerApplier
	 */
	public BreadthFirstTriggerRuleApplier(BodyToQueryTransformer transformer, TriggerComputer computer, TriggerChecker checker, TriggerApplier applier) {
		super(transformer, computer, checker, applier);
		LOG = LoggerFactory.getLogger(BreadthFirstTriggerRuleApplier.class);
	}

	@Override
	public RuleApplicationStepResult apply(Collection<FORule> rules, FactBase fb) {
		Map<FOQuery<?>, Collection<FORule>> rules_by_body = this.groupRulesByBodyQuery(rules);
		Map<FOQuery<?>, Collection<Substitution>> substitutions_by_body = new HashMap<>();

		// Evaluate all rule bodies and store the substitution
		for(FOQuery<?> body : rules_by_body.keySet()) {
			Iterator<Substitution> res;
			try {
				res = this.computer.compute(body, fb);
				substitutions_by_body.put(body, new HashSet<>());
				while(res.hasNext()) {
					substitutions_by_body.get(body).add(res.next());
				}
			} catch (Exception e) {
				throw new RuntimeException(
						String.format("[%s::apply] Error during the application of the rules - " +
										"error while computing the homomorphisms for the body: %s",
								this.getClass(), body),
						e);
			}
		}

		// Apply the substitutions to rule heads and add the new facts
		Collection<FORule> applied_rules = new HashSet<>();
		Collection<Atom> created_facts = new HashSet<>();
		for (FOQuery<?> body : rules_by_body.keySet()) {
			for (Substitution substitution : substitutions_by_body.get(body)) {
				for (FORule rule : rules_by_body.get(body)) {
					boolean toApply;
					try {
						toApply = this.checker.check(rule, substitution, fb);
					} catch (Exception e) {
						throw new RuntimeException(
								String.format("[%s::apply] Error during the application of the rules - " +
												"error while checking the rule %s with homomorphism: %s",
										this.getClass(), rule, substitution),
								e);
					}

					if (toApply) {
						FOFormula application_facts;

						try {
							application_facts = this.applier.apply(rule, substitution, fb);
						} catch (Exception e) {
							Thread.currentThread().interrupt();
							throw new RuntimeException(
									String.format("[%s::apply] Error during the application of the rules - " +
													"error while applying the rule %s with homomorphism: %s",
											this.getClass(), rule, substitution),
									e);
						}

						if (application_facts != null) {
							created_facts.addAll(application_facts.asAtomSet());
							applied_rules.add(rule);
						}
					}
				}
			}
		}
		return new RuleApplicationStepResult(applied_rules, created_facts);
	}

}
