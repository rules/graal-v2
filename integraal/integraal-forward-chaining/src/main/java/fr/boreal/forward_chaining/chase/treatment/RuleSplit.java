package fr.boreal.forward_chaining.chase.treatment;

import java.util.HashSet;
import java.util.Set;

import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.rule.api.FORule;
import fr.lirmm.boreal.util.Rules;

/**
 * When applied, this treatment change the rulebase with an equivalent rule base containing only single piece rules.
 */
public class RuleSplit implements Pretreatment {

	private Chase chase;

	@Override
	public void init(Chase c) {
		this.chase = c;
	}

	@Override
	public void apply() {
		Set<FORule> newBase = new HashSet<>();
		for (FORule r : this.chase.getRuleBase().getRules()) {
			newBase.addAll(Rules.computeSinglePiece(r));
		}
		RuleBase new_rb = new RuleBaseImpl(newBase);
		this.chase.setRuleBase(new_rb);
		this.chase.getRuleScheduler().init(new_rb);
	}
}
