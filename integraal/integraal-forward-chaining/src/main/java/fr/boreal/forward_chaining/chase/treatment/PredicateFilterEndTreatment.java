package fr.boreal.forward_chaining.chase.treatment;

import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;

import java.util.*;

/**
 * End treatment to filter out facts with predicates not in the provided list.
 */
public class PredicateFilterEndTreatment implements EndTreatment {
    private static final int BATCH_SIZE = 100;
    private final Map<Integer, Set<Predicate>> predicatesToRemoveByStep;
    private Chase chase;

    public PredicateFilterEndTreatment(Map<Integer, Set<Predicate>> predicatesToRemoveByStep) {
        this.predicatesToRemoveByStep = predicatesToRemoveByStep;
    }

    @Override
    public void init(Chase chase) {
        this.chase = chase;
    }

    @Override
    public void apply() {
        int currentStep = chase.getStepCount();
        Set<Predicate> predicatesToRemove = predicatesToRemoveByStep.getOrDefault(currentStep, Set.of());

        FactBase factBase = chase.getFactBase();

        for (Predicate predicate : predicatesToRemove) {
            List<Atom> batch = new ArrayList<>(BATCH_SIZE);
            var iterator = factBase.getAtomsByPredicate(predicate);

            while (iterator.hasNext()) {
                batch.add(iterator.next());

                if (batch.size() == BATCH_SIZE) {
                    factBase.removeAll(batch);
                    batch.clear();
                }
            }

            if (!batch.isEmpty()) {
                factBase.removeAll(batch);
            }
        }
    }
}
