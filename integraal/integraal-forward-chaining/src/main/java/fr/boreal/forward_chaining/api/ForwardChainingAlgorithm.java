package fr.boreal.forward_chaining.api;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.rule.api.FORule;

/**
 * A ForwardChaining algorithm saturates a {@link FactBase} according to the given {@link FORule}.
 */
public interface ForwardChainingAlgorithm {

	/**
	 * Execute the algorithm
	 */
    void execute();
	
}
