package fr.boreal.forward_chaining.chase.rule_scheduler;

import java.util.Collection;

import fr.boreal.configuration.keywords.InteGraalKeywords;
import fr.boreal.configuration.parameters.IGParameter;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.rule.api.FORule;

/**
 * Computes the rules that need to be applied according to previously applied rules
 */
public interface RuleScheduler {
	
	/**
	 * Initialize the rule applier for the given chase
	 * @param rb the rulebase
	 */
    void init(RuleBase rb);

	/**
	 * @param last_applied_rules rules applied at the last step
	 * @return the rules to apply at the next step
	 */
    Collection<FORule> getRulesToApply(Collection<FORule> last_applied_rules);

	/**
	 * Defautl method to describe the rule scheduler
	 */
	String describe();

	/**
	 *
	 * @return the parameter corresponding to this object
	 */
	IGParameter<InteGraalKeywords,?> getCorrespondingParameter();
}
