package fr.boreal.forward_chaining.chase;

import fr.boreal.configuration.keywords.InteGraalKeywords;
import fr.boreal.configuration.parameters.IGParameter;
import fr.boreal.configuration.parameters.IGParameterException;
import fr.boreal.core.MultiThreadsByPieceCoreProcessor;
import fr.boreal.forward_chaining.chase.halting_condition.CreatedFactsAtPreviousStep;
import fr.boreal.forward_chaining.chase.halting_condition.ExternalInterruption;
import fr.boreal.forward_chaining.chase.halting_condition.HaltingCondition;
import fr.boreal.forward_chaining.chase.halting_condition.HasRulesToApply;
import fr.boreal.forward_chaining.chase.halting_condition.LimitNumberOfStep;
import fr.boreal.forward_chaining.chase.halting_condition.Timeout;
import fr.boreal.forward_chaining.chase.metachase.stratified.StratifiedChaseBuilder;
import fr.boreal.forward_chaining.chase.rule_applier.*;
import fr.boreal.forward_chaining.chase.rule_applier.body_to_query_transformer.AllTransformer;
import fr.boreal.forward_chaining.chase.rule_applier.body_to_query_transformer.BodyToQueryTransformer;
import fr.boreal.forward_chaining.chase.rule_applier.body_to_query_transformer.FrontierTransformer;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.TriggerApplier;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.TriggerApplierImpl;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.facts_handler.DelegatedApplication;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.facts_handler.DirectApplication;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.facts_handler.FactsHandler;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.renamer.*;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_checker.*;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_computer.NaiveTriggerComputer;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_computer.SemiNaiveComputer;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_computer.TriggerComputer;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_computer.TwoStepComputer;
import fr.boreal.forward_chaining.chase.rule_scheduler.GRDScheduler;
import fr.boreal.forward_chaining.chase.rule_scheduler.NaiveScheduler;
import fr.boreal.forward_chaining.chase.rule_scheduler.RuleScheduler;
import fr.boreal.forward_chaining.chase.treatment.AddCreatedFacts;
import fr.boreal.forward_chaining.chase.treatment.ComputeCore;
import fr.boreal.forward_chaining.chase.treatment.ComputeLocalCore;
import fr.boreal.forward_chaining.chase.treatment.Debug;
import fr.boreal.forward_chaining.chase.treatment.EndTreatment;
import fr.boreal.forward_chaining.chase.treatment.Pretreatment;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.FactoryConstants;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.query_evaluation.generic.FOQueryEvaluatorWithDBMSDelegation;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author Florent Tornil
 * <p>
 * Builder to create a parameterized chase algorithm
 */
public class ChaseBuilder {

    private FactBase fb;
    private RuleBase rb;
    private TermFactory tf = FactoryConstants.DEFAULT_TERM_FACTORY;

    private RuleScheduler rsc;
    private InteGraalKeywords.Algorithms.Parameters.Chase.Scheduler scheduler = InteGraalKeywords.Algorithms.Parameters.Chase.Scheduler.GRD;

    private FOQueryEvaluator<FOFormula> eval;
    private InteGraalKeywords.Algorithms.Parameters.Chase.Evaluator evaluator = InteGraalKeywords.Algorithms.Parameters.Chase.Evaluator.GENERIC;

    private RuleApplier ra;
    private FactsHandler fh;
    private InteGraalKeywords.Algorithms.Parameters.Chase.Applier applier = InteGraalKeywords.Algorithms.Parameters.Chase.Applier.PARALLEL;
    private Application application = Application.PARALLEL;
    private enum Application {DIRECT, PARALLEL}

    private BodyToQueryTransformer transf;
    private InteGraalKeywords.Algorithms.Parameters.Chase.Transformer transformer = InteGraalKeywords.Algorithms.Parameters.Chase.Transformer.FRONTIER;

    private TriggerComputer tc;
    private InteGraalKeywords.Algorithms.Parameters.Chase.Computer computer = InteGraalKeywords.Algorithms.Parameters.Chase.Computer.SEMI_NAIVE;

    private TriggerChecker tch;
    private InteGraalKeywords.Algorithms.Parameters.Chase.Checker checker = InteGraalKeywords.Algorithms.Parameters.Chase.Checker.SEMI_OBLIVIOUS;

    private TriggerApplier ta;

    private TriggerRenamer tn;
    private InteGraalKeywords.Algorithms.Parameters.Chase.Namer namer = InteGraalKeywords.Algorithms.Parameters.Chase.Namer.FRESH;

    private final List<HaltingCondition> hcs = new ArrayList<>();
    private boolean debug = false;

    private final List<Pretreatment> global_pts = new ArrayList<>();
    private final List<Pretreatment> step_pts = new ArrayList<>();
    private final List<EndTreatment> global_end_ts = new ArrayList<>();
    private final List<EndTreatment> end_step_ts = new ArrayList<>();


    ///////////
    // Build //
    ///////////

    /**
     * Create a chase with the current configuration
     * <br/>
     * <p>
     * The minimal configuration requires : <br>
     * * a FactBase to saturate <br>
     * * a RuleBase to get Rules from <br>
     * * a TermFactory to create new terms <br>
     * <br>
     * Default behavior is : <br>
     * * a GRDScheduler for rules <br>
     * * a TriggerRuleApplier with : <br>
     * * * a SemiNaiveTriggerComputer with a default GenericFOQueryEvaluator <br>
     * * * a SemiObliviousChecker <br>
     * * * a Applier with : <br>
     * * * * a FreshNamer <br>
     * * * * a ParallelApplier thus a ParallelApplication <br>
     * * HaltingConditions : <br>
     * * * CreatedFactsAtPreviousStep <br>
     * * * HasRulesToApply <br>
     *
     * @return the created chase or an empty optional if the configuration is incorrect
     */
    public Optional<Chase> build() {
        if (this.getMinimalConfig().isPresent()) {
            return Optional.of(new ChaseImpl(fb, rb, rsc, ra, hcs, global_pts, step_pts, global_end_ts, end_step_ts));
        } else {
            return Optional.empty();
        }
    }

    ////////////////////
    // Default chase  //
    ////////////////////

    /**
     * Builder initialized with the default chase and mandatory parameters
     *
     * @param fb the factbase to saturate
     * @param rb the rule to apply
     * @return the builder initialized with the given parameters
     */
    public static ChaseBuilder defaultBuilder(FactBase fb, RuleBase rb) {
        return new ChaseBuilder()
                .setFactBase(fb)
                .setRuleBase(rb);
    }

    /**
     * Chase initialized with the default parameters and the given mandatory parameters
     *
     * @param fb the factbase to saturate
     * @param rb the rule to apply
     * @return the default chase initialized with the given parameters
     */
    public static Chase defaultChase(FactBase fb, RuleBase rb) {
        return new ChaseBuilder()
                .setFactBase(fb)
                .setRuleBase(rb)
                .build()
                .get();
    }

    /////////////////////////
    // Parameterized chase //
    /////////////////////////

    /**
     * Chase initialized with the given parameters
     *
     * @param fb          the factbase to saturate
     * @param rb          the rule to apply
     * @param chaseParams the chase parameter
     * @return the chase initialized with the given parameters
     */
    public static Chase chase(FactBase fb, RuleBase rb, IGParameter<InteGraalKeywords, ?>... chaseParams) throws IGParameterException {
        return new ChaseBuilder()
                .setFactBase(fb)
                .setRuleBase(rb)
                .setParams(chaseParams)
                .build()
                .get();
    }

    //////////////////////
    // Default settings //

    /// ///////////////////

    private Optional<ChaseBuilder> getMinimalConfig() {
        if (this.fb == null) {
            return Optional.empty();
        }
        if (this.rb == null) {
            return Optional.empty();
        }
        if (this.tf == null) {
            return Optional.empty();
        }
        if (this.rsc == null) {
            switch (this.scheduler) {
                case NAIVE_SCHEDULER:
                    this.setRuleScheduler(new NaiveScheduler(this.rb));
                    break;
                case GRD:
                    this.setRuleScheduler(new GRDScheduler(this.rb));
                    break;
            }
        }
        if (this.eval == null) {
            switch (this.evaluator) {
                case GENERIC:
                    this.setFOQueryEvaluator(GenericFOQueryEvaluator.defaultInstance());
                    break;
                case SMART:
                    this.setFOQueryEvaluator(FOQueryEvaluatorWithDBMSDelegation.defaultInstance());
                    break;
            }
        }
        if (this.ra == null) {
            switch (this.applier) {
                case BREADTH_FIRST:
                case PARALLEL:
                case MULTI_THREAD_PARALLEL: {
                    if (this.transf == null) {
                        switch (this.transformer) {
                            case ALL:
                                this.setBodyToQueryTransformer(new AllTransformer());
                                break;
                            case FRONTIER:
                                this.setBodyToQueryTransformer(new FrontierTransformer());
                                break;
                        }
                    }
                    if (this.tc == null) {
                        switch (this.computer) {
                            case NAIVE_COMPUTER:
                                this.setTriggerComputer(new NaiveTriggerComputer(this.eval));
                                break;
                            case SEMI_NAIVE:
                                this.setTriggerComputer(new SemiNaiveComputer(this.eval));
                                break;
                            case TWO_STEP:
                                this.setTriggerComputer(new TwoStepComputer(this.eval));
                                break;
                        }
                    }
                    if (this.tch == null) {
                        switch (this.checker) {
                            case TRUE:
                                this.setTriggerChecker(new AlwaysTrueChecker());
                                break;
                            case OBLIVIOUS:
                                this.setTriggerChecker(new ObliviousChecker());
                                break;
                            case SEMI_OBLIVIOUS:
                                this.setTriggerChecker(new SemiObliviousChecker());
                                break;
                            case RESTRICTED:
                                this.setTriggerChecker(new RestrictedChecker(this.eval));
                                break;
                            case EQUIVALENT:
                                this.setTriggerChecker(new EquivalentChecker(this.eval));
                                break;
                            case SO_RESTRICTED:
                                this.setTriggerChecker(new MultiTriggerChecker(new SemiObliviousChecker(), new RestrictedChecker(this.eval)));
                                break;
                        }
                    }
                    if (this.ta == null) {
                        if (this.tn == null) {
                            switch (this.namer) {
                                case FRESH:
                                    this.setExistentialsRenamer(new FreshRenamer(this.tf));
                                    break;
                                case BODY:
                                    this.setExistentialsRenamer(new BodySkolem(this.tf));
                                    break;
                                case FRONTIER:
                                    this.setExistentialsRenamer(new FrontierSkolem(this.tf));
                                    break;
                                case FRONTIER_PIECE:
                                    this.setExistentialsRenamer(new FrontierByPieceSkolem(this.tf));
                                    break;
                            }
                        }
                        if (this.fh == null) {
                            switch (this.application) {
                                case Application.DIRECT:
                                    this.setNewFactsHandler(new DirectApplication());
                                    break;
                                case Application.PARALLEL:
                                    this.setNewFactsHandler(new DelegatedApplication());
                                    break;
                            }
                        }
                        this.setTriggerApplier(new TriggerApplierImpl(this.tn, this.fh));
                    }
                    break;
                }
                case SOURCE_DELEGATED_DATALOG: // do nothing
                    break;
            }
            switch (this.applier) {
                case BREADTH_FIRST:
                    this.setRuleApplier(new BreadthFirstTriggerRuleApplier(this.transf, this.tc, this.tch, this.ta));
                    break;
                case PARALLEL:
                    this.setRuleApplier(new ParallelTriggerRuleApplier(this.transf, this.tc, this.tch, this.ta));
                    break;
                case SOURCE_DELEGATED_DATALOG:
                    this.setRuleApplier(new SourceDelegatedDatalogRuleApplier());
                    break;
                case MULTI_THREAD_PARALLEL:
                    this.setRuleApplier(new MultiThreadRuleApplier(this.transf, this.tc, this.tch, this.ta));
                    break;
            }
        }
        if (this.hcs.isEmpty()) {
            this.addStandardHaltingConditions();
        }
        // When any parallel chase is used, we have to add this endOfStep to add the new facts
        if (this.application == Application.PARALLEL) {
            this.end_step_ts.addFirst(new AddCreatedFacts());
        }
        if (this.debug) {
            this.addStepEndTreatments(new Debug());
        }
        return Optional.of(this);
    }


    ///////////////////////////
    // Higher level settings //
    ///////////////////////////

    // Rule Applier //

    /**
     * Use a breadth first trigger applier
     *
     * @return this
     */
    public ChaseBuilder useTriggerRuleApplier() {
        this.applier = InteGraalKeywords.Algorithms.Parameters.Chase.Applier.BREADTH_FIRST;
        return this;
    }

    // FOQuery Evaluator //

    /**
     * Use the generic query evaluator
     *
     * @return this
     */
    public ChaseBuilder useGenericFOQueryEvaluator() {
        this.evaluator = InteGraalKeywords.Algorithms.Parameters.Chase.Evaluator.GENERIC;
        return this;
    }

    /**
     * Use the smart query evaluator
     *
     * @return this
     */
    public ChaseBuilder useSmartFOQueryEvaluator() {
        this.evaluator = InteGraalKeywords.Algorithms.Parameters.Chase.Evaluator.SMART;
        return this;
    }

    // Scheduler //

    /**
     * Use a naive rule scheduler
     *
     * @return this
     */
    public ChaseBuilder useNaiveRuleScheduler() {
        this.scheduler = InteGraalKeywords.Algorithms.Parameters.Chase.Scheduler.NAIVE_SCHEDULER;
        return this;
    }

    /**
     * Use a rule scheduler based on the GRD
     *
     * @return this
     */
    public ChaseBuilder useGRDRuleScheduler() {
        this.scheduler = InteGraalKeywords.Algorithms.Parameters.Chase.Scheduler.GRD;
        return this;
    }

    // Body To Query transformers //

    /**
     * Use a transformation that keep all variables as answer variables when evaluating a rule's body
     *
     * @return this
     */
    public ChaseBuilder useAllTransformer() {
        this.transformer = InteGraalKeywords.Algorithms.Parameters.Chase.Transformer.ALL;
        return this;
    }

    /**
     * Use a transformation that keep only the variables of the frontier as answer variables when evaluating a rule's body
     *
     * @return this
     */
    public ChaseBuilder useFrontierTransformer() {
        this.transformer = InteGraalKeywords.Algorithms.Parameters.Chase.Transformer.FRONTIER;
        return this;
    }

    // Computer //

    /**
     * Use a naive method to compute triggers
     *
     * @return this
     */
    public ChaseBuilder useNaiveComputer() {
        this.computer = InteGraalKeywords.Algorithms.Parameters.Chase.Computer.NAIVE_COMPUTER;
        return this;
    }

    /**
     * Use the semi naive method to compute triggers
     *
     * @return this
     */
    public ChaseBuilder useSemiNaiveComputer() {
        this.computer = InteGraalKeywords.Algorithms.Parameters.Chase.Computer.SEMI_NAIVE;
        return this;
    }

    /**
     * Use the two step method to compute triggers
     *
     * @return this
     */
    public ChaseBuilder useTwoStepComputer() {
        this.computer = InteGraalKeywords.Algorithms.Parameters.Chase.Computer.TWO_STEP;
        return this;
    }

    // Application //

    /**
     * Use a breadth-first application of the triggers
     *
     * @return this
     */
    public ChaseBuilder useBreadthFirstApplier() {
        this.application = Application.DIRECT;
        this.applier = InteGraalKeywords.Algorithms.Parameters.Chase.Applier.BREADTH_FIRST;
        return this;
    }

    /**
     * Use a parallel application of the triggers
     *
     * @return this
     */
    public ChaseBuilder useParallelApplier() {
        this.application = Application.PARALLEL;
        this.applier = InteGraalKeywords.Algorithms.Parameters.Chase.Applier.PARALLEL;
        return this;
    }

    /**
     * Use a multi-threaded rule applier for parallel application of rules.
     * This may not work with stores that do not support concurrent matching or queries
     *
     * @return this
     */
    public ChaseBuilder useMultiThreadRuleApplier() {
        this.application = Application.PARALLEL;
        this.applier = InteGraalKeywords.Algorithms.Parameters.Chase.Applier.MULTI_THREAD_PARALLEL;
        return this;
    }


    /**
     * Use a method of delegating to the source the application of the datalog rules
     *
     * @return this
     */
    public ChaseBuilder useSourceDelegatedDatalogApplier() {
        this.applier = InteGraalKeywords.Algorithms.Parameters.Chase.Applier.SOURCE_DELEGATED_DATALOG;
        return this;
    }

    // Criteria //

    /**
     * Use an always true criteria for the triggers
     *
     * @return this
     */
    public ChaseBuilder useAlwaysTrueChecker() {
        this.checker = InteGraalKeywords.Algorithms.Parameters.Chase.Checker.TRUE;
        return this;
    }

    /**
     * Use a oblivious criteria for the triggers
     *
     * @return this
     */
    public ChaseBuilder useObliviousChecker() {
        this.checker = InteGraalKeywords.Algorithms.Parameters.Chase.Checker.OBLIVIOUS;
        return this;
    }

    /**
     * Use a semi oblivious criteria for the triggers
     *
     * @return this
     */
    public ChaseBuilder useSemiObliviousChecker() {
        this.checker = InteGraalKeywords.Algorithms.Parameters.Chase.Checker.SEMI_OBLIVIOUS;
        return this;
    }

    /**
     * Use a restricted criteria for the triggers
     *
     * @return this
     */
    public ChaseBuilder useRestrictedChecker() {
        this.checker = InteGraalKeywords.Algorithms.Parameters.Chase.Checker.RESTRICTED;
        return this;
    }

    /**
     * Use a combined semi oblivious and restricted criteria for the triggers
     *
     * @return this
     */
    public ChaseBuilder useSORestrictedChecker() {
        this.checker = InteGraalKeywords.Algorithms.Parameters.Chase.Checker.SO_RESTRICTED;
        return this;
    }

    /**
     * Use an equivalent criteria for the triggers
     *
     * @return this
     */
    public ChaseBuilder useEquivalentChecker() {
        this.checker = InteGraalKeywords.Algorithms.Parameters.Chase.Checker.EQUIVALENT;
        return this;
    }

    // Existential namer //

    /**
     * Use a fresh name for the existentials
     *
     * @return this
     */
    public ChaseBuilder useFreshNaming() {
        this.namer = InteGraalKeywords.Algorithms.Parameters.Chase.Namer.FRESH;
        return this;
    }

    /**
     * Use a namer of the body as name for the existentials
     *
     * @return this
     */
    public ChaseBuilder useBodySkolem() {
        this.namer = InteGraalKeywords.Algorithms.Parameters.Chase.Namer.BODY;
        return this;
    }

    /**
     * Use a namer of the body, limited to the frontier as name for the existentials
     *
     * @return this
     */
    public ChaseBuilder useFrontierSkolem() {
        this.namer = InteGraalKeywords.Algorithms.Parameters.Chase.Namer.FRONTIER;
        return this;
    }

    /**
     * Use a namer of the body, limited to the frontier of the piece as name for the existentials
     *
     * @return this
     */
    public ChaseBuilder useFrontierByPieceSkolem() {
        this.namer = InteGraalKeywords.Algorithms.Parameters.Chase.Namer.FRONTIER_PIECE;
        return this;
    }

    /////////////
    // Setters //
    /////////////

    /**
     * Sets the FactBase
     *
     * @param fb the FactBase
     * @return this
     */
    public ChaseBuilder setFactBase(FactBase fb) {
        this.fb = fb;
        return this;
    }

    /**
     * Sets the RuleBase
     *
     * @param rb the RuleBase
     * @return this
     */
    public ChaseBuilder setRuleBase(RuleBase rb) {
        this.rb = rb;
        return this;
    }

    /**
     * Sets the TermFactory
     *
     * @param tf the TermFactory
     * @return this
     */
    public ChaseBuilder setTermFactory(TermFactory tf) {
        this.tf = tf;
        return this;
    }

    /**
     * Sets (and check) the parameter configuration
     * <br/>
     * The chase parameter list is considered in the given order.
     * Two kinds of parameters are considered :
     * - exclusive ones (only one value is considered): when such
     * a parameter occurs several times in the list, only the last value is retained
     * - cumulative ones (several values are possible) that are CHASEHALTER and INTERSTEPTREATMENT:
     * when such a parameter occurs several times in the list, all its values are accumulated
     *
     * @param chaseParams the chase parameters
     * @return this
     * @throws IGParameterException if one of parameters is invalid for the chase
     */
    // TODO for exclusive parameters, one can prefer to generate an exception when the list contains several occurrences
    public ChaseBuilder setParams(IGParameter<InteGraalKeywords, ?>... chaseParams) throws IGParameterException {

        //	Integer rank = 0;
        Duration timeout = Duration.ZERO;

        for (IGParameter<InteGraalKeywords, ?> param : chaseParams) {
            switch (param.name()) {
                case InteGraalKeywords.SCHEDULER:
                    switch ((InteGraalKeywords.Algorithms.Parameters.Chase.Scheduler) param.value()) {
                        case GRD -> this.useGRDRuleScheduler();
                        case NAIVE_SCHEDULER -> this.useNaiveRuleScheduler();
                    }
                    break;
                case InteGraalKeywords.CHECKER:
                    switch ((InteGraalKeywords.Algorithms.Parameters.Chase.Checker) param.value()) {
                        case OBLIVIOUS -> this.useObliviousChecker();
                        case SEMI_OBLIVIOUS -> this.useSemiObliviousChecker();
                        case RESTRICTED -> this.useRestrictedChecker();
                        case SO_RESTRICTED -> this.useSORestrictedChecker();
                        case EQUIVALENT -> this.useEquivalentChecker();
                        case TRUE -> this.useAlwaysTrueChecker();
                    }
                    break;
                case InteGraalKeywords.COMPUTER:
                    switch ((InteGraalKeywords.Algorithms.Parameters.Chase.Computer) param.value()) {
                        case NAIVE_COMPUTER -> this.useNaiveComputer();
                        case SEMI_NAIVE -> this.useSemiNaiveComputer();
                        case TWO_STEP -> this.useTwoStepComputer();
                    }
                    break;
                case InteGraalKeywords.APPLIER:
                    switch ((InteGraalKeywords.Algorithms.Parameters.Chase.Applier) param.value()) {
                        case BREADTH_FIRST -> this.useBreadthFirstApplier();
                        case PARALLEL -> this.useParallelApplier();
                        case MULTI_THREAD_PARALLEL -> this.useMultiThreadRuleApplier();
                        case SOURCE_DELEGATED_DATALOG -> this.useSourceDelegatedDatalogApplier();
                    }
                    break;
                case InteGraalKeywords.EVALUATOR:
                    switch ((InteGraalKeywords.Algorithms.Parameters.Chase.Evaluator) param.value()) {
                        case GENERIC -> this.useGenericFOQueryEvaluator();
                        case SMART -> this.useSmartFOQueryEvaluator();
                    }
                    break;
                case InteGraalKeywords.TRANSFORMER:
                    switch ((InteGraalKeywords.Algorithms.Parameters.Chase.Transformer) param.value()) {
                        case ALL -> this.useAllTransformer();
                        case FRONTIER -> this.useFrontierTransformer();
                    }
                    break;
                case InteGraalKeywords.NAMER:
                    switch ((InteGraalKeywords.Algorithms.Parameters.Chase.Namer) param.value()) {
                        case FRESH -> this.useFreshNaming();
                        case BODY -> this.useBodySkolem();
                        case FRONTIER -> this.useFrontierSkolem();
                        case FRONTIER_PIECE -> this.useFrontierByPieceSkolem();
                    }
                    break;
                case InteGraalKeywords.CHASEHALTER:
                    switch ((InteGraalKeywords.Algorithms.Parameters.Chase.ChaseHalter) param.value()) {
                        case NO_NEW_FACTS -> this.addHaltingConditions(new CreatedFactsAtPreviousStep());
                        case NO_RULES_TO_APPLY -> this.addHaltingConditions(new HasRulesToApply());
                        // note that RANK, TIMEOUT, INTERRUPT below are also a type of parametric chase halter
                    }
                    break;
                case InteGraalKeywords.RANK:
                    Integer rank = (Integer) param.value();
                    if (rank < 0)
                        throw new IGParameterException("Chase configuration: " + param.name() + " has an invalid value " + rank + " (must be a positive integer).");
                    else if (rank > 0) this.addHaltingConditions(new LimitNumberOfStep(rank));
                    break;
                case InteGraalKeywords.TIMEOUT:
                    timeout = (Duration) param.value();
                    if (timeout.isNegative())
                        throw new IGParameterException("Chase configuration: " + param.name() + " has an invalid value " + timeout + " (must be a positive duration).");
                    else if (!timeout.isZero()) this.addHaltingConditions(new Timeout(timeout.toMillis()));
                    break;
                case InteGraalKeywords.INTERRUPT:
                    this.addHaltingConditions(new ExternalInterruption((AtomicBoolean) param.value())); // TODO Correct ??????
                    break;
                case InteGraalKeywords.INTERSTEPTREATMENT:
                    switch ((InteGraalKeywords.Algorithms.Parameters.Chase.InterStepTreatment) param.value()) {
                        case CORE -> this.addStepEndTreatments(new ComputeCore(new MultiThreadsByPieceCoreProcessor(
                                16, MultiThreadsByPieceCoreProcessor.Variant.EXHAUSTIVE)));                // TODO  Propose a way to choose the coreProcessor variant
                        case LOCAL_CORE ->
                                this.addStepEndTreatments(new ComputeLocalCore(new MultiThreadsByPieceCoreProcessor(
                                        16, MultiThreadsByPieceCoreProcessor.Variant.EXHAUSTIVE)));
                        case DEBUG -> this.debug(); //this.addStepEndTreatments(new Debug());
                        case REPORT -> System.out.println("Not implemented");
                        // TODO : Dans l'idée, fournirait un rapport sur ce qui vient d'être fait : num étape, nombre d'atomes inférés, atomes inférés, nb règles à appliquées, temps depuis les début

                    }
                    break;
                default:
                    throw new IGParameterException("Chase configuration: " + param.name() + " is an invalid parameter of chase");
            }
        }
        return this;
    }


    /**
     * Sets the RuleScheduler
     *
     * @param rsc the RuleScheduler
     * @return this
     */
    public ChaseBuilder setRuleScheduler(RuleScheduler rsc) {
        this.rsc = rsc;
        return this;
    }

    /**
     * Sets the FOQueryEvaluator
     *
     * @param eval the FOQueryEvaluator
     * @return this
     */
    public ChaseBuilder setFOQueryEvaluator(FOQueryEvaluator<FOFormula> eval) {
        this.eval = eval;
        return this;
    }

    /**
     * Sets the RuleApplier
     *
     * @param ra the RuleApplier
     * @return this
     */
    public ChaseBuilder setRuleApplier(RuleApplier ra) {
        this.ra = ra;
        return this;
    }

    // Triggers

    /**
     * Sets the BodyToQueryTransformer
     *
     * @param transf the BodyToQueryTransformer
     * @return this
     */
    public ChaseBuilder setBodyToQueryTransformer(BodyToQueryTransformer transf) {
        this.transf = transf;
        return this;
    }

    /**
     * Sets the TriggerComputer
     *
     * @param tc the TriggerComputer
     * @return this
     */
    public ChaseBuilder setTriggerComputer(TriggerComputer tc) {
        this.tc = tc;
        return this;
    }

    /**
     * Sets the TriggerChecker
     *
     * @param tch the TriggerChecker
     * @return this
     */
    public ChaseBuilder setTriggerChecker(TriggerChecker tch) {
        this.tch = tch;
        return this;
    }

    /**
     * Sets the TriggerApplier
     *
     * @param ta the TriggerApplier
     * @return this
     */
    public ChaseBuilder setTriggerApplier(TriggerApplier ta) {
        this.ta = ta;
        return this;
    }

    /**
     * Sets the TriggerRenamer
     *
     * @param tr the TriggerRenamer
     * @return this
     */
    public ChaseBuilder setExistentialsRenamer(TriggerRenamer tr) {
        this.tn = tr;
        return this;
    }

    /**
     * Sets the FactsHandler
     *
     * @param fh the FactsHandler
     * @return this
     */
    public ChaseBuilder setNewFactsHandler(FactsHandler fh) {
        this.fh = fh;
        return this;
    }

    // halting conditions

    /**
     * Adds the standard halting conditions
     *
     * @return this
     */
    public ChaseBuilder addStandardHaltingConditions() {
        this.addHaltingConditions(new CreatedFactsAtPreviousStep(), new HasRulesToApply());
        return this;
    }

    /**
     * Adds the given halting conditions
     *
     * @param hcs halting conditions
     * @return this
     */
    public ChaseBuilder addHaltingConditions(HaltingCondition... hcs) {
        this.hcs.addAll(Arrays.asList(hcs));
        return this;
    }

    /**
     * Adds the given halting conditions
     *
     * @param hcs halting conditions
     * @return this
     */
    public ChaseBuilder addHaltingConditions(Collection<HaltingCondition> hcs) {
        this.hcs.addAll(hcs);
        return this;
    }

    // Global pretreatment

    /**
     * Adds the given Global pretreatment
     *
     * @param pts Global pretreatment
     * @return this
     */
    public ChaseBuilder addGlobalPretreatments(Pretreatment... pts) {
        this.global_pts.addAll(Arrays.asList(pts));
        return this;
    }

    /**
     * Adds the given Global pretreatment
     *
     * @param pts Global pretreatment
     * @return this
     */
    public ChaseBuilder addGlobalPretreatments(Collection<Pretreatment> pts) {
        this.global_pts.addAll(pts);
        return this;
    }

    // Step pretreament

    /**
     * Adds the given Step pretreatment
     *
     * @param pts Step pretreatment
     * @return this
     */
    public ChaseBuilder addStepPretreatments(Pretreatment... pts) {
        this.step_pts.addAll(Arrays.asList(pts));
        return this;
    }

    /**
     * Adds the given Step pretreatment
     *
     * @param pts Step pretreatment
     * @return this
     */
    public ChaseBuilder addStepPretreatments(Collection<Pretreatment> pts) {
        this.step_pts.addAll(pts);
        return this;
    }

    // Global end treatment

    /**
     * Adds the given Global end treatment
     *
     * @param ets Step Global end treatment
     * @return this
     */
    public ChaseBuilder addGlobalEndTreatments(EndTreatment... ets) {
        this.global_end_ts.addAll(Arrays.asList(ets));
        return this;
    }

    /**
     * Adds the given Global end treatment
     *
     * @param ets Step Global end treatment
     * @return this
     */
    public ChaseBuilder addGlobalEndTreatments(Collection<EndTreatment> ets) {
        this.global_end_ts.addAll(ets);
        return this;
    }

    // Step end treatment

    /**
     * Adds the given Step end treatment
     *
     * @param ets Step end treatment
     * @return this
     */
    public ChaseBuilder addStepEndTreatments(EndTreatment... ets) {
        this.end_step_ts.addAll(Arrays.asList(ets));
        return this;
    }

    /**
     * Adds the given Step end treatment
     *
     * @param ets Step end treatment
     * @return this
     */
    public ChaseBuilder addStepEndTreatments(Collection<EndTreatment> ets) {
        this.end_step_ts.addAll(ets);
        return this;
    }

    /**
     * Adds the debug option
     *
     * @return this
     */
    public ChaseBuilder debug() {
        this.debug = true;
        return this;
    }

    /**
     * Returns a StratifiedChaseBuilder for creating a StratifiedChase, with this ChaseBuilder in parameter.
     * Transfers the FactBase and RuleBase if they are already set.
     *
     * @return a new StratifiedChaseBuilder instance.
     */
    public StratifiedChaseBuilder useStratifiedChase() {
        StratifiedChaseBuilder stratifiedChaseBuilder = new StratifiedChaseBuilder(this);
        if (this.fb != null) {
            stratifiedChaseBuilder.setFactBase(this.fb);
        }
        if (this.rb != null) {
            stratifiedChaseBuilder.setRuleBase(this.rb);
        }
        return stratifiedChaseBuilder;
    }

}
