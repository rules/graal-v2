package fr.boreal.forward_chaining.chase.test.treatment;

import java.util.stream.Stream;

import com.mongodb.assertions.Assertions;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.query_evaluation.generic.DefaultGenericQueryEvaluator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import fr.boreal.forward_chaining.chase.ChaseBuilder;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;

public class ComputeChaseWithFunctionsTest {

    /*
     * In the following examples, be careful of the type returned by the functions.
     * Indeed, differences between int and long may cause errors.
     */
    final static String dlgp2 = "@computed fct:<stdfct>\n q1(fct:sum(X,1)):-p1(X). p1(2). ?():-q1(fct:toLong(3)).";

    /*
    This case also tests the fact of evaluating a nested function.
     */
    final static String dlgp3 = "@computed fct:<stdfct>\n q1(fct:toInt(fct:sum(X,1))):-p1(X). p1(2). ?():-q1(3).";

    static Stream<Arguments> provideData() {
        return Stream.of(Arguments.of(dlgp2),Arguments.of(dlgp3));
    }

    @ParameterizedTest
    @MethodSource("provideData")
    public void computeChaseWithFunctionsTest(String inputDLGP) {

        var res = DlgpParser.parseDLGPString(inputDLGP);

        var fb = new SimpleInMemoryGraphStore(res.atoms());
        var rb = new RuleBaseImpl(res.rules());

        var c = ChaseBuilder.defaultBuilder(fb, rb).build().get();

        //System.out.println("pre: " + fb);

        c.execute();

        //System.out.println("post: " + fb);

        for (Query q : res.queries()) {
            var r = DefaultGenericQueryEvaluator.defaultInstance().evaluate(q, fb);
            Assertions.assertTrue(r.hasNext());
        }

    }

}
