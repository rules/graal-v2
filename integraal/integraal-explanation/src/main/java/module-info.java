module fr.boreal.explanation {
    requires transitive fr.boreal.model;
    requires fr.inria.integraal.view_parser.parser;
    requires fr.lirmm.boreal.util;
    requires fr.boreal.storage;
    requires fr.boreal.query_evaluation;
    requires fr.boreal.forward_chaining;
    requires fr.boreal.backward_chaining;
    requires com.google.common;
	requires org.apache.commons.lang3;
    requires fr.boreal.component;
    requires org.checkerframework.checker.qual;
    requires org.slf4j;
}