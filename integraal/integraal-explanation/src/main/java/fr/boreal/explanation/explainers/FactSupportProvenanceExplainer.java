package fr.boreal.explanation.explainers;

import fr.boreal.explanation.api.AtomicQueryExplanationEnumerator;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;

import java.util.Set;

/**
 * Computes fact-support-why-provenance explanations for a knowledge base and a ground atomic query
 */
public class FactSupportProvenanceExplainer implements AtomicQueryExplanationEnumerator<FactBase> {
    public Set<FactBase> explain(Atom query) {
        return Set.of();
    }
}
