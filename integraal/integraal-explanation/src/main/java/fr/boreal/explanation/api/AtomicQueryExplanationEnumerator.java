package fr.boreal.explanation.api;

import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.kb.api.KnowledgeBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.query.api.Query;

import java.util.Arrays;
import java.util.Set;

/**
 * Interface for an explainer which supports only ground atomic queries (expressed as atoms).
 *
 * @param <ExplanationType> the type of the (set of) explanations which is returned by the enumerator
 */
public interface AtomicQueryExplanationEnumerator<ExplanationType extends Object> {

    /**
     * Checks if an atom can be explained
     *
     * @param query
     * @return true iff the query contains only constants and literals
     */
    default boolean canExplain(KnowledgeBase kb, Atom query) {

        var queryOK = Arrays.stream(query.getTerms()).allMatch(term -> term.isConstant() || term.isLiteral());

        // check that all rules body and head are conjunctions
        // TODO : check more in depth that the FOFormulaConjunction contains only atoms
        // TODO : check that in the the data (be careful of federations) there are only constants and literals
        // TODO : check that in the rules there are no functional terms
        var rulesOK = true;
        for (var r : kb.getRuleBase().getRules()) {
            // rules must have a label
            if (r.getLabel() == null || r.getLabel().isBlank()) {
                throw new RuntimeException("rule label is empty: " + r);
            }
            if (!(r.getBody() instanceof FOFormulaConjunction || r.getBody() instanceof Atom)) {
                rulesOK = false;
                break;
            }
            if (!(r.getHead() instanceof FOFormulaConjunction || r.getHead() instanceof Atom)) {
                rulesOK = false;
                break;
            }

        }
        return queryOK && rulesOK;
    }

    /**
     *
     * @param query
     * @return the set of explanations for the query on the current KB
     */
    Set<ExplanationType> explain(Atom query);

}
