package fr.boreal.explanation.ruleFactories;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.KnowledgeBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.*;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.logicalElements.impl.functionalTerms.SpecializableLogicalFunctionalTermImpl;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.impl.FORuleImpl;

import java.util.*;

/**
 * This class implements a rule transformation for :
 *
 * (1) statically computing the GRI (all the GRI is computed in one step)
 * (2) tracing relevant atoms
 */
public class StaticGRIRuleTransformer {

    // factories for creating new terms, predicates and transformed rules
    private TermFactory termFactory = SameObjectTermFactory.instance();
    private static PredicateFactory predicateFactory = SameObjectPredicateFactory.instance();
    private FOFormulaFactory formulaFactory = FOFormulaFactory.instance();

    // fresh REL predicate used for tracing
    public static final Predicate REL = predicateFactory.createOrGetPredicate("REL", 1);

    // mapping between a rule and its corresponding fresh EDGE and REL_EDGE predicats
    private BiMap<FORule, Predicate> ruleToEdgePredicateMap = HashBiMap.create();
    private BiMap<FORule, Predicate> ruleToRelEdgePredicateMap = HashBiMap.create();

    // mapping between a predicate and its corresponding "augmented/+" version
    private BiMap<Predicate, Predicate> predicateToPredicateWithIdentifier = HashBiMap.create();

    // default instance
    private static final StaticGRIRuleTransformer INSTANCE = new StaticGRIRuleTransformer();

    /**
     * @return the default instance of the rule transformation factory
     */
    public static StaticGRIRuleTransformer instance() {
        return INSTANCE;
    }


    /**
     * This method takes a knowledge base and creates a set of rules
     * used for statically building the GRI with functional term idenitifiers
     *
     * @param kb a knowledge base
     * @return a rule base with
     * (1) GRI node building rules
     * (2) GRI edge building rules
     * (3) rel propagation rules
     */

    public RuleBase createTransformedRB(KnowledgeBase kb) {

        RuleBase rb = new RuleBaseImpl();

        for (Iterator<Predicate> it = kb.getFactBase().getPredicates(); it.hasNext(); ) {
            Predicate p = it.next();
            rb.add(createGRINodeBuildingRule(p));
        }

        for (FORule r : kb.getRuleBase().getRules()) {
            rb.add(createGRIEdgeBuildingRule(r));
            rb.add(createRelPropagationRule(r));
        }

        return rb;
    }


    /**
     * This method takes a predicate p
     * and returns a rule of the form p(x) -> p+(x,f_p(x))
     * where p+ is a fresh predicate and
     * where f_p(x) stands for an identifier of the atom p(x)
     *
     * @param p
     * @return a rule of the form p(x) -> p+(x,f_p(x))
     */
    public FORule createGRINodeBuildingRule(Predicate p) {
        List<Term> terms = new ArrayList<>();
        for (int i = 0; i < p.arity(); i++) {
            terms.add(termFactory.createOrGetFreshVariable());
        }
        Atom bodyAtom = new AtomImpl(p, terms);
        Atom headAtom = createAtomWithStoredFnTermIdentifier(bodyAtom);

        return new FORuleImpl(p.label(), bodyAtom, headAtom);
    }

    /**
     * This method takes a rule of the form
     * r : {p_i(x_i)}_i -> {q_j(w_j)}_j
     * and returns a corresponding transformed rule of the form
     * r+ : {p_i+(x_i,id_i)}_i -> {q_j+(w_j,id_j)}_j ^ {E_r(id_1,id_2,...,id_j)}_j
     * The rule r+ will produce an edge E_r(...) on the GRI for every trigger using the rule r
     *
     * @param r
     * @return corresponding edge building rule
     */
    public FORule createGRIEdgeBuildingRule(FORule r) {

        List<Atom> bodyAtoms = getOrderedAtomsFromSet(r.getBody().asAtomSet());
        List<Atom> headAtoms = new ArrayList<>(r.getHead().asAtomSet());

        Predicate edgePredicate = getOrGenerateEdgePredicateFromRule(r);

        List<Atom> bodyAtomsWithIdentifiers = new ArrayList<>();
        List<Term> listOfVariableIdentifiersForBodyAtoms = new ArrayList<>();

        for (Atom b : bodyAtoms) {
            Atom bodyAtomWithVariableIdentifier = createAtomWithVariableIdentifier(b);
            bodyAtomsWithIdentifiers.add(bodyAtomWithVariableIdentifier);

            listOfVariableIdentifiersForBodyAtoms.add(bodyAtomWithVariableIdentifier.getTerm(bodyAtomWithVariableIdentifier.getPredicate().arity() - 1));
        }

        List<Atom> headAtomsWithIdentifiers = new ArrayList<>();

        for (Atom h : headAtoms) {
            Atom headAtomWithFuncTerm = createAtomWithStoredFnTermIdentifier(h);
            headAtomsWithIdentifiers.add(headAtomWithFuncTerm);

            Term functionalTermIdentifierForHeadAtom = headAtomWithFuncTerm.getTerm(headAtomWithFuncTerm.getPredicate().arity() - 1);

            List<Term> edgeTerms = new ArrayList<>(listOfVariableIdentifiersForBodyAtoms);
            edgeTerms.add(functionalTermIdentifierForHeadAtom);

            Atom edgeAtom = new AtomImpl(edgePredicate, edgeTerms);
            headAtomsWithIdentifiers.add(edgeAtom);
        }

        FOFormula body = formulaFactory.createOrGetConjunction(bodyAtomsWithIdentifiers);
        FOFormula head = formulaFactory.createOrGetConjunction(headAtomsWithIdentifiers);

        return new FORuleImpl(r.getLabel(), body, head);

    }

    /**
     * This method takes a rule of the form
     * r : {p_i(x_i)}_i -> {q_j(w_j)}_j
     * and returns a corresponding transformed rule of the form
     * r* : E_r(x_1,x_2,...,Y) ^ REL(Y) -> {REL(x_i)}_i ^ REL_EDGE_r(x_1,x_2,...,w)
     * the rule propagates the REL and RELEDGE predicates for a given trigger
     * to relevant atoms
     *
     * @param r
     * @return corresponding rel propagation rule
     */
    public FORule createRelPropagationRule(FORule r) {

        Predicate edgePredicate = getOrGenerateEdgePredicateFromRule(r);
        Predicate relEdgePredicate = getOrGenerateRelEdgePredicateFromRule(r);

        int edgeAtomArity = relEdgePredicate.arity();

        List<Term> nodeVariableList = new ArrayList<>();
        List<Atom> headAtoms = new ArrayList<>();

        // this is for the cases where there is a computed atom in the rule
        // (this atom won't be in the fact base so we have to reduce the arity of the predicate)
        for (Atom a : r.getBody().asAtomSet()) {
            if (a instanceof ComputedAtom) {
                --edgeAtomArity;
            }
        }

        for (int i = 1; i < edgeAtomArity; i++) {
            Variable Xi = termFactory.createOrGetVariable("X" + i);
            nodeVariableList.add(Xi);
            Atom relX = new AtomImpl(REL, Xi);
            headAtoms.add(relX);
        }

        var relAtomVariable = termFactory.createOrGetVariable("Y");
        nodeVariableList.add(relAtomVariable);

        Atom edgeBodyAtom = new AtomImpl(edgePredicate, nodeVariableList);
        Atom relEdge = new AtomImpl(relEdgePredicate, nodeVariableList);

        headAtoms.add(relEdge);

        Atom relYBodyAtom = new AtomImpl(REL, relAtomVariable);

        return new FORuleImpl(
                r.getLabel(),
                formulaFactory.createOrGetConjunction(edgeBodyAtom, relYBodyAtom),
                formulaFactory.createOrGetConjunction(headAtoms)
        );
    }

    /**
     *
     * @return the rule to edge predicate map
     */
    public BiMap<FORule, Predicate> getRuleToEdgePredicateMap() {
        return ruleToEdgePredicateMap;
    }

    /**
     *
     * @param p
     * @return p+ from p
     */
    public Predicate getPredicateWithIdentifier(Predicate p) {
        return predicateToPredicateWithIdentifier.computeIfAbsent(p,
                predicate -> predicateFactory.createOrGetPredicate(
                        p.toString() + "+",
                        p.arity() + 1));
    }

    /**
     *
     * @param a
     * @return returns p+(x,f_p(x)) from p(x)
     */
    public Atom createAtomWithStoredFnTermIdentifier(Atom a) {
        List<Term> termList = new ArrayList<>(Arrays.asList(a.getTerms()));
        SpecializableLogicalFunctionalTerm identifier = createFnTermIdentifier(a);
        termList.add(identifier);

        return new AtomImpl(getPredicateWithIdentifier(a.getPredicate()), termList);
    }

    /**
     *
     * @param a
     * @return returns f_p(x) from p(x)
     */
    public SpecializableLogicalFunctionalTerm createFnTermIdentifier(Atom a) {
        List<Term> termList = Arrays.asList(a.getTerms());
        return new SpecializableLogicalFunctionalTermImpl(getFnSymbolFromPredicate(a.getPredicate()), termList);
    }

    /**
     *
     * @param a
     * @return returns p+(x,Y) from p(x)
     */
    public Atom createAtomWithVariableIdentifier(Atom a) {
        List<Term> termList = new ArrayList<>(Arrays.asList(a.getTerms()));
        Variable identifier = termFactory.createOrGetFreshVariable();
        termList.add(identifier);

        return new AtomImpl(getPredicateWithIdentifier(a.getPredicate()), termList);
    }

    /**
     * @param t
     * @return returns p(a) from f_p(a)
     */
    public Atom getAtomFromFnTerm(LogicalFunctionalTerm t) {
        return new AtomImpl(getPredicateFromFnTerm(t), t.getFirstLevelTerms());
    }

    /**
     *
     * @param p
     * @return f_p from p
     */
    public static String getFnSymbolFromPredicate(Predicate p) {
        return "f_" + p.label();
    }

    /**
     *
     * @param t
     * @return p from f_p
     */
    public Predicate getPredicateFromFnTerm(LogicalFunctionalTerm t) {
        String predicateLabel = t.getFunctionName().substring(2);
        int arity = t.getFirstLevelTerms().size();
        return predicateFactory.createOrGetPredicate(predicateLabel, arity);
    }


    /**
     *
     * Returns REL_EDGE_r from r and stores the association in the map.
     *
     * @param r
     * @return the predicate E_r
     */
    public Predicate getOrGenerateRelEdgePredicateFromRule(FORule r) {
        List<Atom> bodyAtoms = new ArrayList<>(r.getBody().asAtomSet());

        Predicate relEdgePredicate = ruleToRelEdgePredicateMap.computeIfAbsent(r,
                r2 -> predicateFactory.createOrGetPredicate(
                        "relEdge_" + r2.getLabel(),
                        bodyAtoms.size() + 1
                )
        );
        return relEdgePredicate;
    }

    /**
     * Returns E_r from r and stores the association in the map.
     * @param r
     * @return
     */
    public Predicate getOrGenerateEdgePredicateFromRule(FORule r) {
        List<Atom> bodyAtoms = new ArrayList<>(r.getBody().asAtomSet());

        Predicate edgePredicate = ruleToEdgePredicateMap.computeIfAbsent(r,
                r2 -> predicateFactory.createOrGetPredicate(
                        "E_" + r2.getLabel(),
                        bodyAtoms.size() + 1
                )
        );
        return edgePredicate;
    }

    /**
     *
     * @param p
     * @return r starting from E_r
     */
    public FORule getRuleFromEdgePredicate(Predicate p) {
        return ruleToEdgePredicateMap.inverse().get(p);
    }

    /**
     *
     * @return the rule to edge predicate map
     */
    public BiMap<FORule, Predicate> getRuleToRelEdgePredicateMap() {
        return this.ruleToRelEdgePredicateMap;
    }

    /**
     * TODO for later user
     *
     * @param query
     * @return
     */
    public Predicate getRELPredicateFor(Atom query) {
        return predicateFactory.createOrGetPredicate(REL.label() + query, REL.arity());
    }

    ////////////////////
    // PRIVATE METHODS
    ////////////////////

    /**
     * This method is used to ensure that atoms in the rule bodies are considered in a fixed order.
     * We need this to be consistent in the usage of the E_r(x1..xk) predicate
     *
     * @param atomSet
     * @return the ordered set of atoms
     */
    private List<Atom> getOrderedAtomsFromSet(Set<Atom> atomSet) {
        List<Atom> list = new ArrayList<>(atomSet);
        list.sort(Comparator.comparing(Object::toString));
        return list;
    }
}
