package fr.boreal.explanation.solving_enumerating;

import fr.boreal.explanation.ruleFactories.StaticGRIRuleTransformer;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.KnowledgeBase;
import fr.boreal.model.logicalElements.api.Atom;

import java.util.HashSet;
import java.util.Set;

public class GMUSProcessor {
    private Set<String> gmuses = new HashSet<>();
    private static final GMUSSolver solver = new GMUSSolver();
    private static final GMUSTranslator translator = new GMUSTranslator();

    public Set<KnowledgeBase> computeExplanations(
            FactBase filteredGRI,
            StaticGRIRuleTransformer staticGRIRuleTransformer,
            Atom query) {
        GMUSEncoder encoder = new KBtoGSATEncoder(staticGRIRuleTransformer);
        var encoding = encoder.encode(filteredGRI, query);
        gmuses = solver.solve(encoding.propVarIDMap(), encoding.clauses(), encoding.nbGroup());

        return translator.translateGMUSes(gmuses, encoding.factIDMap(), encoding.ruleIDMap());
    }
}
