package fr.boreal.explanation.GRI.dynamicProcessing;

import fr.boreal.explanation.configuration.DefaultChaseForExplanations;
import fr.boreal.explanation.ruleFactories.StaticGRIRuleTransformer;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.KnowledgeBase;
import fr.boreal.model.kb.impl.KnowledgeBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used to compute all relevant atoms for a given query.
 */
public class RELTracer {

    private static final Logger LOG = LoggerFactory.getLogger(RELTracer.class);

    /**
     * @param gri
     * @param query
     * @return a factbase that contains a REL(f_p(a)) atom for every sup-relevant atom p(a) for the query
     * and a REL_EDGE_r(a1,a2,...) for every sup-relevant trigger for the query
     */
    public FactBase computeQueryRelevant(
            KnowledgeBase gri,
            StaticGRIRuleTransformer ruleTransformerFactory,
            Atom query) {
        gri = new KnowledgeBaseImpl(new SimpleInMemoryGraphStore(gri.getFactBase().getAtomsInMemory()), gri.getRuleBase());

        //first, check that the query is entailed
        Atom queryWithFuncId = ruleTransformerFactory.createAtomWithStoredFnTermIdentifier(query);

        if (!gri.getFactBase().contains(queryWithFuncId)) {
            LOG.warn("Query " + query + " not entailed by the input KB.");
            return new SimpleInMemoryGraphStore();
        }

        // add seed atom to the factbase
        Atom RELquery = new AtomImpl(StaticGRIRuleTransformer.REL, StaticGRIRuleTransformer.instance().createFnTermIdentifier(query));
        gri.getFactBase().add(RELquery);

        //propagate REL and REL_EDGE
        DefaultChaseForExplanations.chase(gri);

        return gri.getFactBase();
    }

}
