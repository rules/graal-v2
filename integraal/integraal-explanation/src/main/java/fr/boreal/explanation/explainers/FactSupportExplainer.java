package fr.boreal.explanation.explainers;

import fr.boreal.explanation.api.AtomicQueryExplanationEnumerator;
import fr.boreal.explanation.ruleFactories.StaticGRIRuleTransformer;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;

import java.util.Set;

/**
 * Computes fact-support explanations for a knowledge base and a ground atomic query
 */
public class FactSupportExplainer implements AtomicQueryExplanationEnumerator<FactBase> {
    static StaticGRIRuleTransformer staticRuleTransformerFactory = StaticGRIRuleTransformer.instance();

    public Set<FactBase> explain(Atom query) {
        return Set.of();
    }
}
