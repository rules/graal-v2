package fr.boreal.explanation.configuration;

import fr.boreal.forward_chaining.api.ForwardChainingAlgorithm;
import fr.boreal.forward_chaining.chase.ChaseBuilder;
import fr.boreal.model.kb.api.KnowledgeBase;

/**
 * sets the default chase used for computing explanations
 */
public class DefaultChaseForExplanations {

    public static void chase(KnowledgeBase kb) {
        ForwardChainingAlgorithm chase = ChaseBuilder.defaultBuilder(kb.getFactBase(), kb.getRuleBase())
                .useObliviousChecker()
                .useNaiveComputer()
                .useNaiveRuleScheduler()
                .build().get();
        chase.execute();
    }
}
