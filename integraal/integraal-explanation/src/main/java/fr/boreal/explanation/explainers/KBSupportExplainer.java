package fr.boreal.explanation.explainers;

import fr.boreal.explanation.api.AtomicQueryExplanationEnumerator;
import fr.boreal.explanation.GRI.dynamicProcessing.RELTracer;
import fr.boreal.explanation.solving_enumerating.GMUSProcessor;
import fr.boreal.explanation.ruleFactories.StaticGRIRuleTransformer;
import fr.boreal.explanation.GRI.staticProcessing.GRIBuilder;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.KnowledgeBase;
import fr.boreal.model.logicalElements.api.Atom;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Set;

/**
 * Computes kb-support explanations for a knowledge base and a ground atomic query
 */
public class KBSupportExplainer implements AtomicQueryExplanationEnumerator<KnowledgeBase> {
    KnowledgeBase gri;
    StaticGRIRuleTransformer ruleTransformer;
    /**
     * Sets the initial KB and compute the GRI
     *
     * @param kb the current kb
     */
    public KBSupportExplainer(KnowledgeBase kb) {
        Pair<KnowledgeBase, StaticGRIRuleTransformer> pair = GRIBuilder.buildGRI(kb);
        gri = pair.getLeft();
        ruleTransformer = pair.getRight();
    }

    public Set<KnowledgeBase> explain(Atom query) {
        RELTracer tracer = new RELTracer();
        FactBase filteredGRI = tracer.computeQueryRelevant(gri, ruleTransformer, query);

        GMUSProcessor gmusProcessor = new GMUSProcessor();
        return gmusProcessor.computeExplanations(filteredGRI, ruleTransformer, query);
    }
}
