package fr.boreal.explanation.solving_enumerating;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import fr.boreal.model.logicalElements.api.Term;

import java.io.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GMUSSolver {

    private static final GMUSSolver INSTANCE = new GMUSSolver();
    public static GMUSSolver instance() {
        return INSTANCE;
    }

    String tmpFileName = "mus.gcnf";
    File gcnfFile = new File(tmpFileName);

    int timeout = 0;
    public Set<String> solve(BiMap<Term, Integer> propVarIDMap,
            List<List<Integer>> clauses,
            int nbGroup) {
        Set<String> gmuses = new HashSet<>();
        writeGCNF(propVarIDMap, clauses, nbGroup);
        gmuses = getGMUSes(gcnfFile);
        return gmuses;
    };

    public void writeGCNF(BiMap<Term, Integer> propVarIDMap,
            List<List<Integer>> clauses,
            int nbGroup
            ) {
        try {
            FileWriter writerCNF = new FileWriter(gcnfFile, false);
            writerCNF.write("p gcnf "
                    + propVarIDMap.size() + " "
                    + clauses.size() + " "
                    + nbGroup + "\n");

            for (List<Integer> clause: clauses) {
                assert clause.getFirst() != null;
                writerCNF.write(
                        "{" + clause.getFirst() + "} "
                );

                for (int i = 1; i < clause.size(); i++) {
                    assert clause.get(i) != null;
                    writerCNF.write(
                            clause.get(i) + " "
                    );
                }
                writerCNF.write(" 0\n");
            }
            writerCNF.close();

        } catch (IOException e1) {
            throw new RuntimeException("Unable to write tmp file for gcnf solver");
        }
    }

    public Set<String> getGMUSes(File gcnfFile) {
        Set<String> gmuses = new HashSet<>();
        ProcessBuilder build = new ProcessBuilder().redirectErrorStream(true);
        if (timeout == 0)
            build.command("MARCO-MUS/marco.py", "-v", "-b", "MUSes", gcnfFile.getAbsolutePath());
        else
            build.command("MARCO-MUS/marco.py", "-v", "-b", "MUSes", "-s", "-T", Long.toString(timeout), gcnfFile.getAbsolutePath());
        Process proc;
        try {
            proc = build.start();
        } catch (IOException e) {
            throw new RuntimeException("MARCO process ended with an error\n" + e);
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
        String line;
        try {
            while ((line = in.readLine()) != null) {
                if (line.startsWith("U")) {
                    gmuses.add(line.split("U ")[1]);
                } else {
                    System.err.println(line);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("MARCO process wrote an incorrect line\n" + e);
        }
        proc.destroy();

        return gmuses;
    }
}
