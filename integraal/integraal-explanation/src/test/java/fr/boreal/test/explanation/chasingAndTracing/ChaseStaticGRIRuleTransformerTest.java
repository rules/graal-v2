package fr.boreal.test.explanation.chasingAndTracing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.boreal.test.explanation.TestData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.boreal.explanation.ruleFactories.StaticGRIRuleTransformer;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.KnowledgeBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.KnowledgeBaseImpl;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;

public class ChaseStaticGRIRuleTransformerTest {

    private static final FOQueryEvaluator<FOFormula> evaluator = GenericFOQueryEvaluator.defaultInstance();
    private final StaticGRIRuleTransformer factory = new StaticGRIRuleTransformer();
    public static PredicateFactory predicateFactory = SameObjectPredicateFactory.instance();
    public static TermFactory termFactory = SameObjectTermFactory.instance();
    public static FOFormulaFactory formulaFactory = FOFormulaFactory.instance();
    @BeforeEach
    public void setUp() {
        // to add if needed
    }
    @Test
    public void ruleTransformerFactoryTestMultiheadedExistential1() {

        FactBase factBase = new SimpleInMemoryGraphStore(Collections.singleton(TestData.pa));
        RuleBase ruleBase = new RuleBaseImpl(List.of(TestData.r3));
        KnowledgeBase kb = new KnowledgeBaseImpl(factBase, ruleBase);

        KnowledgeBase transformedKB = new KnowledgeBaseImpl(factBase, factory.createTransformedRB(kb));
        TestData.chase(transformedKB);

        Predicate edge_r3 = factory.getOrGenerateEdgePredicateFromRule(TestData.r3);
        var fpa = factory.createFnTermIdentifier(TestData.pa);
        var fqa = factory.createFnTermIdentifier(TestData.qa);
        var ftay = factory.createFnTermIdentifier(TestData.tay);
        Atom edge_paqa = new AtomImpl(edge_r3, List.of(fpa, fqa));
        Atom edge_patay = new AtomImpl(edge_r3, List.of(fpa, ftay));


        Assertions.assertTrue(factBase.contains(edge_paqa),
                String.format("Edge missing: %s %n Fact base: %s", edge_paqa, factBase));

        var res = factBase.match(edge_patay);
        var atoms= new ArrayList<Atom>();
        while(res.hasNext()) {
        	atoms.add(res.next());
        }
        
        Assertions.assertTrue(TestData.isIsomorphic(atoms.getFirst(), edge_patay),
                String.format("Edge missing: %s %n Fact base: %s", edge_patay, factBase));
    }

    @Test
    public void ruleTransformerFactoryTestExistential2() {

        FactBase factBase = new SimpleInMemoryGraphStore(Collections.singleton(TestData.pa));
        RuleBase ruleBase = new RuleBaseImpl(List.of(TestData.r3, TestData.r4));
        KnowledgeBase kb = new KnowledgeBaseImpl(factBase, ruleBase);

        KnowledgeBase transformedKB = new KnowledgeBaseImpl(factBase, factory.createTransformedRB(kb));
        TestData.chase(transformedKB);

        Predicate edge_r3 = factory.getOrGenerateEdgePredicateFromRule(TestData.r3);
        Predicate edge_r4 = factory.getOrGenerateEdgePredicateFromRule(TestData.r4);

        var fpa = factory.createFnTermIdentifier(TestData.pa);
        var fqa = factory.createFnTermIdentifier(TestData.qa);
        var ftay = factory.createFnTermIdentifier(TestData.tay);

        Atom edge_paqa = new AtomImpl(edge_r3, List.of(fpa, fqa));
        Atom edge_patay = new AtomImpl(edge_r3, List.of(fpa, ftay));
        Atom edge_qatay = new AtomImpl(edge_r4, List.of(fqa, ftay));


        Assertions.assertTrue(factBase.contains(edge_paqa), String.format("Edge missing %s %n %s", edge_paqa, factBase));
        Assertions.assertTrue(TestData.isIsomorphic(factBase.match(edge_patay).next(), edge_patay),
                String.format("Edge missing: %s %n Fact base: %s", edge_patay, factBase));
        Assertions.assertTrue(TestData.isIsomorphic(factBase.match(edge_qatay).next(), edge_qatay),
                String.format("Edge missing: %s %n Fact base: %s", edge_qatay, factBase));
    }

    @Test
    public void ruleTransformerFactoryTestMultiheaded() {

        FactBase factBase = new SimpleInMemoryGraphStore(Collections.singleton(TestData.pa));
        RuleBase ruleBase = new RuleBaseImpl(List.of(TestData.r2));
        KnowledgeBase kb = new KnowledgeBaseImpl(factBase, ruleBase);

        KnowledgeBase transformedKB = new KnowledgeBaseImpl(factBase, factory.createTransformedRB(kb));
        TestData.chase(transformedKB);

        Predicate edge_r2 = factory.getOrGenerateEdgePredicateFromRule(TestData.r2);
        var fpa = factory.createFnTermIdentifier(TestData.pa);
        var fqa = factory.createFnTermIdentifier(TestData.qa);
        var fta = factory.createFnTermIdentifier(TestData.ta);

        Atom edge_paqa = new AtomImpl(edge_r2, List.of(fpa, fqa));
        Atom edge_pata = new AtomImpl(edge_r2, List.of(fpa, fta));

        Assertions.assertTrue(factBase.contains(edge_paqa),
                String.format("Edge missing: %s %n Fact base: %s", edge_paqa, factBase));

        Assertions.assertTrue(factBase.contains(edge_pata),
                String.format("Edge missing: %s %n Fact base: %s", edge_pata, factBase));
    }
}
