package fr.boreal.test.explanation.chasingAndTracing;

import fr.boreal.explanation.GRI.dynamicProcessing.RELTracer;
import fr.boreal.explanation.GRI.staticProcessing.GRIBuilder;
import fr.boreal.explanation.ruleFactories.StaticGRIRuleTransformer;
import fr.boreal.forward_chaining.api.ForwardChainingAlgorithm;
import fr.boreal.forward_chaining.chase.ChaseBuilder;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.KnowledgeBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.KnowledgeBaseImpl;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;
import fr.boreal.test.explanation.TestData;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

public class RELTracerTest {
    private static final FOQueryEvaluator<FOFormula> evaluator = GenericFOQueryEvaluator.defaultInstance();
    private final StaticGRIRuleTransformer ruleTransformerFactory = new StaticGRIRuleTransformer();
    public static PredicateFactory predicateFactory = SameObjectPredicateFactory.instance();
    public static TermFactory termFactory = SameObjectTermFactory.instance();
    public static FOFormulaFactory formulaFactory = FOFormulaFactory.instance();
    @BeforeEach
    public void setUp() {
        // to do
    }
    public static void chase(KnowledgeBase kb) {
        ForwardChainingAlgorithm chase = ChaseBuilder.defaultBuilder(kb.getFactBase(), kb.getRuleBase())
                .useObliviousChecker()
                .useNaiveComputer()
                .useNaiveRuleScheduler()
                .build().get();
        chase.execute();
    }
    @Test
    public void RELTracerTestMultiheadedExistential1() {
        FactBase factBase = new SimpleInMemoryGraphStore(Collections.singleton(TestData.pa));
        RuleBase ruleBase = new RuleBaseImpl(List.of(TestData.r3));
        KnowledgeBase kb = new KnowledgeBaseImpl(factBase, ruleBase);

        KnowledgeBase transformedKB = new KnowledgeBaseImpl(factBase, ruleTransformerFactory.createTransformedRB(kb));
        chase(transformedKB);

        RELTracer tracer = new RELTracer();
        FactBase relAtoms = tracer.computeQueryRelevant(transformedKB, ruleTransformerFactory, TestData.qa);

        var fpa = ruleTransformerFactory.createFnTermIdentifier(TestData.pa);
        var fqa = ruleTransformerFactory.createFnTermIdentifier(TestData.qa);
        var ftay = ruleTransformerFactory.createFnTermIdentifier(TestData.tay);

        Atom RELpa = new AtomImpl(ruleTransformerFactory.REL, fpa);
        Atom RELtay = new AtomImpl(ruleTransformerFactory.REL, ftay);

        Atom RELedgepaqa = new AtomImpl(
                ruleTransformerFactory.getOrGenerateRelEdgePredicateFromRule(TestData.r3),
                List.of(fpa,fqa));

        Atom RELedgepatay = new AtomImpl(
                ruleTransformerFactory.getOrGenerateRelEdgePredicateFromRule(TestData.r3),
                List.of(fpa,ftay));

        Assertions.assertTrue(relAtoms.contains(RELpa),
                String.format("Relevant atom missing: %s %n Rel Atoms: %s", RELpa, factBase));

        Assertions.assertTrue(relAtoms.contains(RELedgepaqa),
                String.format("Relevant edge missing: %s %n Rel Atoms: %s", RELedgepaqa, factBase));

        Assertions.assertTrue(!relAtoms.contains(RELtay),
                String.format("Irrelevant atom marked as relevant: %s %n Rel Atoms: %s", RELtay, factBase));

        Assertions.assertTrue(!relAtoms.contains(RELedgepatay),
                String.format("Irrelevant edge marked as relevant: %s %n Rel Atoms: %s", RELedgepatay, factBase));

    }

    @Test
    public void RELTracerTestMultiheaded() {

        FactBase factBase = new SimpleInMemoryGraphStore(List.of(TestData.qa, TestData.ta));
        RuleBase ruleBase = new RuleBaseImpl(List.of(TestData.r5));
        KnowledgeBase kb = new KnowledgeBaseImpl(factBase, ruleBase);

        KnowledgeBase transformedKB = new KnowledgeBaseImpl(factBase, ruleTransformerFactory.createTransformedRB(kb));
        chase(transformedKB);

        RELTracer tracer = new RELTracer();
        FactBase relAtoms = tracer.computeQueryRelevant(transformedKB, ruleTransformerFactory, TestData.pa);

        var fpa = ruleTransformerFactory.createFnTermIdentifier(TestData.pa);
        var fqa = ruleTransformerFactory.createFnTermIdentifier(TestData.qa);
        var fta = ruleTransformerFactory.createFnTermIdentifier(TestData.ta);

        Atom RELqa = new AtomImpl(ruleTransformerFactory.REL, fqa);
        Atom RELta = new AtomImpl(ruleTransformerFactory.REL, fta);

        Atom RELedgeqapa = new AtomImpl(
                ruleTransformerFactory.getOrGenerateRelEdgePredicateFromRule(TestData.r5),
                List.of(fqa, fta, fpa));

        Atom RELedgeqata = new AtomImpl(
                ruleTransformerFactory.getOrGenerateRelEdgePredicateFromRule(TestData.r5),
                List.of(fqa, fpa, fta));

        Assertions.assertTrue(relAtoms.contains(RELqa),
                String.format("Relevant atom missing: %s %n Rel Atoms: %s", RELqa, transformedKB));

        Assertions.assertTrue(relAtoms.contains(RELta),
                String.format("Relevant atom missing: %s %n Rel Atoms: %s", RELta, factBase));

        Assertions.assertTrue(relAtoms.contains(RELedgeqapa),
                String.format("Relevant edge missing: %s %n Rel Atoms: %s", RELedgeqapa, factBase));

        Assertions.assertTrue(!relAtoms.contains(RELedgeqata),
                String.format("Irrelevant edge marked as relevant: %s %n Fact base: %s", RELedgeqata, factBase));

    }

    @Test
    public void RELTracerTest2() {
        Atom query = TestData.qa;

        FactBase factBase = new SimpleInMemoryGraphStore(List.of(TestData.saa, TestData.sbc, TestData.tab));
        RuleBase ruleBase = new RuleBaseImpl(List.of(TestData.r6, TestData.r7, TestData.r8));
        KnowledgeBase kb = new KnowledgeBaseImpl(factBase, ruleBase);

        Pair<KnowledgeBase, StaticGRIRuleTransformer> pair = GRIBuilder.buildGRI(kb);
        KnowledgeBase gri = pair.getLeft();
        StaticGRIRuleTransformer ruleTransformerFactory = pair.getRight();

        RELTracer tracer = new RELTracer();
        FactBase relAtoms = tracer.computeQueryRelevant(gri, ruleTransformerFactory, query);

        var fpb = ruleTransformerFactory.createFnTermIdentifier(TestData.pb);
        Atom RELpb = new AtomImpl(ruleTransformerFactory.REL, fpb);

        Assertions.assertTrue(relAtoms.contains(RELpb),
                String.format("Relevant atom missing: %s %n Rel Atoms: %s", RELpb, gri));

    }
}
