package fr.boreal.test.explanation.solvingViaGMUS;

import fr.boreal.explanation.GRI.dynamicProcessing.RELTracer;
import fr.boreal.explanation.solving_enumerating.KBtoGSATEncoder;
import fr.boreal.explanation.ruleFactories.StaticGRIRuleTransformer;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.KnowledgeBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.KnowledgeBaseImpl;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;
import fr.boreal.test.explanation.TestData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class GMUSTranslator {
    KBtoGSATEncoder gmusProcessor;
    fr.boreal.explanation.solving_enumerating.GMUSTranslator gmusTranslator;
    StaticGRIRuleTransformer staticGRIRuleTransformer;

    Predicate REL = staticGRIRuleTransformer.REL;

    @BeforeEach
    public void setUp() {
        this.staticGRIRuleTransformer = new StaticGRIRuleTransformer();
        gmusProcessor = new KBtoGSATEncoder(staticGRIRuleTransformer);
        gmusTranslator = new fr.boreal.explanation.solving_enumerating.GMUSTranslator();
    }


    @Test
    public void gmusToExplTest() {
        Atom query = TestData.qa;

        FactBase factBase = new SimpleInMemoryGraphStore(List.of(TestData.pa, TestData.ta));
        RuleBase ruleBase = new RuleBaseImpl(List.of(TestData.r1, TestData.r4));
        KnowledgeBase kb = new KnowledgeBaseImpl(factBase, ruleBase);

        KnowledgeBase transformedKB = new KnowledgeBaseImpl(factBase, staticGRIRuleTransformer.createTransformedRB(kb));
        TestData.chase(transformedKB);

        RELTracer tracer = new RELTracer();
        FactBase relAtoms = tracer.computeQueryRelevant(transformedKB, staticGRIRuleTransformer, TestData.qa);

        gmusProcessor.assignDefaultGroupNumberAndCreateClauseForStartQuery(query);
        gmusProcessor.assignGroupNumbersAndComputeClausesForRELEdges(relAtoms,query);

        Term queryFnTermIdentifier = staticGRIRuleTransformer.createFnTermIdentifier(query);
        var fpa = staticGRIRuleTransformer.createFnTermIdentifier(TestData.pa);
        var fta = staticGRIRuleTransformer.createFnTermIdentifier(TestData.ta);

        String gmus = "0 1 2";

        KnowledgeBase explanation = gmusTranslator.gmusToExpl(gmus, gmusProcessor.factIDMap, gmusProcessor.ruleIDMap);
        Assertions.assertTrue(explanation.getFactBase().contains(TestData.pa));
        Assertions.assertTrue(explanation.getRuleBase().getRules().contains(TestData.r1));
        Assertions.assertTrue(!explanation.getRuleBase().getRules().contains(TestData.r4));

    }
}
