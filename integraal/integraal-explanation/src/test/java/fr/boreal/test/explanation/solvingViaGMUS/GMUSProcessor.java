package fr.boreal.test.explanation.solvingViaGMUS;

import fr.boreal.explanation.GRI.dynamicProcessing.RELTracer;
import fr.boreal.explanation.solving_enumerating.KBtoGSATEncoder;
import fr.boreal.explanation.ruleFactories.StaticGRIRuleTransformer;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.KnowledgeBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.KnowledgeBaseImpl;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;
import fr.boreal.test.explanation.TestData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class GMUSProcessor {
    KBtoGSATEncoder GMUSProcessor;
    StaticGRIRuleTransformer staticGRIRuleTransformer;

    Predicate REL = staticGRIRuleTransformer.REL;

    @BeforeEach
    public void setUp() {
        staticGRIRuleTransformer = new StaticGRIRuleTransformer();
        GMUSProcessor = new KBtoGSATEncoder(staticGRIRuleTransformer);
    }
    @Test
    public void startQueryTest() {
        Atom query = TestData.qa;
        GMUSProcessor.assignDefaultGroupNumberAndCreateClauseForStartQuery(query);

        Term queryFnTermIdentifier = staticGRIRuleTransformer.createFnTermIdentifier(query);

        Assertions.assertEquals(1, GMUSProcessor.propVarIDMap.get(queryFnTermIdentifier),
                "Incorrect propositional variable ID for query.");
        Assertions.assertTrue(GMUSProcessor.clauses.contains(List.of(0,-1)));
    }

    @Test
    public void labelRELAtomsTest() {
        Atom query = TestData.qa;

        FactBase factBase = new SimpleInMemoryGraphStore(List.of(TestData.pa, TestData.ta));
        RuleBase ruleBase = new RuleBaseImpl(List.of(TestData.r1));
        KnowledgeBase kb = new KnowledgeBaseImpl(factBase, ruleBase);

        KnowledgeBase transformedKB = new KnowledgeBaseImpl(factBase, staticGRIRuleTransformer.createTransformedRB(kb));
        TestData.chase(transformedKB);

        RELTracer tracer = new RELTracer();
        FactBase relAtoms = tracer.computeQueryRelevant(transformedKB, staticGRIRuleTransformer, TestData.qa);

        GMUSProcessor.assignDefaultGroupNumberAndCreateClauseForStartQuery(query);
        GMUSProcessor.assignGroupNumbersAndCreateClauseForEachSupRelAtom(relAtoms, query);


        Term queryFnTermIdentifier = staticGRIRuleTransformer.createFnTermIdentifier(query);
        var fpa = staticGRIRuleTransformer.createFnTermIdentifier(TestData.pa);
        var fta = staticGRIRuleTransformer.createFnTermIdentifier(TestData.ta);

        Assertions.assertEquals(1, GMUSProcessor.propVarIDMap.get(queryFnTermIdentifier),
                "Incorrect propositional variable ID for query.");
        Assertions.assertNotNull(GMUSProcessor.propVarIDMap.get(fpa));
        Assertions.assertTrue(!GMUSProcessor.propVarIDMap.containsKey(fta));
        Assertions.assertEquals(List.of(List.of(0,-1),List.of(1, GMUSProcessor.propVarIDMap.get(fpa))), GMUSProcessor.clauses);
    }

    @Test
    public void labelRELEdgeAtomsTest() {
        Atom query = TestData.qa;

        FactBase factBase = new SimpleInMemoryGraphStore(List.of(TestData.pa, TestData.ta));
        RuleBase ruleBase = new RuleBaseImpl(List.of(TestData.r1));
        KnowledgeBase kb = new KnowledgeBaseImpl(factBase, ruleBase);

        KnowledgeBase transformedKB = new KnowledgeBaseImpl(factBase, staticGRIRuleTransformer.createTransformedRB(kb));
        TestData.chase(transformedKB);

        RELTracer tracer = new RELTracer();
        FactBase relAtoms = tracer.computeQueryRelevant(transformedKB, staticGRIRuleTransformer, TestData.qa);

        GMUSProcessor.assignDefaultGroupNumberAndCreateClauseForStartQuery(query);
        // GMUSProcessor.assignGroupNumbersAndCreateClauseForEachSupRelAtom(relAtoms, query);
        GMUSProcessor.assignGroupNumbersAndComputeClausesForRELEdges(relAtoms,query);


        Term queryFnTermIdentifier = staticGRIRuleTransformer.createFnTermIdentifier(query);
        var fpa = staticGRIRuleTransformer.createFnTermIdentifier(TestData.pa);
        var fta = staticGRIRuleTransformer.createFnTermIdentifier(TestData.ta);

        Assertions.assertEquals(List.of(List.of(0,-1),List.of(1,2), List.of(2, -2, 1)), GMUSProcessor.clauses);
    }
}
