package fr.boreal.redundancy;

import fr.boreal.backward_chaining.pure.PureRewriter;
import fr.boreal.model.formula.FOFormulas;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.query.impl.UnionFOQuery;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.storage.builder.StorageBuilder;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Static methods to compute redundancy properties between rules
 */
public class Redundancy {

	private static final TermFactory termFactory = SameObjectTermFactory.instance();
	private static final FOQueryFactory queryFactory = FOQueryFactory.instance();
	private static final FOQueryEvaluator<FOFormula> evaluator = GenericFOQueryEvaluator.defaultInstance();

	/**
	 * Tests if two rules are equals up to variable renaming
	 * <br/>
	 * Each rule is seen as a simple conjunction implies conjunction.
	 * 
	 * @param r1 the first rule
	 * @param r2 the second rule
	 * @return true iff r1 and r2 are equals up to variable renaming
	 */
	public static boolean isRuleIsomorphism(FORule r1, FORule r2) {
		FactBase r1HeadAtoms = StorageBuilder.defaultStorage();
		r1HeadAtoms.addAll(r1.getHead().asAtomSet());
		FOQuery<? extends FOFormula> r2HeadQuery = queryFactory.createOrGetQuery(r2.getHead(), null, null);

		Iterator<Substitution> headHomomorphisms = evaluator.homomorphism(r2HeadQuery, r1HeadAtoms);
		while(headHomomorphisms.hasNext()) {
			Substitution hHead = headHomomorphisms.next();
			if(isIsomorphism(hHead, r2.getHead(), r1.getHead())) {
				FactBase r1BodyAtoms = StorageBuilder.defaultStorage();
				r1BodyAtoms.addAll(r1.getBody().asAtomSet());
				FOQuery<? extends FOFormula> r2BodyQuery = queryFactory.createOrGetQuery(r2.getBody(), null);

				Iterator<Substitution> bodyHomomorphisms = evaluator.homomorphism(r2BodyQuery, r1BodyAtoms, hHead);
				while(bodyHomomorphisms.hasNext()) {
					Substitution hBody = bodyHomomorphisms.next();
					if(isIsomorphism(hBody, r2.getBody(), r1.getBody())) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * 
	 * @param h a substitution
	 * @param f1 a formula
	 * @param f2 a formula
	 * @return true iff h(f1) == f2
	 */
	private static boolean isIsomorphism(Substitution h, FOFormula f1, FOFormula f2) {
		return isBijection(h) && FOFormulas.createImageWith(f1, h).equals(f2);
	}

	/**
	 * Returns true iff the given substitution is a bijection between two variables set
	 *
	 * @param substitution the substitution to test
	 * @return true iff the given substitution is a bijection between two variables set
	 **/
	private static boolean isBijection(Substitution substitution) {
		Collection<Variable> vars = substitution.keys();
		Substitution inverse = new SubstitutionImpl();
		for(Variable v : vars) {
			Term t = substitution.createImageOf(v);
			if(t.isFrozen(inverse)) {
				return false;
			}
			inverse.add((Variable)t, v);
		}
		return true;
	}

	/**
	 * Uses backward chaining to determine whether a FORule is a logical consequence of a RuleBase
	 *
	 * @param rb a RuleBase
	 * @param r a FORule
	 * @return true iff the rule can be deducted from the RuleBase
	 */
	public static boolean ruleConsequence(RuleBase rb, FORule r) {
		Substitution s = variablesFreezeSubstitution(r.getFrontier());
		FactBase fb = StorageBuilder.defaultStorage();
		fb.addAll(FOFormulas.createImageWith(r.getBody(), s).asAtomSet());
		FOQuery<?> q = queryFactory.createOrGetQuery(FOFormulas.createImageWith(r.getHead(), s), List.of(), null);

		PureRewriter rewriter = new PureRewriter();
		UnionFOQuery ucq = rewriter.rewrite(q, rb);
		return ucq.getQueries().stream().anyMatch(q1 -> evaluator.existHomomorphism(q1, fb));
	}

	/**
	 * Create a mapping to freeze some variables i.e. mapping each variable to a new constant
	 * <br/>
	 * May be used to freeze some variables in a formula whose terms has been created with termFactory
	 * @param variables a set of variables
	 * @return a substitution that maps each variable from variables to a new constant according to termFactory
	 */
	private static Substitution variablesFreezeSubstitution(Collection<Variable> variables) {
		Substitution s = new SubstitutionImpl();
		variables.forEach(v -> s.add(v, termFactory.createOrGetConstant(termFactory.createOrGetFreshVariable().label())));
		return s;
	}

	/**
	 * test whether two FORule are semantically equivalent
	 * @param r1 the first rule
	 * @param r2 the second rule
	 * @return true iff r1 and r2 have the same meaning
	 */
	public static boolean equivalentRules (FORule r1, FORule r2) {
		return ruleConsequence(new RuleBaseImpl(List.of(r1)), r2) &&
				ruleConsequence(new RuleBaseImpl(List.of(r2)), r1);
	}




}
