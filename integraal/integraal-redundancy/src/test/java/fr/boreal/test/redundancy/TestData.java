package fr.boreal.test.redundancy;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;

public class TestData {

	// Predicates;
	public static final Predicate q1 = SameObjectPredicateFactory.instance().createOrGetPredicate("q1", 1);
	public static final Predicate p2 = SameObjectPredicateFactory.instance().createOrGetPredicate("p2", 2);
	public static final Predicate q2 = SameObjectPredicateFactory.instance().createOrGetPredicate("q2", 2);
	public static final Predicate r2 = SameObjectPredicateFactory.instance().createOrGetPredicate("r2", 2);

	// Terms
	public static final Variable x = SameObjectTermFactory.instance().createOrGetVariable("x");
	public static final Variable y = SameObjectTermFactory.instance().createOrGetVariable("y");
	public static final Variable z = SameObjectTermFactory.instance().createOrGetVariable("z");
	public static final Variable fa = SameObjectTermFactory.instance().createOrGetVariable("fa");

	public static final Constant a = SameObjectTermFactory.instance().createOrGetConstant("a");
	public static final Constant b = SameObjectTermFactory.instance().createOrGetConstant("b");

	// Atoms

	public static final Atom qx = new AtomImpl(q1, x);
	public static final Atom qy = new AtomImpl(q1, y);

	public static final Atom pxy = new AtomImpl(p2, x, y);
	public static final Atom pxz = new AtomImpl(p2, x, z);
	public static final Atom pyx = new AtomImpl(p2, y, x);
	public static final Atom qxz = new AtomImpl(q2, x, z);
	public static final Atom rxz = new AtomImpl(r2, x, z);
	public static final Atom qax = new AtomImpl(q2, a, x);

	public static final Atom qbx = new AtomImpl(q2, b, x);
}
