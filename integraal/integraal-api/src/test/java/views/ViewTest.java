package views;

import java.util.Collection;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import fr.boreal.api.high_level_api.EndUserAPI;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.storage.builder.StorageBuilder;
import fr.boreal.views.FederatedFactBase;
import fr.boreal.views.builder.ViewBuilder;
import fr.boreal.views.datasource.AbstractViewWrapper;

@RunWith(Parameterized.class)
class ViewTest {

    // Predicates
    public static final Predicate cityZip = SameObjectPredicateFactory.instance().createOrGetPredicate("cityZip", 2);
    public static final Predicate concept = SameObjectPredicateFactory.instance().createOrGetPredicate("concept", 1);

    // Terms
    public static final Variable x = SameObjectTermFactory.instance().createOrGetVariable("X");
    public static final Variable y = SameObjectTermFactory.instance().createOrGetVariable("Y");

    // Atoms
    public static final Atom cityZipAtom = new AtomImpl(cityZip, x, y);
    public static final Atom conceptAtom = new AtomImpl(concept, x);

    public static final FOQuery<?> cityZipQuery = FOQueryFactory.instance().createOrGetQuery(cityZipAtom, null);
    public static final FOQuery<?> conceptQuery = FOQueryFactory.instance().createOrGetQuery(conceptAtom, null);

    @Parameterized.Parameters
    static Stream<Arguments> data() {
        return Stream.of(
                Arguments.of("./src/test/resources/wikidata_web_api.vd", cityZipQuery),
                Arguments.of("./src/test/resources/dbpedia_sparql_endpoint.vd", conceptQuery)
        );
    }

    @DisplayName("Test querying views from a view declaration file")
    @ParameterizedTest(name = "{index}: querying {1} on {0} should work ...")
    @MethodSource("data")
    public void dlgpViewSupportWithQueryEvaluation(String vdFilePath, FOQuery<?> query) throws ViewBuilder.ViewBuilderException {
        Collection<AbstractViewWrapper<String, ?>> wrappers = ViewBuilder.createFactBases(vdFilePath);
        FederatedFactBase fb = new FederatedFactBase(StorageBuilder.defaultStorage(), wrappers);
        var results = GenericFOQueryEvaluator.defaultInstance().evaluate(query, fb);
        int count = 0;
        while (results.hasNext()) {
            results.next();
            count++;
        }
        Assert.assertTrue(count>0);
    }

    @DisplayName("Test querying views from a view declaration file using API")
    @ParameterizedTest(name = "{index}: querying {1} on {0} should work ...")
    @MethodSource("data")
    public void dlgpViewSupportWithAPIQueryEvaluation(String vdFilePath, FOQuery<?> query) throws ViewBuilder.ViewBuilderException {
        Collection<AbstractViewWrapper<String, ?>> wrappers = ViewBuilder.createFactBases(vdFilePath);
        FederatedFactBase fb = new FederatedFactBase(StorageBuilder.defaultStorage(), wrappers);
        var results = EndUserAPI.evaluateOld(fb, query);
        int count = 0;
        while (results.hasNext()) {
            results.next();
            count++;
        }
        Assert.assertTrue(count>0);
    }


}
