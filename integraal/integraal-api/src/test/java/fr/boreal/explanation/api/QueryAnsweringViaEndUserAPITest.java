package fr.boreal.explanation.api;

import java.time.Duration;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runners.Parameterized.Parameters;

import fr.boreal.api.high_level_api.EndUserAPI;
import fr.boreal.configuration.keywords.InteGraalKeywords;
import fr.boreal.configuration.parameters.IGParameter;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.io.dlgp.ParserResult;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;

/**
 * Checks that rewritings are correctly computed when using the EndUserAPI
 */

public class QueryAnsweringViaEndUserAPITest {

	// Test 1: Test simple case
	private static final String query_and_data1 = """
			?(X,Z) :- s(X, Z).
			s(a,a).
			""";
	private static final boolean expected_answer1 = true;

	// Test 2: Test empty file
	private static final String query_and_data2 = "";
	private static final boolean expected_answer2 = false;

	// Test 3: Test with no rule
	private static final String query_and_data3 = """
			s(a,a).
			""";
	private static final boolean expected_answer3 = false;

	// Test 4 : Test simple case
		private static final String query_and_data4 = """
				?(X,Z) :- s(X, Z).
				s(b.b),s(a,a).
				""";
	@Parameters
	static Stream<Arguments> data() {
		return Stream.of(Arguments.of(query_and_data1, expected_answer1),
				Arguments.of(query_and_data2, expected_answer2), Arguments.of(query_and_data3, expected_answer3));
	}

	@DisplayName("Test query answering (data + queries) ")
	@ParameterizedTest(name = "{index}: QA {0} should give {1})")
	@MethodSource("data")
	public void testQATest(String dlgp, boolean expectedAnswer) {

		ParserResult parser = DlgpParser.parseDLGPString(dlgp);

		FactBase fb = new SimpleInMemoryGraphStore(parser.atoms());

		var computedAnswer = EndUserAPI.evaluateOld(fb, parser.queries());

		Assertions.assertEquals(expectedAnswer, computedAnswer.hasNext());

	}

	@DisplayName("Test query answering (data + queries) with IG parameters ")
	@ParameterizedTest(name = "{index}: QA {0} should give {1})")
	@MethodSource("data")
	public void testQATestNew(String dlgp, boolean expectedAnswer) {

		ParserResult parser = DlgpParser.parseDLGPString(dlgp);

		FactBase fb = new SimpleInMemoryGraphStore(parser.atoms());

		var paramTimeout = new IGParameter(InteGraalKeywords.TIMEOUT, Duration.parse("PT100s"));
		var paramImage = new IGParameter(InteGraalKeywords.IMAGES, InteGraalKeywords.Algorithms.Images.CONSTANT);

		var computedAnswer = EndUserAPI.evaluate(fb, parser.queries(), paramTimeout, paramImage);

		Assertions.assertEquals(expectedAnswer, computedAnswer.hasNext());

	}

	@DisplayName("Test query answering (data + queries) with filtering ")
	@ParameterizedTest(name = "{index}: QA {0} should give {1})")
	@MethodSource("data")
	public void testQATestNewFiltering(String dlgp, boolean expectedAnswer) {
		
		if(!expectedAnswer) {
			return;
		}
		
		ParserResult parser = DlgpParser.parseDLGPString(dlgp);

		FactBase fb = new SimpleInMemoryGraphStore(parser.atoms());

		var paramMaxAnswers = new IGParameter(InteGraalKeywords.MAX,0);
	
		var computedAnswerUnfiltered = EndUserAPI.evaluate(fb, parser.queries());
		var computedAnswerFiltered = EndUserAPI.evaluate(fb, parser.queries(), paramMaxAnswers);

		int filteredAns=0;
		int unfilteredAns=0;
		
		while(computedAnswerUnfiltered.hasNext()) {
			unfilteredAns++;
			computedAnswerUnfiltered.next();
		}
		
		while(computedAnswerFiltered.hasNext()) {
			filteredAns++;
			computedAnswerFiltered.next();
		}
		
		Assertions.assertTrue(unfilteredAns != filteredAns);

	}

}
