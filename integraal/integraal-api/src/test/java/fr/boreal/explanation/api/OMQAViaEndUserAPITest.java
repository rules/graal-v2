package fr.boreal.explanation.api;

import java.util.Iterator;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runners.Parameterized.Parameters;

import fr.boreal.api.high_level_api.EndUserAPI;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.io.dlgp.ParserResult;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;

/**
 * Checks that rewritings are correctly computed when using the EndUserAPI
 */
public class OMQAViaEndUserAPITest {

	// Test 1: Test simple case
	private static final String OMQA1 = """
			t(X, Z) :- s(X, Z).
			?() :- t(X, Z).
			s(a,a).
			""";
	private static final String expected_answer1 = "true";

	// Test 2: Test empty file
	private static final String rule_and_data2 = "";
	private static final String expected_answer2 = "";

	// Test 3: Test with no rule
	private static final String rule_and_data3 = """
			s(a,a).
			""";
	private static final String expected_answer3 = """
			s(a,a).
			""";

	// Test 4: Test with no rule_and_data
	private static final String rule_and_data4 = """
			?() :- t(X,Z).
			t(X, Z) :- s(X, Y), s(Y, Z).
			s(X, Z) :- t(X, Y), t(Y, Z).
			s(a,a).
			""";
	private static final String expected_answer4 = "true";


	@Parameters
	static Stream<Arguments> data() {
		return Stream.of(Arguments.of(OMQA1, expected_answer1), Arguments.of(rule_and_data2, expected_answer2),
				Arguments.of(rule_and_data3, expected_answer3), Arguments.of(rule_and_data4, expected_answer4));
	}

	@DisplayName("Test ontology mediated query answering (data + queries + rules) ")
	@ParameterizedTest(name = "{index}: OMQA {0} should give {1}")
	@MethodSource("data")
	public void testOMQATest(String dlgp, String expectedAnswer) {

		ParserResult parser = DlgpParser.parseDLGPString(dlgp);

		RuleBase rb = new RuleBaseImpl(parser.rules());

		FactBase fb = new SimpleInMemoryGraphStore(parser.atoms());

		Iterator<Substitution> computedAnswerSat = EndUserAPI.evaluateSatOld(fb, parser.queries(), rb);

		if (expectedAnswer.equals("true")) {
			Assertions.assertTrue(computedAnswerSat.hasNext());
		} else {
            Assertions.assertFalse(computedAnswerSat.hasNext());
		}

		var computedAnswerRew = EndUserAPI.evaluateRewOld(fb, parser.queries(), rb);

		if (expectedAnswer.equals("true")) {
			Assertions.assertTrue(computedAnswerRew.hasNext());
		} else {
            Assertions.assertFalse(computedAnswerRew.hasNext());
		}
		// test that rb has not changed
		// We should test isomorphism instead of double homomorphism
		Assertions.assertEquals(parser.rules(), rb.getRules());

	}
	
	@DisplayName("Test ontology mediated query answering (data + queries + rules) ")
	@ParameterizedTest(name = "{index}: OMQA {0} should give {1}")
	@MethodSource("data")
	public void testOMQANewTest(String dlgp, String expectedAnswer) {

		ParserResult parser = DlgpParser.parseDLGPString(dlgp);

		RuleBase rb = new RuleBaseImpl(parser.rules());

		FactBase fb = new SimpleInMemoryGraphStore(parser.atoms());

		Iterator<Substitution> computedAnswerSat = EndUserAPI.evaluateSatOld(fb, parser.queries(), rb);

		if (expectedAnswer.equals("true")) {
			Assertions.assertTrue(computedAnswerSat.hasNext());
		} else {
            Assertions.assertFalse(computedAnswerSat.hasNext());
		}

		var computedAnswerRew = EndUserAPI.evaluateRew(fb, parser.queries(), rb);

		if (expectedAnswer.equals("true")) {
			Assertions.assertTrue(computedAnswerRew.hasNext());
		} else {
            Assertions.assertFalse(computedAnswerRew.hasNext());
		}
		// test that rb has not changed
		// We should test isomorphism instead of double homomorphism
		Assertions.assertEquals(parser.rules(), rb.getRules());

	}

}
