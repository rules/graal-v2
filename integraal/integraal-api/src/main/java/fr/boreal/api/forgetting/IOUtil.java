package fr.boreal.api.forgetting;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import fr.boreal.io.api.ParseException;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.rule.api.FORule;

public class IOUtil {
	public static RuleBase readDlgpFile(TermFactory termfactory, PredicateFactory predicatefactory, String filepath) throws FileNotFoundException {
		File file = new File(filepath);
		DlgpParser dlgp_parseur = new DlgpParser(file);
		ArrayList<FORule> rules = new ArrayList<>();
		while (dlgp_parseur.hasNext()) {
			try {
				Object result = dlgp_parseur.next();
				if (result instanceof FORule) {
					rules.add((FORule)result);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		dlgp_parseur.close();
		return new RuleBaseImpl(rules);
	}
}
