package fr.boreal.api.high_level_api;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import fr.boreal.io.api.ParseException;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.io.dlgp.DlgpUtil;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.FactoryConstants;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.storage.builder.StorageBuilder;

/**
 * Represents an environment for a logic-based system,
 * containing a fact base, rule base, and a collection of queries.
 */
public class Environment {

	private final TermFactory termFactory = FactoryConstants.DEFAULT_TERM_FACTORY;
	private final PredicateFactory predicateFactory = FactoryConstants.DEFAULT_PREDICATE_FACTORY;
	private FactBase factbase;
	private RuleBase rulebase;
	private Collection<Query> queries;

	/**
	 * Constructs an empty environment with
	 * a simple storage system in memory as the fact base,
	 * an empty rule base,
	 * and an empty collection of queries.
	 */
	public Environment() {
		this(StorageBuilder.defaultStorage(), new RuleBaseImpl(), new HashSet<>());
	}

	/**
	 * Constructs an environment with the given fact base, rule base, and queries.
	 *
	 * @param fb      The fact base of the environment.
	 * @param rb      The rule base of the environment.
	 * @param queries The collection of queries of the environment.
	 */
	public Environment(FactBase fb, RuleBase rb, Collection<Query> queries) {
		this.factbase = fb;
		this.rulebase = rb;
		this.queries = queries;
	}

	/**
	 * Constructs an environment by parsing the provided DLGP input stream.
	 *
	 * @param dlgp An input stream containing DLGP representation of facts,
	 *             rules, and queries.
	 * @throws ParseException if an exception occur while parsing DLGP input data
	 */
	public Environment(InputStream dlgp) throws ParseException {
		this();

		Collection<Atom> atoms = new ArrayList<>();
		Collection<FORule> rules = new ArrayList<>();
		Collection<Query> queries = new ArrayList<>();

		try (DlgpParser dlgp_parser = new DlgpParser(dlgp)) {
			while (dlgp_parser.hasNext()) {
				Object result = dlgp_parser.next();
				if (result instanceof Atom) {
					atoms.add((Atom) result);
				} else if (result instanceof FORule) {
					rules.add((FORule) result);
				} else if (result instanceof FOQuery) {
					queries.add((Query) result);
				}
			}
		}

		this.factbase.addAll(atoms);
		this.rulebase = new RuleBaseImpl(rules);
		this.queries = queries;
	}

	/**
	 * Constructs an environment by parsing the provided DLGP string.
	 *
	 * @param dlgp A string containing DLGP representation of facts,
	 *             rules, and queries.
	 * @throws ParseException if an exception occur while parsing DLGP input data
	 */
	public Environment(String dlgp) throws ParseException {
		this(new ByteArrayInputStream(dlgp.getBytes()));
	}

	/**
	 * Returns the term factory of this environment.
	 *
	 * @return The term factory.
	 */
	public TermFactory getTermFactory() {
		return this.termFactory;
	}

	/**
	 * Returns the predicate factory of this environment.
	 *
	 * @return The predicate factory.
	 */
	public PredicateFactory getPredicateFactory() {
		return this.predicateFactory;
	}

	/**
	 * Returns the FactBase of this environment.
	 *
	 * @return The FactBase.
	 */
	public FactBase getFactBase() {
		return this.factbase;
	}

	/**
	 * Returns the RuleBase of this environment.
	 *
	 * @return The RuleBase.
	 */
	public RuleBase getRuleBase() {
		return this.rulebase;
	}

	/**
	 * Returns the queries of this environment.
	 *
	 * @return The queries.
	 */
	public Collection<Query> getQueries() {
		return queries;
	}

	/**
	 * Loads a factBase.
	 * @param facts The factBase.
	 */
	public void setFactBase(FactBase facts) {
		this.factbase = facts;
	}

	/**
	 * Loads a ruleBase.
	 * @param rules The ruleBase.
	 */
	public void setRuleBase(RuleBase rules) {
		this.rulebase = rules;
	}

	/**
	 * Loads a collection of queries.
	 * @param queries The FOQuery collection.
	 */
	public void setQueries(Collection<Query> queries) {
		this.queries = queries;
	}

	/**
	 * Loads a set of atoms from a string.
	 * @param facts The string containing the facts to be loaded.
	 * @throws ParseException if an exception occur while parsing DLGP input data
	 */
	public void setFactBase(String facts) throws ParseException {
		FactBase fb = StorageBuilder.defaultStorage();
		fb.addAll(DlgpUtil.parseFacts(facts));
		this.setFactBase(fb);
	}

	/**
	 * Loads a set of rules from a string.
	 * @param rules The string containing the rules to be loaded.
	 * @throws ParseException if an exception occur while parsing DLGP input data
	 */
	public void setRuleBase(String rules) throws ParseException {
		RuleBase rb = new RuleBaseImpl(DlgpUtil.parseRules(rules));
		this.setRuleBase(rb);
	}

	/**
	 * Loads a set of queries from a string.
	 * @param queries The string containing the queries to be loaded.
	 * @throws ParseException if an exception occur while parsing DLGP input data
	 */
	public void setQueries(String queries) throws ParseException {
		this.setQueries(DlgpUtil.parseQueries(queries));
	}

}
