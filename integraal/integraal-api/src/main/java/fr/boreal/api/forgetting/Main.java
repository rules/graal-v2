package fr.boreal.api.forgetting;

import static fr.boreal.redundancy.Redundancy.ruleConsequence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.BooleanUtils;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

import fr.boreal.api.high_level_api.EndUserAPI;
import fr.boreal.forgetting.Forgetting;
import fr.boreal.io.dlgp.DlgpWriter;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.rule.api.FORule;
import fr.lirmm.boreal.util.validator.rule.DatalogRuleValidator;

public class Main {

	private static RuleBase removeInterRulesRedundancy(Collection<FORule> ruleColl, Collection<FORule> toKeep){
		if(ruleColl == null) {
			return new RuleBaseImpl(List.of());
		}
		RuleBase rb1 = new RuleBaseImpl(ruleColl);
		List<FORule> compiled = new ArrayList<>();
		for(FORule r : ruleColl) {
			rb1.remove(r);
			if(toKeep != null && toKeep.contains(r) || !ruleConsequence(rb1, r)) {
				compiled.add(r);
			}
		}
		return new RuleBaseImpl(compiled);
	}

	private static void reply(String rulesPath, boolean useFastForget, boolean keepInsteadOfForget, Collection<String> predicates, String redundancyParam) throws Exception {


		TermFactory termfactory = SameObjectTermFactory.instance();
		PredicateFactory predicatefactory = SameObjectPredicateFactory.instance();

		RuleBase rb = IOUtil.readDlgpFile(termfactory, predicatefactory, rulesPath);
		if(!EndUserAPI.isFusOld(rb)){
			System.err.println("WARNING : I'm not sure that the given rule base is FUS so maybe I won't terminate.");
		}

		DatalogRuleValidator dv = new DatalogRuleValidator();
		if(!rb.getRules().stream().allMatch(dv::check)){
			System.err.println("WARNING : found some non-datalog rule(s) the result will probably not be what you expect");
		}

		DlgpWriter writer = new DlgpWriter();

		HashSet<String> preds = new HashSet<>(predicates);
		RuleBase output = preds.isEmpty() ?
				Forgetting.bodyUnfolding(rb) :
					useFastForget ?
							Forgetting.semiNaiveDatalogForget(rb, p -> preds.contains(p.label()) != keepInsteadOfForget) :
								Forgetting.naiveDatalogForget(rb, p -> preds.contains(p.label()) != keepInsteadOfForget);

		if (redundancyParam.equals(CommandParams.REDUN_ALL)){
			output = removeInterRulesRedundancy(output.getRules(), null);
		} else if (redundancyParam.equals (CommandParams.REDUN_ADDED)){
			output = removeInterRulesRedundancy(output.getRules(), rb.getRules());
		}
		writer.write(output);
		writer.close();
	}


	public static void globalValidation(CommandParams p) {
		if (Stream.of(p.close,! p.toKeep.isEmpty(),! p.toForget.isEmpty())
				.mapToInt(BooleanUtils::toInteger).sum() > 1){
			throw new ParameterException("Only one of '-close/-c'-keep'/-k' and '-datalogForget/-f' must be specified");
		}
		if (p.close && p.slowMethod){
			System.err.println("WARNING: specification of both '-slow/-noopt' and '-close' is redundant");
		}
		p.keepInsteadOfForget = ! p.toKeep.isEmpty();
		p.predicates = p.keepInsteadOfForget ? p.toKeep : p.toForget;
		p.predicates = p.predicates.stream().map(s -> s.split("\\W+")).flatMap(Arrays::stream)
				.collect(Collectors.toList());
		p.redundancy = p.redundancy.toLowerCase();


	}
	public static void main(String ... args) throws Exception {
		CommandParams params = new CommandParams();
		JCommander jc = JCommander.newBuilder().addObject(params).build();
		jc.setProgramName("<command> [rulebase DLGP file]");
		jc.setCaseSensitiveOptions(false);
		if (args.length == 0){
			jc.usage();
			return;
		}

		jc.parse(args);
		globalValidation(params);


		reply(params.rulesPath == null ? params.rulesPathMain: params.rulesPath,
				! params.slowMethod,params.keepInsteadOfForget, params.predicates
				, params.redundancy
				);


	}


}
