package fr.boreal.api.integraal_repl;

import java.io.PrintWriter;

import org.jline.reader.LineReader;

import fr.boreal.api.integraal_repl.IGRepl.PrintLevel;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;

/**
 * An object that gathers REPL commands relating to general use of the REPL tool.
 * 
 * @author Pierre Bisquert
 */
public class ToolCommands {
	/**
	 * Top-level command that just prints help.
	 */
	@Command(name = "",
			usageHelpAutoWidth = true,
			footer = {"", "Press Ctrl-D to exit."},
			subcommands = {VerboseCommand.class})
	static class REPLCommands implements Runnable {
		PrintWriter out;

		REPLCommands() {}

		public void setLineReader(LineReader reader){
			out = reader.getTerminal().writer();
		}

		public void run() {
			out.println(new CommandLine(this).getUsageMessage());
		}
	}
	
	/**
	 * Command class for the "verbose" command.
	 */
	@Command(mixinStandardHelpOptions = true, usageHelpAutoWidth = true, name = "verbose",
			description = "Makes the interactive tool talk more or less.") 
	static class VerboseCommand implements Runnable {
		
		@Parameters(paramLabel = "LEVEL", arity = "0..1")
		PrintLevel level;

		@Override
		public void run() {
			if (this.level == null)
				IGRepl.writeIfVerbose("Current verbosity level: " + IGRepl.verbosityLevel + "\n\n", PrintLevel.MINIMAL);
			else {
				switch (this.level) {
				case MINIMAL:
					IGRepl.writeIfVerbose("InteGraal will now be quiet.\n\n", PrintLevel.MINIMAL);
					break;
				case WARNING:
					IGRepl.writeIfVerbose("InteGraal will now show warnings.\n\n", PrintLevel.MINIMAL);
					break;
				case MAXIMAL:
					IGRepl.writeIfVerbose("InteGraal will now talk quite a bit.\n\n", PrintLevel.MINIMAL);
					break;
				}
				IGRepl.verbosityLevel = this.level;
			}
		}
	}
}
