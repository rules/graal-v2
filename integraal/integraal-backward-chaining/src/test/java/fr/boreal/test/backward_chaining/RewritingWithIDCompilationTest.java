package fr.boreal.test.backward_chaining;

import fr.boreal.backward_chaining.api.BackwardChainingAlgorithm;
import fr.boreal.backward_chaining.homomorphism.QueryHomomorphism;
import fr.boreal.backward_chaining.pure.PureRewriter;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.partition.TermPartition;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.query.impl.UnionFOQuery;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.impl.FORuleImpl;
import fr.boreal.model.ruleCompilation.api.RuleCompilation;
import fr.boreal.model.ruleCompilation.id.IDRuleCompilation;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

@RunWith(Parameterized.class)
class RewritingWithIDCompilationTest extends TestData {

	// Test 1
	// Check that piece unifiers are correctly computed and that the piece is not lost
	//
	// 	?(X) :- q(X, Y), p(y).
	// rewritten with
	// 	p(X) :- pp(X)
	// 	pp(X) :- ppp(X)
	// 	ppp(Y), q(X, Y) :- r(X)
	// should give
	// 	?(X) :- q(X, Y), p(y).
	// 	?(X) :- r(X).

	private static final FORule T1_R1 = new FORuleImpl(ppx, px);
	private static final FORule T1_R2 = new FORuleImpl(pppx, ppx);
	private static final FORule T1_R3 = new FORuleImpl(rx ,
			FOFormulaFactory.instance().createOrGetConjunction(pppy, qxy));

	private static final FOQuery<?> T1_Q = FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
			qxy, py), 
			List.of(x));
	private static final FOQuery<?> T1_Q2 = FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
			rx), 
			List.of(x));


	// Test 2
	// Check with unifiers aggregations on the same rule
	//
	// 	?(X, Y) :- t(X, Y).
	// rewritten with
	// 	t(X, Z) :- s(X, Y), s(Y, Z).
	// 	s(X, Z) :- q(X, Y), q(Y, Z).
	// should give
	// 	?(X, Y) :- t(X, Y).
	// 	?(X, Y) :- s(X, U), s(U, Y).
	// 	?(X, Y) :- q(X, V), q(V, U), s(U, Y).
	// 	?(X, Y) :- s(X, U), q(U, V), q(V, Y).
	// 	?(X, Y) :- q(X, V), q(V, U), q(U, W), q(W, Y).

	private static final FORule T2_R1 = new FORuleImpl(
			FOFormulaFactory.instance().createOrGetConjunction(sxy, syz), 
			FOFormulaFactory.instance().createOrGetConjunction(txz));
	private static final FORule T2_R2 = new FORuleImpl(
			FOFormulaFactory.instance().createOrGetConjunction(qxy, qyz),
			FOFormulaFactory.instance().createOrGetConjunction(sxz));

	private static final FOQuery<?> T2_Q = FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
			txy), 
			List.of(x, y));
	private static final FOQuery<?> T2_Q2 = FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
			sxu, suy), 
			List.of(x, y));
	private static final FOQuery<?> T2_Q3 = FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
			qxv, qvu, suy), 
			List.of(x, y));
	private static final FOQuery<?> T2_Q4 = FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
			sxu, quv, qvy), 
			List.of(x, y));
	private static final FOQuery<?> T2_Q5 = FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
			qxv, qvu, quw, qwy), 
			List.of(x, y));

	// Test 3
	// Check with non-atomic pieces and multiple steps on unifier aggregations from the same rule
	//
	// 	?(X, Y) :- t(X, Y), t(Y, X), q(X, Y).
	// rewritten with
	// 	t(X, Y) :- s(X, Y).
	// should give
	// 	?(X, Y) :- t(X, Y), t(Y, X), q(X, Y).

	private static final FORule T3_R1 = new FORuleImpl(sxy, txy);

	private static final FOQuery<?> T3_Q = FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
			txy, tyx, qxy), 
			List.of(x, y));

	// Test 4
	// Check with constants during rewriting and query homomorphism
	// Also checks for boolean queries
	//
	// 	?() :- p(a, X), p(X, b).
	// rewritten with
	// 	p(X, Y) :- q(X, Y).
	// should give
	// 	?() :- p(a, X), p(X, b)

	private static final FORule T4_R1 = new FORuleImpl(qxy, pxy);

	private static final FOQuery<?> T4_Q = FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
			pax, pxb), 
			List.of());

	// Test 5
	// Check with constants in the rules during rewriting
	//
	// 	?(X) :- p(X, Y), p(X, Z).
	// rewritten with
	// 	p(b, X) :- q(b).
	// 	p(a, X) :- q(a).
	// should give
	// 	?(X) :- p(X, Y), p(X, Z).
	// 	?(X) :- q(b) \{X:b}.
	// 	?(X) :- q(a) \{X:a}.

	private static final FORule T5_R1 = new FORuleImpl(qb, pbx);
	private static final FORule T5_R2 = new FORuleImpl(qa, pax);

	private static final FOQuery<?> T5_Q = FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
			pxy, pxz), 
			List.of(x));
	private static final FOQuery<?> T5_Q2 = FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
			qb), 
			List.of(x),
			new TermPartition(Set.of(Set.of(x, b))));
	private static final FOQuery<?> T5_Q3 = FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
			qa), 
			List.of(x),
			new TermPartition(Set.of(Set.of(x, a))));

	// Test 6
	// Check aggregation and prunability of the operator
	//
	// 	?() :- r(U, V, W), r(W, Z, U).
	// rewritten with
	// 	r(X, Y, X) :- p(X, Y).
	// should give
	// 	?() :- r(U, V, W), r(W, Z, U).

	private static final FORule T6_R1 = new FORuleImpl(pxy, rxyx);

	private static final FOQuery<?> T6_Q = FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
			ruvw, rwzu), 
			List.of());

	// Test 7
	// Check query homomorphism with multiple occurrences of a single answer variable
	//
	// 	?(X, Y) :- p(X, Y).
	// rewritten with
	// 	p(X, Y) :- q(X, Y).
	//  p(X, X) :- q(X, Y).
	// should give
	//  ?(X, Y) :- p(X, Y).

	private static final FORule T7_R1 = new FORuleImpl(qxy, pxy);
	private static final FORule T7_R2 = new FORuleImpl(qxy, pxx);

	private static final FOQuery<?> T7_Q = FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
			pxy), 
			List.of(x, y));

	@Parameters
	static Stream<Arguments> data() {
		return Stream.of(
				Arguments.of(T1_Q, Set.of(T1_R1, T1_R2, T1_R3), Set.of(T1_Q, T1_Q2)),
				Arguments.of(T2_Q, Set.of(T2_R1, T2_R2), Set.of(T2_Q, T2_Q2, T2_Q3, T2_Q4, T2_Q5)),
				Arguments.of(T3_Q, Set.of(T3_R1), Set.of(T3_Q)),
				Arguments.of(T4_Q, Set.of(T4_R1), Set.of(T4_Q)),
				Arguments.of(T5_Q, Set.of(T5_R1, T5_R2), Set.of(T5_Q, T5_Q2, T5_Q3)),
				Arguments.of(T6_Q, Set.of(T6_R1), Set.of(T6_Q)),
				Arguments.of(T7_Q, Set.of(T7_R1, T7_R2), Set.of(T7_Q))
				);
	}

	@DisplayName("Test rewriting with ID compilation")
	@ParameterizedTest(name = "{index}: rewrite {0} with {1} should give {2})")
	@MethodSource("data")
	public void rewritingTestWithIDCompilation(FOQuery<?> query, Collection<FORule> rules,  Collection<FOQuery<?>> expected) {
		RuleBase rb = new RuleBaseImpl(new HashSet<>(rules));
		RuleCompilation compilation = new IDRuleCompilation();
		compilation.compile(rb);

		BackwardChainingAlgorithm algo = new PureRewriter(compilation);
		UnionFOQuery result = algo.rewrite(query, rb);

		Assert.assertEquals(expected.size(), result.getQueries().size());

		QueryHomomorphism h = new QueryHomomorphism();
		for(FOQuery<?> q : expected) {
			int count = 0;
			for(FOQuery<?> q2 : result.getQueries()) {
				boolean exist = h.exists(q, q2) && h.exists(q2, q);
				if(exist) {
					count++;
				}
			}
			Assert.assertEquals(1, count);
		}
	}

}

