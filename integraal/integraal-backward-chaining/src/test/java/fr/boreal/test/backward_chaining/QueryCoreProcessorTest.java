package fr.boreal.test.backward_chaining;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import fr.boreal.backward_chaining.core.QueryCoreProcessorImpl;
import fr.boreal.backward_chaining.core.QueryCoreProcessor;
import fr.boreal.backward_chaining.homomorphism.QueryHomomorphism;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;

@RunWith(Parameterized.class)
class QueryCoreProcessorTest extends TestData {

	public static final FOQuery<?> Q1 = FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
			pax, pyx, pyz, pvz, pvu, pbu, pax2, px2c, pbx2, pax1, px1c, pbx1), 
			List.of());
	public static final FOQuery<?> Q1Core = FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
			pax, pbx, pxc), 
			List.of());

	public static final FOQuery<?> Q2 = FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
			pfax, pyx, pyz, pvz, pvu, pfbu, pfax2, px2fc, pfbx2, pfax1, px1fc, pfbx1), 
			List.of(fa, fb, fc));
	public static final FOQuery<?> Q2Core = FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(
			pfax, pfbx, pxfc), 
			List.of(fa, fb, fc));

	//TODO: complete unit test

	@Parameters
	static Stream<Arguments> data() {
		return Stream.of(
				Arguments.of(Q1, Q1Core),
				Arguments.of(Q2, Q2Core)
				);
	}

	@DisplayName("Test query core")
	@ParameterizedTest(name = "{index}: core of {0} should be {1})")
	@MethodSource("data")
	public void rewritingTest(FOQuery<?> query, FOQuery<?> expected) {

		QueryHomomorphism h = new QueryHomomorphism();
		QueryCoreProcessor algo = new QueryCoreProcessorImpl();
		FOQuery<?> result = algo.computeCore(query);

		int expected_size = expected.getFormula().asAtomSet().size();
		int result_size = result.getFormula().asAtomSet().size();
		Assert.assertEquals(expected_size, result_size);

		boolean exist = h.exists(result, expected) && h.exists(expected, result);
		Assert.assertTrue(exist);
	}

}
