package fr.boreal.test.backward_chaining;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import fr.boreal.model.query.impl.UnionFOQuery;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import fr.boreal.backward_chaining.api.BackwardChainingAlgorithm;
import fr.boreal.backward_chaining.homomorphism.QueryHomomorphism;
import fr.boreal.backward_chaining.source_target.SourceTargetRewriter;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.impl.FORuleImpl;

@RunWith(Parameterized.class)
class OneStepRewritingTest extends TestData {

    // Test 1
    // 	?(X) :- q(X)
    // 	q(Y) :- p(Y)
    // should give
    // 	?(X) :- p(X).

    private static final FORule T1_R1 = new FORuleImpl(py, qy);
    private static final FOQuery<?> T1_Q = FOQueryFactory.instance().createOrGetQuery(
            qx,
            List.of(x));

    private static final FOQuery<?> T1_Q2 = FOQueryFactory.instance().createOrGetQuery(
            px,
            List.of(x));


    @Parameters
    public static Stream<Arguments> data() {
        return Stream.of(
                Arguments.of(T1_Q, Set.of(T1_R1), Set.of(T1_Q2))
        );
    }

    @DisplayName("Test one step rewriting")
    @ParameterizedTest(name = "{index}: rewrite in a single step {0} with {1} should give {2})")
    @MethodSource("data")
    public void oneStepRewritingTest(FOQuery<?> query, Collection<FORule> rules, Collection<FOQuery<?>> expected) {
        BackwardChainingAlgorithm algo = new SourceTargetRewriter();
        UnionFOQuery result = algo.rewrite(query, new RuleBaseImpl(rules));

        Assert.assertEquals(expected.size(), result.getQueries().size());

        QueryHomomorphism h = new QueryHomomorphism();
        for(FOQuery<?> q : expected) {
            int count = 0;
            for(FOQuery<?> q2 : result.getQueries()) {
                boolean exist = h.exists(q, q2) && h.exists(q2, q);
                if(exist) {
                    count++;
                }
            }
            Assert.assertEquals(1, count);
        }
    }


}
