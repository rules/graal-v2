package fr.boreal.test.backward_chaining;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import fr.boreal.backward_chaining.api.BackwardChainingAlgorithm;
import fr.boreal.backward_chaining.homomorphism.QueryHomomorphism;
import fr.boreal.backward_chaining.pure.PureRewriter;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.io.dlgp.ParserResult;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.query.impl.UnionFOQuery;
import fr.boreal.model.ruleCompilation.HierarchicalRuleCompilation;
import fr.boreal.model.ruleCompilation.api.RuleCompilation;
import fr.boreal.model.ruleCompilation.id.IDRuleCompilation;

@RunWith(Parameterized.class)
class FunctionsRewritingTest {

	private static final String baseFolder = "src/test/resources/";

	@Parameters
	static Stream<Arguments> rewritingData() {
		return Stream.of(Arguments.of(baseFolder + "functions_rewriting.dlgp",
				baseFolder + "functions_rewriting_expected.dlgp"));
	}

	@DisplayName("Test rewriting with functions")
	@ParameterizedTest(name = "{index}: Rewriting with functions should work")
	@MethodSource("rewritingData")
	public void rewritingWithFunctionsTest(String input, String expected) throws Exception {

		ParserResult inputObjects = DlgpParser.parseFile(input);

		Query preinputQuery = inputObjects.queries().iterator().next();

		if (!(preinputQuery instanceof FOQuery<?> inputQuery)) {
			throw new Exception("Expected FOQuery<?> type, but got : " + preinputQuery.getClass().toString());
		}

		RuleBase rb = new RuleBaseImpl(inputObjects.rules());

		BackwardChainingAlgorithm rewriter = new PureRewriter();
		UnionFOQuery rewritings = rewriter.rewrite(inputQuery, rb);

		ParserResult expectedObjects = DlgpParser.parseFile(expected);
		Collection<Query> preexpectedQueries = expectedObjects.queries();
		Collection<FOQuery<?>> expectedQueries = new ArrayList<>();

		for (Query q : preexpectedQueries) {
			if (!(q instanceof FOQuery<?> qq)) {
				throw new Exception("Expected FOQuery<?> type, but got : " + q.getClass().toString());
			}
			expectedQueries.add(qq);
		}

		QueryHomomorphism h = new QueryHomomorphism();
		for (FOQuery<?> q : expectedQueries) {
			int count = 0;
			for (FOQuery<?> q2 : rewritings.getQueries()) {
				boolean exist = h.exists(q, q2) && h.exists(q2, q);
				if (exist) {
					count++;
				}
			}
			Assert.assertEquals(1, count);
		}
	}

	@DisplayName("Test rewriting with functions and hierarchical compilation")
	@ParameterizedTest(name = "{index}: Hierarchical compilation with functions should work (but not compile functions)")
	@MethodSource("rewritingData")
	public void rewritingWithFunctionsAndHierarchicalCompilationTest(String input, String expected) throws Exception {

		ParserResult inputObjects = DlgpParser.parseFile(input);

		Query preinputQuery = inputObjects.queries().iterator().next();

		if (!(preinputQuery instanceof FOQuery<?> inputQuery)) {
			throw new Exception("Expected FOQuery<?> type, but got : " + preinputQuery.getClass().toString());
		}
		RuleBase rb = new RuleBaseImpl(inputObjects.rules());

		RuleCompilation compilation = new HierarchicalRuleCompilation();
		compilation.compile(rb);
		BackwardChainingAlgorithm rewriter = new PureRewriter(compilation);
		UnionFOQuery rewritings = rewriter.rewrite(inputQuery, rb);

		ParserResult expectedObjects = DlgpParser.parseFile(expected);
		Collection<Query> preexpectedQueries = expectedObjects.queries();
		Collection<FOQuery<?>> expectedQueries = new ArrayList<>();

		for (Query q : preexpectedQueries) {
			if (!(q instanceof FOQuery<?> qq)) {
				throw new Exception("Expected FOQuery<?> type, but got : " + q.getClass().toString());
			}
			expectedQueries.add(qq);
		}

		QueryHomomorphism h = new QueryHomomorphism(compilation);
		for (FOQuery<?> q : expectedQueries) {
			int count = 0;
			for (FOQuery<?> q2 : rewritings.getQueries()) {
				boolean exist = h.exists(q, q2) && h.exists(q2, q);
				if (exist) {
					count++;
				}
			}
			Assert.assertEquals(1, count);
		}
	}

	@DisplayName("Test rewriting with functions and ID compilation")
	@ParameterizedTest(name = "{index}: ID compilation with functions should work (but not compile functions)")
	@MethodSource("rewritingData")
	public void rewritingWithFunctionsAndIDCompilationTest(String input, String expected) throws Exception {

		ParserResult inputObjects = DlgpParser.parseFile(input);

		Query preinputQuery = inputObjects.queries().iterator().next();
		if (!(preinputQuery instanceof FOQuery<?> inputQuery)) {
			throw new Exception("Expected FOQuery<?> type, but got : " + preinputQuery.getClass().toString());
		}
		RuleBase rb = new RuleBaseImpl(inputObjects.rules());

		RuleCompilation compilation = new IDRuleCompilation();
		compilation.compile(rb);
		BackwardChainingAlgorithm rewriter = new PureRewriter(compilation);
		UnionFOQuery rewritings = rewriter.rewrite(inputQuery, rb);

		ParserResult expectedObjects = DlgpParser.parseFile(expected);
		Collection<Query> preexpectedQueries = expectedObjects.queries();
		Collection<FOQuery<?>> expectedQueries = new ArrayList<>();

		for (Query q : preexpectedQueries) {
			if (!(q instanceof FOQuery<?> qq)) {
				throw new Exception("Expected FOQuery<?> type, but got : " + q.getClass().toString());
			}
			expectedQueries.add(qq);
		}

		QueryHomomorphism h = new QueryHomomorphism(compilation);
		for (FOQuery<?> q : expectedQueries) {
			int count = 0;
			for (FOQuery<?> q2 : rewritings.getQueries()) {
				boolean exist = h.exists(q, q2) && h.exists(q2, q);
				if (exist) {
					count++;
				}
			}
			Assert.assertEquals(1, count);
		}
	}

}
