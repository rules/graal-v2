package fr.boreal.backward_chaining.pure.rewriting_operator;

import fr.boreal.backward_chaining.pure.PureRewriter;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.ruleCompilation.NoRuleCompilation;
import fr.boreal.model.ruleCompilation.api.RuleCompilation;
import fr.boreal.unifier.QueryUnifier;
import fr.boreal.unifier.QueryUnifierAlgorithm;
import fr.lirmm.boreal.util.Rules;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * From Melanie Konïg's thesis Rewriting operator SRA
 * <br/>
 * Query rewriting engine that rewrite query using aggregation by rule of most
 * general single piece-unifiers
 * From Melanie Konïg's thesis
 * Rewriting operator SRA
 * <br/>
 * Query rewriting engine that rewrite query using
 * aggregation by rule of most general single piece-unifiers
 */
public class SingleRuleAggregator implements RewritingOperator {

	private final RuleCompilation compilation;

	private final QueryUnifierAlgorithm unifier_algo;

	/**
	 * Creates a new SingleRuleAggregator using default parameters query unifier
	 * algorithm : QueryUnifierAlgorithm
	 */
	public SingleRuleAggregator() {
		this(new QueryUnifierAlgorithm());
	}

	/**
	 * Creates a new SingleRuleAggregator using default parameters query unifier
	 * algorithm : QueryUnifierAlgorithm
	 *
	 * @param compilation the compilation of the rules to use
	 */
	public SingleRuleAggregator(RuleCompilation compilation) {
		this(new QueryUnifierAlgorithm(compilation), compilation);
	}

	/**
	 * Creates a new SingleRuleAggregator using the given parameters
	 *
	 * @param unifier_algo the query unifier algorithm to use
	 */
	public SingleRuleAggregator(QueryUnifierAlgorithm unifier_algo) {
		this(unifier_algo, NoRuleCompilation.instance());
	}

	/**
	 * Creates a new SingleRuleAggregator using the given parameters
	 * <br/>
	 * The unifier algorithm must have been instantiated with the given compilation if it is not an empty compilation
	 * @param unifier_algo the query unifier algorithm to use
	 * @param compilation  the compilation of the rules to use
	 */
	public SingleRuleAggregator(QueryUnifierAlgorithm unifier_algo, RuleCompilation compilation) {
		this.unifier_algo = unifier_algo;
		this.compilation = compilation;
	}

	@Override
	public Set<FOQuery<?>> rewrite(FOQuery<?> query, RuleBase rb) {

		// Select the subset of the rules that are possible candidates
		// This is the rules which have at least one atom with a corresponding predicate to the query
		Set<FORule> candidates = new HashSet<>(rb.getRules().size());
		for(Atom a1 : query.getFormula().asAtomSet()) {
			for(Predicate p : this.compilation.getCompatiblePredicates(a1.getPredicate()))
				candidates.addAll(rb.getRulesByHeadPredicate(p));
		}

		// Compute all the unifiers of the query with all the candidates
		Set<QueryUnifier> unifiers = new HashSet<>();
		for (FORule r : candidates) {
			unifiers.addAll(this.getSingleRuleUnifiers(query, r));
		}

		// Rewrite the query with the unifiers
		Set<FOQuery<?>> rewritings = new HashSet<>(unifiers.size());
		for (QueryUnifier unifier : unifiers) {
			rewritings.add(unifier.apply(query));
		}
		return rewritings;
	}

	/**
	 * Computes all the possible unifiers between the given query and rule The
	 * possible unifiers are the most general single piece unifiers as well as all
	 * the possible aggregations between them
	 *
	 * @return all the possible unifiers between the given query and rule
	 */
	private Collection<QueryUnifier> getSingleRuleUnifiers(FOQuery<?> query, FORule r) {
		// Assure rule is on a fresh variable set
		r = Rules.freshRenaming(r);

		// Compute single piece unifiers
		Collection<QueryUnifier> all_unifiers = unifier_algo.getMostGeneralSinglePieceUnifiers(query, r);

		// We use a list to handle the unifiers that haven't been handled yet
		// this let us aggregate together aggregated unifiers or single piece unifiers
		// and aggregated ones
		// We use this list as a queue by only checking further than the current index
		// for aggregation because of it's associativity
		List<QueryUnifier> to_handle = new ArrayList<>(all_unifiers);
		int index = 0;

		while (index < to_handle.size()) {
			// We store the new unifiers here
			// and add them back in the end of the loop
			Collection<QueryUnifier> new_unifiers = new HashSet<>();

			// We select an unifier
			QueryUnifier u = to_handle.get(index);

			// Aggregate the selected unifier with all the other unifiers if they are
			// compatible
			// We only need to check the unifiers that are further in the list because of
			// the associativity of the aggregation
			for (int i = index + 1; i < to_handle.size(); ++i) {
				QueryUnifier u2 = to_handle.get(i);
				Optional<QueryUnifier> optAggregatedUnifier = u.safeAggregate(u2);
                optAggregatedUnifier.ifPresent(new_unifiers::add);
			}

			// Add the new unifiers to the set of all unifiers
			// and also to the queue of unifiers to handle
			for (QueryUnifier u2 : new_unifiers) {
				if (all_unifiers.add(u2)) {
					to_handle.add(u2);
				}
			}
			++index;
		}
		return all_unifiers;
	}

}
