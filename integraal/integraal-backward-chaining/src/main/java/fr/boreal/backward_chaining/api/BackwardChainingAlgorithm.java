package fr.boreal.backward_chaining.api;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.impl.UnionFOQuery;

import java.util.Set;

/**
 * A Backward Chaining algorithm rewrites a {@link FOQuery} according to the
 * given {@link RuleBase}.
 */
public interface BackwardChainingAlgorithm {

	/**
	 * Execute the algorithm
	 * 
	 * @param query the query to rewrite
	 * @param rules the rules with which to rewrite the query
	 * @return the union of all rewritings of the query with the rules
	 */
	default UnionFOQuery rewrite(FOQuery<? extends FOFormula> query, RuleBase rules) {
		return this.rewrite(new UnionFOQuery(Set.of(query)), rules);
	}

	/**
	 * Execute the algorithm
	 *
	 * @param queries the ucq to rewrite
	 * @param rules the rules with which to rewrite the query
	 * @return the union of all rewritings of the queries with the rules
	 */
	UnionFOQuery rewrite(UnionFOQuery queries, RuleBase rules);

}
