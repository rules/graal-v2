package fr.boreal.backward_chaining.source_target.dlx.data;

/**
 * Object
 */
public class DataObject {
	/**
	 * L
	 */
	public DataObject L;
	
	/**
	 * R
	 */
	public DataObject R;
	
	/**
	 * U
	 */
	public DataObject U;
	
	/**
	 * D
	 */
	public DataObject D;
	
	/**
	 * C
	 */
	public DataObject C;
}
