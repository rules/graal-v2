package fr.boreal.backward_chaining.cover;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import fr.boreal.backward_chaining.homomorphism.QueryHomomorphism;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.ruleCompilation.NoRuleCompilation;
import fr.boreal.model.ruleCompilation.api.RuleCompilation;

/**
 * Computes the cover of a set of queries using homomorphism to detect if a query is more general than another
 */
public class QueryCover implements CoverFunction {
	
	private final QueryHomomorphism queryHomomorphism;

	/**
	 * Default constructor.
	 * Uses no compilation
	 */
	public QueryCover() {
		this(NoRuleCompilation.instance());
	}

	/**
	 * Constructor with compilation
	 * @param compilation the compilation to use
	 */
	public QueryCover(RuleCompilation compilation) {
		this.queryHomomorphism = new QueryHomomorphism(compilation);
	}

	@Override
	public Set<FOQuery<? extends FOFormula>> cover(Set<FOQuery<? extends FOFormula>> queries) {
		Set<FOQuery<? extends FOFormula>> cover = new HashSet<>();
		for(FOQuery<? extends FOFormula> q1 : queries) {
			boolean toAdd = true;
			Iterator<FOQuery<? extends FOFormula>> coverIt = cover.iterator();
			while (coverIt.hasNext()) {
				FOQuery<? extends FOFormula> q2 = coverIt.next();
				if(this.isMoreGeneralThan(q2, q1)) {
					toAdd = false;
					break;
				}
				else if(this.isMoreGeneralThan(q1, q2)) {
					coverIt.remove();
				}
			}
			if(toAdd) {
				cover.add(q1);
			}
		}
		return cover;
	}

	/**
	 * @return true iff q1 subsumes q2
	 */
	private boolean isMoreGeneralThan(FOQuery<? extends FOFormula> q1, FOQuery<? extends FOFormula> q2) {
		return this.queryHomomorphism.exists(q1, q2);
	}

}
