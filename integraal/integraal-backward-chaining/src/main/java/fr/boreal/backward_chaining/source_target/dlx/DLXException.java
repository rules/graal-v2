package fr.boreal.backward_chaining.source_target.dlx;

import java.io.Serial;

/**
 * Exception during DLX execution
 *
 */
public class DLXException extends RuntimeException {

	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * Empty exception
	 */
	public DLXException() {
		// Do nothing
	}

	/**
	 * Construct an exception with a message
	 * @param message the message of the exception
	 */
	public DLXException(String message) {
		super(message);
	}

}
