package fr.boreal.backward_chaining.core;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.query.api.FOQuery;

/**
 * @author Florent Tornil
 * Core computing implementation that does nothing
 * Use this if you don't want to compute the core but an algorithm ask for one. 
 */
public class NoCoreProcessor implements QueryCoreProcessor {

	@Override
	public FOQuery<? extends FOFormula> computeCore(FOQuery<? extends FOFormula> query) {
		return query;
	}
}
