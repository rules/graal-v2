package fr.boreal.backward_chaining.core;

import fr.boreal.core.CoreProcessor;
import fr.boreal.core.NaiveCoreProcessor;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.partition.TermPartition;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.model.ruleCompilation.NoRuleCompilation;
import fr.boreal.model.ruleCompilation.api.RuleCompilation;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Core computing for small sets of atoms, used mostly for queries
 * This is based on an implementation by Guillaume Pérution-Kihli
 *
 * @author Florent Tornil
 * @author Guillaume Pérution-Kihli
 */
public class QueryCoreProcessorImpl implements QueryCoreProcessor {

	private final FOQueryEvaluator<FOFormula> evaluator;
	private final CoreProcessor coreProcessor;

	/**
	 * Default constructor using generic homomorphism
	 */
	public QueryCoreProcessorImpl() {
		this.evaluator = GenericFOQueryEvaluator.defaultInstance();
		this.coreProcessor = new NaiveCoreProcessor();
	}

	/**
	 * Constructor using a specific core processor
	 * @param coreProcessor the core processor
	 */
	public QueryCoreProcessorImpl(CoreProcessor coreProcessor) {
		this.coreProcessor = coreProcessor;
		this.evaluator = GenericFOQueryEvaluator.defaultInstance();

	}

	/**
	 * Constructor using a homomorphism with compilation test
	 * @param compilation the compilation of the rules to use
	 */
	public QueryCoreProcessorImpl(RuleCompilation compilation) {
		this.coreProcessor = new NaiveCoreProcessor();
		if(compilation.equals(NoRuleCompilation.instance())) {
			this.evaluator = GenericFOQueryEvaluator.defaultInstance();
        } else {
			this.evaluator = GenericFOQueryEvaluator.defaultInstanceWithInfCompilation(compilation);
        }

    }

	/**
	 * Constructor using a homomorphism with compilation test and a specific core processor
	 * @param compilation the compilation of the rules to use
	 * @param coreProcessor the core processor
	 */
	public QueryCoreProcessorImpl(RuleCompilation compilation, CoreProcessor coreProcessor) {
		this.coreProcessor = coreProcessor;
		if(compilation.equals(NoRuleCompilation.instance())) {
			this.evaluator = GenericFOQueryEvaluator.defaultInstance();
		} else {
			this.evaluator = GenericFOQueryEvaluator.defaultInstanceWithInfCompilation(compilation);
		}
	}

	@Override
	public FOQuery<? extends FOFormula> computeCore(FOQuery<? extends FOFormula> query) {
		// Create a copy of the query atoms in memory
		SimpleInMemoryGraphStore fb = new SimpleInMemoryGraphStore();
		fb.addAll(query.getFormula().asAtomSet());

		if(fb.size() <= 1) {
			return query;
		}

		Collection<Variable> answer_variables = query.getAnswerVariables();
		if (answer_variables instanceof Set<Variable>) {
			coreProcessor.computeCore(fb, (Set<Variable>) answer_variables);
		} else {
			coreProcessor.computeCore(fb, new HashSet<>(answer_variables));
		}

		// TODO: remove from the equalities the variables that have been removed by the core
		// Keep only the equalities concerning query variables (answer or body)
		Set<Variable> bodyVariables = fb.getVariables().collect(Collectors.toSet());
		TermPartition newEqualities = new TermPartition();
		for(Set<Term> classs : query.getVariableEqualities().getClasses()) {
			Set<Term> toKeep = new HashSet<>();
			for(Term t : classs) {
				if(t.isFrozen(null) || query.getAnswerVariables().contains(t) || bodyVariables.contains(t)) {
					toKeep.add(t);
				}
			}
			if(toKeep.size() > 1) {
				newEqualities.addClass(toKeep);
			}
		}

		return FOQueryFactory.instance().createOrGetQuery(
				FOFormulaFactory.instance().createOrGetConjunction(fb.getAtoms().collect(Collectors.toSet())),
				query.getAnswerVariables(), newEqualities);
	}
}
