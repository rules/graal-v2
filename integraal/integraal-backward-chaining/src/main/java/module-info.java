/**
 * Module for backward chaining elements of InteGraal
 * 
 * @author Florent Tornil
 *
 */
module fr.boreal.backward_chaining {

	requires transitive fr.boreal.model;
	requires transitive fr.boreal.unifiers;

	requires transitive fr.boreal.query_evaluation;
//	requires fr.boreal.storage;
	requires com.google.common;
	requires fr.lirmm.boreal.util;
	requires org.apache.commons.lang3;
	requires fr.boreal.io;
	requires org.slf4j;
    requires fr.boreal.core;

	exports fr.boreal.backward_chaining.api;
	exports fr.boreal.backward_chaining.core;
	exports fr.boreal.backward_chaining.cover;
	exports fr.boreal.backward_chaining.homomorphism;
	exports fr.boreal.backward_chaining.pure;
	exports fr.boreal.backward_chaining.pure.rewriting_operator;
	exports fr.boreal.backward_chaining.unfolding;
    exports fr.boreal.backward_chaining.source_target;

}