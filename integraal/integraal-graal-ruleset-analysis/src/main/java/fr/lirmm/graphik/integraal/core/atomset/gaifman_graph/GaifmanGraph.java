/**
 * 
 */
package fr.lirmm.graphik.integraal.core.atomset.gaifman_graph;

import java.util.Collection;
import java.util.Set;

import fr.lirmm.graphik.integraal.api.core.AtomSetException;
import fr.lirmm.graphik.integraal.api.core.Term;

/**
 * A gaifman graph of an atom set is an undirected graph with vertex set containing the terms of the atomset 
 * and there is an edge between two vertices (terms) u,v if u and v appear in the same atom
 * 
 * @author Guillaume Pérution-Kihli
 *
 */
public interface GaifmanGraph {
	/**
	 * @return the set of terms which are the vertices of the graph
	 */
	Set<? extends Term> getVertices() throws AtomSetException;
	
	/**
	 * @return the of the edges of the graph
	 */
	Set<Edge> getEdges() throws AtomSetException;
	
	/**
	 * Compute and return a collection of the connected components of the graph
	 * 
	 * @return the set of the connected components of the graph
	 */
	Collection<? extends Set<? extends Term>> getConnectedComponents() throws AtomSetException;
}
