package fr.lirmm.graphik.integraal.core.atomset.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import fr.lirmm.graphik.integraal.api.core.Atom;
import fr.lirmm.graphik.integraal.api.core.AtomSetException;
import fr.lirmm.graphik.integraal.api.core.Predicate;
import fr.lirmm.graphik.integraal.api.core.Substitution;
import fr.lirmm.graphik.integraal.api.core.Term;
import fr.lirmm.graphik.integraal.api.core.TermGenerator;
import fr.lirmm.graphik.integraal.api.core.Variable;
import fr.lirmm.graphik.integraal.api.core.Term.Type;
import fr.lirmm.graphik.integraal.api.store.BatchProcessor;
import fr.lirmm.graphik.integraal.api.store.Store;
import fr.lirmm.graphik.integraal.core.AtomType;
import fr.lirmm.graphik.integraal.core.DefaultVariableGenerator;
import fr.lirmm.graphik.integraal.core.TypeFilter;
import fr.lirmm.graphik.integraal.core.atomset.AbstractInMemoryAtomSet;
import fr.lirmm.graphik.integraal.core.store.DefaultBatchProcessor;
import fr.lirmm.graphik.util.stream.CloseableIterableAdapter;
import fr.lirmm.graphik.util.stream.CloseableIteratorAdapter;
import fr.lirmm.graphik.util.stream.CloseableIteratorWithoutException;
import fr.lirmm.graphik.util.stream.Iterators;
import fr.lirmm.graphik.util.stream.filter.FilterIteratorWithoutException;

@SuppressWarnings("deprecation")
public class SimpleInMemoryGraphStore extends AbstractInMemoryAtomSet implements Store {

	private Map<Predicate, PredicateVertex> predicateVertices;
	private Map<Term, TermVertex> termVertices;
	int size;
	private TermGenerator freshSymbolGenerator = new DefaultVariableGenerator("EE", this);
	
	public SimpleInMemoryGraphStore () {
		predicateVertices = new HashMap<>();
		termVertices = new HashMap<>();
		size = 0;
	}
	
	private class PredicateVertex {
		Predicate predicate;
		Set<Atom> adjacentEdges;
		List<Set<Term>> termPositions;
		
		public PredicateVertex (Predicate p) {
			predicate = p;
			
			termPositions =  new ArrayList<>(p.getArity());
			for (int i = 0; i < p.getArity(); ++i) {
				termPositions.add(new HashSet<>());
			}
			
			adjacentEdges = new HashSet<>();
		}
		
		public boolean addEdge (Atom a) {
			if (!a.getPredicate().equals(predicate)) {
				return false;
			}
			
			adjacentEdges.add(a);
			
			List<Term> terms = a.getTerms();
			for (int i = 0; i < terms.size(); ++i) {
				termPositions.get(i).add(terms.get(i));
			}
			
			return true;
		}
		
		public boolean removeEdge (Atom a) {
			if (!a.getPredicate().equals(predicate)) {
				return false;
			}
			
			if (adjacentEdges.remove(a)) {
				List<Term> terms = a.getTerms();
				for (int i = 0; i < terms.size(); ++i) {
					Optional<Set<Atom>> edgesAtPosition = getAtomsWithPredicateAndTermAtPosition(predicate, terms.get(i), i);
					if (!edgesAtPosition.isPresent() 
							|| edgesAtPosition.get().isEmpty()
							|| (edgesAtPosition.get().size() == 1 && edgesAtPosition.get().contains(a))) {
						termPositions.get(i).remove(terms.get(i));
					}
				}
			
				return true;
			}
			
			return false;
		}
		
		public Set<Term> getTermsAtPosition (int position) {
			return termPositions.get(position);
		}
		
		public Set<Atom> getAdjacentEdges () {
			return adjacentEdges;
		}
		
		public Predicate getPredicate () {
			return predicate;
		}
	}
	
	private static class TermVertex {
		Term term;
		Map<Predicate,List<Set<Atom>>> edgesByPosition;
		Set<Atom> adjacentEdges;
		
		public TermVertex (Term t) {
			term = t;
			edgesByPosition = new HashMap<>();
			adjacentEdges = new HashSet<>();
		}
		
		public boolean addEdge (Atom a) {
			boolean hasBeenModified = false;
			
			List<Term> terms = a.getTerms();
			List<Set<Atom>> positions = edgesByPosition.get(a.getPredicate());
			for (int i = 0; i < terms.size(); ++i) {
				Term t = terms.get(i);
				if (t.equals(term)) {
					hasBeenModified = true;
					
					if (positions == null) {
						positions = new ArrayList<Set<Atom>>();
						for (int j = 0; j < terms.size(); ++j) {
							positions.add(new HashSet<>());
						}
						edgesByPosition.put(a.getPredicate(), positions);
					}
					
					positions.get(i).add(a);
				}
			}
			
			if (hasBeenModified) {
				adjacentEdges.add(a);
			}
			
			return hasBeenModified;
		}
		
		public boolean removeEdge (Atom a) {
			if (!adjacentEdges.contains(a)) {
				return false;
			}
			
			List<Term> terms = a.getTerms();
			List<Set<Atom>> positions = edgesByPosition.get(a.getPredicate());
			for (int i = 0; i < terms.size(); ++i) {
				Term t = terms.get(i);
				if (t.equals(term)) {
					positions.get(i).remove(a);
				}
			}
			
			return adjacentEdges.remove(a);
		}
		
		public Set<Atom> getAdjacentEdges () {
			return adjacentEdges;
		}
		
		public Set<Atom> getAdjacentEdgesAtPosition (Predicate p, int position) {
			List<Set<Atom>> edgesOfPredicate = edgesByPosition.get(p);
			
			if (edgesOfPredicate == null) {
				return Collections.emptySet();
			}
			
			Set<Atom> edges = edgesOfPredicate.get(position);
			
			if (edges == null) {
				return Collections.emptySet();
			}
			
			return edges;
		}
		
		public Term getTerm () {
			return term;
		}
	} 
	
	private Optional<Set<Atom>> getAtomsWithPredicateAndTermAtPosition (Predicate p, Term t, int position) {
		TermVertex tv = this.termVertices.get(t);
		
		if (tv == null) {
			return Optional.empty();
		}
		
		return Optional.of(tv.getAdjacentEdgesAtPosition(p, position));
	}
	
	private Optional<TermVertex> getTermVertex (Term t) {
		return Optional.ofNullable(this.termVertices.get(t));
	}
	
	private Optional<PredicateVertex> getPredicateVertex (Predicate p) {
		return Optional.ofNullable(this.predicateVertices.get(p));
	}
	
	private TermVertex createTermVertex (Term t) {
		TermVertex tv = new TermVertex(t);
		this.termVertices.put(t, tv);
		return tv;
	}
	
	private PredicateVertex createPredicateVertex (Predicate p) {
		PredicateVertex pv = new PredicateVertex(p);
		this.predicateVertices.put(p, pv);
		return pv;
	}

	@Override
	public CloseableIteratorWithoutException<Atom> match(Atom atom, Substitution s) {
		CloseableIteratorWithoutException<Atom> it = null;
		final AtomType atomType = new AtomType(atom, s);
		if(atomType.isThereConstant()) {
			// find smallest iterator
			int i = -1;
			int size = Integer.MAX_VALUE;
			for (Term t : atom.getTerms()) {
				++i;
				if (t.isConstant() || s.getTerms().contains(t)) {
					Optional<TermVertex> otv = this.getTermVertex(s.createImageOf(t));
					
					if (otv.isPresent()) {
						TermVertex tv = otv.get();
						int tmpSize = tv.getAdjacentEdgesAtPosition(atom.getPredicate(), i).size();
						if(tmpSize < size) {
							size = tmpSize;
							it = new CloseableIteratorAdapter<Atom>(tv.getAdjacentEdgesAtPosition(atom.getPredicate(), i).iterator());
						}
					} else {
						size = 0;
						it = Iterators.<Atom>emptyIterator();
					}
				}
			}
		} else {
			 it = this.atomsByPredicate(atom.getPredicate());
		}
		
		if(atomType.isThereConstraint()) {
			return new FilterIteratorWithoutException<Atom, Atom>(it, new TypeFilter(atomType, s.createImageOf(atom)));
		} else {
			return it;
		}
	}

	@Override
	public CloseableIteratorWithoutException<Atom> atomsByPredicate(Predicate p) {
		Optional<PredicateVertex> pv = this.getPredicateVertex(p);
		if (!pv.isPresent()) {
			return Iterators.<Atom>emptyIterator();
		}
		
		return new CloseableIteratorAdapter<Atom>(pv.get().getAdjacentEdges().iterator());
	}

	@Override
	public CloseableIteratorWithoutException<Term> termsByPredicatePosition(Predicate p, int position) {
		if (this.getPredicateVertex(p).isPresent()) {
			return new CloseableIteratorAdapter<Term>(
					this.predicateVertices.get(p).getTermsAtPosition(position).iterator());
		}
		
		return Iterators.<Term>emptyIterator();
	}

	@Override
	public CloseableIteratorWithoutException<Predicate> predicatesIterator() {
		return new CloseableIteratorAdapter<Predicate>(this.getPredicates().iterator());
	}

	@Override
	public CloseableIteratorWithoutException<Term> termsIterator() {
		return new CloseableIteratorAdapter<Term>(this.getTerms().iterator());
	}

	@Override
	@Deprecated
	public CloseableIteratorWithoutException<Term> termsIterator(Type type) {
		return new CloseableIteratorAdapter<Term>(this.getTerms(type).iterator());
	}

	@Override
	public CloseableIteratorWithoutException<Atom> iterator() {
		// We create  a stream on the keySet of this.predicates in order to go through the predicates
		return new CloseableIteratorAdapter<Atom>(this.predicateVertices.keySet().stream() 
				// For each predicate, we create a stream of AtomEdge with this predicate
				// The flatMap transforms the Stream<Stream<Edge>> into a Stream<Edge>
				.flatMap(p -> this.predicateVertices.get(p).getAdjacentEdges().stream()) 
				// We transform the stream into an iterator
				// There will be a lazy evaluation of the stream when we will use the iterator
				.iterator());
	}

	@Override
	public boolean add(Atom atom) {
		if (this.contains(atom)) {
			return false;
		}
		
		Optional<PredicateVertex> opv = this.getPredicateVertex(atom.getPredicate());
		PredicateVertex pv;
		
		if (opv.isPresent()) {
			pv = opv.get();
		} else {
			pv = this.createPredicateVertex(atom.getPredicate());
		}
		pv.addEdge(atom);
		
		for (Term t : atom.getTerms()) {
			Optional<TermVertex> otv = this.getTermVertex(t);
			TermVertex tv;
			
			if (otv.isPresent()) {
				tv = otv.get();
			} else {
				tv = this.createTermVertex(t);
			}
			
			tv.addEdge(atom);
		}
		
		++size;
		
		return true;
	}

	@Override
	public void removeWithoutCheck(Atom atom) {
		Optional<PredicateVertex> opv = this.getPredicateVertex(atom.getPredicate());

		if (opv.isPresent()) {
			PredicateVertex pv = opv.get();
			if (pv.removeEdge(atom)) {
				if (pv.getAdjacentEdges().isEmpty()) {
					this.predicateVertices.remove(atom.getPredicate());
				}
				
				for (Term t : atom.getTerms()) {
					Optional<TermVertex> otv = this.getTermVertex(t);
					
					if (otv.isPresent()) {
						TermVertex tv = otv.get();
						tv.removeEdge(atom);
						if (tv.getAdjacentEdges().isEmpty()) {
							this.termVertices.remove(t);
						}
					}
				}
				
				--size;
			}
		}
	}
 
	@Override
	public void clear() {
		size = 0;
		this.predicateVertices.clear();
		this.termVertices.clear();
	}

	@Override
	public TermGenerator getFreshSymbolGenerator() {
		return freshSymbolGenerator;
	}
	@Override
	public Set<Predicate> getPredicates() {
		return Collections.unmodifiableSet(this.predicateVertices.keySet());
	}
	
	@Override
	public CloseableIteratorWithoutException<Atom> atomsByTerm (Term term)
	{
		// If the term is not in the atom set, we return an empty set
		if(!this.getTermVertex(term).isPresent()){
			return Iterators.<Atom>emptyIterator();
		}
		
		// Otherwise, we simply return the neighbors of the term vertex in the graph
		return (new CloseableIterableAdapter<Atom>(this.getTermVertex(term).get().getAdjacentEdges())).iterator();
	}

	@Override
	public boolean contains(Atom atom) {
		PredicateVertex predicateVertex = this.predicateVertices.get(atom.getPredicate());
		if (predicateVertex == null) {
			return false;
		}
		return predicateVertex.getAdjacentEdges().contains(atom);
	}

	@Override
	public int size(Predicate p) {
		Optional<PredicateVertex> pred = this.getPredicateVertex(p);
		return (!pred.isPresent()) ? 0 : pred.get().getAdjacentEdges().size();
	}

	@Override
	public int getDomainSize() {
		return this.termVertices.size();
	}

	@Override
	public Set<Term> getTerms() {
		return Collections.<Term>unmodifiableSet(this.termVertices.keySet());
	}

	@Override
	@Deprecated
	public Set<Term> getTerms(Type type) {
		Set<Term> set = new HashSet<Term>();
		for (Term t : this.termVertices.keySet())
			if (type.equals(t.getType()))
				set.add(t);

		return set;
	}
	
	public int size() {
		return this.size;
	}

	@Override
	public BatchProcessor createBatchProcessor() throws AtomSetException {
		return new DefaultBatchProcessor(this);
	}

	@Override
	public boolean isWriteable() {
		return true;
	}

	@Override
	public void close() {
	}
    
    @Override
    public boolean containsTerm(Term t) throws AtomSetException {
		return this.termVertices.containsKey(t);
	}
    
    @Override
    public Set<Variable> getVariables() {
    	return this.termVertices.keySet().parallelStream()
    			.filter(t -> t.isVariable())
    			.map(v -> (Variable)v)
    			.collect(Collectors.toSet());
    }
}
