/**
 * 
 */
package fr.lirmm.graphik.integraal.core.atomset.gaifman_graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import fr.lirmm.graphik.integraal.api.core.Atom;
import fr.lirmm.graphik.integraal.api.core.AtomSet;
import fr.lirmm.graphik.integraal.api.core.AtomSetException;
import fr.lirmm.graphik.integraal.api.core.Variable;
import fr.lirmm.graphik.util.stream.CloseableIterator;
import fr.lirmm.graphik.util.stream.IteratorException;

/**
 * This class represents the gaifman graph of variables (ie the only terms in the graph are the variables of the atomset)
 * 
 * @author Guillaume Pérution-Kihli
 *
 */
public class VariablesGaifmanGraph implements GaifmanGraph {
	private AtomSet atomset;
	private Set<Edge> edges;
	private Set<Variable> vertices;
	
	public VariablesGaifmanGraph (AtomSet as) throws IteratorException, AtomSetException {
		atomset = as;
		vertices = as.getVariables();
		buildEdges();
	}
	
	public VariablesGaifmanGraph (AtomSet as, Set<Variable> existentialVariables) throws IteratorException {
		atomset = as;
		vertices = existentialVariables;
		buildEdges();
	}
	
	@Override
	public Set<Variable> getVertices() {
		return vertices;
	}

	@Override
	public Set<Edge> getEdges() {
		return edges;
	}

	@Override
	public Collection<Set<Variable>> getConnectedComponents() throws AtomSetException {
		// Each Variable is linked to its component which is a set of Variables
		Map<Variable,Set<Variable>> components = new HashMap<Variable,Set<Variable>>();
		
		// Before we look at the edges, each Variable is in its own connected components
		for (Variable v : getVertices()) {
			components.put(v, new HashSet<Variable>());
			components.get(v).add(v);
		}
		
		// For each edge, we merge the connected components of the variables if necessary
		for (Edge edge : edges)	{
			Set<Variable> x = components.get(edge.getTerms().getLeft());
			Set<Variable> y = components.get(edge.getTerms().getRight());

			// We compare the references of the connected components
			if (x != y)	{
				// We merge the smaller component into the bigger oner
				if (x.size() > y.size()) {
					for (Variable v : y)
						components.put(v, x);
					x.addAll(y);
				}
				else {
					for (Variable v : x)
						components.put(v, y);
					y.addAll(x);
				}
			}
		}
		
		Collection<Set<Variable>> result = new ArrayList<>();
		Set<Variable> alreadyAdded = new HashSet<>();
		for (Map.Entry<Variable,Set<Variable>> e : components.entrySet()) {
			if (!alreadyAdded.contains(e.getKey())) {
				alreadyAdded.addAll(e.getValue());
				result.add(e.getValue());
			}
		}

		return result;
	}

	private void buildEdges () throws IteratorException {
		edges = new HashSet<Edge>();
		
		// Two variables are connected if they appear in the same atom
		CloseableIterator<Atom> it = atomset.iterator();
		while (it.hasNext()) {
			Atom a = it.next();
			Iterator<Variable> itVar = a.getVariables().iterator();
			
			while (itVar.hasNext()) {
				Variable v1 = itVar.next();
				Iterator<Variable> itVar2 = a.getVariables().iterator();
				
				while (itVar2.hasNext()) {
					Variable v2 = itVar2.next();
					if (vertices.contains(v1) && vertices.contains(v2) && !v1.equals(v2)) {
						edges.add(new Edge(v1, v2));
					}
				}
			}
		}
		
	}
}
