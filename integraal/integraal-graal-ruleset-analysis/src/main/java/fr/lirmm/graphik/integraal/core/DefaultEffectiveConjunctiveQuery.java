package fr.lirmm.graphik.integraal.core;

import fr.lirmm.graphik.integraal.api.core.ConjunctiveQuery;
import fr.lirmm.graphik.integraal.api.core.EffectiveConjunctiveQuery;
import fr.lirmm.graphik.integraal.api.core.Substitution;

public class DefaultEffectiveConjunctiveQuery extends DefaultEffectiveQuery<ConjunctiveQuery, Substitution> implements EffectiveConjunctiveQuery {

	public DefaultEffectiveConjunctiveQuery(ConjunctiveQuery q, Substitution s) {
		super(q, s);
	}
}
