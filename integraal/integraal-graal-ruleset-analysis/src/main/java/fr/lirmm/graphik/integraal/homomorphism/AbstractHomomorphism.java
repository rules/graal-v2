/*
 * Copyright (C) Inria Sophia Antipolis - Méditerranée / LIRMM
 * (Université de Montpellier & CNRS) (2014 - 2017)
 *
 * Contributors :
 *
 * Clément SIPIETER <clement.sipieter@inria.fr>
 * Mélanie KÖNIG
 * Swan ROCHER
 * Jean-François BAGET
 * Michel LECLÈRE
 * Marie-Laure MUGNIER <mugnier@lirmm.fr>
 *
 *
 * This file is part of Graal <https://graphik-team.github.io/graal/>.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lirmm.graphik.integraal.homomorphism;

import fr.lirmm.graphik.integraal.api.core.AtomSet;
import fr.lirmm.graphik.integraal.api.core.Substitution;
import fr.lirmm.graphik.integraal.api.homomorphism.Homomorphism;
import fr.lirmm.graphik.integraal.api.homomorphism.HomomorphismException;
import fr.lirmm.graphik.integraal.core.Substitutions;
import fr.lirmm.graphik.util.profiler.AbstractProfilable;
import fr.lirmm.graphik.util.stream.CloseableIterator;
import fr.lirmm.graphik.util.stream.IteratorException;

/**
 * This class provides a default implementation of three methods imposed by the
 * interface Homomorphism. These are exists (both with and without substitution)
 * and execute (only without substitution). The evaluation of the methods is
 * reduced to the evaluation of the method execute (with substitution) <br/>
 * that must be implemented by the concrete class. Obviously, from this follows
 * that without the implementation <br/>
 * of the excute (with substitution) method none of the above three can work.
 * 
 * @author Clément Sipieter (INRIA) {@literal <clement@6pi.fr>}
 *
 */

public abstract class AbstractHomomorphism<T1 extends Object, T2 extends AtomSet> extends AbstractProfilable
		implements Homomorphism<T1, T2> {

	// /////////////////////////////////////////////////////////////////////////
	// PUBLIC METHODS
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Execute without substitution is reduced to a call to execute, passing an
	 * empty substitution.
	 */
	@Override
	public CloseableIterator<Substitution> execute(T1 q, T2 a) throws HomomorphismException {
		return this.execute(q, a, Substitutions.emptySubstitution());
	}

	/**
	 * Exists without substitution is reduced to execute, passing an empty
	 * substitition. Then, to handle the boolean case, it is checked if the
	 * enumeration yields at least one solution.
	 */
	@Override
	public boolean exist(T1 q, T2 a) throws HomomorphismException {
		CloseableIterator<Substitution> results = this.execute(q, a, Substitutions.emptySubstitution());
		boolean val;
		try {
			val = results.hasNext();
		} catch (IteratorException e) {
			throw new HomomorphismException(e);
		}
		results.close();
		return val;
	}

	/**
	 * Exists with substitution is reduced to execute with the given
	 * substitition. Then, to handle the boolean case, it is checked if the
	 * enumeration yields at least one solution.
	 */

	@Override
	public boolean exist(T1 q, T2 a, Substitution s) throws HomomorphismException {
		CloseableIterator<Substitution> results = this.execute(q, a, s);
		boolean val;
		try {
			val = results.hasNext();
		} catch (IteratorException e) {
			throw new HomomorphismException(e);
		}
		results.close();
		return val;
	}

}
