package fr.lirmm.graphik.util.stream;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;
import java.util.stream.Collector;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class StreamAdapter<T> implements Stream<T> {
	AutoCloseable toClose = null;
	Stream<T> stream;

	public StreamAdapter(CloseableIterator<T> iterator) {
		super();
		this.toClose = iterator;
        Spliterator<T> spitr = Spliterators.spliteratorUnknownSize( 
                new IteratorAdapter<T>(iterator), Spliterator.NONNULL); 
        stream = StreamSupport.stream(spitr, false); 
	}
	
	public StreamAdapter(Iterator<T> iterator) {
		super();
		if (iterator instanceof AutoCloseable) {
			this.toClose = (AutoCloseable)iterator;
		}
        Spliterator<T> spitr = Spliterators.spliteratorUnknownSize( 
                iterator, Spliterator.NONNULL); 
        stream = StreamSupport.stream(spitr, false); 
	}

	public boolean allMatch(Predicate<? super T> arg0) {
		return stream.allMatch(arg0);
	}

	public boolean anyMatch(Predicate<? super T> arg0) {
		return stream.anyMatch(arg0);
	}

	public void close() {
		if (toClose != null) {
			try {
				toClose.close();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		stream.close();
	}

	public <R, A> R collect(Collector<? super T, A, R> arg0) {
		return stream.collect(arg0);
	}

	public <R> R collect(Supplier<R> arg0, BiConsumer<R, ? super T> arg1, BiConsumer<R, R> arg2) {
		return stream.collect(arg0, arg1, arg2);
	}

	public long count() {
		return stream.count();
	}

	public Stream<T> distinct() {
		return stream.distinct();
	}

	public Stream<T> filter(Predicate<? super T> arg0) {
		return stream.filter(arg0);
	}

	public Optional<T> findAny() {
		return stream.findAny();
	}

	public Optional<T> findFirst() {
		return stream.findFirst();
	}

	public <R> Stream<R> flatMap(Function<? super T, ? extends Stream<? extends R>> arg0) {
		return stream.flatMap(arg0);
	}

	public DoubleStream flatMapToDouble(Function<? super T, ? extends DoubleStream> arg0) {
		return stream.flatMapToDouble(arg0);
	}

	public IntStream flatMapToInt(Function<? super T, ? extends IntStream> arg0) {
		return stream.flatMapToInt(arg0);
	}

	public LongStream flatMapToLong(Function<? super T, ? extends LongStream> arg0) {
		return stream.flatMapToLong(arg0);
	}

	public void forEach(Consumer<? super T> arg0) {
		stream.forEach(arg0);
	}

	public void forEachOrdered(Consumer<? super T> arg0) {
		stream.forEachOrdered(arg0);
	}

	public boolean isParallel() {
		return stream.isParallel();
	}

	public Iterator<T> iterator() {
		return stream.iterator();
	}

	public Stream<T> limit(long arg0) {
		return stream.limit(arg0);
	}

	public <R> Stream<R> map(Function<? super T, ? extends R> arg0) {
		return stream.map(arg0);
	}

	public DoubleStream mapToDouble(ToDoubleFunction<? super T> arg0) {
		return stream.mapToDouble(arg0);
	}

	public IntStream mapToInt(ToIntFunction<? super T> arg0) {
		return stream.mapToInt(arg0);
	}

	public LongStream mapToLong(ToLongFunction<? super T> arg0) {
		return stream.mapToLong(arg0);
	}

	public Optional<T> max(Comparator<? super T> arg0) {
		return stream.max(arg0);
	}

	public Optional<T> min(Comparator<? super T> arg0) {
		return stream.min(arg0);
	}

	public boolean noneMatch(Predicate<? super T> arg0) {
		return stream.noneMatch(arg0);
	}

	public Stream<T> onClose(Runnable arg0) {
		return stream.onClose(arg0);
	}

	public Stream<T> parallel() {
		return stream.parallel();
	}

	public Stream<T> peek(Consumer<? super T> arg0) {
		return stream.peek(arg0);
	}

	public Optional<T> reduce(BinaryOperator<T> arg0) {
		return stream.reduce(arg0);
	}

	public T reduce(T arg0, BinaryOperator<T> arg1) {
		return stream.reduce(arg0, arg1);
	}

	public <U> U reduce(U arg0, BiFunction<U, ? super T, U> arg1, BinaryOperator<U> arg2) {
		return stream.reduce(arg0, arg1, arg2);
	}

	public Stream<T> sequential() {
		return stream.sequential();
	}

	public Stream<T> skip(long arg0) {
		return stream.skip(arg0);
	}

	public Stream<T> sorted() {
		return stream.sorted();
	}

	public Stream<T> sorted(Comparator<? super T> arg0) {
		return stream.sorted(arg0);
	}

	public Spliterator<T> spliterator() {
		return stream.spliterator();
	}

	public Object[] toArray() {
		return stream.toArray();
	}

	public <A> A[] toArray(IntFunction<A[]> arg0) {
		return stream.toArray(arg0);
	}

	public Stream<T> unordered() {
		return stream.unordered();
	}
}
