/**
 * 
 */
package fr.lirmm.graphik.integraal.core.atomset.graph;

import fr.lirmm.graphik.integraal.api.core.Atom;
import fr.lirmm.graphik.integraal.api.core.AtomSet;
import fr.lirmm.graphik.integraal.api.core.InMemoryAtomSet;
import fr.lirmm.graphik.integraal.api.factory.InMemoryAtomSetFactory;
import fr.lirmm.graphik.util.stream.CloseableIterator;
import fr.lirmm.graphik.util.stream.CloseableIteratorWithoutException;
import fr.lirmm.graphik.util.stream.IteratorException;

/**
 * @author guillaume
 *
 */
public class DefaultInMemoryGraphStoreFactory implements InMemoryAtomSetFactory {

	private static DefaultInMemoryGraphStoreFactory instance = new DefaultInMemoryGraphStoreFactory();

	private DefaultInMemoryGraphStoreFactory() {
	}

	public static DefaultInMemoryGraphStoreFactory instance() {
		return instance;
	}
	
	@Override
	public InMemoryAtomSet create(Atom... atoms) {
		InMemoryAtomSet atomset = this.create();
		for (Atom a : atoms) {
			atomset.add(a);
		}
		return atomset;
	}
	
	@Override
	public InMemoryAtomSet create(CloseableIterator<Atom> atoms) throws IteratorException {
		InMemoryAtomSet atomset = this.create();
		while(atoms.hasNext()) {
			atomset.add(atoms.next());
		}
		return atomset;
	}
	
	@Override
	public InMemoryAtomSet create(CloseableIteratorWithoutException<Atom> atoms) {
		InMemoryAtomSet atomset = this.create();
		while(atoms.hasNext()) {
			atomset.add(atoms.next());
		}
		return atomset;
	}

	@Override
	public InMemoryAtomSet create(AtomSet src) throws IteratorException {
		InMemoryAtomSet atomset = this.create();
		CloseableIterator<Atom> it = src.iterator();
		while(it.hasNext()) {
			Atom a = it.next();
			atomset.add(a);
		}
		return atomset;
	}

	@Override
	public InMemoryAtomSet create(InMemoryAtomSet src) {
		try {
			return create((AtomSet) src);
		} catch (IteratorException e) {
			throw new Error("Should never happen");
		}
	}
	
	@Override
	public InMemoryAtomSet create() {
		return new DefaultInMemoryGraphStore();
	}
	
	@Override
	public InMemoryAtomSet create(Atom atom) {
		InMemoryAtomSet as = new DefaultInMemoryGraphStore();
		as.add(atom);
		return as;
	}
}
