package fr.lirmm.graphik.integraal.core.atomset.splitter;

import java.util.Collection;

import fr.lirmm.graphik.integraal.api.core.AtomSet;
import fr.lirmm.graphik.integraal.api.core.AtomSetException;
import fr.lirmm.graphik.integraal.api.core.InMemoryAtomSet;
import fr.lirmm.graphik.util.stream.IteratorException;

/**
 * An AtomSetSplitter cut an atom set in several atom sets, which are a partition of the atom set
 * 
 * @author Guillaume Pérution-Kihli
 *
 */
public interface AtomSetSplitter 
{
	/**
	 * Split an atom set in several atom sets
	 * 
	 * @param toSplit the set of atoms to split
	 * @return a Collection of atom sets
	 * @throws IteratorException
	 * @throws AtomSetException
	 */
	Collection<InMemoryAtomSet> split (AtomSet toSplit) throws IteratorException, AtomSetException;
}
