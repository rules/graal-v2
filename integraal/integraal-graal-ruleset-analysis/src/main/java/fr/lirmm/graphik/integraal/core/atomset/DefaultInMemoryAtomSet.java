/**
 * 
 */
package fr.lirmm.graphik.integraal.core.atomset;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.Spliterator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fr.lirmm.graphik.integraal.api.core.Atom;
import fr.lirmm.graphik.integraal.api.core.AtomSet;
import fr.lirmm.graphik.integraal.api.core.InMemoryAtomSet;
import fr.lirmm.graphik.integraal.api.core.Predicate;
import fr.lirmm.graphik.integraal.api.core.Substitution;
import fr.lirmm.graphik.integraal.api.core.Term;
import fr.lirmm.graphik.integraal.api.core.TermGenerator;
import fr.lirmm.graphik.integraal.api.core.Term.Type;
import fr.lirmm.graphik.integraal.core.AtomType;
import fr.lirmm.graphik.integraal.core.DefaultAtom;
import fr.lirmm.graphik.integraal.core.DefaultVariableGenerator;
import fr.lirmm.graphik.integraal.core.TypeFilter;
import fr.lirmm.graphik.util.stream.CloseableIterator;
import fr.lirmm.graphik.util.stream.CloseableIteratorAdapter;
import fr.lirmm.graphik.util.stream.CloseableIteratorWithoutException;
import fr.lirmm.graphik.util.stream.IteratorException;
import fr.lirmm.graphik.util.stream.filter.FilterIteratorWithoutException;

/**
 * The DefaultInMemoryAtomSet is a simple in memory atom set based on a java set.
 * It is a wrapper for any Collection that implements the Set interface.
 * Recommended sets :
 * - By default, use a HashSet of Atom
 * - If you want an ordered set, you can use a TreeSet of Atom
 * - If you want to access elements in the order there are added into the set,
 * 	use LinkedHashSet of Atom that combine a HashSet and a Linked List
 * 
 * @author Guillaume Pérution-Kihli
 *
 */
@SuppressWarnings("deprecation")
public class DefaultInMemoryAtomSet extends AbstractInMemoryAtomSet {
	private Set<Atom> atomSet;
	private TermGenerator freshSymbolGenerator = new DefaultVariableGenerator("EE", this);
	
	/**
	 * Default constructor
	 * By default, we use a HashSet of Atom
	 */
	public DefaultInMemoryAtomSet () {
		atomSet = new HashSet<Atom>();
	}

	/**
	 * This constructor allows you to use any Collection that implements
	 * the Set of Atom interface
	 * @param javaAtomSet the Set that will contain all the atoms
	 */
	public DefaultInMemoryAtomSet (Set<Atom> javaAtomSet) {
		atomSet = javaAtomSet;
	}
	
	/**
	 * A copy constructor that can copy any Collection of Atom
	 * @param collection
	 */
	public DefaultInMemoryAtomSet (Collection<Atom> collection) {
		this();
		this.atomSet.addAll(collection);
	}
	
	public DefaultInMemoryAtomSet(Atom... atoms) {
		this();
		for (Atom a : atoms)
			this.atomSet.add(a);
	}

	/**
	 * Construct the atom set with the elements contained in the iterator in parameter
	 * @param it The elements to add
	 * @throws IteratorException
	 */
	public DefaultInMemoryAtomSet(CloseableIterator<Atom> it) throws IteratorException {
		this();
		while (it.hasNext()) {
			this.atomSet.add(it.next());
		}
	}


	/**
	 * Construct the atom set with the elements contained in the iterator in parameter
	 * @param it The elements to add
	 */
	public DefaultInMemoryAtomSet(CloseableIteratorWithoutException<Atom> it) {
		this();
		while (it.hasNext()) {
			this.atomSet.add(it.next());
		}
	}

	/**
	 * Construct the atom set with the elements contained in the AtomSet in parameter
	 * @param atomset the elements to add
	 * @throws IteratorException
	 */
	public DefaultInMemoryAtomSet(AtomSet atomset) throws IteratorException {
		this();
		CloseableIterator<Atom> it = atomset.iterator();
		while (it.hasNext()) {
			this.add(new DefaultAtom(it.next()));
		}
	}

	/**
	 * Construct the atom set with the elements contained in the AtomSet in parameter
	 * @param atomset the elements to add
	 */
	public DefaultInMemoryAtomSet(InMemoryAtomSet atomset) {
		this();
		CloseableIteratorWithoutException<Atom> it = atomset.iterator();
		while (it.hasNext()) {
			this.add(new DefaultAtom(it.next()));
		}
	}
	
	
	@Override
	public CloseableIteratorWithoutException<Atom> match(Atom atom, Substitution s) {
		// We create an AtomType, we get all the atoms with the same predicate as atom
		// And we filter the atoms with the AtomType
		final AtomType atomType = new AtomType(atom, s);
		return new FilterIteratorWithoutException<Atom, Atom>(this.atomsByPredicate(atom.getPredicate()), 
				new TypeFilter(atomType, s.createImageOf(atom)));
	}

	@Override
	public CloseableIteratorWithoutException<Atom> atomsByPredicate(Predicate p) {
		return new CloseableIteratorAdapter<Atom>(this.atomSet.stream() // We create a stream through the atoms
				.filter(a -> a.getPredicate().equals(p)) // We filter the atoms with the predicate p
				.iterator()); // We create an iterator from the stream
	}

	@Override
	public CloseableIteratorWithoutException<Term> termsByPredicatePosition(Predicate p, int position) {
		return new CloseableIteratorAdapter<Term>(this.atomSet.stream() // We create a stream through the atoms
				.filter(a -> a.getPredicate().equals(p)) // We filter the atoms with the predicate p
				.map(a -> a.getTerm(position)) // We map the atoms to the term at the position position
				.distinct() // We want distinct elements
				.iterator()); // We create an iterator from the stream
	}

	@Override
	public CloseableIteratorWithoutException<Predicate> predicatesIterator() {
		return new CloseableIteratorAdapter<Predicate>(this.atomSet.stream() // We create a stream through the atoms
				.map(a -> a.getPredicate()) // We get the predicate of each atom
				.distinct() // We want distinct elements
				.iterator()); // We create an iterator from the stream
	}

	@Override
	public CloseableIteratorWithoutException<Term> termsIterator() {
		return new CloseableIteratorAdapter<Term>(this.atomSet.stream() // We create a stream through the atoms
				.flatMap(a -> a.getTerms().stream()) // We get the the terms of the atoms a convert them to a stream
				 // The flatMap transforms the Stream<Stream<Term>> into a Stream<term>
				.distinct() // We want distinct elements
				.iterator()); // We create an iterator from the stream
	}

	@Override
	@Deprecated
	public CloseableIteratorWithoutException<Term> termsIterator(Type type) {
		return new CloseableIteratorAdapter<Term>(this.getTerms(type).iterator());
	}

	@Override
	public CloseableIteratorWithoutException<Atom> iterator() {
		return new CloseableIteratorAdapter<Atom>(this.atomSet.iterator());
	}

	@Override
	public boolean add(Atom atom) {
		return this.atomSet.add(atom);
	}

	@Override
	public void removeWithoutCheck(Atom atom) {
		this.atomSet.remove(atom);
	}
	
	@Override
	public boolean remove(Atom atom) {
		return this.atomSet.remove(atom);
	}

	@Override
	public void clear() {
		this.atomSet.clear();
	}

	@Override
	public TermGenerator getFreshSymbolGenerator() {
		return freshSymbolGenerator;
	}

	@Override
	public int size() {
		return this.atomSet.size();
	}
	
	@Override
	public boolean isEmpty() {
		return this.atomSet.isEmpty();
	}

	@Override
	public Set<Term> getTerms() {
		return this.atomSet.parallelStream() // We create a parallel stream through the atoms (multi threads stream)
				.flatMap(a -> a.getTerms().stream()) // We get the the terms of the atoms a convert them to a stream
				 // The flatMap transforms the Stream<Stream<Term>> into a Stream<term>
				.collect(Collectors.toSet()); // We collect the elements of the stream into a Set<Term>
	}
	
	@Override
	public Set<Predicate> getPredicates() {
		return this.atomSet.parallelStream() // We create a parallel stream through the atoms (multi threads stream)
				.map(a -> a.getPredicate()) // We get the predicate from the atom a
				.collect(Collectors.toSet());  // We collect the predicates into a Set<Predicate>
	}
	
    @Override
    public Spliterator<Atom> spliterator() {
        return this.atomSet.spliterator();
    }

    @Override
    public Stream<Atom> stream() {
        return this.atomSet.stream();
    }

    @Override
    public Stream<Atom> parallelStream() {
        return this.atomSet.parallelStream();
    }
}
