/*
 * Copyright (C) Inria Sophia Antipolis - Méditerranée / LIRMM
 * (Université de Montpellier & CNRS) (2014 - 2017)
 *
 * Contributors :
 *
 * Clément SIPIETER <clement.sipieter@inria.fr>
 * Mélanie KÖNIG
 * Swan ROCHER
 * Jean-François BAGET
 * Michel LECLÈRE
 * Marie-Laure MUGNIER <mugnier@lirmm.fr>
 *
 *
 * This file is part of Graal <https://graphik-team.github.io/graal/>.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lirmm.graphik.integraal.homomorphism;

import fr.lirmm.graphik.integraal.api.core.AtomSet;
import fr.lirmm.graphik.integraal.api.core.RulesCompilation;
import fr.lirmm.graphik.integraal.api.core.Substitution;
import fr.lirmm.graphik.integraal.api.homomorphism.HomomorphismException;
import fr.lirmm.graphik.integraal.api.homomorphism.HomomorphismWithCompilation;
import fr.lirmm.graphik.integraal.core.Substitutions;
import fr.lirmm.graphik.integraal.core.compilation.NoCompilation;
import fr.lirmm.graphik.util.stream.CloseableIterator;
import fr.lirmm.graphik.util.stream.IteratorException;

/**
 * This class provides a default implementation of seven methods (out of eight)
 * imposed by the interface HomomorphismWithCompilation that have to be
 * implemented by a homomorphism solver which is able to take into account a
 * compilation. The methos are : (1) exist (both with / without substitutions,
 * and with / without compilation) and (2) execute (both with / without
 * substitution, only without substitution). Note that the evaluation of the
 * methods is always reduced to the evaluation of the baseline method execute
 * (with substitution and with compilation) that must be implemented by the
 * concrete class extending this one. Obviously, from this follows that without
 * the implementation of the excute (with substitution) method none of the above
 * three can work.
 * 
 * 
 * @author Clément Sipieter (INRIA) {@literal <clement@6pi.fr>}
 *
 */

public abstract class AbstractHomomorphismWithCompilation<T1 extends Object, T2 extends AtomSet>
		extends AbstractHomomorphism<T1, T2> implements HomomorphismWithCompilation<T1, T2> {

	/**
	 * Reduce execute without compilation and substitutions to reduce with empty
	 * compilation and empty substitution.
	 */
	@Override
	public CloseableIterator<Substitution> execute(T1 q, T2 a) throws HomomorphismException {
		return this.execute(q, a, NoCompilation.instance(), Substitutions.emptySubstitution());
	}

	/**
	 * Reduce execute without substitutions to reduce with empty substitution.
	 */
	@Override
	public CloseableIterator<Substitution> execute(T1 q, T2 a, RulesCompilation compilation)
			throws HomomorphismException {
		return this.execute(q, a, compilation, Substitutions.emptySubstitution());
	}

	/**
	 * Reduce execute without compilation to reduce with empty compilation.
	 */

	@Override
	public CloseableIterator<Substitution> execute(T1 q, T2 a, Substitution s) throws HomomorphismException {
		return this.execute(q, a, NoCompilation.instance(), s);
	}

	/**
	 * Reduce exists without substitutions to reduce with empty substitutions,
	 * then check that at least one homomorphism has been found.
	 */

	@Override
	public boolean exist(T1 q, T2 a, RulesCompilation compilation) throws HomomorphismException {
		return this.exist(q, a, compilation, Substitutions.emptySubstitution());
	}

	/**
	 * Reduce exists to reduce, then check that at least one homomorphism has
	 * been found.
	 */
	@Override
	public boolean exist(T1 q, T2 a, RulesCompilation compilation, Substitution s) throws HomomorphismException {
		CloseableIterator<Substitution> results = this.execute(q, a, compilation, s);
		boolean val;
		try {
			val = results.hasNext();
		} catch (IteratorException e) {
			throw new HomomorphismException(e);
		}
		results.close();
		return val;
	}

	/**
	 * Reduce exists without substitutions and compilation to exits without
	 * substitutions and empty compilation, then check that at least one
	 * homomorphism is computed.
	 */

	@Override
	public boolean exist(T1 source, T2 target) throws HomomorphismException {
		return this.exist(source, target, NoCompilation.instance());
	}

	/**
	 * Reduce exists without compilation to exits with empty compilation, then
	 * check that at least one homomorphism is computed.
	 */

	@Override
	public boolean exist(T1 source, T2 target, Substitution s) throws HomomorphismException {
		return this.exist(source, target, NoCompilation.instance(), s);
	}

}
