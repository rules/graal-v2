/**
 * 
 */
package fr.lirmm.graphik.integraal.core.atomset.splitter;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.lirmm.graphik.integraal.api.core.Atom;
import fr.lirmm.graphik.integraal.api.core.AtomSet;
import fr.lirmm.graphik.integraal.api.core.AtomSetException;
import fr.lirmm.graphik.integraal.api.core.InMemoryAtomSet;
import fr.lirmm.graphik.integraal.api.core.Variable;
import fr.lirmm.graphik.integraal.core.atomset.graph.DefaultInMemoryGraphStore;
import fr.lirmm.graphik.util.stream.CloseableIterator;
import fr.lirmm.graphik.util.stream.IteratorException;

/**
 * Split a set of atoms in pieces
 * A piece of an atom set is a set of atoms such that the atoms containing a variable v
 * are all in the same piece and the size of the pieces is minimal for this property
 * 
 * @author Guillaume Pérution-Kihli
 *
 */
public class PiecesSplitter implements AtomSetSplitter {
	private boolean includeGroundedAtoms;
	private Set<Variable> existentialVariables;
	
	public PiecesSplitter ()
	{
		this(true);
	}
	
	public PiecesSplitter (boolean includeGroundedAtoms)
	{
		this(true, null);
	}
	
	public PiecesSplitter (boolean includeGroundedAtoms, Set<Variable> existentialVariables)
	{
		this.includeGroundedAtoms = includeGroundedAtoms;
		this.existentialVariables = existentialVariables;
	}
	
	@Override
	public Collection<InMemoryAtomSet> split(AtomSet toSplit) throws IteratorException, AtomSetException {
		List<InMemoryAtomSet> pieces = new ArrayList<InMemoryAtomSet>();
		Set<Variable> variables = this.existentialVariables == null ? toSplit.getVariables() : this.existentialVariables;
		Set<Variable> varToTreat = new HashSet<>();
		varToTreat.addAll(variables);
		
		while (!varToTreat.isEmpty()) {
			Variable root = varToTreat.iterator().next();
			Deque<Variable> queue = new ArrayDeque<>();
			queue.add(root);
			InMemoryAtomSet piece = new DefaultInMemoryGraphStore();
			
			while (!queue.isEmpty()) {
				Variable v = queue.pollFirst();
				varToTreat.remove(v);
				
				CloseableIterator<Atom> it = toSplit.atomsByTerm(v);
				while (it.hasNext()) {
					Atom a = it.next();

					a.getVariables().stream()
							.filter(vv -> variables.contains(vv) && varToTreat.contains(vv))
							.distinct()
							.forEach(vv -> { queue.add(vv); varToTreat.remove(vv); });
					
					piece.add(a);
				}
			}

			pieces.add(piece);
		}
		
		if (this.includeGroundedAtoms) {
			CloseableIterator<Atom> it = toSplit.iterator();
			while (it.hasNext()) {
				Atom a = it.next();
				long nbVar = a.getVariables().stream()
						.filter(v -> variables.contains(v))
						.distinct()
						.count();
				
				// If the atom is not totally grounded
				if (nbVar == 0) {
					InMemoryAtomSet atomSet = new DefaultInMemoryGraphStore();
					atomSet.add(a);
					pieces.add(atomSet);
				}
			}
		}
		
		return pieces;
	}
	
	/*@Override
	public Collection<InMemoryAtomSet> split(AtomSet toSplit) throws IteratorException, AtomSetException {
		Map<Variable, InMemoryAtomSet> referenceVariableToPieces = new HashMap<Variable,InMemoryAtomSet>();
		Map<Variable,Variable> variableToReferenceVariable = new HashMap<Variable,Variable>();
		
		List<InMemoryAtomSet> pieces = new ArrayList<InMemoryAtomSet>();
		Set<Variable> variables = this.existentialVariables == null ? toSplit.getVariables() : this.existentialVariables;

		// Now, we put the atoms in the corresponding pieces
		CloseableIterator<Atom> it = toSplit.iterator();
		while (it.hasNext()) {
			Atom a = it.next();
			List<Variable> atomVariables = a.getVariables().stream()
					.filter(v -> variables.contains(v))
					.distinct()
					.collect(Collectors.toList());
			
			// If the atom is not totally grounded
			if (atomVariables.size() > 0) {
				Variable v = atomVariables.get(0);
				
				if (variableToReferenceVariable.containsKey(v)) {
					v = variableToReferenceVariable.get(v);
				} else {
					variableToReferenceVariable.put(v, v);
				}
				
				InMemoryAtomSet vPiece = referenceVariableToPieces.get(v);
				
				if (vPiece == null) {
					vPiece = new DefaultInMemoryAtomSet();
					referenceVariableToPieces.put(v, vPiece);
				}

				vPiece.add(a);
				
				for (int i = 1; i < atomVariables.size(); ++i) {
					Variable iv = atomVariables.get(i);
					
					Variable irv = variableToReferenceVariable.get(iv);
					
					if (irv == null) {
						variableToReferenceVariable.put(iv, v);
					} else if (irv != v) {
						InMemoryAtomSet iPiece = referenceVariableToPieces.get(irv);
						
						if (!variables.contains(atomVariables.get(i))) {
							throw new RuntimeException();
						}
						
						if (vPiece != iPiece) {
							if (iPiece != null) {
								vPiece.addAll(iPiece);
							}
							variableToReferenceVariable.put(iv, v);
							referenceVariableToPieces.remove(iv);
						}
					}
				}
				
				for (Variable vv : vPiece.getVariables()) {
					if (!variables.contains(vv) || v == vv) {
						continue;
					}

					InMemoryAtomSet vvPiece = referenceVariableToPieces.get(variableToReferenceVariable.get(vv));
					if (vvPiece != null) {
						vPiece.addAll(vvPiece);
					}
					variableToReferenceVariable.put(vv, v);
					referenceVariableToPieces.remove(vv);
				}
			}
			// If the atom is totally grounded, it has its own piece
			else if (this.includeGroundedAtoms) {
				InMemoryAtomSet atomSet = new DefaultInMemoryGraphStore();
				atomSet.add(a);
				pieces.add(atomSet);
			}
		}

		pieces.addAll(referenceVariableToPieces.values());

		// We finally, we return the set of pieces
		return pieces;
	}
	
	/*@Override
	public Collection<InMemoryAtomSet> split(AtomSet toSplit) throws IteratorException, AtomSetException {
		Map<Term, InMemoryAtomSet> termToPieces = new HashMap<Term,InMemoryAtomSet>();
		List<InMemoryAtomSet> pieces = new ArrayList<InMemoryAtomSet>();
		Set<Variable> variables = this.existentialVariables == null ? toSplit.getVariables() : this.existentialVariables;
		GaifmanGraph gg = new VariablesGaifmanGraph(toSplit, variables);
		Collection<? extends Set<? extends Term>> components = gg.getConnectedComponents();
		
		// For each component, we create the corresponding piece and we map the variables to the pieces
		for (Set<? extends Term> s : components) {
			InMemoryAtomSet atomSet = new DefaultInMemoryGraphStore();
			pieces.add(atomSet);
			
			// For each variable in the component, we bind it to its piece
			for (Term t : s) {
				termToPieces.put(t, atomSet);
			}
		}
		
		// Now, we put the atoms in the corresponding pieces
		CloseableIterator<Atom> it = toSplit.iterator();
		while (it.hasNext()) {
			Atom a = it.next();
			
			// If the atom is not totally grounded
			if (a.getVariables().stream().filter(v -> variables.contains(v)).count() > 0) {
				// We need to know one existential variable to add the atom in its corresponding piece
				Iterator<Variable> varsIt = a.getVariables().iterator();
				Variable v;
				do {
					v = varsIt.next();
				} while (varsIt.hasNext() && !variables.contains(v));
				
				// If there isn't any variable that is existential, then the atom has is own piece
				if (!variables.contains(v)) {
					InMemoryAtomSet atomSet = new DefaultInMemoryGraphStore();
					atomSet.add(a);
					pieces.add(atomSet);
				} else {
					// Now we add the atom into its piece
					termToPieces.get(v).add(a);
				}
			}
			// If the atom is totally grounded, it has its own piece
			else if (this.includeGroundedAtoms) {
				InMemoryAtomSet atomSet = new DefaultInMemoryGraphStore();
				atomSet.add(a);
				pieces.add(atomSet);
			}
		}
		
		// We finally, we return the set of pieces
		return pieces;
	}*/
}
