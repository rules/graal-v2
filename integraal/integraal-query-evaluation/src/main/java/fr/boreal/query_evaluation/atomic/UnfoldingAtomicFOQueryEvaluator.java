package fr.boreal.query_evaluation.atomic;

import com.google.common.collect.Iterators;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.model.ruleCompilation.api.RuleCompilation;
import fr.boreal.query_evaluation.FOQueryEvaluators;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

/**
 * Evaluates an Atomic FOQuery using the {@literal <=} condition given from a rule compilation
 * <br/>
 * Given a factbase F
 * an atomic query A
 * and unfold(A) = {A′, A′′, A′′′}
 * The ≲-homomorphism(A, F) ≡
 * homomorphism(A′, F)
 * homomorphism(A′′, F)
 * homomorphism(A′′′, F)
 * 
 * @author Florent Tornil
 *
 */
public class UnfoldingAtomicFOQueryEvaluator implements FOQueryEvaluator<Atom> {

	private final RuleCompilation compilation;
	private final FOQueryEvaluator<Atom> atomicEvaluator;

	/**
	 * Constructor using a compilation
	 * <br>
	 * Using the AtomicFOQueryEvaluator by default
	 * @param compilation the rule compilation to compute homomorphisms according to
	 */
	public UnfoldingAtomicFOQueryEvaluator(RuleCompilation compilation) {
		this(compilation, new AtomicFOQueryEvaluator());
	}

	/**
	 * Constructor using a compilation and a specific atomic query evaluator
	 * 
	 * @param compilation the rule compilation to compute homomorphisms according to
	 * @param atomicEvaluator the evaluator for unfolded queries
	 */
	public UnfoldingAtomicFOQueryEvaluator(RuleCompilation compilation, FOQueryEvaluator<Atom> atomicEvaluator) {
		this.compilation = compilation;
		this.atomicEvaluator = atomicEvaluator;
	}

	/**
	 * Proceeds by first unfolding the atomic queries, and then by answering every unfolding.
	 * 
	 */
	@Override
	public Iterator<Substitution> evaluate(FOQuery<? extends Atom> query, FactBase factbase, Collection<Variable> vars, Substitution preHomomorphism) {
		FOQuery<? extends Atom> handledQuery = query;
		Substitution handledPreHomomorphism = preHomomorphism;

		boolean needsHandling = !query.getVariableEqualities().getElements().isEmpty();
		if(needsHandling) {
			Optional<? extends Pair<? extends FOQuery<? extends Atom>, Substitution>> equalityHandledParameters = FOQueryEvaluators.handleEqualities(query, preHomomorphism);
			if(equalityHandledParameters.isEmpty()) {
				return fr.lirmm.graphik.util.stream.Iterators.emptyIterator();
			}
			handledQuery = equalityHandledParameters.get().getLeft();
			handledPreHomomorphism = equalityHandledParameters.get().getRight();
		}

		Iterator<Substitution> allResults = null;
		Set<Pair<Atom, Substitution>> unfoldings = compilation.unfold(handledQuery.getFormula());
		for(Pair<Atom, Substitution> unfolding : unfoldings) {
			FOQuery<Atom> newQuery = FOQueryFactory.instance().createOrGetQuery(
					unfolding.getLeft(),
					handledQuery.getAnswerVariables());
			// As the substitution from the unfoldings is always empty, we "safely" ignore it
			Iterator<Substitution> result = this.atomicEvaluator.homomorphism(newQuery, factbase, handledPreHomomorphism);
			if(allResults == null) {
				allResults = result;
			} else {
				allResults = Iterators.concat(allResults, result);
			}
		}

		if(needsHandling) {
			allResults = new FOQueryEvaluators.RebuildAnswerIterator(allResults, query);
		}
		return postprocessResult(allResults, vars) ;
	}

}
