package fr.boreal.query_evaluation.union;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.impl.UnionFOQuery;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.model.queryEvaluation.api.QueryEvaluator;
import fr.boreal.query_evaluation.atomic.AtomicFOQueryEvaluator;
import fr.boreal.query_evaluation.conjunction.backtrack.BacktrackEvaluator;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.query_evaluation.negation.NegationFOQueryEvaluator;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

/**
 * Evaluates a {@link UnionFOQuery} by making the union of the results;
 * <br>
 * The intersection of two substitutions is a substitution from the variables of
 *  the two substitutions An intersection is considered correct if for
 * each variable that appear in both substitutions, the associated terms are
 * equals
 */
public class UnionFOQueryEvaluator implements QueryEvaluator<UnionFOQuery> {

	/**
	 * Query evaluator used to evaluate the sub-queries
	 */
	private final FOQueryEvaluator<FOFormula> evaluator;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new evaluator using the given evaluator to evaluate sub-queries
	 * 
	 * @param evaluator to evaluate sub-queries
	 */
	public UnionFOQueryEvaluator(FOQueryEvaluator<FOFormula> evaluator) {
		this.evaluator = evaluator;
	}

	
	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	
	/**
	 * The default instance is used to give a default behavior to the evaluator when
	 * one doesn't want to configure it.
	 * @return the default instance of the evaluator
	 */
	public static UnionFOQueryEvaluator defaultInstance() {
		return new UnionFOQueryEvaluator(GenericFOQueryEvaluator.defaultInstance());
	}

	
	@Override
	public Iterator<Substitution> evaluate(UnionFOQuery query, FactBase factbase, Collection<Variable> variablesThatMustBeMappedToConstants, Substitution preHomomorphism) {
		Collection<Substitution> results = new HashSet<>();
		for (FOQuery<? extends FOFormula> subquery : query.getQueries()) {
			Iterator<Substitution> res = this.evaluator.evaluate(subquery, factbase, preHomomorphism);
			res.forEachRemaining(results::add);
		}
		return postprocessResult(results.iterator(), variablesThatMustBeMappedToConstants);
	}

}
