package fr.boreal.query_evaluation.conjunction.backtrack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

import com.google.common.collect.Iterators;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;

import java.util.*;
import java.util.function.Predicate;

/**
 * First simple implementation of the {@link SolutionManager} using maps to
 * index results.
 */
public class SimpleSolutionManager implements SolutionManager {

	/**
	 * Store the current solution
	 */
	protected Substitution currentSubstitution;

	/**
	 * Index solutions by level
	 */
	protected final Map<Integer, Iterator<Substitution>> substitutionsByLevel;

	protected final Substitution initialSubstitution;

	/**
	 * Index assigned variables by level
	 */
	protected final Map<Integer, Collection<Variable>> variableByLevel;

	protected Predicate<Substitution> filterSubstitution = substitution -> true; //always true predicate

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new solution manager with default objects
	 */
	public SimpleSolutionManager() {
		this(new SubstitutionImpl());
	}

	/**
	 * Creates a new solution manager with default objects
	 */
	public SimpleSolutionManager(Substitution initialSubstitution) {
		this.substitutionsByLevel = new HashMap<>();
		this.variableByLevel = new HashMap<>();
		this.currentSubstitution = new SubstitutionImpl();
		this.initialSubstitution = initialSubstitution;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	public Substitution getCurrentSolution() {
		return this.currentSubstitution;
	}

	/**
	 * Try to use the next solution for the given level. Recursively iterate over
	 * the solutions for the given level if needed.
	 * 
	 * @return true iff there is a next solution for this level
	 */
	public boolean next(int level) {

		// remove entries from the current substitution for variables defined after the
		// given level
		this.currentSubstitution = this.currentSubstitution.limitedTo(this.variablesBeforeLevel(level));
		this.currentSubstitution = this.currentSubstitution.merged(this.initialSubstitution).orElseThrow();

		// get the next solution for the given level
		Iterator<Substitution> next_result_it = this.substitutionsByLevel.get(level);
		while (next_result_it.hasNext()) {
			// merge the current solution with the new one
			Optional<Substitution> merged_substitutions = this.currentSubstitution.merged(next_result_it.next());
			if (merged_substitutions.isPresent()) {
				if (filterSubstitution.test(merged_substitutions.get())) {
					this.currentSubstitution = merged_substitutions.get();
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Adds new results for the given level.
	 *
	 * @param level   The level to add results to
	 * @param results An iterator of substitutions to add
	 */
	public void add(int level, Iterator<Substitution> results) {
		// Delete old results from the given level
		Collection<Variable> current_vars = new HashSet<>(this.currentSubstitution.keys());
		for (Variable v : current_vars) {
			if (!this.variablesBeforeLevel(level).contains(v)) {
				currentSubstitution.remove(v);
			}
		}

		if (results.hasNext()) {
			// Retrieve the first substitution
			Substitution firstSubstitution = results.next();

			// Update indexes with the keys from the first substitution
			this.variableByLevel.put(level, firstSubstitution.keys());

			// Create a new iterator that concatenates the first substitution with the rest of the iterator
			Iterator<Substitution> concatenatedIterator = Iterators.concat(
					Iterators.singletonIterator(firstSubstitution), results);

			this.substitutionsByLevel.put(level, concatenatedIterator);
		}
	}

	/////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////

	/**
	 * @return a collection of variables assigned before the given level
	 */
	protected Collection<Variable> variablesBeforeLevel(int level) {
		Collection<Variable> vars = new ArrayList<>();
		for (int i = 0; i < level; i++) {
			vars.addAll(this.variableByLevel.get(i));
		}
		return vars;
	}

}
