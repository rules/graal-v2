package fr.boreal.query_evaluation.negation;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaNegation;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Evaluates a negation by negating the result of the sub query.
 */
public class NegationFOQueryEvaluator implements FOQueryEvaluator<FOFormulaNegation> {

	/**
	 * Query evaluator used to evaluate the sub-queries
	 */
	private final FOQueryEvaluator<FOFormula> evaluator;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new evaluator using the given evaluator to evaluate sub-queries
	 * 
	 * @param evaluator to evaluate sub-queries
	 */
	public NegationFOQueryEvaluator(FOQueryEvaluator<FOFormula> evaluator) {
		this.evaluator = evaluator;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	/**
	 * Evaluates the inner-query inside the negated part ; then negates the result
	 * of the inner query. Returns an empty iterator if the inner-query has a
	 * result. Returns an iterator with an empty substitution if the inner-query has
	 * no result.
	 */
	@Override
	public Iterator<Substitution> evaluate(FOQuery<? extends FOFormulaNegation> query, FactBase factbase, Collection<Variable> variablesThatMustBeMappedToConstants, Substitution preHomomorphism) {
		
		FOQuery<? extends FOFormula> sub_query = FOQueryFactory.instance().createOrGetQuery(
				query.getFormula().element(),
				query.getAnswerVariables(),
				query.getVariableEqualities());

		boolean has_answer = this.evaluator.existHomomorphism(sub_query, factbase, preHomomorphism);
		if (!has_answer) {
			return List.<Substitution>of(new SubstitutionImpl()).iterator();
		} else {
			return Collections.emptyIterator();
		}
	}

}
