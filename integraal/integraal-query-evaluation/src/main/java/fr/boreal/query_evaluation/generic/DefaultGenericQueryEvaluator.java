package fr.boreal.query_evaluation.generic;

import java.util.Collection;
import java.util.Iterator;

import fr.boreal.model.kb.api.DatalogDelegable;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.query.impl.UnionFOQuery;
import fr.boreal.model.queryEvaluation.api.QueryEvaluator;
import fr.boreal.query_evaluation.union.UnionFOQueryEvaluator;
import fr.lirmm.boreal.util.validator.query.EvaluableQueryValidator;

/**
 * DefaultGenericQueryEvaluator is a default implementation of the
 * {@link QueryEvaluator} interface. It handles the evaluation of various query
 * types over a given {@link FactBase}.
 * 
 * <p>
 * This evaluator supports:
 * <ul>
 * <li>First-order/Formula queries ({@link FOQuery}) using
 * {@link GenericFOQueryEvaluator}</li>
 * <li>Union of first-order queries ({@link UnionFOQuery}) using
 * {@link UnionFOQueryEvaluator}</li>
 * </ul>
 * Unsupported query types will result in an {@link IllegalArgumentException}.
 * 
 */
public class DefaultGenericQueryEvaluator implements QueryEvaluator<Query> {

	/**
	 * The default instance is used to give a default behavior to the evaluator when
	 * one doesn't want to configure it.
	 * 
	 * @return the default instance of the evaluator
	 */
	public static DefaultGenericQueryEvaluator defaultInstance() {
		return new DefaultGenericQueryEvaluator();
	}

	/**
	 * Evaluates the given query over the specified fact base, applying a
	 * pre-existing substitution and ensuring that specified variables are mapped to
	 * constants.
	 *
	 * @param query                                The query to evaluate.
	 * @param factbase                             The fact base used for
	 *                                             evaluation.
	 * @param variablesThatMustBeMappedToConstants A collection of variables that
	 *                                             must be mapped to constants.
	 * @param preHomomorphism                      A pre-existing substitution to be
	 *                                             applied during evaluation.
	 * @return An iterator over the resulting {@link Substitution} objects.
	 * @throws IllegalArgumentException If the query type is unsupported.
	 */
	public Iterator<Substitution> evaluate(Query query, FactBase factbase,
			Collection<Variable> variablesThatMustBeMappedToConstants, Substitution preHomomorphism) {

		if(!EvaluableQueryValidator.check(query)){
			throw new IllegalArgumentException("unsupported query " + query);
		}

		var cq_evaluator = switch (factbase) {
		// use an evaluator for dbms if possible
		case DatalogDelegable dbms -> new FOQueryEvaluatorWithDBMSDelegation();
		// otherwise the generic one
		default -> GenericFOQueryEvaluator.defaultInstance();
		};

		
		//now switch according to the query
		return switch (query) {
		case FOQuery<?> cq ->
			cq_evaluator.evaluate(cq, factbase, variablesThatMustBeMappedToConstants, preHomomorphism);

		case UnionFOQuery ucq -> UnionFOQueryEvaluator.defaultInstance().evaluate(ucq, factbase,
				variablesThatMustBeMappedToConstants, preHomomorphism);

		default -> throw new IllegalArgumentException("unsupported query type : " + query.getClass());

		};
	}

}
