package fr.boreal.query_evaluation.conjunction.backtrack.isomorphism;

import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.query_evaluation.conjunction.backtrack.SimpleSolutionManager;

/**
 * 
 * Solution Manager that accepts only invertible substitutions (injective + no map to constants).
 * 
 */
public class InvertibleSolutionManager extends SimpleSolutionManager {

	public InvertibleSolutionManager() {
		super();
		filterSubstitution = Substitution::isInvertible;
	}

}
