package fr.boreal.query_evaluation;

import fr.boreal.model.formula.FOFormulas;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.lirmm.boreal.util.FOQueries;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;

public class FOQueryEvaluators {

    /**
     * Handles the equalities of the query
     * <p>
     * Equalities are handled by applying them which means :
     * <ul>
     *     <li>Renaming the query's formula with the representative of each equality class</li>
     *     <li>Removing from the query the answer variables that have been renamed</li>
     *     <li>Adding {X:Y} to the pre-homomorphism iff X=X' & X is a representative and {X':Y} is in the pre-homomorphism</li>
     * </ul>
     *
     * After handling the equalities, one need to reconstruct the correct answers to the initial query by adding back the answers variables that have been removed according to equalities
     *
     * @param query the initial query, with equalities
     * @param preHomomorphism the pre-homomorphism
     * @return the query (without equalities) renamed according to the equalities and the updated pre-homomorphism <br/>
     * or an empty optional iff the query and the pre-homomorphism are not consistent
     */
    public static <Formula extends FOFormula> Optional<Pair<FOQuery<Formula>, Substitution>> handleEqualities(FOQuery<Formula> query, Substitution preHomomorphism) {
        if(!FOQueries.isConsistent(query, preHomomorphism)) {
            return Optional.empty();
        }

        Substitution renaming = query.getVariableEqualities().getAssociatedSubstitution(query).orElseThrow();

        Formula renamedFormula = FOFormulas.createImageWith(query.getFormula(), renaming);
        Collection<Variable> newAnsVar = new ArrayList<>(query.getAnswerVariables());
        newAnsVar.removeIf(v -> !renamedFormula.getVariables().contains(v));
        FOQuery<Formula> renamedQuery = FOQueryFactory.instance().createOrGetQuery(renamedFormula, newAnsVar);

        Substitution renamedPreHomomorphism = new SubstitutionImpl(preHomomorphism);
        for(Variable v : preHomomorphism.keys()) {
            Term renamedV = renaming.createImageOf(v);
            if(renamedV != v && renamedV.isVariable()) {
                renamedPreHomomorphism.add((Variable) renamedV, renamedPreHomomorphism.createImageOf(v));
                renamedPreHomomorphism.remove(v);
            }
        }

        return Optional.of(Pair.of(renamedQuery, renamedPreHomomorphism));
    }

    /**
     * Rebuilds the answer to a query with equalities according to an answer of the query whose equalities have been handled.
     * <p>
     * Requirements : the answer have been computed on <code>handleEqualities(query, h')</code>.
     * <br>
     * Where <code>h'</code> is a subset (potentially empty) of the given answer
     *
     * @param query the query with equalities
     * @param answer the answer of the query whose equalities have been handled
     * @return the answer to the query according to the given answer and the query equalities
     */
    public static Substitution rebuildAnswer(FOQuery<?> query, Substitution answer) {
    Substitution completeAnswer = new SubstitutionImpl(answer);
        for(Variable v : query.getAnswerVariables()) {
            if(!completeAnswer.keys().contains(v)) {
                completeAnswer.add(v, completeAnswer.createImageOf(query.getVariableEqualities().getRepresentative(v)));
            }
        }
        return completeAnswer;
    }

    public static class RebuildAnswerIterator implements Iterator<Substitution> {

        private final Iterator<Substitution> partialAnswers;
        private final FOQuery<?> initialQuery;

        public RebuildAnswerIterator(Iterator<Substitution> partialAnswers, FOQuery<?> initialQuery) {
            this.partialAnswers = partialAnswers;
            this.initialQuery = initialQuery;
        }

        @Override
        public boolean hasNext() {
            return this.partialAnswers.hasNext();
        }

        @Override
        public Substitution next() {
            return FOQueryEvaluators.rebuildAnswer(this.initialQuery, this.partialAnswers.next());
        }
    }
}
