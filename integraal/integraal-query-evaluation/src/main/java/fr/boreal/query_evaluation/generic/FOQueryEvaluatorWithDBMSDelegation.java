package fr.boreal.query_evaluation.generic;

import java.util.Collection;
import java.util.Iterator;

import fr.boreal.model.formula.api.FOFormula;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.boreal.model.kb.api.DatalogDelegable;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.lirmm.boreal.util.object_analyzer.ObjectAnalizer;

/**
 *
 * Evaluates a {@link FOQuery} by using the an (external) data management
 * system, if this is one is present.
 *
 */
public class FOQueryEvaluatorWithDBMSDelegation implements FOQueryEvaluator<FOFormula> {

	private static final Logger LOG = LoggerFactory.getLogger(FOQueryEvaluatorWithDBMSDelegation.class);

	private static final FOQueryEvaluator<FOFormula> default_instance = new FOQueryEvaluatorWithDBMSDelegation();

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * @return the default configuration
	 */
	public static FOQueryEvaluator<FOFormula> defaultInstance() {
		return default_instance;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	/**
	 * If using a DBMS, we ask the DBMS storage to evaluate the whole query If
	 * anything throws an exception, we use the {@link GenericFOQueryEvaluator} as a
	 * fallback
	 */
	@Override
	public Iterator<Substitution> evaluate(FOQuery<? extends FOFormula> query,
										   FactBase factbase,
										   Collection<Variable> variablesThatMustBeMappedToConstants,
										   Substitution preHomomorphism) {
		LOG.debug("evaluating {} ", ObjectAnalizer.digest(query));

		try {
			if (factbase instanceof DatalogDelegable dbms) {
				return postprocessResult(dbms.delegate(query), variablesThatMustBeMappedToConstants);
			}
		} catch (Exception e) {
			LOG.warn("An error occured during delegation of the query, default method will be used instead.");
		}

		return postprocessResult(GenericFOQueryEvaluator.defaultInstance().evaluate(query, factbase), variablesThatMustBeMappedToConstants);
	}

	/**
	 * If using a DBMS, we ask the DBMS storage to evaluate the whole query If
	 * anything throws an exception, we use the {@link GenericFOQueryEvaluator} as a
	 * fallback
	 */
	@Override
	public long countAnswers(FOQuery<? extends FOFormula> query, FactBase factbase) {
		LOG.debug("evaluating {} ", ObjectAnalizer.digest(query));

		Iterator<Substitution> result;
		try {
			if (factbase instanceof DatalogDelegable dbms) {
				result = dbms.delegate(query, true);
				return extractCount(result);
			}
		} catch (Exception e) {
			LOG.warn("An error occured during delegation of the query, default method will be used instead.");
		}
		return -1;
	}

	private long extractCount(Iterator<Substitution> result) {
		if (result == null || !result.hasNext()) {
			LOG.error("could not compute result size");
			throw new RuntimeException(
					String.format(
							"[%s::extractCount] Could not compute result size.",
							this.getClass()));
		}

		// we assume the counting answers substitution has only one result
		Substitution res = result.next();
		var answer_count = res.rangeTerms().stream().findAny();
		return Long.parseLong(answer_count.get().toString());
	}

}
