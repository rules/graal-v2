package fr.boreal.query_evaluation.conjunction.backtrack.isomorphism;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.query_evaluation.conjunction.backtrack.BacktrackEvaluator;

import java.util.*;
import java.util.stream.Collectors;


/**
 * Enumerates substitutions witnessing a graph-subgraph isomorphism.
 */

public class SubGraphIsomorphismBacktrackEvaluator extends BacktrackEvaluator {

	public SubGraphIsomorphismBacktrackEvaluator(FOQueryEvaluator<FOFormula> evaluator) {
		super(evaluator);
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	/**
	 * Enumerates substitutions witnessing a graph-subgraph isomorphism.
	 */
	@Override
	public Iterator<Substitution> evaluate(FOQuery<? extends FOFormulaConjunction> query, FactBase factbase, Collection<Variable> vars, Substitution preHomomorphism) {
		if (noPossibleSolution(query, factbase)) {
			return Collections.emptyIterator();
		}
		return new BacktrackIteratorIso(evaluator, query, factbase, vars, preHomomorphism);
	}

	/////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////

	/**
	 * 
	 * @param factbase
	 * @param query
	 * @return true if a condition ruling out any solution to the subgraph
	 *         isomorphism is met
	 */
	protected boolean noPossibleSolution(FOQuery<? extends FOFormulaConjunction> query, FactBase factbase) {
		// no solution if there are not enough image atoms in the factbase
		return query.getFormula().asAtomSet().size() > factbase.size();
	}

	/**
	 * The BacktrackIterator is used to redefine the iterator return as an answer
	 * to the query. It computes results only when needed. Calling hasNext()
	 * effectively computes the next answer.
	 */
	public class BacktrackIteratorIso extends BacktrackIterator {

		private final Set<Atom> factbaseAtomsPreviouslyChecked = new LinkedHashSet<>();
		private final Set<Atom> queryAtomsPreviouslyChecked = new LinkedHashSet<>();

		public BacktrackIteratorIso(FOQueryEvaluator<FOFormula> evaluator, FOQuery<? extends FOFormulaConjunction> query, FactBase factbase,
				Collection<Variable> variablesThatMustBeMappedToConstants, Substitution preHomomorphism) {
			super(evaluator, query, factbase, variablesThatMustBeMappedToConstants, preHomomorphism);
			solutionManager = new InvertibleSolutionManager();
		}

		protected boolean solutionFound() {
			Substitution currentSolution = solutionManager.getCurrentSolution();
			Set<Atom> factbaseAtomsCovered = computeCoveredFactbaseAtoms(currentSolution);
			Set<Atom> queryAtomsCovered = computeCoveredQueryAtoms(currentSolution);

			boolean sizesMatch = factbaseAtomsCovered.size() == queryAtomsCovered.size();
			if (!sizesMatch) {
				return false;
			}

			updatePreviouslyCheckedAtoms(factbaseAtomsCovered, queryAtomsCovered);

			return hasCoveredAllQueryAtoms();
		}

		private Set<Atom> computeCoveredFactbaseAtoms(Substitution sub) {
			return factbase.getAtoms()
					.filter(atom -> areAtomTermsCoveredBySubstitution(atom, sub, factbaseAtomsPreviouslyChecked, false))
					.collect(Collectors.toSet());
		}

		private Set<Atom> computeCoveredQueryAtoms(Substitution sub) {
			return query.getFormula().asAtomSet().stream()
					.filter(atom -> areAtomTermsCoveredBySubstitution(atom, sub, queryAtomsPreviouslyChecked, true))
					.collect(Collectors.toSet());
		}

		/**
		 * 
		 * @param atom
		 * @param sub
		 * @param previouslyCheckedAtoms
		 * @param rangeTerms             if terms to be considered are range terms ;
		 *                               true if terms to be considered are image
		 *                               (variables!)
		 * @return
		 */
		private boolean areAtomTermsCoveredBySubstitution(Atom atom, Substitution sub, Set<Atom> previouslyCheckedAtoms,
				boolean rangeTerms) {
			return !previouslyCheckedAtoms.contains(atom) && Arrays.stream(atom.getTerms())
					.allMatch(rangeTerms ? sub.rangeTerms()::contains : sub.keys()::contains);
		}

		private void updatePreviouslyCheckedAtoms(Set<Atom> factbaseAtoms, Set<Atom> queryAtoms) {
			factbaseAtomsPreviouslyChecked.addAll(factbaseAtoms);
			queryAtomsPreviouslyChecked.addAll(queryAtoms);
		}

		private boolean hasCoveredAllQueryAtoms() {
			return this.level == this.query.getFormula().getSubElements().size();
		}

	}

}
