package fr.boreal.query_evaluation.conjunction.backtrack.isomorphism;

import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.query_evaluation.conjunction.backtrack.SimpleSolutionManager;

/**
 * 
 * Solution Manager that accepts only injective substitutions.
 * 
 */
public class InjectiveSolutionManager extends SimpleSolutionManager {

	public InjectiveSolutionManager() {
		super();
		filterSubstitution = Substitution::isInjective;
	}
}
