package fr.boreal.query_evaluation.conjunction.backtrack;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.ComputedAtom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.query.api.FOQuery;

import java.util.List;
import java.util.stream.Collectors;

/**
 * This (static) scheduler gives the next part to evaluate by keeping the parts containing computed atoms and negation at the end.
 * This make it sure that the current solution contains the elements that are needed to evaluate the given part (iff the initial conjunction is correct)
 */
public class SimpleScheduler implements Scheduler {

	private final List<FOFormula> order;

	/**
	 * Creates a new scheduler over the given query
	 * @param query the query to schedule
	 */
	public SimpleScheduler(FOQuery<? extends FOFormulaConjunction> query) {
		this.order = query.getFormula().getSubElements().stream().sorted((a,b) -> {

			boolean aNegation = a.isNegation();
			boolean aAtomic = a.isAtomic();
			boolean aComputed = aAtomic && a instanceof ComputedAtom;
			boolean aFunctions = false;
			int aConstantCount = 0;
			if (aAtomic) {
				Atom aa = ((Atom) a);
				for (Term t : aa.getTerms()) {
					if (t.isFunctionalTerm()) {
						aFunctions = true;
					}
					if (t.isConstant()) {
						aConstantCount++;
					}
				}
			}

			boolean bNegation = b.isNegation();
			boolean bAtomic = b.isAtomic();
			boolean bComputed = bAtomic && b instanceof ComputedAtom;
			boolean bFunctions = false;
			int bConstantCount = 0;
			if (bAtomic) {
				Atom bb = ((Atom) b);
				for (Term t : bb.getTerms()) {
					if (t.isFunctionalTerm()) {
						bFunctions = true;
					}
					if (t.isConstant()) {
						bConstantCount++;
					}
				}
			}

			// Prioritize computed atoms last
			if (aComputed != bComputed) {
				return aComputed ? 1 : -1;
			}

			// Prioritize atoms with functional terms last
			if (aFunctions != bFunctions) {
				return aFunctions ? 1 : -1;
			}

			// Prioritize negations last
			if (aNegation != bNegation) {
				return aNegation ? 1 : -1;
			}

			// For atomic formulas, prioritize those with more constants
			if (aAtomic && bAtomic) {
				return Integer.compare(bConstantCount, aConstantCount);
			}

			// Ensure atomic formulas come before non-atomic ones
			if (aAtomic != bAtomic) {
				return aAtomic ? -1 : 1;
			}

			return 0;

		}).collect(Collectors.toList());
	}

	@Override
	public FOFormula next(int level, Substitution currentSolution) {
		return order.get(level);
	}

	@Override
	public boolean hasNext(int level) {
		return order.size() >= level;
	}

}
