package fr.boreal.query_evaluation.conjunction;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;

import java.util.*;

/**
 * Evaluates a conjunction by using intersection to aggregate the
 * result of each sub-query
 * <br/>
 * The intersection of two substitutions is a substitution from the variables of
 * the two substitutions An intersection is considered correct if for
 * each variable that appear in both substitutions, the associated terms are
 * equals
 */
public class IntersectFOQueryConjunctionEvaluator implements FOQueryEvaluator<FOFormulaConjunction> {

	/**
	 * Query evaluator used to evaluate the sub-queries
	 */
	private final FOQueryEvaluator<FOFormula> evaluator;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new evaluator using the given evaluator to evaluate sub-queries
	 *
	 * @param evaluator to evaluate sub-queries
	 */
	public IntersectFOQueryConjunctionEvaluator(FOQueryEvaluator<FOFormula> evaluator) {
		this.evaluator = evaluator;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public Iterator<Substitution> evaluate(FOQuery<? extends FOFormulaConjunction> query, FactBase factbase, Collection<Variable> vars, Substitution preHomomorphism) {

		Iterator<Substitution> result = Collections.emptyIterator();

		// Compute all the substitutions
		for (FOFormula subFormula : query.getFormula().getSubElements()) {
			FOQuery<? extends FOFormula> subquery_opt = FOQueryFactory.instance().createOrGetQuery(subFormula, query.getAnswerVariables(), query.getVariableEqualities());
			Iterator<Substitution> current_subst = evaluator.homomorphism(subquery_opt, factbase, preHomomorphism);
			result = intersectAll(result, current_subst);
		}

		return postprocessResult(result, vars);
	}

	/////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////

	/**
	 * Computes and returns the intersection of sets of substitutions
	 * // TODO: Add lazy behavior
	 */
	private Iterator<Substitution> intersectAll(Iterator<Substitution> a, Iterator<Substitution> b) {
		Collection<Substitution> results = new ArrayList<>();

		// because you can't start over the iterator
		Collection<Substitution> b_save = new ArrayList<>();
		while (b.hasNext()) {
			b_save.add(b.next());
		}

		while (a.hasNext()) {
			Substitution current_a = a.next();
			b = b_save.iterator();
			while (b.hasNext()) {
				Substitution current_b = b.next();
				Optional<Substitution> res = intersect(current_a, current_b);
				res.ifPresent(results::add);
			}
		}

		return results.iterator();
	}

	/**
	 * @return a {@link Substitution} being the intersection of the two substitution
	 *         given or an empty optional if the two substitutions cannot be
	 *         intersected
	 */
	private Optional<Substitution> intersect(Substitution a, Substitution b) {
		Substitution result = new SubstitutionImpl(a);
		for (Variable current_variable : b.keys()) {
			Term b_image = b.createImageOf(current_variable);
			if (result.keys().contains(current_variable)) {
				Term current_image = result.createImageOf(current_variable);
				if (!current_image.equals(b_image)) {
					return Optional.empty();
				}
			} else {
				result.add(current_variable, b_image);
			}
		}
		return Optional.of(result);
	}

}
