package fr.boreal.query_evaluation.conjunction.backtrack.isomorphism;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;

import java.util.Iterator;
import java.util.Set;

/**
 * Checks if a conjunctive query is isomorphic to a factbase 
 */
public class BacktrackIsomorphismChecker extends SubGraphIsomorphismBacktrackEvaluator {

	public BacktrackIsomorphismChecker(FOQueryEvaluator<FOFormula> evaluator) {
		super(evaluator);
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	public Iterator<Substitution> checkIsomorphism(FOQuery<? extends FOFormulaConjunction> query, FactBase factbase) {
		return new BacktrackIteratorIso(evaluator, query, factbase, Set.of(), new SubstitutionImpl());
	}

	/**
	 * 
	 * @param factbase
	 * @param query
	 * @return true if a condition ruling out any solution to the subgraph
	 *         isomorphism is met
	 */
	protected boolean noPossibleSolution(FOQuery<? extends FOFormulaConjunction> query, FactBase factbase) {
		// no solution if source and target atoms have different cardinality
		return query.getFormula().asAtomSet().size() != factbase.size();
	}

}
