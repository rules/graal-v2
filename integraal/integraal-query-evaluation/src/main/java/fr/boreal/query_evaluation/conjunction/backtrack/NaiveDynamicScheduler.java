package fr.boreal.query_evaluation.conjunction.backtrack;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.api.FOFormulaDisjunction;
import fr.boreal.model.formula.api.FOFormulaNegation;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.*;
import fr.boreal.model.query.api.FOQuery;

import java.util.*;
import java.util.stream.Collectors;

public class NaiveDynamicScheduler implements Scheduler {

    private final List<FOFormula> notOrdered;
    private final List<FOFormula> ordered;
    private final FactBase factBase;
    private final Map<FormulaWrapper, Set<Variable>> sharedVariables;
    private final Map<FormulaWrapper, Boolean> containsFunctionalTerm;
    private final Map<FormulaWrapper, Set<Variable>> functionalTermVariables;

    /**
     * Creates a new scheduler over the given query and fact base
     * @param query the query to schedule
     * @param factBase the fact base to consider
     */
    public NaiveDynamicScheduler(FOQuery<? extends FOFormulaConjunction> query, FactBase factBase) {
        this.factBase = factBase;
        this.notOrdered = new ArrayList<>();
        this.ordered = new ArrayList<>();
        this.sharedVariables = new HashMap<>();
        this.containsFunctionalTerm = new HashMap<>();
        this.functionalTermVariables = new HashMap<>();

        // Populate notOrdered list with initial formulas
        for (FOFormula formula : query.getFormula().getSubElements()) {
            notOrdered.add(formula);
            FormulaWrapper formulaWrapper = new FormulaWrapper(formula);
            sharedVariables.put(formulaWrapper, computeSharedVariables(formula, query.getFormula().getSubElements()));
            containsFunctionalTerm.put(formulaWrapper, computeContainsFunctionalTerm(formula));
            if (containsFunctionalTerm.get(formulaWrapper)) {
                functionalTermVariables.put(formulaWrapper, computeFunctionalTermVariables(formula));
            }
        }

        // Perform static sorting
        staticSort();
    }

    private void staticSort() {
        this.notOrdered.sort((b, a) -> {
            boolean aAtomic = a.isAtomic();
            boolean bAtomic = b.isAtomic();

            if (aAtomic && bAtomic) {
                // Prioritize computed atoms last
                boolean aComputed = a instanceof ComputedAtom;
                boolean bComputed = b instanceof ComputedAtom;
                if (aComputed != bComputed) {
                    return aComputed ? 1 : -1;
                }

                // Prioritize atoms with functional terms last
                boolean aFunctions = containsFunctionalTerm((Atom) a);
                boolean bFunctions = containsFunctionalTerm((Atom) b);
                if (aFunctions != bFunctions) {
                    return aFunctions ? 1 : -1;
                }

                // For other atomic formulas, prioritize those with more constants
                int bConstantCount = countConstants(b);
                int aConstantCount = countConstants(a);
                return Integer.compare(bConstantCount, aConstantCount);
            }

            // Prioritize negations last
            boolean aNegation = a.isNegation();
            boolean bNegation = b.isNegation();
            if (aNegation != bNegation) {
                return aNegation ? 1 : -1;
            }

            // Ensure atomic formulas come before non-atomic ones
            if (aAtomic != bAtomic) {
                return aAtomic ? -1 : 1;
            }

            return 0;
        });
    }

    private boolean computeContainsFunctionalTerm(FOFormula formula) {
        return switch (formula) {
            case Atom atom -> Arrays.stream(atom.getTerms()).anyMatch(Term::isEvaluableFunction);
            case FOFormulaNegation neg -> computeContainsFunctionalTerm(neg.element());
            case FOFormulaConjunction conj -> conj.getSubElements().stream().anyMatch(this::computeContainsFunctionalTerm);
            case FOFormulaDisjunction disj -> disj.getSubElements().stream().anyMatch(this::computeContainsFunctionalTerm);
            default -> throw new IllegalStateException("Unexpected value: " + formula);
        };
    }

    private Set<Variable> computeFunctionalTermVariables(FOFormula formula) {
        return switch (formula) {
            case Atom atom -> {
                Set<Variable> variables = new HashSet<>();
                for (Term term : atom.getTerms()) {
                    if (term.isEvaluableFunction()) {
                        EvaluableFunction fterm = (EvaluableFunction) term;
                        variables.addAll(fterm.getVariables());
                    }
                }
                yield variables;
            }
            case FOFormulaNegation neg -> computeFunctionalTermVariables(neg.element());
            case FOFormulaConjunction conj -> conj.getSubElements().stream()
                    .flatMap(f -> this.computeFunctionalTermVariables(f).stream()).collect(Collectors.toSet());
            case FOFormulaDisjunction disj -> disj.getSubElements().stream()
                    .flatMap(f -> this.computeFunctionalTermVariables(f).stream()).collect(Collectors.toSet());
            default -> throw new IllegalStateException("Unexpected value: " + formula);
        };
    }

    private boolean containsFunctionalTerm(Atom atom) {
        //return Arrays.stream(atom.getTerms()).anyMatch(Term::isEvaluableFunction);
        return containsFunctionalTerm.get(new FormulaWrapper(atom));
    }

    private int countConstants(FOFormula formula) {
        return formula.getConstants().size();
    }

    @Override
    public FOFormula next(int level, Substitution currentSolution) {
        // Ensure previous levels are cleared
        while (level < ordered.size()) {
            notOrdered.addLast(ordered.removeLast());
        }

        if (notOrdered.size() == 1) {
            ordered.add(notOrdered.removeLast());
            return ordered.getLast();
        }

        // Prioritize evaluable computed atoms
        for (FOFormula formula : notOrdered) {
            if (formula instanceof ComputedAtom computedAtom) {
                if (isEvaluable(computedAtom, currentSolution)) {
                    notOrdered.remove(formula);
                    ordered.add(formula);
                    return formula;
                }
            } else if (formula instanceof FOFormulaNegation negation && negation.element() instanceof Atom atom) {
                if (atom instanceof ComputedAtom computedAtom) {
                    if (isEvaluable(computedAtom, currentSolution)) {
                        notOrdered.remove(formula);
                        ordered.add(formula);
                        return formula;
                    }
                } else {
                    if (containsFunctionalTerm(atom) && !areFunctionalTermsEvaluable(atom, currentSolution)) {
                        continue;
                    }

                    Optional<Long> estimate = factBase.estimateMatchCount(atom, currentSolution);
                    if (estimate.isPresent() && estimate.get() > 0) {
                        notOrdered.remove(formula);
                        ordered.add(formula);
                        return formula;
                    }
                }
            }
        }

        // Dynamic sorting based on fact base information
        FOFormula bestFormula = null;
        long bestEstimate = Long.MAX_VALUE;
        for (FOFormula formula : notOrdered) {
            if (formula instanceof Atom atom && !(atom instanceof ComputedAtom)) {
                if (containsFunctionalTerm(atom) && !areFunctionalTermsEvaluable(atom, currentSolution)) {
                    continue;
                }

                Optional<Long> estimate = factBase.estimateMatchCount(atom, currentSolution);

                if (estimate.isPresent() && estimate.get() == 0) {
                    notOrdered.remove(formula);
                    ordered.add(formula);
                    return formula;
                }

                if (!factBase.canPerformIndexedMatch(atom, currentSolution)) {
                    notOrdered.remove(formula);
                    ordered.add(formula);
                    return formula;
                }

                long currentEstimate = estimate.orElse(Long.MAX_VALUE);
                if (currentEstimate < bestEstimate) {
                    bestEstimate = currentEstimate;
                    bestFormula = formula;
                }
            }
        }

        if (bestFormula != null) {
            notOrdered.remove(bestFormula);
            ordered.add(bestFormula);
            return bestFormula;
        } else if (!notOrdered.isEmpty()) {
            bestFormula = notOrdered.removeLast();
            ordered.add(bestFormula);
            return bestFormula;
        } else {
            throw new NoSuchElementException("No more subqueries available.");
        }
    }

    @Override
    public boolean hasNext(int level) {
        return level < ordered.size() + notOrdered.size();
    }

    private boolean isEvaluable(ComputedAtom computedAtom, Substitution currentSolution) {
        Set<Variable> sharedVars = sharedVariables.get(new FormulaWrapper(computedAtom));
        Collection<Variable> solutionVars = currentSolution.keys();
        return solutionVars.containsAll(sharedVars);
    }

    private Set<Variable> computeSharedVariables(FOFormula formula, Collection<? extends FOFormula> allFormulas) {
        Set<Variable> allVariables = allFormulas.stream()
                .filter(f -> f != formula)
                .flatMap(f -> f.getVariables().stream())
                .collect(Collectors.toSet());
        return formula.getVariables().stream()
                .filter(allVariables::contains)
                .collect(Collectors.toSet());
    }

    private boolean areFunctionalTermsEvaluable(Atom atom, Substitution currentSolution) {
        Set<Variable> functionalVars = functionalTermVariables.get(new FormulaWrapper(atom));
        return currentSolution.keys().containsAll(functionalVars);
    }

    // Wrapper class to use default hashCode and equals based on object reference
    private record FormulaWrapper(FOFormula formula) {
        @Override
        public int hashCode() {
            return System.identityHashCode(formula);
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof FormulaWrapper other)) {
                return false;
            }

            return formula == other.formula;
        }
    }
}
