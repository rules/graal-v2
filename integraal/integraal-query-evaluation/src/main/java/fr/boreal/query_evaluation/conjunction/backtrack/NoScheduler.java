package fr.boreal.query_evaluation.conjunction.backtrack;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.query.api.FOQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * This (static) scheduler gives the next part to evaluate by keeping the given order.
 */
public class NoScheduler implements Scheduler {

	private final List<FOFormula> order;

	/**
	 * Creates a new scheduler over the given query
	 * @param query the query to schedule
	 */
	public NoScheduler(FOQuery<? extends FOFormulaConjunction> query) {
		this.order = new ArrayList<>(query.getFormula().getSubElements());
	}

	@Override
	public FOFormula next(int level, Substitution currentSolution) {
		return order.get(level);
	}

	@Override
	public boolean hasNext(int level) {
		return order.size() >= level;
	}

}
