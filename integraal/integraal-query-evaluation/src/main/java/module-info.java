/**
 * Module for query evaluation elements of InteGraal
 * 
 * @author Florent Tornil
 *
 */
module fr.boreal.query_evaluation {
	
	requires transitive fr.boreal.model;
	
	requires fr.boreal.storage;
	requires fr.boreal.io;
	requires transitive fr.lirmm.boreal.util;
	requires fr.boreal.configuration;
 
	
	requires com.google.common;
	requires java.sql;
	requires org.apache.commons.lang3;
	requires org.slf4j;
	requires org.hsqldb;

	exports fr.boreal.query_evaluation.atomic;
	exports fr.boreal.query_evaluation.conjunction;
	exports fr.boreal.query_evaluation.conjunction.backtrack;
	exports fr.boreal.query_evaluation.generic;
	exports fr.boreal.query_evaluation.negation;
    exports fr.boreal.query_evaluation.union;
	
}