package fr.boreal.test.query_evaluation;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.query.impl.UnionFOQuery;
import fr.boreal.query_evaluation.generic.DefaultGenericQueryEvaluator;
import fr.boreal.storage.natives.DefaultInMemoryAtomSet;

@RunWith(Parameterized.class)
class DefaultEvaluatorsTest {

	static Stream<Arguments> data() {
		final Predicate p2 = SameObjectPredicateFactory.instance().createOrGetPredicate("p2", 2);
		final Variable x = SameObjectTermFactory.instance().createOrGetVariable("x");
		final Constant a = SameObjectTermFactory.instance().createOrGetConstant("a");

		final Atom pax = new AtomImpl(p2, a, x);
		
		var factbase = new DefaultInMemoryAtomSet(pax);
		
		final FOQuery<?> cq = FOQueryFactory.instance()
				.createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(pax), List.of());
		final UnionFOQuery ucq = new UnionFOQuery(List.of(cq));

		return Stream.of(
				Arguments.of(cq, factbase, true),Arguments.of(ucq, factbase, true));
	}

	@DisplayName("Test Query Answering with DLGP input")
	@ParameterizedTest(name = "{index}: DLGP {0} should give {1}")
	@MethodSource("data")
	public void dlgpHomomorphismTest(Query q, FactBase f, boolean result) throws Exception {

		Iterator<Substitution> resultsub = DefaultGenericQueryEvaluator.defaultInstance().evaluate(q, f);

		Assert.assertEquals(result, resultsub.hasNext());
	}
}
