package fr.boreal.test.query_evaluation;

import java.util.List;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.LogicalFunctionalTerm;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.logicalElements.impl.functionalTerms.SpecializableLogicalFunctionalTermImpl;

public class TestData {

    // Predicates
    public static final Predicate p1 = SameObjectPredicateFactory.instance().createOrGetPredicate("p1", 1);
    public static final Predicate r1 = SameObjectPredicateFactory.instance().createOrGetPredicate("r1", 1);
    public static final Predicate p2 = SameObjectPredicateFactory.instance().createOrGetPredicate("p2", 2);
    public static final Predicate q2 = SameObjectPredicateFactory.instance().createOrGetPredicate("q2", 2);
    public static final Predicate r2 = SameObjectPredicateFactory.instance().createOrGetPredicate("r2", 2);
    public static final Predicate p3 = SameObjectPredicateFactory.instance().createOrGetPredicate("p3", 3);

    // Terms
    public static final Variable u = SameObjectTermFactory.instance().createOrGetVariable("u");
    public static final Variable v = SameObjectTermFactory.instance().createOrGetVariable("v");
    public static final Variable x = SameObjectTermFactory.instance().createOrGetVariable("x");
    public static final Variable y = SameObjectTermFactory.instance().createOrGetVariable("y");
    public static final Variable z = SameObjectTermFactory.instance().createOrGetVariable("z");

    public static final Constant a = SameObjectTermFactory.instance().createOrGetConstant("a");
    public static final Constant b = SameObjectTermFactory.instance().createOrGetConstant("b");
    public static final Constant c = SameObjectTermFactory.instance().createOrGetConstant("c");

    public static final LogicalFunctionalTerm fx = new SpecializableLogicalFunctionalTermImpl("f", List.of(x));
    public static final LogicalFunctionalTerm gx = new SpecializableLogicalFunctionalTermImpl("g", List.of(x));
    public static final LogicalFunctionalTerm fa = new SpecializableLogicalFunctionalTermImpl("f", List.of(a));
    public static final LogicalFunctionalTerm ga = new SpecializableLogicalFunctionalTermImpl("g", List.of(a));
    public static final LogicalFunctionalTerm gb = new SpecializableLogicalFunctionalTermImpl("g", List.of(b));

    // Atoms

    public static final Atom pxy = new AtomImpl(p2, x, y);
    public static final Atom qxy = new AtomImpl(q2, x, y);
    public static final Atom qyx = new AtomImpl(q2, y, x);
    public static final Atom pab = new AtomImpl(p2, a, b);
    public static final Atom paafa = new AtomImpl(p3, a, a, fa);
    public static final Atom pcbfa = new AtomImpl(p3, c, b, fa);
    public static final Atom pabfa = new AtomImpl(p3, a, b, fa);
    public static final Atom pabga = new AtomImpl(p3, a, b, ga);
    public static final Atom pabgx = new AtomImpl(p3, a, b, gx);
    public static final Atom pfa = new AtomImpl(p1, fa);
    public static final Atom pga = new AtomImpl(p1, ga);
    public static final Atom pgb = new AtomImpl(p1, gb);
    public static final Atom pfx = new AtomImpl(p1, fx);
    public static final Atom pgx = new AtomImpl(p1, gx);
    public static final Atom pfxgx = new AtomImpl(p2, fx,gx);
    public static final Atom pfagb = new AtomImpl(p2, fa,gb);
}
