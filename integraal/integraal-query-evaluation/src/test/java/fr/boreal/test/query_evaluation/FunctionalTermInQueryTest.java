package fr.boreal.test.query_evaluation;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.logicalElements.impl.PredicateImpl;
import fr.boreal.model.logicalElements.impl.VariableImpl;
import fr.boreal.model.partition.TermPartition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.query_evaluation.generic.DefaultGenericQueryEvaluator;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;

class FunctionalTermInQueryTest {


    /**
     * Test that the match on a view uses the list semantic
     */
    @Test
    public void MatchWithFunctionalTerms() {
        FOQuery<Atom> Q1 = FOQueryFactory.instance().createOrGetQuery(TestData.pabgx,
                List.of(), null);

        FactBase f1 = new SimpleInMemoryGraphStore(List.of(TestData.paafa, TestData.pcbfa, TestData.pabfa, TestData.pabga));

        Iterator<?> result = DefaultGenericQueryEvaluator.defaultInstance().homomorphism(Q1, f1);

        int resultCount = 0;
        while (result.hasNext()) {
            result.next();
            resultCount++;
        }
        Assertions.assertEquals(1, resultCount);
    }

    @Test
    public void MatchNoConstants() {
        FOQuery<Atom> Q1 = FOQueryFactory.instance().createOrGetQuery(TestData.pfx,
                List.of(), null);

        FactBase f1 = new SimpleInMemoryGraphStore(List.of(TestData.pfa));

        Iterator<?> result = DefaultGenericQueryEvaluator.defaultInstance().homomorphism(Q1, f1);

        int resultCount = 0;
        while (result.hasNext()) {
            result.next();
            resultCount++;
        }
        Assertions.assertEquals(1, resultCount);
    }


    @Test
    public void MatchNoConstantsCoherentHomomorphisms() {
        FOQuery<Atom> Q1 = FOQueryFactory.instance().createOrGetQuery(TestData.pfxgx,
                List.of(), null);

        FactBase f1 = new SimpleInMemoryGraphStore(List.of(TestData.pfagb));

        Iterator<?> result = DefaultGenericQueryEvaluator.defaultInstance().homomorphism(Q1, f1);

        int resultCount = 0;
        while (result.hasNext()) {
            result.next();
            resultCount++;
        }
        Assertions.assertEquals(0, resultCount);
    }


    @Test
    public void MatchNoConstantsCoherentHomomorphismsMultipleAtoms() {
        FOFormulaConjunction conj=FOFormulaFactory.instance().createOrGetConjunction(TestData.pfx,TestData.pgx);

        FOQuery<FOFormulaConjunction> Q1 =
                FOQueryFactory.instance().createOrGetQuery(conj, List.of(), new TermPartition());

        FactBase f1 = new SimpleInMemoryGraphStore(List.of(TestData.pfa,TestData.pgb));

        Assertions.assertThrows(IllegalArgumentException.class,()->DefaultGenericQueryEvaluator.defaultInstance().homomorphism(Q1, f1));

    }


}