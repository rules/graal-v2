package fr.boreal.test.query_evaluation;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.impl.FORuleImpl;
import fr.boreal.model.ruleCompilation.api.RuleCompilation;
import fr.boreal.model.ruleCompilation.id.IDRuleCompilation;
import fr.boreal.query_evaluation.atomic.InfAtomicFOQueryEvaluator;
import fr.boreal.query_evaluation.atomic.UnfoldingAtomicFOQueryEvaluator;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.*;
import java.util.stream.Stream;

@RunWith(Parameterized.class)
class AtomicHomomorphismWithCompilationTest extends TestData {

	private static final FOQuery<Atom> Q1 = FOQueryFactory.instance().createOrGetQuery(
			qxy,
			List.of(x, y),
			null);
	private static final FORule R1 = new FORuleImpl(pxy, qxy);
	private static final FactBase F1 = new SimpleInMemoryGraphStore(Set.of(pab));
	private static final Substitution S1 = new SubstitutionImpl(Map.of(x, a, y, b));

	private static final FORule R2 = new FORuleImpl(pxy, qyx);
	private static final Substitution S2 = new SubstitutionImpl(Map.of(x, b, y, a));

	@Parameters
	static Stream<Arguments> data() {
		return Stream.of(
				Arguments.of(Q1, List.of(R1), F1, Set.of(S1)),
				Arguments.of(Q1, List.of(R2), F1, Set.of(S2))
		);
	}

	@DisplayName("Test Homomorphism with compilation using inf tests")
	@ParameterizedTest(name = "{index}: Query {0} with {1} on {2} should give {3})")
	@MethodSource("data")
	public void infHomomorphismTest(FOQuery<Atom> query, Collection<FORule> rules, FactBase factbase, Collection<Substitution> expected) {
		Set<FORule> copiedRules = new HashSet<>(rules);
		RuleBase rb = new RuleBaseImpl(copiedRules);
		RuleCompilation compilation = new IDRuleCompilation();
		compilation.compile(rb);
		FOQueryEvaluator<Atom> infEvaluator = new InfAtomicFOQueryEvaluator(compilation);

		test(infEvaluator, query, factbase, expected);
	}

	@DisplayName("Test Homomorphism with compilation using unfolding")
	@ParameterizedTest(name = "{index}: Query {0} with {1} on {2} should give {3})")
	@MethodSource("data")
	public void unfoldHomomorphismTest(FOQuery<Atom> query, Collection<FORule> rules, FactBase factbase, Collection<Substitution> expected) {
		Set<FORule> copiedRules = new HashSet<>(rules);
		RuleBase rb = new RuleBaseImpl(copiedRules);
		RuleCompilation compilation = new IDRuleCompilation();
		compilation.compile(rb);
		FOQueryEvaluator<Atom> unfoldEvaluator = new UnfoldingAtomicFOQueryEvaluator(compilation);

		test(unfoldEvaluator, query, factbase, expected);
	}

	private void test(FOQueryEvaluator<Atom> evaluator, FOQuery<Atom> query, FactBase factbase, Collection<Substitution> expected) {
		Iterator<Substitution> result = evaluator.homomorphism(query, factbase);
		Set<Substitution> allSubstitutions = new HashSet<>();
		while(result.hasNext()) {
			Substitution s = result.next();
			Assert.assertTrue("The result must be expected (correctness)", expected.contains(s));
			allSubstitutions.add(s);
		}
		Assert.assertEquals("The number of results must be the same as expected (completeness)",
				expected.size(), allSubstitutions.size());
	}

}

