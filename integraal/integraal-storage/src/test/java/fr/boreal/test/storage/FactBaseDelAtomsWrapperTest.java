package fr.boreal.test.storage;

import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.ColumnType;
import fr.boreal.model.kb.api.FactBaseType;
import fr.boreal.model.kb.impl.FactBaseDescription;
import fr.boreal.model.logicalElements.api.*;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.storage.natives.FactBaseDelAtomsWrapper;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;
import fr.boreal.storage.natives.DefaultInMemoryAtomSet;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.api.Assertions;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
/**
 * Unit tests for the {@link FactBaseDelAtomsWrapper} class. This test suite checks
 * the behavior of the wrapper when used with two types of fact bases:
 * {@link SimpleInMemoryGraphStore} and {@link DefaultInMemoryAtomSet}.
 * <p>
 * The tests ensure that virtual deletions work as expected and do not affect
 * the underlying fact base unless the deletions are concreted. It verifies
 * that additions of atoms are correctly handled in the underlying fact base, as
 * only deletions are virtual in the wrapper.
 * <p>
 * Tests included:
 * - Virtual deletions of single and multiple atoms.
 * - Checking that virtual deletions do not modify the underlying fact base.
 * - Adding atoms and ensuring they are correctly added to the underlying fact base.
 * - Concreting deletions and ensuring the atoms are permanently removed.
 *
 * @author Guillaume Pérution-Kihli
 */


public class FactBaseDelAtomsWrapperTest {

    // Predicates and terms to be used in tests
    private static final Predicate p2 = SameObjectPredicateFactory.instance().createOrGetPredicate("p", 2);
    private static final Constant a = SameObjectTermFactory.instance().createOrGetConstant("a");
    private static final Constant b = SameObjectTermFactory.instance().createOrGetConstant("b");
    private static final Constant c = SameObjectTermFactory.instance().createOrGetConstant("c");
    private static final Variable X = SameObjectTermFactory.instance().createOrGetVariable("X");
    private static final Variable Y = SameObjectTermFactory.instance().createOrGetVariable("Y");

    // Atoms created with constants and variables
    private static final Atom pab = new AtomImpl(p2, a, b); // p(a, b)
    private static final Atom pbc = new AtomImpl(p2, b, c); // p(b, c)
    private static final Atom pXY = new AtomImpl(p2, X, Y); // p(X, Y)

    // Provides different types of fact bases to the tests
    static Stream<FactBase> provideFactBases() {
        Set<Atom> atomSet = new HashSet<>();
        atomSet.add(pab);
        atomSet.add(pbc);
        atomSet.add(pXY);

        return Stream.of(
                new SimpleInMemoryGraphStore(atomSet),
                new DefaultInMemoryAtomSet(atomSet)
        );
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testGetAtoms(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Virtual deletion of the atom pab
        wrapper.remove(pab);

        // Ensure the atom is virtually removed
        Collection<Atom> atoms = wrapper.getAtoms().toList();
        Assertions.assertEquals(2, atoms.size(), "Expected only 2 atoms after virtual removal");
        Assertions.assertFalse(atoms.contains(pab), "The removed atom should not be in the result");

        // Ensure the base of facts is not modified
        Assertions.assertTrue(base.getAtomsInMemory().contains(pab), "The base should still contain the removed atom");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testGetAtomsInMemory(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Virtual deletion of pab
        wrapper.remove(pab);

        // Ensure that getAtomsInMemory returns all atoms except the virtually removed ones
        Set<Atom> atomsInMemory = wrapper.getAtomsInMemory();
        Assertions.assertEquals(2, atomsInMemory.size(), "Expected only 2 atoms in memory after virtual removal");

        // Ensure the atoms in memory are correct
        Assertions.assertFalse(atomsInMemory.contains(pab), "The set should not contain the virtually removed atom p(a, b)");
        Assertions.assertTrue(atomsInMemory.contains(pbc), "The set should contain the atom p(b, c)");
        Assertions.assertTrue(atomsInMemory.contains(pXY), "The set should contain the atom p(X, Y)");

        // Verify the iterator of the set
        Iterator<Atom> iterator = atomsInMemory.iterator();
        Set<Atom> foundAtoms = new HashSet<>();
        while (iterator.hasNext()) {
            foundAtoms.add(iterator.next());
        }

        Assertions.assertEquals(atomsInMemory, foundAtoms, "The iterator should return the same atoms as the set");

        // Ensure the base of facts is not modified
        Assertions.assertEquals(3, base.getAtomsInMemory().size(), "The base should still contain all 3 original atoms");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testGetVariables(FactBase base) {
        // Case 1: Add p(X, b) first, then remove p(X, Y)
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Add p(X, b) to the base
        Atom pXb = new AtomImpl(p2, X, b); // New atom p(X, b)
        wrapper.add(pXb);

        // Now remove p(X, Y) virtually
        wrapper.remove(pXY);

        // Ensure that variable X is still present (because of p(X, b)), but Y is absent
        Set<Variable> variables = wrapper.getVariables().collect(Collectors.toSet());
        Assertions.assertTrue(variables.contains(X), "Variable X should still be present because it's in p(X, b)");
        Assertions.assertFalse(variables.contains(Y), "Variable Y should be absent after removing p(X, Y)");

        // Ensure the base still contains all variables, including those from removed atoms
        Set<Variable> baseVariables = base.getVariables().collect(Collectors.toSet());
        Assertions.assertTrue(baseVariables.contains(X), "The base should still contain variable X from p(X, Y)");
        Assertions.assertTrue(baseVariables.contains(Y), "The base should still contain variable Y from p(X, Y)");

        // Case 2: Remove p(X, Y) first, then add p(X, b)
        // Remove p(X, b) from the base to reset it for the second case
        base.remove(pXb); // Remove the atom from the base's memory

        // Create a new wrapper for the second case
        wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // First, remove p(X, Y)
        wrapper.remove(pXY);

        // Then, add p(X, b)
        wrapper.add(pXb);

        // Ensure that variable X is present and Y is absent
        variables = wrapper.getVariables().collect(Collectors.toSet());
        Assertions.assertTrue(variables.contains(X), "Variable X should be present after adding p(X, b)");
        Assertions.assertFalse(variables.contains(Y), "Variable Y should remain absent after removing p(X, Y)");

        // Ensure the base still contains all variables, including those from removed atoms
        baseVariables = base.getVariables().collect(Collectors.toSet());
        Assertions.assertTrue(baseVariables.contains(X), "The base should still contain variable X from p(X, Y)");
        Assertions.assertTrue(baseVariables.contains(Y), "The base should still contain variable Y from p(X, Y)");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testGetTerms(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Ensure all terms are returned initially
        Set<Term> terms = wrapper.getTerms().collect(Collectors.toSet());
        Assertions.assertTrue(terms.contains(a), "The wrapper should contain constant 'a'");
        Assertions.assertTrue(terms.contains(b), "The wrapper should contain constant 'b'");
        Assertions.assertTrue(terms.contains(c), "The wrapper should contain constant 'c'");
        Assertions.assertTrue(terms.contains(X), "The wrapper should contain variable 'X'");
        Assertions.assertTrue(terms.contains(Y), "The wrapper should contain variable 'Y'");

        // Case 1: Virtual deletion of p(X, Y)
        wrapper.remove(pXY);

        // Ensure that terms X and Y are no longer present
        terms = wrapper.getTerms().collect(Collectors.toSet());
        Assertions.assertFalse(terms.contains(X), "Variable X should be absent after removing p(X, Y)");
        Assertions.assertFalse(terms.contains(Y), "Variable Y should be absent after removing p(X, Y)");

        // Ensure that constants 'a', 'b', and 'c' are still present
        Assertions.assertTrue(terms.contains(a), "Constant 'a' should still be present");
        Assertions.assertTrue(terms.contains(b), "Constant 'b' should still be present");
        Assertions.assertTrue(terms.contains(c), "Constant 'c' should still be present");

        // Case 2: Add an atom that contains term X (e.g., p(X, b)) after removing p(X, Y)
        Atom pXb = new AtomImpl(p2, X, b); // New atom p(X, b)
        wrapper.add(pXb);

        // Ensure that term X is present again after adding p(X, b), but Y remains absent
        terms = wrapper.getTerms().collect(Collectors.toSet());
        Assertions.assertTrue(terms.contains(X), "Variable X should be present after adding p(X, b)");
        Assertions.assertFalse(terms.contains(Y), "Variable Y should remain absent after removing p(X, Y)");

        // Ensure the base still contains all terms, including those from removed atoms
        Set<Term> baseTerms = base.getTerms().collect(Collectors.toSet());
        Assertions.assertTrue(baseTerms.contains(X), "The base should still contain variable X from p(X, Y)");
        Assertions.assertTrue(baseTerms.contains(Y), "The base should still contain variable Y from p(X, Y)");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testGetConstants(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Ensure all constants are returned initially
        Set<Constant> constants = wrapper.getConstants().collect(Collectors.toSet());
        Assertions.assertTrue(constants.contains(a), "The wrapper should contain constant 'a'");
        Assertions.assertTrue(constants.contains(b), "The wrapper should contain constant 'b'");
        Assertions.assertTrue(constants.contains(c), "The wrapper should contain constant 'c'");

        // Case 1: Virtual deletion of p(b, c)
        wrapper.remove(pbc);

        // Ensure that constant 'c' is no longer present, but 'b' remains because it appears in p(a, b)
        constants = wrapper.getConstants().collect(Collectors.toSet());
        Assertions.assertTrue(constants.contains(b), "Constant 'b' should still be present because it's in p(a, b)");
        Assertions.assertFalse(constants.contains(c), "Constant 'c' should be absent after removing p(b, c)");

        // Ensure that constant 'a' is still present
        Assertions.assertTrue(constants.contains(a), "Constant 'a' should still be present");

        // Ensure the base still contains all constants, including those from removed atoms
        Set<Constant> baseConstants = base.getConstants().collect(Collectors.toSet());
        Assertions.assertTrue(baseConstants.contains(a), "The base should still contain constant 'a' from p(a,b)");
        Assertions.assertTrue(baseConstants.contains(b), "The base should still contain constant 'b' from p(b, c)");
        Assertions.assertTrue(baseConstants.contains(c), "The base should still contain constant 'c' from p(b, c)");

        // Case 2: Add an atom that contains constant c (e.g., p(a, c)) after removing p(b, c)
        Atom pac = new AtomImpl(p2, a, c); // New atom p(a, c)
        wrapper.add(pac);

        // Ensure that constant 'c' is present again after adding p(a, c)
        constants = wrapper.getConstants().collect(Collectors.toSet());
        Assertions.assertTrue(constants.contains(c), "Constant 'c' should be present after adding p(a, c)");
        Assertions.assertTrue(constants.contains(a), "Constant 'a' should still be present");
        Assertions.assertTrue(constants.contains(b), "Constant 'b' should still be present");

        // Ensure the base still contains all constants, including those from removed atoms
        baseConstants = base.getConstants().collect(Collectors.toSet());
        Assertions.assertTrue(baseConstants.contains(b), "The base should still contain constant 'b' from p(b, c)");
        Assertions.assertTrue(baseConstants.contains(c), "The base should still contain constant 'c' from p(b, c)");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testGetLiterals(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Create some literal terms to use in the tests
        Literal<String> lit1 = SameObjectTermFactory.instance().createOrGetLiteral("lit1");
        Literal<Integer> lit2 = SameObjectTermFactory.instance().createOrGetLiteral(42);

        // Add atoms containing literals to the base
        Atom atomWithLiteral1 = new AtomImpl(p2, a, lit1); // p(a, lit1)
        Atom atomWithLiteral2 = new AtomImpl(p2, b, lit2); // p(b, 42)
        wrapper.add(atomWithLiteral1);
        wrapper.add(atomWithLiteral2);

        // Ensure all literals are returned initially
        Set<Literal<?>> literals = wrapper.getLiterals().collect(Collectors.toSet());
        Assertions.assertTrue(literals.contains(lit1), "The wrapper should contain literal 'lit1'");
        Assertions.assertTrue(literals.contains(lit2), "The wrapper should contain literal '42'");

        // Case 1: Virtual deletion of atom containing lit1
        wrapper.remove(atomWithLiteral1);

        // Ensure that lit1 is no longer present, but lit2 remains
        literals = wrapper.getLiterals().collect(Collectors.toSet());
        Assertions.assertFalse(literals.contains(lit1), "Literal 'lit1' should be absent after removing the atom");
        Assertions.assertTrue(literals.contains(lit2), "Literal '42' should still be present");

        // Ensure the base still contains all literals, including those from removed atoms
        Set<Literal<?>> baseLiterals = base.getLiterals().collect(Collectors.toSet());
        Assertions.assertTrue(baseLiterals.contains(lit1), "The base should still contain literal 'lit1'");
        Assertions.assertTrue(baseLiterals.contains(lit2), "The base should still contain literal '42'");

        // Case 2: Add an atom that contains a literal (lit1) after removing it
        wrapper.add(atomWithLiteral1);

        // Ensure that lit1 is present again after re-adding it
        literals = wrapper.getLiterals().collect(Collectors.toSet());
        Assertions.assertTrue(literals.contains(lit1), "Literal 'lit1' should be present after re-adding the atom");
        Assertions.assertTrue(literals.contains(lit2), "Literal '42' should still be present");

        // Ensure the base still contains all literals, including those from removed atoms
        baseLiterals = base.getLiterals().collect(Collectors.toSet());
        Assertions.assertTrue(baseLiterals.contains(lit1), "The base should still contain literal 'lit1'");
        Assertions.assertTrue(baseLiterals.contains(lit2), "The base should still contain literal '42'");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testGetAtomsByPredicate(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Create a new predicate and some atoms with this predicate
        Predicate pTest = SameObjectPredicateFactory.instance().createOrGetPredicate("pTest", 2);
        Atom atom1 = new AtomImpl(pTest, a, b); // pTest(a, b)
        Atom atom2 = new AtomImpl(pTest, b, c); // pTest(b, c)
        wrapper.add(atom1);
        wrapper.add(atom2);

        // Ensure the wrapper returns the correct atoms for the predicate pTest
        Iterator<Atom> atomsIterator = wrapper.getAtomsByPredicate(pTest);
        Set<Atom> atomsWithPredicate = new HashSet<>();
        atomsIterator.forEachRemaining(atomsWithPredicate::add);

        Assertions.assertTrue(atomsWithPredicate.contains(atom1), "The wrapper should return atom pTest(a, b)");
        Assertions.assertTrue(atomsWithPredicate.contains(atom2), "The wrapper should return atom pTest(b, c)");
        Assertions.assertEquals(2, atomsWithPredicate.size(), "The wrapper should return exactly 2 atoms with the predicate pTest");

        // Case 1: Virtual deletion of atom pTest(a, b)
        wrapper.remove(atom1);

        // Ensure that atom1 is no longer present for the predicate pTest
        atomsIterator = wrapper.getAtomsByPredicate(pTest);
        atomsWithPredicate = new HashSet<>();
        atomsIterator.forEachRemaining(atomsWithPredicate::add);

        Assertions.assertFalse(atomsWithPredicate.contains(atom1), "The wrapper should no longer return atom pTest(a, b)");
        Assertions.assertTrue(atomsWithPredicate.contains(atom2), "The wrapper should still return atom pTest(b, c)");
        Assertions.assertEquals(1, atomsWithPredicate.size(), "The wrapper should return exactly 1 atom after removing pTest(a, b)");

        // Ensure the base still contains both atoms
        Iterator<Atom> baseIterator = base.getAtomsByPredicate(pTest);
        Set<Atom> baseAtomsWithPredicate = new HashSet<>();
        baseIterator.forEachRemaining(baseAtomsWithPredicate::add);

        Assertions.assertTrue(baseAtomsWithPredicate.contains(atom1), "The base should still contain atom pTest(a, b)");
        Assertions.assertTrue(baseAtomsWithPredicate.contains(atom2), "The base should still contain atom pTest(b, c)");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testGetAtomsByTerm(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Create some atoms with different terms
        Atom atom1 = new AtomImpl(p2, a, b); // p(a, b)
        Atom atom2 = new AtomImpl(p2, b, c); // p(b, c)
        Atom atom3 = new AtomImpl(p2, a, c); // p(a, c)
        wrapper.add(atom1);
        wrapper.add(atom2);
        wrapper.add(atom3);

        // Ensure the wrapper returns the correct atoms for the term 'a'
        Iterator<Atom> atomsIterator = wrapper.getAtomsByTerm(a);
        Set<Atom> atomsWithTerm = new HashSet<>();
        atomsIterator.forEachRemaining(atomsWithTerm::add);

        Assertions.assertTrue(atomsWithTerm.contains(atom1), "The wrapper should return atom p(a, b)");
        Assertions.assertTrue(atomsWithTerm.contains(atom3), "The wrapper should return atom p(a, c)");
        Assertions.assertEquals(2, atomsWithTerm.size(), "The wrapper should return exactly 2 atoms containing the term 'a'");

        // Case 1: Virtual deletion of atom p(a, b)
        wrapper.remove(atom1);

        // Ensure that atom1 is no longer present for the term 'a'
        atomsIterator = wrapper.getAtomsByTerm(a);
        atomsWithTerm = new HashSet<>();
        atomsIterator.forEachRemaining(atomsWithTerm::add);

        Assertions.assertFalse(atomsWithTerm.contains(atom1), "The wrapper should no longer return atom p(a, b)");
        Assertions.assertTrue(atomsWithTerm.contains(atom3), "The wrapper should still return atom p(a, c)");
        Assertions.assertEquals(1, atomsWithTerm.size(), "The wrapper should return exactly 1 atom after removing p(a, b)");

        // Ensure the base still contains both atoms
        Iterator<Atom> baseIterator = base.getAtomsByTerm(a);
        Set<Atom> baseAtomsWithTerm = new HashSet<>();
        baseIterator.forEachRemaining(baseAtomsWithTerm::add);

        Assertions.assertTrue(baseAtomsWithTerm.contains(atom1), "The base should still contain atom p(a, b)");
        Assertions.assertTrue(baseAtomsWithTerm.contains(atom3), "The base should still contain atom p(a, c)");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testGetPredicates(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Initial predicates from the provided base (p(a, b), p(b, c), p(X, Y))
        Predicate p2 = SameObjectPredicateFactory.instance().createOrGetPredicate("p", 2);

        // Create additional atoms with different predicates
        Predicate p1 = SameObjectPredicateFactory.instance().createOrGetPredicate("p1", 2);
        Atom atom1 = new AtomImpl(p1, a, b); // p1(a, b)
        Atom atom2 = new AtomImpl(p1, b, c); // p1(b, c)
        wrapper.add(atom1);
        wrapper.add(atom2);

        // Ensure the wrapper returns the correct predicates
        Iterator<Predicate> predicatesIterator = wrapper.getPredicates();
        Set<Predicate> predicates = new HashSet<>();
        predicatesIterator.forEachRemaining(predicates::add);

        Assertions.assertTrue(predicates.contains(p1), "The wrapper should return predicate p1");
        Assertions.assertTrue(predicates.contains(p2), "The wrapper should return predicate p2 from the initial base");
        Assertions.assertEquals(2, predicates.size(), "The wrapper should return exactly 2 distinct predicates");

        // Case 1: Virtual deletion of all atoms with predicate p1
        wrapper.remove(atom1);
        wrapper.remove(atom2);

        // Ensure that predicate p1 is no longer present in the wrapper
        predicatesIterator = wrapper.getPredicates();
        predicates = new HashSet<>();
        predicatesIterator.forEachRemaining(predicates::add);

        Assertions.assertFalse(predicates.contains(p1), "Predicate p1 should no longer be present after removing all atoms with p1");
        Assertions.assertTrue(predicates.contains(p2), "Predicate p2 should still be present from the initial base");
        Assertions.assertEquals(1, predicates.size(), "The wrapper should return exactly 1 predicate after removing atoms with p1");

        // Ensure the base still contains both predicates
        Iterator<Predicate> baseIterator = base.getPredicates();
        Set<Predicate> basePredicates = new HashSet<>();
        baseIterator.forEachRemaining(basePredicates::add);

        Assertions.assertTrue(basePredicates.contains(p1), "The base should still contain predicate p1");
        Assertions.assertTrue(basePredicates.contains(p2), "The base should still contain predicate p2 from the initial base");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testGetTermsByPredicatePosition(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Predicate from the provided base (p(a, b), p(b, c), p(X, Y))
        Predicate p2 = SameObjectPredicateFactory.instance().createOrGetPredicate("p", 2);

        // Ensure the wrapper returns the correct terms at position 0 for the predicate p2
        Iterator<Term> termsIterator = wrapper.getTermsByPredicatePosition(p2, 0);
        Set<Term> termsAtPosition0 = new HashSet<>();
        termsIterator.forEachRemaining(termsAtPosition0::add);

        Assertions.assertTrue(termsAtPosition0.contains(a), "The wrapper should return term 'a' at position 0 for predicate p");
        Assertions.assertTrue(termsAtPosition0.contains(b), "The wrapper should return term 'b' at position 0 for predicate p");
        Assertions.assertTrue(termsAtPosition0.contains(X), "The wrapper should return variable 'X' at position 0 for predicate p");
        Assertions.assertEquals(3, termsAtPosition0.size(), "The wrapper should return exactly 3 distinct terms at position 0");

        // Ensure the wrapper returns the correct terms at position 1 for the predicate p2
        termsIterator = wrapper.getTermsByPredicatePosition(p2, 1);
        Set<Term> termsAtPosition1 = new HashSet<>();
        termsIterator.forEachRemaining(termsAtPosition1::add);

        Assertions.assertTrue(termsAtPosition1.contains(b), "The wrapper should return term 'b' at position 1 for predicate p");
        Assertions.assertTrue(termsAtPosition1.contains(c), "The wrapper should return term 'c' at position 1 for predicate p");
        Assertions.assertTrue(termsAtPosition1.contains(Y), "The wrapper should return variable 'Y' at position 1 for predicate p");
        Assertions.assertEquals(3, termsAtPosition1.size(), "The wrapper should return exactly 3 distinct terms at position 1");

        // Case 1: Virtual deletion of p(a, b)
        wrapper.remove(pab);

        // Ensure that term 'a' is no longer present at position 0 for predicate p
        termsIterator = wrapper.getTermsByPredicatePosition(p2, 0);
        termsAtPosition0 = new HashSet<>();
        termsIterator.forEachRemaining(termsAtPosition0::add);

        Assertions.assertFalse(termsAtPosition0.contains(a), "Term 'a' should no longer be present at position 0 after removing p(a, b)");
        Assertions.assertTrue(termsAtPosition0.contains(b), "Term 'b' should still be present at position 0 for predicate p");
        Assertions.assertTrue(termsAtPosition0.contains(X), "Variable 'X' should still be present at position 0 for predicate p");
        Assertions.assertEquals(2, termsAtPosition0.size(), "The wrapper should return exactly 2 distinct terms at position 0 after removing p(a, b)");

        // Ensure the base still contains term 'a' at position 0
        Iterator<Term> baseIterator = base.getTermsByPredicatePosition(p2, 0);
        Set<Term> baseTermsAtPosition0 = new HashSet<>();
        baseIterator.forEachRemaining(baseTermsAtPosition0::add);

        Assertions.assertTrue(baseTermsAtPosition0.contains(a), "The base should still contain term 'a' at position 0");
        Assertions.assertTrue(baseTermsAtPosition0.contains(b), "The base should still contain term 'b' at position 0");
        Assertions.assertTrue(baseTermsAtPosition0.contains(X), "The base should still contain variable 'X' at position 0");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testContains(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Atoms that exist in the base
        Atom pab = new AtomImpl(p2, a, b); // p(a, b)
        Atom pbc = new AtomImpl(p2, b, c); // p(b, c)
        Atom pXY = new AtomImpl(p2, X, Y); // p(X, Y)

        // Atom that does not exist
        Atom pac = new AtomImpl(p2, a, c); // p(a, c)

        // Ensure the wrapper contains the atoms initially present
        Assertions.assertTrue(wrapper.contains(pab), "The wrapper should contain the atom p(a, b)");
        Assertions.assertTrue(wrapper.contains(pbc), "The wrapper should contain the atom p(b, c)");
        Assertions.assertTrue(wrapper.contains(pXY), "The wrapper should contain the atom p(X, Y)");

        // Ensure the wrapper does not contain the atom p(a, c) (which was not added)
        Assertions.assertFalse(wrapper.contains(pac), "The wrapper should not contain the atom p(a, c)");

        // Case 1: Virtual deletion of p(a, b)
        wrapper.remove(pab);

        // Ensure that p(a, b) is no longer contained in the wrapper after removal
        Assertions.assertFalse(wrapper.contains(pab), "The wrapper should no longer contain the atom p(a, b) after virtual removal");

        // Ensure that the base still contains p(a, b)
        Assertions.assertTrue(base.contains(pab), "The base should still contain the atom p(a, b) after virtual removal");

        // Case 2: Add the atom p(a, c) to the wrapper
        wrapper.add(pac);

        // Ensure that p(a, c) is now contained in the wrapper
        Assertions.assertTrue(wrapper.contains(pac), "The wrapper should contain the atom p(a, c) after it is added");

        // Ensure that the base also contains p(a, c) after it is added
        Assertions.assertTrue(base.contains(pac), "The base should contain the atom p(a, c) after it is added");
    }


    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testAddAtom(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Virtual deletion of pab
        wrapper.remove(pab);

        // Re-add the virtually removed atom pab
        wrapper.add(pab);

        // Ensure the atom is re-added
        Collection<Atom> atoms = wrapper.getAtoms().toList();
        Assertions.assertTrue(atoms.contains(pab), "The atom should have been re-added");

        // Ensure the base of facts still contains the atom
        Assertions.assertTrue(base.getAtomsInMemory().contains(pab), "The base should still contain the atom");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testRemoveAtom(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Ensure pab is initially present
        Assertions.assertTrue(wrapper.getAtoms().anyMatch(atom -> atom.equals(pab)), "The atom p(a, b) should initially be present in the wrapper");

        // Virtual removal of pab
        boolean isRemoved = wrapper.remove(pab);

        // Ensure the atom is virtually removed
        Assertions.assertTrue(isRemoved, "The atom p(a, b) should be removed successfully");
        Assertions.assertTrue(wrapper.getAtoms().noneMatch(atom -> atom.equals(pab)), "The atom p(a, b) should be virtually removed in the wrapper");
        Assertions.assertTrue(base.getAtomsInMemory().contains(pab), "The base should still contain the atom p(a, b) after virtual removal");

        // Concrete the deletions
        wrapper.concreteDeletions();

        // Ensure the atom is now concretely removed from both the wrapper and the base
        Assertions.assertFalse(base.getAtomsInMemory().contains(pab), "The base should no longer contain the atom p(a, b) after concretization");
        Assertions.assertTrue(wrapper.getAtoms().noneMatch(atom -> atom.equals(pab)), "The wrapper should no longer contain the atom p(a, b) after concretization");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testRemoveFOFormula(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Create a conjunction of atoms to remove: p(a, b) and p(b, c)
        FOFormulaFactory formulaFactory = FOFormulaFactory.instance();
        FOFormulaConjunction atomsToRemove = formulaFactory.createOrGetConjunction(pab, pbc);

        // Ensure pab and pbc are initially present
        Assertions.assertTrue(wrapper.getAtoms().anyMatch(atom -> atom.equals(pab)), "The atom p(a, b) should initially be present in the wrapper");
        Assertions.assertTrue(wrapper.getAtoms().anyMatch(atom -> atom.equals(pbc)), "The atom p(b, c) should initially be present in the wrapper");

        // Virtual removal of the conjunction
        boolean isRemoved = wrapper.remove(atomsToRemove);

        // Ensure that both atoms are virtually removed
        Assertions.assertTrue(isRemoved, "At least one atom should be removed successfully");
        Assertions.assertTrue(wrapper.getAtoms().noneMatch(atom -> atom.equals(pab)), "The atom p(a, b) should be virtually removed in the wrapper");
        Assertions.assertTrue(wrapper.getAtoms().noneMatch(atom -> atom.equals(pbc)), "The atom p(b, c) should be virtually removed in the wrapper");

        // Ensure the base still contains both atoms
        Assertions.assertTrue(base.getAtomsInMemory().contains(pab), "The base should still contain the atom p(a, b) after virtual removal");
        Assertions.assertTrue(base.getAtomsInMemory().contains(pbc), "The base should still contain the atom p(b, c) after virtual removal");

        // Concrete the deletions
        wrapper.concreteDeletions();

        // Ensure both atoms are now concretely removed from both the wrapper and the base
        Assertions.assertFalse(base.getAtomsInMemory().contains(pab), "The base should no longer contain the atom p(a, b) after concretization");
        Assertions.assertFalse(base.getAtomsInMemory().contains(pbc), "The base should no longer contain the atom p(b, c) after concretization");
        Assertions.assertTrue(wrapper.getAtoms().noneMatch(atom -> atom.equals(pab)), "The wrapper should no longer contain the atom p(a, b) after concretization");
        Assertions.assertTrue(wrapper.getAtoms().noneMatch(atom -> atom.equals(pbc)), "The wrapper should no longer contain the atom p(b, c) after concretization");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testRemoveAllAtoms(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Ensure pab and pbc are initially present
        Assertions.assertTrue(wrapper.getAtoms().anyMatch(atom -> atom.equals(pab)), "The atom p(a, b) should initially be present in the wrapper");
        Assertions.assertTrue(wrapper.getAtoms().anyMatch(atom -> atom.equals(pbc)), "The atom p(b, c) should initially be present in the wrapper");

        // Create a collection of atoms to remove
        Set<Atom> atomsToRemove = new HashSet<>();
        atomsToRemove.add(pab);
        atomsToRemove.add(pbc);

        // Virtual removal of atoms
        boolean isRemoved = wrapper.removeAll(atomsToRemove);

        // Ensure that both atoms are virtually removed
        Assertions.assertTrue(isRemoved, "At least one atom should be removed successfully");
        Assertions.assertTrue(wrapper.getAtoms().noneMatch(atom -> atom.equals(pab)), "The atom p(a, b) should be virtually removed in the wrapper");
        Assertions.assertTrue(wrapper.getAtoms().noneMatch(atom -> atom.equals(pbc)), "The atom p(b, c) should be virtually removed in the wrapper");

        // Ensure the base still contains both atoms
        Assertions.assertTrue(base.getAtomsInMemory().contains(pab), "The base should still contain the atom p(a, b) after virtual removal");
        Assertions.assertTrue(base.getAtomsInMemory().contains(pbc), "The base should still contain the atom p(b, c) after virtual removal");

        // Concrete the deletions
        wrapper.concreteDeletions();

        // Ensure both atoms are now concretely removed from both the wrapper and the base
        Assertions.assertFalse(base.getAtomsInMemory().contains(pab), "The base should no longer contain the atom p(a, b) after concretization");
        Assertions.assertFalse(base.getAtomsInMemory().contains(pbc), "The base should no longer contain the atom p(b, c) after concretization");
        Assertions.assertTrue(wrapper.getAtoms().noneMatch(atom -> atom.equals(pab)), "The wrapper should no longer contain the atom p(a, b) after concretization");
        Assertions.assertTrue(wrapper.getAtoms().noneMatch(atom -> atom.equals(pbc)), "The wrapper should no longer contain the atom p(b, c) after concretization");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testRemoveAllStreamAtoms(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Ensure pab and pbc are initially present
        Assertions.assertTrue(wrapper.getAtoms().anyMatch(atom -> atom.equals(pab)), "The atom p(a, b) should initially be present in the wrapper");
        Assertions.assertTrue(wrapper.getAtoms().anyMatch(atom -> atom.equals(pbc)), "The atom p(b, c) should initially be present in the wrapper");

        // Create a stream of atoms to remove
        Stream<Atom> atomsToRemove = Stream.of(pab, pbc);

        // Virtual removal of atoms
        boolean isRemoved = wrapper.removeAll(atomsToRemove);

        // Ensure that both atoms are virtually removed
        Assertions.assertTrue(isRemoved, "At least one atom should be removed successfully");
        Assertions.assertTrue(wrapper.getAtoms().noneMatch(atom -> atom.equals(pab)), "The atom p(a, b) should be virtually removed in the wrapper");
        Assertions.assertTrue(wrapper.getAtoms().noneMatch(atom -> atom.equals(pbc)), "The atom p(b, c) should be virtually removed in the wrapper");

        // Ensure the base still contains both atoms
        Assertions.assertTrue(base.getAtomsInMemory().contains(pab), "The base should still contain the atom p(a, b) after virtual removal");
        Assertions.assertTrue(base.getAtomsInMemory().contains(pbc), "The base should still contain the atom p(b, c) after virtual removal");

        // Concrete the deletions
        wrapper.concreteDeletions();

        // Ensure both atoms are now concretely removed from both the wrapper and the base
        Assertions.assertFalse(base.getAtomsInMemory().contains(pab), "The base should no longer contain the atom p(a, b) after concretization");
        Assertions.assertFalse(base.getAtomsInMemory().contains(pbc), "The base should no longer contain the atom p(b, c) after concretization");
        Assertions.assertTrue(wrapper.getAtoms().noneMatch(atom -> atom.equals(pab)), "The wrapper should no longer contain the atom p(a, b) after concretization");
        Assertions.assertTrue(wrapper.getAtoms().noneMatch(atom -> atom.equals(pbc)), "The wrapper should no longer contain the atom p(b, c) after concretization");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testAddAllAtoms(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Virtual deletion of pab (p(a, b))
        wrapper.remove(pab);

        // Ensure pab is virtually removed
        Assertions.assertTrue(wrapper.getAtoms().noneMatch(atom -> atom.equals(pab)), "The atom p(a, b) should be virtually removed in the wrapper");

        // Add a set that includes the virtually removed atom and a new atom pac (p(a, c))
        Atom pac = new AtomImpl(p2, a, c);  // A new atom p(a, c)
        Set<Atom> atomsToAdd = new HashSet<>();
        atomsToAdd.add(pac);
        atomsToAdd.add(pab);

        // Add all atoms from the set using Collection
        boolean isNew = wrapper.addAll(atomsToAdd);

        // Ensure that at least one atom is new (p(a, c))
        Assertions.assertTrue(isNew, "At least one atom (p(a, c)) should be new and added successfully");

        // Ensure the new atom (p(a, c)) is in the base and wrapper
        Assertions.assertTrue(base.getAtomsInMemory().contains(pac), "The base should contain the new atom p(a, c)");
        Assertions.assertTrue(wrapper.getAtoms().anyMatch(atom -> atom.equals(pac)), "The wrapper should contain the new atom p(a, c)");

        // Ensure the re-added atom (p(a, b)) is no longer virtually removed
        Assertions.assertTrue(wrapper.getAtoms().anyMatch(atom -> atom.equals(pab)), "The atom p(a, b) should be active again in the wrapper");

        // Add all atoms again using Stream (should not add anything new)
        isNew = wrapper.addAll(atomsToAdd.stream());

        // Ensure no new atoms are added (since both are already in the base)
        Assertions.assertFalse(isNew, "No atoms should be considered new on the second addition");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testAddAllStreamAtoms(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Virtual deletion of pab (p(a, b))
        wrapper.remove(pab);

        // Ensure pab is virtually removed
        Assertions.assertTrue(wrapper.getAtoms().noneMatch(atom -> atom.equals(pab)), "The atom p(a, b) should be virtually removed in the wrapper");

        // Add a stream of atoms that includes the virtually removed atom and a new atom pac (p(a, c))
        Atom pac = new AtomImpl(p2, a, c);  // A new atom p(a, c)
        Stream<Atom> atomsToAdd = Stream.of(pab, pac);

        // Add all atoms from the stream
        boolean isNew = wrapper.addAll(atomsToAdd);

        // Ensure that at least one atom is new (p(a, c))
        Assertions.assertTrue(isNew, "At least one atom (p(a, c)) should be new and added successfully");

        // Ensure the new atom (p(a, c)) is in the base and wrapper
        Assertions.assertTrue(base.getAtomsInMemory().contains(pac), "The base should contain the new atom p(a, c)");
        Assertions.assertTrue(wrapper.getAtoms().anyMatch(atom -> atom.equals(pac)), "The wrapper should contain the new atom p(a, c)");

        // Ensure the re-added atom (p(a, b)) is no longer virtually removed
        Assertions.assertTrue(wrapper.getAtoms().anyMatch(atom -> atom.equals(pab)), "The atom p(a, b) should be active again in the wrapper");

        // Add the stream of atoms again (should not add anything new)
        isNew = wrapper.addAll(Stream.of(pab, pac));

        // Ensure no new atoms are added (since both are already in the base)
        Assertions.assertFalse(isNew, "No atoms should be considered new on the second addition");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testSize(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Ensure initial size is correct (should match the number of atoms in the base)
        Assertions.assertEquals(3, wrapper.size(), "Expected wrapper size to be 3 initially");

        // Case 1: Virtual deletion of pab (p(a, b))
        wrapper.remove(pab);

        // Ensure the size is reduced in the wrapper after virtual deletion
        Assertions.assertEquals(2, wrapper.size(), "Expected wrapper size to be 2 after virtual removal of p(a, b)");

        // Ensure the base of facts is not modified after virtual deletion
        Assertions.assertEquals(3, base.size(), "Expected base size to still be 3 after virtual removal of p(a, b)");

        // Case 2: Add a new atom p(a, c) to the wrapper
        Atom pac = new AtomImpl(p2, a, c); // p(a, c)
        wrapper.add(pac);

        // Ensure the size increases in the wrapper after adding the new atom
        Assertions.assertEquals(3, wrapper.size(), "Expected wrapper size to be 3 after adding p(a, c)");

        // Ensure the base of facts size is also updated
        Assertions.assertEquals(4, base.size(), "Expected base size to be 4 after adding p(a, c)");

        // Case 3: Add an atom that was previously virtually removed (pab)
        wrapper.add(pab);

        // Ensure the size increases to 4 after re-adding pab to the wrapper
        Assertions.assertEquals(4, wrapper.size(), "Expected wrapper size to be 4 after re-adding p(a, b)");

        // Ensure the base still contains all 4 atoms, as pab was never truly removed from it
        Assertions.assertEquals(4, base.size(), "Expected base size to still be 4 after re-adding p(a, b)");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testGetDescription(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        Predicate pTest = SameObjectPredicateFactory.instance().createOrGetPredicate("pTest", 2);

        // Ensure the description returned by the wrapper matches the base initially
        FactBaseDescription baseDescription = base.getDescription(pTest);
        FactBaseDescription wrapperDescription = wrapper.getDescription(pTest);
        Assertions.assertEquals(baseDescription, wrapperDescription, "The wrapper should return the same description as the base initially");

        // Case 1: Virtual deletion of an atom
        wrapper.remove(pab); // remove p(a, b)

        // Ensure the description remains unchanged after virtual deletion
        wrapperDescription = wrapper.getDescription(pTest);
        Assertions.assertEquals(baseDescription, wrapperDescription, "The description should remain the same after virtual deletion");

        // Case 2: Add a new atom
        Atom pac = new AtomImpl(pTest, a, c); // pTest(a, c)
        wrapper.add(pac);

        // Ensure the description remains the same after adding a new atom
        wrapperDescription = wrapper.getDescription(pTest);
        Assertions.assertEquals(baseDescription, wrapperDescription, "The description should remain the same after adding an atom");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testGetType(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        Predicate pTest = SameObjectPredicateFactory.instance().createOrGetPredicate("pTest", 2);

        // Ensure the type returned by the wrapper matches the base initially
        FactBaseType baseType = base.getType(pTest);
        FactBaseType wrapperType = wrapper.getType(pTest);
        Assertions.assertEquals(baseType, wrapperType, "The wrapper should return the same type as the base initially");

        // Case 1: Virtual deletion of an atom
        wrapper.remove(pab); // remove p(a, b)

        // Ensure the type remains unchanged after virtual deletion
        wrapperType = wrapper.getType(pTest);
        Assertions.assertEquals(baseType, wrapperType, "The type should remain the same after virtual deletion");

        // Case 2: Add a new atom
        Atom pac = new AtomImpl(pTest, a, c); // pTest(a, c)
        wrapper.add(pac);

        // Ensure the type remains the same after adding a new atom
        wrapperType = wrapper.getType(pTest);
        Assertions.assertEquals(baseType, wrapperType, "The type should remain the same after adding an atom");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testGetColumnsType(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        Predicate pTest = SameObjectPredicateFactory.instance().createOrGetPredicate("pTest", 2);

        // Ensure the column type returned by the wrapper matches the base initially
        List<ColumnType> baseColumnsType = base.getColumnsType(pTest);
        List<ColumnType> wrapperColumnsType = wrapper.getColumnsType(pTest);
        Assertions.assertEquals(baseColumnsType, wrapperColumnsType, "The wrapper should return the same columns type as the base initially");

        // Case 1: Virtual deletion of an atom
        wrapper.remove(pab); // remove p(a, b)

        // Ensure the column type remains unchanged after virtual deletion
        wrapperColumnsType = wrapper.getColumnsType(pTest);
        Assertions.assertEquals(baseColumnsType, wrapperColumnsType, "The columns type should remain the same after virtual deletion");

        // Case 2: Add a new atom
        Atom pac = new AtomImpl(pTest, a, c); // pTest(a, c)
        wrapper.add(pac);

        // Ensure the column type remains the same after adding a new atom
        wrapperColumnsType = wrapper.getColumnsType(pTest);
        Assertions.assertEquals(baseColumnsType, wrapperColumnsType, "The columns type should remain the same after adding an atom");
    }


    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testConcreteDeletions(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Virtual deletion of pab
        wrapper.remove(pab);

        // Concrete the deletion
        wrapper.concreteDeletions();

        // Ensure the atom is removed from the underlying base
        Assertions.assertFalse(base.getAtomsInMemory().contains(pab), "The atom should be removed from the underlying base");
        Assertions.assertEquals(2, base.getAtomsInMemory().size(), "Expected the base to have 2 atoms after concrete deletion");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testClear(FactBase base) {
        // Initial wrapper with no atoms virtually removed
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Ensure that initially the wrapper contains all atoms from the base
        long initialSize = base.size();
        Assertions.assertEquals(initialSize, wrapper.size(), "The wrapper should initially contain the same number of atoms as the base");

        // Perform the clear operation on the wrapper
        wrapper.clear();

        // Ensure that the wrapper is now empty
        Assertions.assertEquals(0, wrapper.size(), "The wrapper should be empty after the clear operation");

        // Ensure that the base of facts is not affected by the virtual removal
        Assertions.assertEquals(initialSize, base.size(), "The base should still contain the same number of atoms after the wrapper is cleared");

        // Add an atom to the wrapper and verify it
        Atom pac = new AtomImpl(p2, a, c); // p(a, c)
        wrapper.add(pac);

        // Ensure the wrapper contains the new atom and is no longer empty
        Assertions.assertEquals(1, wrapper.size(), "The wrapper should contain the newly added atom after clear");

        // Ensure the base of facts also contains the new atom
        Assertions.assertEquals(initialSize + 1, base.size(), "The base should also contain the newly added atom after clear");

        // Perform the clear operation again to virtually remove all atoms, including the newly added atom
        wrapper.clear();

        // Ensure that the wrapper is now empty again
        Assertions.assertEquals(0, wrapper.size(), "The wrapper should be empty after the second clear operation");

        // Ensure that the base of facts still contains the original atoms and the newly added one
        Assertions.assertEquals(initialSize + 1, base.size(), "The base should still contain the original and the newly added atom after the second clear");

        // Call concreteDeletions() to persist the virtual deletions
        wrapper.concreteDeletions();

        // After concreteDeletions, the base should be empty, as all atoms (including the newly added one) have been virtually deleted
        Assertions.assertEquals(0, wrapper.size(), "The wrapper should be empty after calling concreteDeletions");
        Assertions.assertEquals(0, base.size(), "The base should also be empty after calling concreteDeletions");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testMatch(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Atoms in the base
        Atom pab = new AtomImpl(p2, a, b); // p(a, b)
        Atom pbc = new AtomImpl(p2, b, c); // p(b, c)
        Atom pXY = new AtomImpl(p2, X, Y); // p(X, Y)

        // Case 1: Simple match without substitution (all atoms with the same predicate should match)
        Iterator<Atom> matchedAtoms = wrapper.match(pXY);
        List<Atom> matchedList = new ArrayList<>();
        matchedAtoms.forEachRemaining(matchedList::add);

        // Ensure that all atoms with the same predicate are matched, including p(X, Y)
        Assertions.assertTrue(matchedList.contains(pab), "The atom p(a, b) should be matched");
        Assertions.assertTrue(matchedList.contains(pbc), "The atom p(b, c) should be matched");
        Assertions.assertTrue(matchedList.contains(pXY), "The atom p(X, Y) should be matched");
        Assertions.assertEquals(3, matchedList.size(), "Three atoms should be matched as they share the same predicate");

        // Case 2: Match with a substitution that freezes the variable X as 'a'
        SubstitutionImpl substitution = new SubstitutionImpl();
        substitution.add(X, a);

        Iterator<Atom> matchedWithSubstitution = wrapper.match(pXY, substitution);
        List<Atom> matchedSubstitutedList = new ArrayList<>();
        matchedWithSubstitution.forEachRemaining(matchedSubstitutedList::add);

        // Ensure that only the atom p(a, b) is matched with the substitution
        Assertions.assertTrue(matchedSubstitutedList.contains(pab), "The atom p(a, b) should be matched with the substitution X -> a");
        Assertions.assertFalse(matchedSubstitutedList.contains(pbc), "The atom p(b, c) should not be matched with the substitution X -> a");
        Assertions.assertFalse(matchedSubstitutedList.contains(pXY), "The atom p(X, Y) should not be matched with the substitution X -> a");
        Assertions.assertEquals(1, matchedSubstitutedList.size(), "Only one atom should match with the substitution X -> a");

        // Case 3: Virtual deletion of p(a, b)
        wrapper.remove(pab);

        matchedWithSubstitution = wrapper.match(pXY, substitution);
        matchedSubstitutedList = new ArrayList<>();
        matchedWithSubstitution.forEachRemaining(matchedSubstitutedList::add);

        // Ensure that p(a, b) is no longer matched after virtual deletion
        Assertions.assertTrue(matchedSubstitutedList.isEmpty(), "The atom p(a, b) should not be matched after virtual deletion");

        // Ensure the base still contains p(a, b) despite virtual deletion
        Assertions.assertTrue(base.match(pXY, substitution).hasNext(), "The base should still contain p(a, b) after virtual deletion");

        // Case 4: Match without substitution after virtual deletion (p(b, c) and p(X, Y) should still be matched)
        matchedAtoms = wrapper.match(pXY);
        matchedList = new ArrayList<>();
        matchedAtoms.forEachRemaining(matchedList::add);

        Assertions.assertTrue(matchedList.contains(pbc), "The atom p(b, c) should still be matched after virtual deletion of p(a, b)");
        Assertions.assertTrue(matchedList.contains(pXY), "The atom p(X, Y) should still be matched after virtual deletion of p(a, b)");
        Assertions.assertEquals(2, matchedList.size(), "Two atoms should be matched after the virtual deletion of p(a, b)");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testAddSingleAtom(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Atom to be added
        Atom pac = new AtomImpl(p2, a, c); // p(a, c)

        // Ensure that the atom p(a, c) is not initially in the base or wrapper
        Assertions.assertFalse(base.getAtoms().anyMatch(atom -> atom.equals(pac)), "The base should not contain p(a, c) initially");
        Assertions.assertFalse(wrapper.getAtoms().anyMatch(atom -> atom.equals(pac)), "The wrapper should not contain p(a, c) initially");

        // Add p(a, c) to the wrapper
        boolean isNew = wrapper.add(pac);

        // Ensure the atom was added (it is new)
        Assertions.assertTrue(isNew, "The atom p(a, c) should be new and added successfully");
        Assertions.assertTrue(wrapper.getAtoms().anyMatch(atom -> atom.equals(pac)), "The wrapper should contain p(a, c) after adding it");
        Assertions.assertTrue(base.getAtoms().anyMatch(atom -> atom.equals(pac)), "The base should also contain p(a, c) after adding it");

        // Add the same atom again
        isNew = wrapper.add(pac);

        // Ensure the atom is not considered new this time
        Assertions.assertFalse(isNew, "The atom p(a, c) should not be considered new on the second addition");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testAddConjunctionFormula(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Create a conjunction of two atoms: p(a, b) and p(b, c)
        FOFormulaFactory formulaFactory = FOFormulaFactory.instance();
        FOFormulaConjunction conjunction = formulaFactory.createOrGetConjunction(
                new AtomImpl(p2, a, b),   // p(a, b) (already exists)
                new AtomImpl(p2, b, c),   // p(b, c) (already exists)
                new AtomImpl(p2, a, c)    // p(a, c) (new atom)
        );

        // Add the conjunction formula to the wrapper
        boolean isNew = wrapper.add(conjunction);

        // Ensure that at least one atom (p(a, c)) was new
        Assertions.assertTrue(isNew, "At least one atom (p(a, c)) should be new and added successfully");

        // Ensure all atoms from the conjunction are now in the wrapper
        Assertions.assertTrue(wrapper.getAtoms().anyMatch(atom -> atom.equals(pab)), "The wrapper should contain p(a, b)");
        Assertions.assertTrue(wrapper.getAtoms().anyMatch(atom -> atom.equals(pbc)), "The wrapper should contain p(b, c)");
        Assertions.assertTrue(wrapper.getAtoms().anyMatch(atom -> atom.equals(new AtomImpl(p2, a, c))), "The wrapper should contain p(a, c)");

        // Ensure all atoms from the conjunction are now in the base as well
        Assertions.assertTrue(base.getAtoms().anyMatch(atom -> atom.equals(pab)), "The base should contain p(a, b)");
        Assertions.assertTrue(base.getAtoms().anyMatch(atom -> atom.equals(pbc)), "The base should contain p(b, c)");
        Assertions.assertTrue(base.getAtoms().anyMatch(atom -> atom.equals(new AtomImpl(p2, a, c))), "The base should contain p(a, c)");

        // Add the same conjunction again
        isNew = wrapper.add(conjunction);

        // Ensure no atoms are considered new this time
        Assertions.assertFalse(isNew, "No atoms should be considered new on the second addition of the same conjunction");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testEstimateMatchCountWithoutSubstitution(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Estimate match count for p(X, Y)
        Optional<Long> estimateBeforeRemoval = wrapper.estimateMatchCount(pXY);

        // Ensure that an estimate is provided if the base provides one
        Optional<Long> baseEstimate = base.estimateMatchCount(pXY);
        long matchingAtomsCount = wrapper.getAtoms().filter(atom -> atom.getPredicate().equals(p2)).count();

        if (baseEstimate.isPresent()) {
            Assertions.assertTrue(estimateBeforeRemoval.isPresent(), "The wrapper should provide an estimate if the base provides one");
            Assertions.assertEquals(baseEstimate.get(), estimateBeforeRemoval.get(), "The estimate should match the base estimate before removal");
            Assertions.assertTrue(estimateBeforeRemoval.get() >= matchingAtomsCount, "The estimate should be greater than or equal to the number of matching atoms");
        }

        // Virtual removal of pab (p(a, b))
        wrapper.remove(pab);

        // Estimate match count after virtual removal
        Optional<Long> estimateAfterRemoval = wrapper.estimateMatchCount(pXY);
        matchingAtomsCount = wrapper.getAtoms().filter(atom -> atom.getPredicate().equals(p2)).count();

        if (baseEstimate.isPresent()) {
            Assertions.assertTrue(estimateAfterRemoval.isPresent(), "The wrapper should still provide an estimate after virtual removal");
            Assertions.assertTrue(estimateAfterRemoval.get() >= matchingAtomsCount, "The estimate after removal should still be greater than or equal to the number of matching atoms");
        }
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testEstimateMatchCountWithSubstitution(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Create a substitution for X -> a
        SubstitutionImpl substitution = new SubstitutionImpl();
        substitution.add(X, a);

        // Estimate match count for p(X, Y) with substitution X -> a (should match p(a, Y))
        Optional<Long> estimateBeforeRemoval = wrapper.estimateMatchCount(pXY, substitution);
        Optional<Long> baseEstimate = base.estimateMatchCount(pXY, substitution);

        // Count matching atoms
        long matchingAtomsCount = wrapper.getAtoms().filter(atom -> atom.getPredicate().equals(p2) && atom.getTerm(0).equals(a)).count();

        if (baseEstimate.isPresent()) {
            Assertions.assertTrue(estimateBeforeRemoval.isPresent(), "The wrapper should provide an estimate if the base provides one");
            Assertions.assertEquals(baseEstimate.get(), estimateBeforeRemoval.get(), "The estimate should match the base estimate before removal");
            Assertions.assertTrue(estimateBeforeRemoval.get() >= matchingAtomsCount, "The estimate should be greater than or equal to the number of matching atoms");
        }

        // Virtual removal of p(a, b)
        wrapper.remove(pab);

        // Estimate match count after virtual removal
        Optional<Long> estimateAfterRemoval = wrapper.estimateMatchCount(pXY, substitution);
        matchingAtomsCount = wrapper.getAtoms().filter(atom -> atom.getPredicate().equals(p2) && atom.getTerm(0).equals(a)).count();

        if (baseEstimate.isPresent()) {
            Assertions.assertTrue(estimateAfterRemoval.isPresent(), "The wrapper should still provide an estimate after virtual removal");
            Assertions.assertTrue(estimateAfterRemoval.get() >= matchingAtomsCount, "The estimate after removal should still be greater than or equal to the number of matching atoms");
        }
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testCanPerformIndexedMatch(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Test without substitution
        Assertions.assertEquals(base.canPerformIndexedMatch(pab), wrapper.canPerformIndexedMatch(pab),
                "The indexed match capability should match the base for atom p(a, b)");

        // Test with substitution
        SubstitutionImpl substitution = new SubstitutionImpl();
        substitution.add(X, a);
        Assertions.assertEquals(base.canPerformIndexedMatch(pXY, substitution), wrapper.canPerformIndexedMatch(pXY, substitution),
                "The indexed match capability should match the base for atom p(X, Y) with substitution X -> a");

        // Virtual removal of p(a, b)
        wrapper.remove(pab);

        // Ensure the capability does not change after the virtual removal
        Assertions.assertEquals(base.canPerformIndexedMatch(pab), wrapper.canPerformIndexedMatch(pab),
                "The indexed match capability should still match the base after virtual removal of p(a, b)");

        // Ensure the capability remains the same for the atom with substitution
        Assertions.assertEquals(base.canPerformIndexedMatch(pXY, substitution), wrapper.canPerformIndexedMatch(pXY, substitution),
                "The indexed match capability should still match the base after virtual removal of p(a, b)");
    }

    @ParameterizedTest
    @MethodSource("provideFactBases")
    public void testGetAtomsWithTerm(FactBase base) {
        FactBaseDelAtomsWrapper wrapper = new FactBaseDelAtomsWrapper(base, new HashSet<>());

        // Define the term to search for
        Term searchTerm = a; // Using the constant 'a' as an example

        // Get the stream of atoms containing the term
        List<Atom> atomsWithTerm = wrapper.getAtoms(searchTerm).toList();

        // Verify that all returned atoms contain the specified term
        for (Atom atom : atomsWithTerm) {
            Assertions.assertTrue(atom.contains(searchTerm),
                    "All atoms returned should contain the term: " + searchTerm);
        }

        // Additionally, check if the correct atoms are returned
        Set<Atom> expectedAtoms = base.getAtoms().filter(a -> a.contains(searchTerm)).collect(Collectors.toSet());
        Assertions.assertEquals(expectedAtoms.size(), atomsWithTerm.size(),
                "The number of atoms returned should match the expected count.");
        Assertions.assertTrue(expectedAtoms.containsAll(atomsWithTerm),
                "The returned atoms should match the expected atoms containing the term.");
    }

}
