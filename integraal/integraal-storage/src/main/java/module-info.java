/**
 * Module for storage elements of InteGraal
 *
 * @author Florent Tornil
 *
 */
module fr.boreal.storage {

	requires transitive fr.boreal.model;

	requires fr.lirmm.boreal.util;
	requires org.slf4j;

	requires transitive java.sql;
	requires org.apache.commons.dbutils;
	requires org.hsqldb;
	requires mysql.connector.j;
	requires org.postgresql.jdbc;
	requires org.xerial.sqlitejdbc;

	requires jsonld.java;
	requires org.mongodb.driver.sync.client;
	requires transitive org.mongodb.bson;
	requires org.mongodb.driver.core;

	requires rdf4j.rio.api;
	requires rdf4j.common.iterator;
	requires rdf4j.common.exception;
	requires rdf4j.http.client;
	requires rdf4j.query;
	requires rdf4j.repository.sparql;
	requires rdf4j.model.api;
	requires transitive rdf4j.repository.api;
	requires rdf4j.repository.sail;
	requires rdf4j.sail.memory;
	requires rdf4j.sail.api;
	requires rdf4j.queryalgebra.model;

	requires java.net.http;
	requires org.json;
	requires com.google.common;
    requires org.apache.commons.lang3;


    exports fr.boreal.storage.builder;
	exports fr.boreal.storage.natives;

	exports fr.boreal.storage.external.evaluator;
	exports fr.boreal.storage.external.triplestore;

	exports fr.boreal.storage.external.rdbms;
	exports fr.boreal.storage.external.rdbms.driver;
	exports fr.boreal.storage.external.rdbms.layout;
}