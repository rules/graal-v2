package fr.boreal.storage.builder;

import java.util.Optional;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.storage.external.rdbms.RDBMSStore;
import fr.boreal.storage.external.rdbms.driver.HSQLDBDriver;
import fr.boreal.storage.external.rdbms.driver.MySQLDriver;
import fr.boreal.storage.external.rdbms.driver.PostgreSQLDriver;
import fr.boreal.storage.external.rdbms.driver.RDBMSDriver;
import fr.boreal.storage.external.rdbms.driver.SQLiteDriver;
import fr.boreal.storage.external.rdbms.layout.AdHocSQLLayout;
import fr.boreal.storage.external.rdbms.layout.EncodingAdHocSQLLayout;
import fr.boreal.storage.external.rdbms.layout.NaturalSQLLayout;
import fr.boreal.storage.external.rdbms.layout.RDBMSStorageLayout;
import fr.boreal.storage.external.triplestore.TripleStoreStore;
import fr.boreal.storage.natives.DefaultInMemoryAtomSet;
import fr.boreal.storage.natives.SimpleFOFormulaStore;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;

/**
 * This Builder creates storages for atoms according to the configuration
 */
public class StorageBuilder {

	private TermFactory tf = SameObjectTermFactory.instance();
	private PredicateFactory pf = SameObjectPredicateFactory.instance();

	private FactBase fs;

	private DBType dbType = DBType.SQL;

	private enum DBType {
		SQL, SPARQL
	}

	private RDBMSDriver driver;
	private DriverType driverType = DriverType.HSQLDB;

	private enum DriverType {
		HSQLDB, MySQL, PostgreSQL, SQLite
	}

	private RDBMSStorageLayout storageStrategy;
	private Strategies strategy = Strategies.EncodingAdHocSQL;

	private enum Strategies {
		AdHocSQL, EncodingAdHocSQL, NaturalSQL
	}

	private String dbParams;
	private boolean clearDBOnConstruction;

	///////////
	// Build //
	///////////

	/**
	 * Creates and return the FactStorage associated to the current configuration
	 * 
	 * @return an optional containing the created storage if the configuration was
	 *         correct or an empty optional otherwise.
	 */
	public Optional<FactBase> build() {
		if (this.getMinimalConfig().isPresent()) {
			return Optional.of(this.fs);
		} else {
			return Optional.empty();
		}
	}

	//////////////////////
	// Default settings //
	//////////////////////

	private Optional<StorageBuilder> getMinimalConfig() {
		if (this.fs == null) {
			try {
				switch (this.dbType) {
				case SPARQL:
					if (this.dbParams == null) {
						this.setFactBase(new TripleStoreStore(this.tf, this.pf));
					} else {
						this.setFactBase(new TripleStoreStore(this.dbParams, this.tf, this.pf));
					}
					break;
				case SQL:
					if (this.driver == null) {
						switch (this.driverType) {
						case HSQLDB:
							this.setDriver(new HSQLDBDriver(this.dbParams));
							break;
						case MySQL:
							this.setDriver(new MySQLDriver(this.dbParams));
							break;
						case PostgreSQL:
							this.setDriver(new PostgreSQLDriver(this.dbParams));
							break;
						case SQLite:
							this.setDriver(new SQLiteDriver(this.dbParams));
							break;
						}
					}
					if (this.storageStrategy == null) {
						switch (this.strategy) {
						case AdHocSQL:
							this.setStorageStrategy(new AdHocSQLLayout(this.driver));
							break;
						case EncodingAdHocSQL:
							this.setStorageStrategy(new EncodingAdHocSQLLayout(this.driver));
							break;
						case NaturalSQL:
							this.setStorageStrategy(new NaturalSQLLayout((RDBMSDriver) this.driver));
							break;
						}
					}
					this.setFactBase(new RDBMSStore(this.driver, this.storageStrategy, this.tf, this.pf));
					break;
				}
			} catch (Exception e) {
				throw new RuntimeException(String.format("[%s::getMinimalConfig] An error occurred during the building " +
						"of the storage", this.getClass()), e);
			}
		}

		if (this.clearDBOnConstruction) {
			if (this.fs instanceof RDBMSStore s) {
				s.clear();
			}
		}

		return Optional.of(this);
	}

	/////////////////////
	// Default builder //
	/////////////////////

	/**
	 * @return a default configuration of the builder
	 */
	public static StorageBuilder defaultBuilder() {
		return new StorageBuilder();
	}

	///////////////////////
	// In-memory storage //
	///////////////////////

	/**
	 * The default storage is a {@link SimpleInMemoryGraphStore}
	 * 
	 * @return the default storage used by Graal
	 */
	public static FactBase defaultStorage() {
		return StorageBuilder.getSimpleInMemoryGraphStore();
	}

	/**
	 * @return a new {@link SimpleInMemoryGraphStore}
	 */
	public static SimpleInMemoryGraphStore getSimpleInMemoryGraphStore() {
		return new SimpleInMemoryGraphStore();
	}

	/**
	 * @return a new {@link DefaultInMemoryAtomSet}
	 */
	public static DefaultInMemoryAtomSet getDefaultInMemoryAtomSet() {
		return new DefaultInMemoryAtomSet();
	}

	/**
	 * @return a new {@link SimpleFOFormulaStore}
	 */
	public static SimpleFOFormulaStore getSimpleFOFormulaStore() {
		return new SimpleFOFormulaStore();
	}

	///////////////////////////
	// Higher level settings //
	///////////////////////////

	/**
	 * Configure this builder to use a HSQLDB driver with the given parameters
	 * 
	 * @param params jdbc parameters
	 * @return this object
	 */
	public StorageBuilder useHSQLDB(String params) {
		this.dbType = DBType.SQL;
		this.driverType = DriverType.HSQLDB;
		this.dbParams = params;
		return this;
	}

	/**
	 * Configure this builder to use a MySQL driver with the given parameters
	 * 
	 * @param params jdbc parameters
	 * @return this object
	 */
	public StorageBuilder useMySQLDB(String params) {
		this.dbType = DBType.SQL;
		this.driverType = DriverType.MySQL;
		this.dbParams = params;
		return this;
	}

	/**
	 * Configure this builder to use a Postgres driver with the given parameters
	 * 
	 * @param params jdbc parameters
	 * @return this object
	 */
	public StorageBuilder usePostgreSQLDB(String params) {
		this.dbType = DBType.SQL;
		this.driverType = DriverType.PostgreSQL;
		this.dbParams = params;
		return this;
	}

	/**
	 * Configure this builder to clear the database at construction 
	 * 
	 * @return this object
	 */
	public StorageBuilder clearDBOnConstruction() {
		this.clearDBOnConstruction = true;
		return this;
	}

	/**
	 * Configure this builder to use a SQLite driver with the given parameters
	 * 
	 * @param params jdbc parameters
	 * @return this object
	 */
	public StorageBuilder useSQLiteDB(String params) {
		this.dbType = DBType.SQL;
		this.driverType = DriverType.SQLite;
		this.dbParams = params;
		return this;
	}

	/**
	 * Configure this builder to use a SPARQL endpoint with the given parameters
	 * 
	 * @param params rdf4j parameters, null for memory store
	 * @return this object
	 */
	public StorageBuilder useSPARQLEndpoint(String params) {
		this.dbType = DBType.SPARQL;
		this.dbParams = params;
		return this;
	}

	/**
	 * Configure this builder to use a AdHoc storage strategy
	 * 
	 * @return this object
	 */
	public StorageBuilder useAdHocSQLStrategy() {
		this.strategy = Strategies.AdHocSQL;
		return this;
	}

	/**
	 * Configure this builder to use a AdHoc storage strategy with encoding
	 * 
	 * @return this object
	 */
	public StorageBuilder useEncodingAdHocSQLStrategy() {
		this.strategy = Strategies.EncodingAdHocSQL;
		return this;
	}

	/**
	 * Configure this builder to use a Natural SQL storage strategy
	 *
	 * @return this object
	 */
	public StorageBuilder useNaturalSQLStrategy() {
		this.strategy = Strategies.NaturalSQL;
		return this;
	}

	/////////////
	// Setters //
	/////////////

	/**
	 * Directly sets the factory for terms for this builder as given by the user
	 * 
	 * @param f the term factory
	 * @return this object
	 */
	public StorageBuilder setTermFactory(TermFactory f) {
		this.tf = f;
		return this;
	}

	/**
	 * Directly sets the factory for predicates for this builder as given by the
	 * user
	 * 
	 * @param f the predicate factory
	 * @return this object
	 */
	public StorageBuilder setPredicateFactory(PredicateFactory f) {
		this.pf = f;
		return this;
	}

	/**
	 * Directly sets the RDBMS driver for this builder as given by the user
	 * 
	 * @param d the rdbms driver
	 * @return this object
	 */
	public StorageBuilder setDriver(RDBMSDriver d) {
		this.driver = d;
		return this;
	}

	/**
	 * Directly sets the storage strategy for this builder as given by the user
	 * 
	 * @param s the rdbms storage strategy
	 * @return this object
	 */
	public StorageBuilder setStorageStrategy(RDBMSStorageLayout s) {
		this.storageStrategy = s;
		return this;
	}

	/**
	 * Directly sets the fact storage for this builder as given by the user
	 * 
	 * @param f the factbase
	 * @return this object
	 */
	public StorageBuilder setFactBase(FactBase f) {
		this.fs = f;
		return this;
	}

}
