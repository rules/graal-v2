package fr.boreal.storage.external.evaluator;

import java.util.List;
import java.util.Optional;

import org.bson.Document;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

/**
 * Sends a MongoDB query
 * This evaluator uses com.mongodb {@link MongoClient} to send the query.
 * The result is a {@link MongoCursor}
 * 
 * @author Florent Tornil
 */
public class MongoDBQueryEvaluator implements NativeQueryEvaluator<String, MongoCursor<Document>> {

	MongoClient client;
	MongoDatabase database;
	MongoCollection<Document> collection;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new driver using the given parameters
	 * @param host the hostname/IP of the database
	 * @param user the username used to connect to the database
	 * @param password the password used to connect to the database
	 * @param database the database name
	 * @param collection the collection name
	 */
	public MongoDBQueryEvaluator(String host, String user, String password, String database, String collection) {
		this("mongodb://" + user + ":" + password + "@" + host, database, collection);
	}

	/**
	 * Creates a new driver over the given mongodb string
	 * @param uri mongodb connection uri
	 * @param database_name the database name
	 * @param collection_name the collection name
	 */
	public MongoDBQueryEvaluator(String uri, String database_name, String collection_name) {
		this.client = MongoClients.create(uri);
		this.database = this.client.getDatabase(database_name);
		this.collection = this.database.getCollection(collection_name);
	}

	@Override
	public Optional<MongoCursor<Document>> evaluate(String query) {
		Document bson_query = Document.parse(query);
		try {
			var result = this.collection.aggregate(List.of(bson_query));
			return Optional.of(result.iterator());
		} catch (Exception e) {
			throw new RuntimeException(String.format("[%s::evaluate] An error occurred during the evaluation " +
					"of the following query on MongoDB: %s", this.getClass(), query), e);
		}
	}

}
