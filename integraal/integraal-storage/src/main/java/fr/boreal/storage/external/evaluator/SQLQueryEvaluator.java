package fr.boreal.storage.external.evaluator;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ArrayListHandler;

import fr.boreal.storage.external.rdbms.driver.RDBMSDriver;

/**
 * Evaluates a SQL query using the associated driver.
 * This evaluator uses Apache's {@link QueryRunner} to execute the query on the database
 * The result is a List&lt;Object[]&gt; which represents the list of all resulting tuples.
 * <br/>
 * This evaluator also provides additional methods for inserting data on the database.
 */
public class SQLQueryEvaluator implements NativeQueryEvaluator<String, List<Object[]>> {

	private final QueryRunner runner;
	private final Connection connection;

	/**
	 * Construct a new evaluator over the database represented by the given driver
	 * @param driver representing the database to connect to
	 * @throws SQLException if the initial connection to the database cannot be performed
	 */
	public SQLQueryEvaluator(RDBMSDriver driver) throws SQLException {
		DataSource ds = driver.getDatasource();
		this.connection = ds.getConnection();
		this.runner = new QueryRunner(ds);
	}

	@Override
	public Optional<List<Object[]>> evaluate(String query) {
		try {
			return Optional.of(this.runner.query(this.connection, query, new ArrayListHandler()));
		} catch (Exception e) {
			throw new RuntimeException(String.format("[%s::evaluate] An error occurred during the evaluation " +
					"of the following query: %s", this.getClass(), query), e);
		}
	}

}
