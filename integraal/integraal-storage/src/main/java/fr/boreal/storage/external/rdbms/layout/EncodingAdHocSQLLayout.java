package fr.boreal.storage.external.rdbms.layout;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.storage.external.rdbms.driver.RDBMSDriver;
import org.apache.commons.dbutils.handlers.ArrayListHandler;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

/**
 * This strategy works in the same way as the AdHoc but additionally
 * Each term is also stored with it's encoding (a number)
 * In the tables representing atoms, only the encoding is stored
 * This lets us work only with numbers for comparison and also potentially reduces the storage cost of string representations for each occurrence of the term.
 */
public class EncodingAdHocSQLLayout extends AdHocSQLLayout {

	private final BiMap<Term, Long> dictionary= HashBiMap.create();

	private long encoding_counter = -1;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new Strategy over the given driver
	 * @param driver the database driver
	 */
	public EncodingAdHocSQLLayout(RDBMSDriver driver) {
		super(driver);
	}

	/////////////////////////////////////////////////
	// SQLStorageStrategy methods
	/////////////////////////////////////////////////

	@Override
	public void handleTerms(Set<Term> terms) throws SQLException {
		if(this.encoding_counter == -1) {
			List<Object[]> result = this.runner.query(this.driver.getConnection(), this.get_max_term_encoding_query(), new ArrayListHandler());
			if(result.isEmpty() || result.getFirst()[0] == null) {
				this.encoding_counter = 0;
			} else {
				this.encoding_counter = Long.parseLong(result.getFirst()[0].toString());
			}
		}

		String query = this.driver.getBaseSafeInsertQuery();
		query = query.replace("%t", this.getTermsTableName() + "(term, term_type, encoding)");
		query = query.replace("%f", "? ,?, ?");
		List<String[]> arguments = terms.stream()
				.filter(term -> ! this.dictionary.containsKey(term))
				.map(term -> {
					long term_encoding = ++this.encoding_counter;
					this.dictionary.put(term, term_encoding);
					return new String[] {term.label(), getType(term), String.valueOf(term_encoding)};
				}).toList();
		if(!arguments.isEmpty()) {
			try {
				this.runner.insertBatch(this.driver.getConnection(), query, r -> null, arguments.toArray(new String[arguments.size()][]));
			} catch(SQLException e) {
				throw new SQLException("[AdHocSQLStrategy] An SQL error occurred while trying to insert terms", e);
			}
		}
	}

	@Override
	public String getRepresentation(Term t) throws SQLException {
		if(this.dictionary.containsKey(t)) {
			return String.valueOf(this.dictionary.get(t));
		} else {
			List<Object[]> result = this.runner.query(this.driver.getConnection(), this.get_term_table_query_from_label(), new ArrayListHandler(), t.label());
			if(!result.isEmpty()) {
				this.dictionary.put(t, Long.valueOf(result.getFirst()[0].toString()));
				return result.getFirst()[0].toString();
			}
			return null;
		}
	}

	@Override
	public Term createTerm(String term_encoding, TermFactory factory) throws SQLException {
		if(this.dictionary.containsValue(Long.valueOf(term_encoding))) {
			return this.dictionary.inverse().get(Long.valueOf(term_encoding));
		}

		List<Object[]> result = this.runner.query(this.driver.getConnection(), this.get_term_table_query_from_encoding(), new ArrayListHandler(), term_encoding);
		if(!result.isEmpty()) {
			String term_label = result.getFirst()[0].toString();
			String type = result.getFirst()[1].toString();
			return switch (type) {
				case "V" -> factory.createOrGetVariable(term_label);
				case "C" -> factory.createOrGetConstant(term_label);
				case "L" -> factory.createOrGetLiteral(term_label);
				default -> throw new IllegalArgumentException("Unexpected value: " + type);
			};
		} else {
			throw new SQLException(
					String.format(
							"[%s::createTerm]  Could not determine the type of term for the term with encoding: %s.",
							this.getClass(), term_encoding));
		}
	}

	/////////////////////////////////////////////////
	// Constant getters
	/////////////////////////////////////////////////

	@Override
	protected String get_create_terms_table_query() { return "CREATE TABLE " + this.getTermsTableName() + "(term %s, term_type %s, encoding %i);"; }

	/**
	 * @return the SQL query to get a term by its encoding
	 */
	protected String get_term_table_query_from_encoding() { return "SELECT term, term_type" +
			" FROM " + this.getTermsTableName() +
			" WHERE encoding = ?;";
	}

	/**
	 * @return the SQL query to get the encoding of a term
	 */
	protected String get_term_table_query_from_label() { return "SELECT encoding" +
			" FROM " + this.getTermsTableName() +
			" WHERE term = ?;";
	}

	/**
	 * @return the SQL query to get the max encoding already used
	 */
	protected String get_max_term_encoding_query() { return "SELECT MAX(encoding) FROM " + this.getTermsTableName() + ";";}
}
