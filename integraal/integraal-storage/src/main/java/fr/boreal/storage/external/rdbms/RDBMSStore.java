package fr.boreal.storage.external.rdbms;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.api.FOFormulaDisjunction;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.CSVCopyable;
import fr.boreal.model.kb.api.DatalogDelegable;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.FactBaseType;
import fr.boreal.model.kb.impl.FactBaseDescription;
import fr.boreal.model.logicalElements.api.*;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.storage.external.evaluator.SQLParameterizedQueryEvaluator;
import fr.boreal.storage.external.rdbms.driver.PostgreSQLDriver;
import fr.boreal.storage.external.rdbms.driver.RDBMSDriver;
import fr.boreal.storage.external.rdbms.layout.RDBMSStorageLayout;
import fr.lirmm.boreal.util.stream.ThrowingFunction;
import fr.lirmm.boreal.util.time.TimeUtils;
import fr.lirmm.boreal.util.validator.rule.ConjunctionFormulaValidator;
import fr.lirmm.boreal.util.validator.rule.PositiveFormulaValidator;
import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * This wrapper represents RDBMS handled by InteGraal. They are accessed by
 * translating atoms, queries and even some rules into SQL queries following the
 * given storage strategy.
 */
public class RDBMSStore implements FactBase, DatalogDelegable, CSVCopyable {

	static final Logger LOG = LoggerFactory.getLogger(RDBMSStore.class);
	private static final String SQL_VAR_PREFIX = "SQLVAR";
	public static final String COUNT_VARIABLE_NAME = "number_of_answers";
	private static final int BATCH_SIZE = 100;

	private final SQLParameterizedQueryEvaluator evaluator;

	private final RDBMSDriver driver;
	private final RDBMSStorageLayout strategy;

	private final TermFactory termFactory;
	private final PredicateFactory predicateFactory;

	private final Map<FORule, Collection<SQLParameterizedQuery>> already_generated_rules = new HashMap<>();

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new wrapper over a RDBMS using user-given parameters
	 *
	 * @param driver           the driver to use to connect to the database
	 * @param strategy         the strategy for representing atoms
	 * @param termFactory      the term factory
	 * @param predicateFactory the predicate factory
	 * @throws SQLException if an error occurs during connection or initialization
	 */
	public RDBMSStore(RDBMSDriver driver, RDBMSStorageLayout strategy, TermFactory termFactory,
					  PredicateFactory predicateFactory) throws SQLException {
		this.driver = driver;
		this.strategy = strategy;
		this.termFactory = termFactory;
		this.predicateFactory = predicateFactory;

		if (!this.strategy.hasCorrectDatabaseSchema(driver)) {
			this.strategy.createDatabaseSchema(driver);
		}

		this.evaluator = new SQLParameterizedQueryEvaluator(driver);
	}

	/////////////////////////////////////////////////
	// FactStorage methods
	/////////////////////////////////////////////////

	@Override
	public Iterator<Atom> match(Atom a) {
		return this.match(a, new SubstitutionImpl());
	}

	@Override
	public Iterator<Atom> match(Atom a, Substitution s) {

		Collection<Variable> answer_variables = new ArrayList<>();
		for (Term t : a.getTerms()) {
			if (t.isVariable()) {
				answer_variables.add((Variable) t);
			} else {
				Variable new_var = this.termFactory.createOrGetVariable(SQL_VAR_PREFIX + t.label());
				answer_variables.add(new_var);
				s.add(new_var, t);
			}
		}

		FOQuery<?> q = FOQueryFactory.instance().createOrGetQuery(a, answer_variables);
		SQLParameterizedQuery sql_query;
		try {
			sql_query = this.translate(q, s);

			Optional<List<Object[]>> result_opt = evaluator.evaluate(sql_query);
			if (result_opt.isEmpty()) {
				return Collections.emptyIterator();
			} else {
				var result = result_opt.get();
				return result.stream().map(ThrowingFunction.unchecked(o -> {
					Predicate p = this.predicateFactory.createOrGetPredicate(a.getPredicate().label(),
							a.getPredicate().arity());
					Term[] terms = new Term[o.length];
					for (int i = 0; i < o.length; ++i) {
						terms[i] = this.strategy.createTerm(o[i].toString(), this.termFactory);
					}
					return (Atom) new AtomImpl(p, terms);
				})).iterator();
			}
		} catch (SQLException e) {
			return Collections.emptyIterator();
		}
	}

	@Override
	public boolean add(FOFormula atoms) {
		if (PositiveFormulaValidator.instance().check(atoms)
				&& ConjunctionFormulaValidator.instance().check(atoms)) {
			return this.addAll(atoms.asAtomSet());
		} else {
			throw new IllegalArgumentException(
					String.format("[%s::add] Cannot add non-positive-conjunctions formulas",
							this.getClass()));
		}
	}

	@Override
	public boolean addAll(Stream<Atom> atoms) {
		Collection<Atom> batch = new ArrayList<>();

		try {
			atoms.forEach(atom -> {
				batch.add(atom);
				if (batch.size() == RDBMSStore.BATCH_SIZE) {
					addBatch(batch);
					batch.clear();
				}
			});
			return addBatch(batch);

		} catch (Exception e) {
			throw new RuntimeException(
					String.format(
							"[%s::addAll] %s",
							this.getClass(), e.getMessage()), e);
		}
	}

	@Override
	public boolean addAll(Collection<Atom> atoms) {
		return addAll(atoms.stream());
	}

	/**
	 * adds a batch of atoms
	 *
	 * @param atoms a
	 * @return true iff at least one atom is new
	 *
	 */
	private boolean addBatch(Collection<Atom> atoms) {

		try {
			Map<String, List<Object[]>> parameters_by_table = new HashMap<>();

			// We need to go over the atoms twice :
			// The first time to give all the terms to the storage to handle possible
			// specific storage.
			// The second time is to get the specific label to use in the tables for the term
			// This is mostly useful in case of encoding

			// We do not want to do it term by term because we can use batch insertion to
			// handle it faster
			// In most cases, going over the atoms twice will be faster than sending
			// multiple insert queries to the database

			// handling terms
			Set<Term> all_terms = new HashSet<>();
			for (Atom atom : atoms) {
				Predicate p = atom.getPredicate();
				for (int i = 0; i < p.arity(); ++i) {
					Term t = atom.getTerm(i);
					all_terms.add(t);
				}
			}
			if (!all_terms.isEmpty()) {
				this.strategy.handleTerms(all_terms);
			}

			// handling atoms by table
			for (Atom atom : atoms) {

				Predicate p = atom.getPredicate();
				Object[] new_parameters = new Object[p.arity()];
				for (int i = 0; i < p.arity(); ++i) {
					Term t = atom.getTerm(i);
					new_parameters[i] = this.strategy.getRepresentation(t);
				}

				// handling predicates
				String table_name = this.strategy.getTableName(atom);
				if (!parameters_by_table.containsKey(table_name)) {
					parameters_by_table.put(table_name, new ArrayList<>());
				}
				List<Object[]> parameters = parameters_by_table.get(table_name);
				parameters.add(new_parameters);
			}

			// handling atoms
			for (String table_name : parameters_by_table.keySet()) {
				String query = this.driver.getBaseInsertQuery();
				query = query.replace("%t", table_name);
				List<Object[]> parameters = parameters_by_table.get(table_name);
				int arg_count = parameters.getFirst().length;
				for (int i = 0; i < arg_count; ++i) {
					if (i == arg_count - 1) {
						query = query.replace("%d", "?");
					} else {
						query = query.replace("%d", "?, %d");
					}
				}

				this.evaluator.insertBatch(query, parameters);

			}
			return true;

		} catch (SQLException e) {
			throw new RuntimeException(String.format("[%s::addBatch] Failed to add atoms %s", this.getClass(), atoms), e);
		}
	}

	@Override
	public boolean add(Atom atom) {
		return this.addAll(List.of(atom));
	}

	@Override
	public boolean remove(Atom atom) {
		throw new NotImplementedException(
				String.format("[%s::remove] This method is not implemented", this.getClass()));
	}

	@Override
	public boolean remove(FOFormula atoms) {
		throw new NotImplementedException(
				String.format("[%s::remove] This method is not implemented", this.getClass()));
	}

	@Override
	public boolean removeAll(Collection<Atom> atoms) {
		throw new NotImplementedException(
				String.format("[%s::removeAll] This method is not implemented", this.getClass()));
	}

	@Override
	public Stream<Atom> getAtoms() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Iterator<Atom> getAtomsByPredicate(Predicate predicate) {
		List<Term> variables = new ArrayList<>();
		for (int i = 0; i < predicate.arity(); i++) {
			variables.add(this.termFactory.createOrGetFreshVariable());
		}
		Atom query = new AtomImpl(predicate, variables);
		return this.match(query);
	}

	@Override
	public Iterator<Predicate> getPredicates() {
		try {
			return this.strategy.getAllPredicates(this.predicateFactory).iterator();
		} catch (SQLException e) {
            LOG.error("An SQL Error occurred while trying to get all the predicates \n {}", e.toString());
			throw new RuntimeException(
					String.format(
							"[%s::getPredicates] An SQL Error occurred while trying to get all the predicates.",
							this.getClass()), e);
		}
	}

	@Override
	public Iterator<Term> getTermsByPredicatePosition(Predicate p, int position) {
		return StreamSupport.stream(((Iterable<Atom>) () -> this.getAtomsByPredicate(p)).spliterator(), false)
				.map(a -> a.getTerm(position)).distinct().iterator();
	}

	/////////////////////////////////////////////////
	// Specific methods
	/////////////////////////////////////////////////

	/**
	 * @return the evaluator
	 */
	public SQLParameterizedQueryEvaluator getEvaluator() {
		return evaluator;
	}

	/**
	 * @return the driver
	 */
	public RDBMSDriver getDriver() {
		return driver;
	}

	/**
	 * @return the storage layout
	 */
	public RDBMSStorageLayout getStrategy() {
		return strategy;
	}

	@Override
	public boolean delegate(Collection<FORule> rules) throws Exception {
		long old_size = this.size();
		for (FORule r : rules) {
			Collection<SQLParameterizedQuery> sql_queries = this.translate(r);
			for (SQLParameterizedQuery sql_query : sql_queries) {
				this.evaluator.insert(sql_query);
			}
		}
		return old_size != this.size();
	}

	@Override
	public Iterator<Substitution> delegate(FOQuery<?> query, boolean onlyCountAnswers) throws Exception {

		SQLParameterizedQuery sql_query = this.translate(query, new SubstitutionImpl());

		if (onlyCountAnswers) {
			// transform into a counting query
			String sqlQueryCount = "SELECT COUNT(*) AS " + COUNT_VARIABLE_NAME + " FROM ( " + sql_query.query() + " ) as t;";
			sql_query = new SQLParameterizedQuery(sqlQueryCount, sql_query.arguments());
		}

		Optional<List<Object[]>> result_opt = this.evaluator.evaluate(sql_query);
		if (result_opt.isEmpty()) {
			return Collections.emptyIterator();
		} else {
			var result = result_opt.get();

			return result.stream().map(ThrowingFunction.unchecked(o -> {
				Iterator<Variable> answer_variables = query.getAnswerVariables().iterator();
				Substitution s = new SubstitutionImpl();
				for (int i = 0; i < o.length && answer_variables.hasNext(); ++i) {
					Variable ans = answer_variables.next();
					s.add(ans, this.strategy.createTerm(o[i].toString(), this.termFactory));
				}
				return s;
			})).iterator();
		}
	}

	@Override
	public boolean copy(String csvFilePath, char separator, int headerSize, Atom witness) throws SQLException {

		var startTime = System.currentTimeMillis();

		String tableName;
		if (witness.getPredicate().label().equals(this.strategy.getTermsTableName())) {
			tableName = this.strategy.getTermsTableName();
		} else {
			tableName = this.strategy.getTableName(witness);
		}

		boolean result;
		String sqlCopyQuery = this.driver.getCSVCopyQuery(tableName, csvFilePath, separator, headerSize);
		if (this.driver instanceof PostgreSQLDriver) {
			result = this.evaluator.copyFromSTDIn(sqlCopyQuery, csvFilePath);
		} else {
			result = this.evaluator.copy(sqlCopyQuery);
		}

		var endTime = System.currentTimeMillis();
		LOG.debug("Copy-based loading for {} \t <-- {} ", csvFilePath,
				TimeUtils.displayTimeInComprehensibleFormat(endTime - startTime));

		LOG.debug("factbase size : {} \t dictionary : {}", this.size(), this.dictionarySize());

		if (witness.getPredicate().arity() > 0) {

			String indexQuery = "CREATE INDEX IF NOT EXISTS " + RDBMSStorageLayout.INDEX_NAME_PREFIX + "_" + tableName
					+ " ON " + tableName + " (term0);";
			this.evaluator.insert(new SQLParameterizedQuery(indexQuery, new ArrayList<>()));
		}
		if (witness.getPredicate().arity() > 1) {
			String indexQuery = "CREATE INDEX IF NOT EXISTS " + RDBMSStorageLayout.INDEX_NAME_PREFIX + "_" + tableName
					+ "1 ON " + tableName + " (term1);";
			this.evaluator.insert(new SQLParameterizedQuery(indexQuery, new ArrayList<>()));
		}

        LOG.debug("Index created : {}", tableName);

		return result;
	}

	/////////////////////////////////////////////////
	// Redefining default methods
	/////////////////////////////////////////////////

	@Override
	public Stream<Atom> getAtoms(Term t) {
		return this.getAtoms().filter(a -> a.contains(t));
	}

	@Override
	public boolean contains(Atom a) {
		Substitution s = new SubstitutionImpl();
		int arity = a.getPredicate().arity();
		Term[] terms = new Term[arity];
		for (int i = 0; i < arity; ++i) {
			Variable v = this.termFactory.createOrGetFreshVariable();
			terms[i] = v;
			s.add(v, a.getTerm(i));
		}
		Atom to_match = new AtomImpl(a.getPredicate(), terms);
		return this.match(to_match, s).hasNext();
	}

	@Override
	public long size() {
		long count = 0;
		Collection<String> table_names;
		try {
			table_names = this.strategy.getAllTableNames();
			for (String table : table_names) {
				String sql_query = "SELECT DISTINCT count(*) FROM " + table + ";";
				Optional<List<Object[]>> result_opt = this.evaluator
						.evaluate(new SQLParameterizedQuery(sql_query, List.of()));
				if (result_opt.isEmpty()) {
					return -1;
				} else {
					count += Long.parseLong(result_opt.get().getFirst()[0].toString());
				}

			}
		} catch (SQLException e) {
			return -1;
		}
		return count;
	}

	public long dictionarySize() {
		long count;
		String sql_query = String.format("SELECT DISTINCT count(*) FROM %s ;", strategy.getTermsTableName());

		Optional<List<Object[]>> result_opt = this.evaluator.evaluate(new SQLParameterizedQuery(sql_query, List.of()));

		if (result_opt.isEmpty()) {
			return -1;
		} else {
			count = Long.parseLong(result_opt.get().getFirst()[0].toString());
		}
		return count;
	}

	public String dictionaryList() {

		String sql_query = String.format("SELECT * FROM %s ;", strategy.getTermsTableName());

		Optional<List<Object[]>> result_opt = evaluator.evaluate(new SQLParameterizedQuery(sql_query, List.of()));
		if (result_opt.isEmpty()) {
			return "";
		} else {
			var result = result_opt.get();
			return result.stream().map(r -> "\n" + r[0].toString() + "\t" + r[1].toString() + "\t" + r[2].toString())
					.collect(Collectors.joining());

		}

	}

	@Override
	public FactBaseType getType(Predicate viewPredicate) {
		return FactBaseType.RDBMS;
	}

	@Override
	public FactBaseDescription getDescription(Predicate viewPredicate) {
		int arity = viewPredicate.arity();

		List<Term> terms = new ArrayList<>(arity);
		List<Variable> answerVariables = new ArrayList<>(arity);
		for (int i = 0; i < arity; i++) {
			Variable v = this.termFactory.createOrGetFreshVariable();
			terms.add(v);
			answerVariables.add(v);
		}
		Atom a = new AtomImpl(viewPredicate, terms);
		FOQuery<?> predicateAsQuery = FOQueryFactory.instance().createOrGetQuery(a, answerVariables);

		String query, jdbc;
		try {
			query = this.translate(predicateAsQuery, new SubstitutionImpl()).query();
			jdbc = this.driver.getJDBCString();
		} catch (SQLException e) {
			return null;
		}

		return new FactBaseDescription(jdbc, query);
	}

	/////////////////////////////////////////////////
	// Translation methods
	/////////////////////////////////////////////////

	private SQLParameterizedQuery translate(FOQuery<?> query, Substitution s) throws SQLException {
		return switch (query.getFormula()) {
			case Atom a -> translateAtomicQuery((FOQuery<Atom>) a, s);
			case FOFormulaConjunction c -> translateConj((FOQuery<FOFormulaConjunction>) c, s);
			case FOFormulaDisjunction d -> translateDisj((FOQuery<FOFormulaDisjunction>) d, s);
			default -> throw new UnsupportedOperationException("cannot deal with query  " + query.getClass());
		};

	}

	private SQLParameterizedQuery translateAtomicQuery(FOQuery<Atom> query, Substitution s) throws SQLException {
		return this.translateConj(FOQueryFactory.instance().createOrGetQuery(
				FOFormulaFactory.instance().createOrGetConjunction(query.getFormula()),
				query.getAnswerVariables(),
				query.getVariableEqualities()), s);
	}

	private SQLParameterizedQuery translateConj(FOQuery<FOFormulaConjunction> query, Substitution s) throws SQLException {
		Collection<Atom> query_atoms = query.getFormula().asAtomSet();
		Map<Atom, String[]> table_name_by_atom = this.init_tables_by_atom(query_atoms);
		String sql_query = this.driver.getBaseSelectFilteredQuery();

		// Fill the SELECT clause
		sql_query = this.handle_select_clause(table_name_by_atom, sql_query, query.getAnswerVariables(), s);

		// Fill the FROM clause
		sql_query = this.handle_from_clause(table_name_by_atom, sql_query);

		// Fill the WHERE clause
		SQLParameterizedQuery result = this.handle_where_clause(table_name_by_atom, sql_query, s, query);
		sql_query = result.query();
		List<Object> arguments = result.arguments();

		// cleanup
		sql_query = this.cleanup_query(sql_query);

        return new SQLParameterizedQuery(sql_query, arguments);
	}

	private SQLParameterizedQuery translateDisj(FOQuery<FOFormulaDisjunction> disjunction,  Substitution s) throws SQLException {

		String sqlQuery = "";
		List<Object> arguments = new ArrayList<>();
		for (FOFormula query : disjunction.getFormula().getSubElements()) {

			if (!(query instanceof FOFormulaConjunction qq)) {
				throw new IllegalArgumentException("expected a conjunction here");
			}

			FOQuery<FOFormulaConjunction> q = FOQueryFactory.instance().createOrGetQuery(qq,
					disjunction.getAnswerVariables(),
					disjunction.getVariableEqualities());
			SQLParameterizedQuery sqlp = this.translateConj(q, s);
			sqlQuery = sqlQuery.concat(sqlp.query());
			sqlQuery = sqlQuery.substring(0, sqlQuery.length() - ";".length());
			sqlQuery = sqlQuery.concat(" UNION ");
			arguments.addAll(sqlp.arguments());
		}

		sqlQuery = sqlQuery.substring(0, sqlQuery.length() - " UNION ".length());
		// sqlQuery = sqlQuery.concat(";");
		return new SQLParameterizedQuery(sqlQuery, arguments);
	}

	/**
	 * Preconditions : - the given rule have a body that can be translated by
	 * this.translate(FOQuery) - the head have no existential variables
	 *
	 * @param rule the rule to translate
	 * @return all the queries and there arguments that should be executed to apply
	 *         the given rule on the database
	 * @throws SQLException if an error occur during translation
	 */
	public Collection<SQLParameterizedQuery> translate(FORule rule) throws SQLException {
		if (this.already_generated_rules.containsKey(rule)) {
			return this.already_generated_rules.get(rule);
		}

		Collection<Atom> head_atoms = rule.getHead().asAtomSet();

		// All terms that could be unknown get handled first
		Set<Term> non_variables_terms = new HashSet<>();
		for (Atom a : head_atoms) {
			for (Term t : a.getTerms()) {
				if (!t.isVariable()) {
					non_variables_terms.add(t);
				}
			}
		}

		if (!non_variables_terms.isEmpty()) {
			this.strategy.handleTerms(non_variables_terms);
		}

		// We create a query for each atom in the head of the rule
		Collection<SQLParameterizedQuery> queries = new ArrayList<>();
		for (Atom a : head_atoms) {
			Collection<Variable> answer_variables = new ArrayList<>();
			Substitution initial_substitution = new SubstitutionImpl();
			for (Term t : a.getTerms()) {
				if (t.isVariable()) {
					answer_variables.add((Variable) t);
				} else {
					Variable new_var = this.termFactory.createOrGetVariable(SQL_VAR_PREFIX + t.label());
					answer_variables.add(new_var);
					initial_substitution.add(new_var, t);
				}
			}

			FOQuery<?> sub_query = FOQueryFactory.instance().createOrGetQuery(rule.getBody(), answer_variables);
			SQLParameterizedQuery body_select_query = this.translate(sub_query, initial_substitution);
			String select_query = body_select_query.query();
			select_query = select_query.substring(0, select_query.length() - 1); // remove the last character : ;

			String table_name = this.strategy.getTableName(a);

			String insert_query = this.driver.getBaseSafeInsertSelectQuery();
			insert_query = insert_query.replace("%t", table_name);
			insert_query = insert_query.replace("%s", select_query);

			queries.add(new SQLParameterizedQuery(insert_query, body_select_query.arguments()));

		}

		this.already_generated_rules.put(rule, queries);
		return queries;
	}

	public void clear() {
        LOG.debug("Clearing factbase.\n Factbase size before clear :{}", this.size());
		try {
			this.driver.dropAllTables();
			this.strategy.createDatabaseSchema(driver);
            LOG.debug("Cleared factbase.\n Factbase size after clear :{}", this.size());
		} catch (SQLException e) {
            LOG.error("Error while clearing factbase from {}", getClass().getSimpleName());
			throw new RuntimeException(
					String.format(
							"[%s::clear] Error while clearing the fact base.",
							this.getClass()), e);
		}
	}

	/////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////

	/**
	 * Initialize database tables to query for each atom, and it's alias each atom
	 * need its own table even if it's the same predicate except if there are
	 * redundancy in the given query at which point we accept to have redundancy in
	 * the SQL query
	 */
	private Map<Atom, String[]> init_tables_by_atom(Collection<Atom> query_atoms) throws SQLException {
		Map<Atom, String[]> table_name_by_atom = new HashMap<>();
		int atom_index = 0;
		for (Atom a : query_atoms) {
			table_name_by_atom.put(a,
					new String[] { this.strategy.getTableName(a), this.driver.getBaseTableAlias() + atom_index++ });
		}
		return table_name_by_atom;
	}

	/**
	 * Add all the given terms to the select clause of the sql_query in the
	 * iteration order
	 *
	 * @throws SQLException error
	 */
	private String handle_select_clause(Map<Atom, String[]> table_name_by_atom, String sql_query,
										Collection<Variable> answer_terms, Substitution initial_substitution) throws SQLException {
		if (answer_terms.isEmpty()) {
			sql_query = sql_query.replace("%s", "1");
		} else {
			int answer_terms_count = 0;
			for (Variable t : answer_terms) {
				answer_terms_count++;
				String replacement;
				if (initial_substitution.keys().contains(t)) {
					// 'a'
					replacement = "'" + this.strategy.getRepresentation(initial_substitution.createImageOf(t)) + "'";
				} else {
					Optional<Atom> opt_first_occurence = table_name_by_atom.keySet().stream().filter(a -> a.contains(t))
							.findFirst();
					if (opt_first_occurence.isEmpty()) {
						// FIXME: error, an answer variable is not bound to any atom in the query ;
						// should not happen if the query is correctly created
						// This could happen if translating rules with existential variables
						replacement = "";
					} else {
						// atomX.TERMY
						final Atom first_occurence = opt_first_occurence.get();
						replacement = table_name_by_atom.get(first_occurence)[1] + "." + this.strategy
								.getColumnName(table_name_by_atom.get(first_occurence)[0], first_occurence.indexOf(t));
					}
				}
				if (answer_terms_count == answer_terms.size()) {
					sql_query = sql_query.replace("%s", replacement);
				} else {
					sql_query = sql_query.replace("%s", replacement + ", %s");
				}
			}
		}
		return sql_query;
	}

	private String handle_from_clause(Map<Atom, String[]> table_name_by_atom, String sql_query) {
		int atom_index = 0;
		for (Atom a : table_name_by_atom.keySet()) {
			atom_index++;
			String table_as = table_name_by_atom.get(a)[0] + " AS " + table_name_by_atom.get(a)[1];
			if (atom_index == table_name_by_atom.size()) {
				sql_query = sql_query.replace("%t", table_as);
			} else {
				sql_query = sql_query.replace("%t", table_as + ", %t");
			}
		}
		return sql_query;
	}

	private SQLParameterizedQuery handle_where_clause(Map<Atom, String[]> table_name_by_atom, String sql_query,
													  Substitution initial_substitution, FOQuery<?> query) throws SQLException {
		List<Object> arguments = new ArrayList<>();
		Map<Term, String> already_seen = new HashMap<>();

		for (Atom a : table_name_by_atom.keySet()) {
			for (int term_index = 0; term_index < a.getPredicate().arity(); term_index++) {
				Term t = a.getTerm(term_index);
				String column_name = this.strategy.getColumnName(table_name_by_atom.get(a)[0], term_index);
				String t_database_alias = table_name_by_atom.get(a)[1] + "." + column_name;
				if (t.isFrozen(initial_substitution)) {
					sql_query = sql_query.replace("%c", t_database_alias + " = ? AND %c");
					arguments.add(this.strategy.getRepresentation(initial_substitution.createImageOf(t)));
				} else if (already_seen.containsKey(t)) {
					sql_query = sql_query.replace("%c", t_database_alias + " = " + already_seen.get(t) + " AND %c");
				} else {
					already_seen.put(t, t_database_alias);
				}
			}
		}

		// Add the storage strategy specific conditions
		return this.strategy.addSpecificConditions(sql_query, arguments, query);
	}

	private String cleanup_query(String sql_query) {
		if (sql_query.endsWith("WHERE %c")) {
			sql_query = sql_query.substring(0, sql_query.length() - "WHERE %c".length());
		} else if (sql_query.endsWith("AND %c")) {
			sql_query = sql_query.substring(0, sql_query.length() - "AND %c".length());
		}
		sql_query = sql_query.concat(";");
		return sql_query;
	}

}
