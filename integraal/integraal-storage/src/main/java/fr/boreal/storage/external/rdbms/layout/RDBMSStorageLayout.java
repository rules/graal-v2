package fr.boreal.storage.external.rdbms.layout;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Literal;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.storage.external.rdbms.SQLParameterizedQuery;
import fr.boreal.storage.external.rdbms.driver.RDBMSDriver;

/**
 * This represents how the RDBMS stores atoms into tables and rows.
 */
public interface RDBMSStorageLayout {

	String ATOM_TABLE_PREDICATE_PREFIX = "table_pred";
	String TERM_TABLE_NAME = "table_term";
	String PREDICATE_TABLE_NAME = "table_predicate";
	String INDEX_NAME_PREFIX = "index";
	
	/**
	 * Filtering of the query is represented by the WHERE conditions in SQL.
	 * If the strategy can generate the conditions in every case, then this method should return true.
	 * Otherwise, it should return false and additional filtering will be needed
	 * @return true iff this strategy can handle complete filtering of the query
	 */
	boolean canHandleFiltering();

	/**
	 * If the table in which the given atom does not exist, this method creates it and return its name
	 * @param atom the atom for which to get the table name
	 * @return The name of the table in which the given atom is stored
	 * @throws SQLException if an exception occur
	 */
	String getTableName(Atom atom) throws SQLException;

	/**
	 * Handle the given terms, storing them if needed
	 * @param all_terms the terms to handle
	 * @throws SQLException if an exception occur
	 */
	void handleTerms(Set<Term> all_terms) throws SQLException;

	/**
	 * @param table the name of the database table
	 * @param term_index the column number
	 * @return The name of the column which store the term at the given index in the given table
	 */
	String getColumnName(String table, int term_index);

	/**
	 * Add strategy specific conditions to the given query
	 * @param sql_query an SQL query represented as string
	 * @param arguments the arguments to add to the query
	 * @param q the query for the conditions
	 * @return is not yet defined
	 */
	SQLParameterizedQuery addSpecificConditions(String sql_query, List<Object> arguments, FOQuery<?> q);

	/**
	 * @param driver the database driver
	 * @return true iff the given driver's database have the correct schema of data for this strategy 
	 * @throws SQLException if an exception occur
	 */
	boolean hasCorrectDatabaseSchema(RDBMSDriver driver) throws SQLException;

	/**
	 * Create the schema associated to this strategy on the given driver's database
	 * @param driver the database driver
	 * @throws SQLException if an exception occur
	 */
	void createDatabaseSchema(RDBMSDriver driver) throws SQLException;

	/**
	 * @return the name of all the tables storing atoms
	 * @throws SQLException if an exception occur
	 */
	Collection<String> getAllTableNames() throws SQLException;

	/**
	 * @return The name of the terms table
	 */
	String getTermsTableName();
	
	/**
	 * @param factory the predicate factory
	 * @return all the predicates stored in the database; even if no atom exists with this predicate
	 * @throws SQLException if an exception occur
	 */
	Collection<Predicate> getAllPredicates(PredicateFactory factory) throws SQLException;

	/**
	 * This is the reverse operation of getRepresentation
	 * @param term string representation
	 * @param factory to instantiate the term
	 * @return the term associated with the given string representation
	 * @throws SQLException if an exception occur
	 */
	Term createTerm(String term, TermFactory factory) throws SQLException;

	/**
	 * @param predicate string representation
	 * @param arity predicate arity
	 * @param factory to instantiate the predicate
	 * @return the predicate associated with the given string representation and arity
	 */
	Predicate createPredicate(String predicate, int arity, PredicateFactory factory);

	/**
	 * This is the reverse operation of createTerm
	 * @param t the term
	 * @return the string representation according to the storage strategy of the given term
	 * @throws SQLException if an exception occur
	 */
	default Object getRepresentation(Term t) throws SQLException {
		if (t instanceof Literal<?> l) {
			return l.value();
		}
		return t.label();
	}

}
