/**
 * 
 */
package fr.boreal.storage.natives;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.FactBaseType;
import fr.boreal.model.kb.impl.FactBaseDescription;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.lirmm.boreal.util.AtomType;
import fr.lirmm.boreal.util.stream.filter.FilterIteratorWithoutException;
import fr.lirmm.boreal.util.stream.filter.TypeFilter;

import java.util.*;
import java.util.stream.Stream;

/**
 * The DefaultInMemoryAtomSet is a simple in memory atom set based on a java
 * set. It is a wrapper for any Collection that implements the Set interface.
 * Recommended sets: - By default, use a HashSet&lt;Atom&gt; - If you want an
 * ordered set, you can use a TreeSet&lt;Atom&gt; - If you want to access
 * elements in the order there are added into the set, use
 * LinkedHashSet&lt;Atom&gt; that combine a HashSet and a Linked List
 * 
 * @author Guillaume Pérution-Kihli
 *
 */
public class DefaultInMemoryAtomSet implements FactBase {

	private final Set<Atom> atomSet;

	/**
	 * Default constructor By default, we use a HashSet&lt;Atom&gt;
	 */
	public DefaultInMemoryAtomSet() {
		atomSet = new HashSet<>();
	}

	/**
	 * This constructor allows you to use any Collection that implements the
	 * Set&lt;Atom&gt; interface
	 * 
	 * @param javaAtomSet the Set that will contain all the atoms
	 */
	public DefaultInMemoryAtomSet(Set<Atom> javaAtomSet) {
		atomSet = javaAtomSet;
	}

	/**
	 * A copy constructor that can copy any Collection&lt;Atom&gt;
	 * 
	 * @param collection the collection to copy
	 */
	public DefaultInMemoryAtomSet(Collection<Atom> collection) {
		this();
		this.atomSet.addAll(collection);
	}

	/**
	 * A copy constructor using a variable number of Atoms
	 * 
	 * @param atoms the atoms to initialize the storage with
	 */
	public DefaultInMemoryAtomSet(Atom... atoms) {
		this();
        Collections.addAll(this.atomSet, atoms);
	}

	public Set<Atom> getAtomsInMemory() {
		return atomSet;
	}

	@Override
	public Iterator<Atom> match(Atom a) {
		return this.match(a, new SubstitutionImpl());
	}

	@Override
	public Iterator<Atom> match(Atom a, Substitution s) {
		final AtomType atomType = new AtomType(a, s);
		return new FilterIteratorWithoutException<>(this.getAtomsByPredicate(a.getPredicate()),
				new TypeFilter(atomType, s.createImageOf(a)));
	}

	@Override
	public Iterator<Atom> getAtomsByPredicate(Predicate p) {
		return this.atomSet.stream().filter(a -> a.getPredicate().equals(p)).iterator();
	}

	@Override
	public Iterator<Predicate> getPredicates() {
		return this.atomSet.stream().map(Atom::getPredicate).distinct().iterator();
	}

	@Override
	public Iterator<Term> getTermsByPredicatePosition(Predicate p, int position) {
		return this.atomSet.stream().filter(a -> a.getPredicate().equals(p)).map(a -> a.getTerm(position)).distinct()
				.iterator();
	}

	@Override
	public boolean add(FOFormula atoms) {
		return this.addAll(atoms.asAtomSet());
	}

	@Override
	public boolean addAll(Collection<Atom> atoms) {
		return this.atomSet.addAll(atoms);
	}

	@Override
	public boolean add(Atom atom) {
		return this.atomSet.add(atom);
	}

	@Override
	public boolean remove(Atom atom) {
		return this.atomSet.remove(atom);
	}

	@Override
	public boolean remove(FOFormula atoms) {
		return this.atomSet.removeAll(atoms.asAtomSet());
	}

	@Override
	public boolean removeAll(Collection<Atom> atoms) {
		return this.atomSet.removeAll(atoms);
	}

	@Override
	public boolean removeAll(Stream<Atom> atoms) {
		try {
			atoms.forEach(atomSet::remove);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public Stream<Atom> getAtoms() {
		return this.atomSet.stream();
	}

	@Override
	public FactBaseDescription getDescription(Predicate viewPredicate) {
		return null;
	}

	@Override
	public FactBaseType getType(Predicate viewPredicate) {
		return FactBaseType.GRAAL;
	}

	@Override
	public String toString() {
		return this.atomSet.toString();
	}
}
