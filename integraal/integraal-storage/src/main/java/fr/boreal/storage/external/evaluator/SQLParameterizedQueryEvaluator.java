package fr.boreal.storage.external.evaluator;

import fr.boreal.storage.external.rdbms.SQLParameterizedQuery;
import fr.boreal.storage.external.rdbms.driver.RDBMSDriver;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ArrayListHandler;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * Evaluates a SQL query using the associated driver.
 * This evaluator uses Apache's {@link QueryRunner} to execute the query on the database
 * The result is a List&lt;Object[]&gt; which represents the list of all resulting tuples.
 * <br/>
 * This evaluator also provides additional methods for inserting data on the database.
 */
public class SQLParameterizedQueryEvaluator implements NativeQueryEvaluator<SQLParameterizedQuery, List<Object[]>> {

	private final QueryRunner runner;
	private final Connection connection;

	/**
	 * Construct a new evaluator over the database represented by the given driver
	 * @param driver representing the database to connect to
	 * @throws SQLException if the initial connection to the database cannot be performed
	 */
	public SQLParameterizedQueryEvaluator(RDBMSDriver driver) throws SQLException {
		DataSource ds = driver.getDatasource();
		this.connection = ds.getConnection();
		this.runner = new QueryRunner(ds);
	}

	@Override
	public Optional<List<Object[]>> evaluate(SQLParameterizedQuery query) {
		try {
			return Optional.of(this.runner.query(this.connection, query.query(), new ArrayListHandler(), query.arguments().toArray()));
		} catch (Exception e) {
			throw new RuntimeException(String.format("[%s::evaluate] An error occurred during the evaluation " +
					"of the following parametrized query: %s", this.getClass(), query), e);
		}
	}

	/**
	 * Execute the given insert query
	 * @param query to execute, representing an insert query
	 * @return true iff the query has been executed without problem
	 */
	public boolean insert(SQLParameterizedQuery query) {
		try {
			runner.insert(this.connection, query.query(), new ArrayListHandler(), query.arguments().toArray());
			return true;
		} catch (SQLException e) {
			throw new RuntimeException(String.format("[%s::insert] An error occurred during the evaluation " +
					"of the following parametrized query: %s", this.getClass(), query), e);
		}
	}

	/**
	 * Insert the given values, using a batch insert and parameters for the query
	 * This is more efficient than looping over the parameters and executing each query at a time
	 * @param query the parameterized query
	 * @param parameters The query replacement parameters
	 * @return true iff the queries have been executed without problem
	 */
	public boolean insertBatch(String query, List<Object[]> parameters) {
		try {
			runner.insertBatch(this.connection, query, ResultSet::next, parameters.toArray(new Object[parameters.size()][]));
			return true;
		} catch (SQLException e) {
			throw new RuntimeException(String.format("[%s::insertBatch] An error occurred during the evaluation " +
					"of the following query: %s, parameters: %s", this.getClass(), query, parameters), e);
		}
	}

	/**
	 * @param sqlCopyQuery sql query to copy csv data
	 * @return true iff the copy succeeded
	 * @throws SQLException iff an SQL exception occur
	 */
	public boolean copy(String sqlCopyQuery) throws SQLException {
		return runner.execute(this.connection, sqlCopyQuery) > 0;
	}
	
	/**
	 * @param sqlCopyQuery sql query to copy csv data
	 * @param csvFileName csv file to copy
	 * @return true iff the copy succeeded
	 * @throws SQLException iff an SQL exception occur
	 */
	public boolean copyFromSTDIn(String sqlCopyQuery, String csvFileName) throws SQLException {
		CopyManager mgr = new CopyManager((BaseConnection) this.connection);
		try {
			Reader in = new BufferedReader(new FileReader(csvFileName));
			return mgr.copyIn(sqlCopyQuery, in) > 0;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * @param sqlFilePath path to a file containing SQL queries
	 * @return true iff the copy succeeded
	 */
	public boolean execute(String sqlFilePath) {
		try(BufferedReader reader = new BufferedReader(new FileReader(sqlFilePath))) {
			while(true) {
				String sqlQuery = reader.readLine();
				if(sqlQuery == null) {
					break;
				}
			runner.insert(this.connection, sqlQuery, ResultSet::next, new Object[0]);
			}
			return true;
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
        return false;
	}

}
