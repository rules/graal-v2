package fr.boreal.storage.natives;

import com.google.common.collect.Sets;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.FactBaseType;
import fr.boreal.model.kb.impl.FactBaseDescription;
import fr.boreal.model.logicalElements.api.*;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.lirmm.boreal.util.AtomType;
import fr.lirmm.boreal.util.stream.CloseableIteratorAdapter;
import fr.lirmm.boreal.util.stream.CloseableIteratorWithoutException;
import fr.lirmm.boreal.util.stream.filter.FilterIteratorWithoutException;
import fr.lirmm.boreal.util.stream.filter.TypeFilter;
import fr.lirmm.boreal.util.validator.rule.ConjunctionFormulaValidator;
import fr.lirmm.boreal.util.validator.rule.PositiveFormulaValidator;

import java.util.*;
import java.util.stream.Stream;


/**
 * This storage represents the atoms as a Graph in memory.
 * Nodes are Atoms
 * Vertices are Predicates and Terms used by the corresponding atoms.
 * There are also some indexes used to retrieve all the vertices corresponding to a given Predicate or Term.
 */
public class SimpleInMemoryGraphStore implements FactBase {

    private final Map<Predicate, PredicateVertex> predicateVertices;
    private final Map<Term, TermVertex> termVertices;
    private long size;

    /////////////////////////////////////////////////
    // Constructors
    /////////////////////////////////////////////////

    /**
     * Creates a new SimpleInMemoryGraphStore
     */
    public SimpleInMemoryGraphStore() {
        predicateVertices = new HashMap<>();
        termVertices = new HashMap<>();
        size = 0;
    }


    /**
     * Creates a new SimpleInMemoryGraphStore and adds all the given atoms to it
     *
     * @param atoms initial atoms of the graph
     */
    public SimpleInMemoryGraphStore(Collection<Atom> atoms) {
        this();
        this.addAll(atoms);
    }

    /////////////////////////////////////////////////
    // Inner Classes

    /// //////////////////////////////////////////////

    private class PredicateVertex {
        final Predicate predicate;
        final Set<Atom> adjacentEdges;
        final List<Set<Term>> termPositions;

        public PredicateVertex(Predicate p) {
            predicate = p;

            termPositions = new ArrayList<>(p.arity());
            for (int i = 0; i < p.arity(); ++i) {
                termPositions.add(new HashSet<>());
            }

            adjacentEdges = new HashSet<>();
        }

        public boolean addEdge(Atom a) {
            if (!a.getPredicate().equals(predicate)) {
                return false;
            }

            List<Term> terms = List.of(a.getTerms());
            for (int i = 0; i < terms.size(); ++i) {
                termPositions.get(i).add(terms.get(i));
            }

            adjacentEdges.add(a);
            return true;
        }

        public boolean removeEdge(Atom a) {
            if (!a.getPredicate().equals(predicate)) {
                return false;
            }
            if (adjacentEdges.remove(a)) {
                Term[] terms = a.getTerms();
                for (int i = 0; i < terms.length; ++i) {
                    Optional<Set<Atom>> edgesAtPosition = getAtomsWithPredicateAndTermAtPosition(predicate, terms[i], i);
                    if (edgesAtPosition.isEmpty()
                            || edgesAtPosition.get().isEmpty()
                            || (edgesAtPosition.get().size() == 1 && edgesAtPosition.get().contains(a))) {
                        termPositions.get(i).remove(terms[i]);
                    }
                }
                return true;
            }
            return false;
        }


        public Set<Atom> getAdjacentEdges() {
            return adjacentEdges;
        }

        public Set<Term> getTermsAtPosition(int position) {
            return termPositions.get(position);
        }

    }

    private static class TermVertex {
        final Term term;
        final Map<Predicate, List<Set<Atom>>> edgesByPosition;
        final Set<Atom> adjacentEdges;

        public TermVertex(Term t) {
            term = t;
            edgesByPosition = new HashMap<>();
            adjacentEdges = new HashSet<>();
        }

        public boolean addEdge(Atom a) {
            boolean hasBeenModified = false;

            List<Set<Atom>> positions = edgesByPosition.get(a.getPredicate());
            for (int i = 0; i < a.getPredicate().arity(); ++i) {
                Term t = a.getTerm(i);
                if (t.equals(term)) {
                    hasBeenModified = true;

                    if (positions == null) {
                        positions = new ArrayList<>();
                        for (int j = 0; j < a.getPredicate().arity(); ++j) {
                            positions.add(new HashSet<>());
                        }
                        edgesByPosition.put(a.getPredicate(), positions);
                    }

                    positions.get(i).add(a);
                }
            }

            if (hasBeenModified) {
                adjacentEdges.add(a);
            }

            return hasBeenModified;
        }

        public boolean removeEdge(Atom a) {
            if (!adjacentEdges.contains(a)) {
                return false;
            }
            Term[] terms = a.getTerms();
            List<Set<Atom>> positions = edgesByPosition.get(a.getPredicate());
            for (int i = 0; i < terms.length; ++i) {
                Term t = terms[i];
                if (t.equals(term)) {
                    positions.get(i).remove(a);
                }
            }
            return adjacentEdges.remove(a);
        }

        public Set<Atom> getAdjacentEdges() {
            return adjacentEdges;
        }

        public Set<Atom> getAdjacentEdgesAtPosition(Predicate p, int position) {
            List<Set<Atom>> edgesOfPredicate = edgesByPosition.get(p);

            if (edgesOfPredicate == null) {
                return Collections.emptySet();
            }

            Set<Atom> edges = edgesOfPredicate.get(position);
            if (edges == null) {
                return Collections.emptySet();
            }

            return edges;
        }
    }

    /////////////////////////////////////////////////
    // Public methods

    /// //////////////////////////////////////////////

    @Override
    public long size() {
        return this.size;
    }

    @Override
    public CloseableIteratorWithoutException<Atom> match(Atom atom) {
        return this.match(atom, new SubstitutionImpl());
    }

    @Override
    public CloseableIteratorWithoutException<Atom> match(Atom atom, Substitution s) {
        CloseableIteratorWithoutException<Atom> it = null;
        final AtomType atomType = new AtomType(atom, s);
        if (!atomType.isThereConstant() ){
            // there are no constants, but there may be some functional terms that need to be filtered later
            it = new CloseableIteratorAdapter<>(this.getAtomsByPredicate(atom.getPredicate()));
        } else {
            // case of constants
            // find smallest iterator
            int size = Integer.MAX_VALUE;
            for (int i = 0; i < atom.getPredicate().arity(); i++) {
                if (atomType.getType(i) == AtomType.CONSTANT_OR_FROZEN_VAR_OR_GROUND_FUNCTIONAL_TERM) {
                    Term t = atom.getTerm(i);
                    Optional<TermVertex> otv = this.getTermVertex(s.createImageOf(t));
                    if (otv.isPresent()) {
                        TermVertex tv = otv.get();
                        int tmpSize = tv.getAdjacentEdgesAtPosition(atom.getPredicate(), i).size();
                        if (tmpSize < size) {
                            size = tmpSize;
                            it = new CloseableIteratorAdapter<>(tv.getAdjacentEdgesAtPosition(atom.getPredicate(), i).iterator());
                        }
                    } else {
                        size = 0;
                        it = new CloseableIteratorAdapter<>(Collections.emptyIterator());
                    }
                }
            }
        }

        if (atomType.isThereConstraint()||atomType.isThereFunctionalTermWithVars()) {
            return new FilterIteratorWithoutException<>(it, new TypeFilter(atomType, s.createImageOf(atom)));
        } else {
            return it;
        }
    }

    @Override
    public Optional<Long> estimateMatchCount(Atom atom, Substitution substitution) {
        long minSize = Long.MAX_VALUE;

        for (int i = 0; i < atom.getPredicate().arity(); i++) {
            Term term = atom.getTerm(i);
            if (term.isFrozen(substitution)) {
                Term substitutedTerm = substitution.createImageOf(term);
                Optional<TermVertex> otv = this.getTermVertex(substitutedTerm);

                if (otv.isPresent()) {
                    TermVertex tv = otv.get();
                    long size = tv.getAdjacentEdgesAtPosition(atom.getPredicate(), i).size();
                    if (size < minSize) {
                        minSize = size;
                    }
                } else {
                    return Optional.of(0L);
                }
            }
        }

        PredicateVertex pv = this.predicateVertices.get(atom.getPredicate());
        if (pv != null) {
            minSize = pv.getAdjacentEdges().size();
        } else {
            minSize = 0;
        }

        return Optional.of(minSize);
    }

    @Override
    public boolean canPerformIndexedMatch(Atom atom, Substitution substitution) {
        return true;
    }

    @Override
    public boolean add(FOFormula atoms) {
        if (PositiveFormulaValidator.instance().check(atoms)
                && ConjunctionFormulaValidator.instance().check(atoms)) {
            return this.addAll(atoms.asAtomSet());
        } else {
            throw new IllegalArgumentException("[SimpleInMemoryGraphStore] Cannot add non-positive-conjunctions formulas");
        }
    }

    @Override
    public boolean addAll(Collection<Atom> atoms) {
        boolean changed = false;
        for (Atom a : atoms) {
            changed = this.add(a) || changed;
        }
        return changed;
    }

    @Override
    public boolean add(Atom atom) {
        if (this.contains(atom)) {
            return false;
        }

        Optional<PredicateVertex> opv = this.getPredicateVertex(atom.getPredicate());
        PredicateVertex pv;

        pv = opv.orElseGet(() -> this.createPredicateVertex(atom.getPredicate()));
        pv.addEdge(atom);

        for (int i = 0; i < atom.getPredicate().arity(); i++) {
            Term t = atom.getTerm(i);
            Optional<TermVertex> otv = this.getTermVertex(t);
            TermVertex tv = otv.orElseGet(() -> this.createTermVertex(t));
            tv.addEdge(atom);
        }
        ++size;
        return true;
    }

    @Override
    public boolean removeAll(Collection<Atom> atoms) {
        boolean updated = false;
        for (Atom a : atoms) {
            updated = updated | this.remove(a);
        }
        return updated;
    }

    @Override
    public boolean remove(FOFormula atoms) {
        return this.removeAll(atoms.asAtomSet());
    }

    @Override
    public boolean remove(Atom atom) {
        Optional<PredicateVertex> opv = this.getPredicateVertex(atom.getPredicate());
        if (opv.isPresent()) {
            PredicateVertex pv = opv.get();
            if (pv.removeEdge(atom)) {
                if (pv.getAdjacentEdges().isEmpty()) {
                    this.predicateVertices.remove(atom.getPredicate());
                }
                for (Term t : atom.getTerms()) {
                    Optional<TermVertex> otv = this.getTermVertex(t);
                    if (otv.isPresent()) {
                        TermVertex tv = otv.get();
                        tv.removeEdge(atom);
                        if (tv.getAdjacentEdges().isEmpty()) {
                            this.termVertices.remove(t);
                        }
                    }
                }
                --size;
                return true;
            }
        }
        return false;
    }


    @Override
    public Stream<Atom> getAtoms() {
        return this.predicateVertices.keySet().stream()
                .map(this::getAtomsByPredicate)
                .map(Sets::newHashSet)
                .flatMap(Collection::stream);
    }

    @Override
    public Iterator<Atom> getAtomsByPredicate(Predicate p) {
        Optional<PredicateVertex> pv = this.getPredicateVertex(p);
        return pv.map(predicateVertex -> predicateVertex.getAdjacentEdges().iterator()).orElse(Collections.emptyIterator());
    }

    @Override
    public Iterator<Atom> getAtomsByTerm(Term term) {
        Optional<TermVertex> tv = this.getTermVertex(term);
        return tv.map(termVertex -> termVertex.getAdjacentEdges().iterator()).orElse(Collections.emptyIterator());
    }

    @Override
    public Iterator<Predicate> getPredicates() {
        return this.predicateVertices.keySet().iterator();
    }

    @Override
    public Iterator<Term> getTermsByPredicatePosition(Predicate p, int position) {
        if (this.getPredicateVertex(p).isPresent()) {
            return this.predicateVertices.get(p).getTermsAtPosition(position).iterator();
        }
        return Collections.emptyIterator();

    }

    @Override
    public boolean contains(Atom atom) {
        PredicateVertex predicateVertex = this.predicateVertices.get(atom.getPredicate());
        if (predicateVertex == null) {
            return false;
        }
        return predicateVertex.getAdjacentEdges().contains(atom);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o == null) {
            return false;
        } else if (o instanceof FactBase other) {
            if (other.size() == this.size()) {
                return this.getAtoms().allMatch(other::contains);
            }
        }
        return false;
    }

	@Override
	public int hashCode() {
		return this.getAtomsInMemory().hashCode();
	}

    /////////////////////////////////////////////////
    // Private methods

    /// //////////////////////////////////////////////

    private Optional<TermVertex> getTermVertex(Term t) {
        return Optional.ofNullable(this.termVertices.get(t));
    }

    private Optional<PredicateVertex> getPredicateVertex(Predicate p) {
        return Optional.ofNullable(this.predicateVertices.get(p));
    }

    private TermVertex createTermVertex(Term t) {
        TermVertex tv = new TermVertex(t);
        this.termVertices.put(t, tv);
        return tv;
    }

    private PredicateVertex createPredicateVertex(Predicate p) {
        PredicateVertex pv = new PredicateVertex(p);
        this.predicateVertices.put(p, pv);
        return pv;
    }

    private Optional<Set<Atom>> getAtomsWithPredicateAndTermAtPosition(Predicate p, Term t, int position) {
        TermVertex tv = this.termVertices.get(t);
        if (tv == null) {
            return Optional.empty();
        }
        return Optional.of(tv.getAdjacentEdgesAtPosition(p, position));
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (var p : this.predicateVertices.keySet()) {
            var atom_it = this.getAtomsByPredicate(p);
            while (atom_it.hasNext()) {
                sb.append(atom_it.next().toString());
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    @Override
    public FactBaseDescription getDescription(Predicate viewPredicate) {
        return null;
    }

    @Override
    public FactBaseType getType(Predicate viewPredicate) {
        return FactBaseType.GRAAL;
    }

    @Override
    public Set<Atom> getAtomsInMemory() {
        return new AbstractSet<>() {

            @Override
            public Iterator<Atom> iterator() {
                return SimpleInMemoryGraphStore.this.getAtoms().iterator();
            }

            @Override
            public int size() {
                return (int) SimpleInMemoryGraphStore.this.size();
            }

            @Override
            public boolean contains(Object o) {
                if (o instanceof Atom) {
                    return SimpleInMemoryGraphStore.this.contains((Atom) o);
                }
                return false;
            }
        };
    }

    @Override
    public Stream<Variable> getVariables() {
        return getTerms().filter(Term::isVariable).map(t -> (Variable) t);
    }

    @Override
    public Stream<Term> getTerms() {
        return termVertices.keySet().stream();
    }

    @Override
    public Stream<Constant> getConstants() {
        return getTerms().filter(Term::isConstant).map(t -> (Constant) t);
    }

    @Override
    public Stream<Literal<?>> getLiterals() {
        return getTerms().filter(Term::isLiteral).map(t -> (Literal<?>) t);
    }

}
