package fr.boreal.storage.external.rdbms;

import java.util.List;

/**
 * This record represents a parametric SQL query with its parameters.
 * This is used because we often need to return both values together.
 */
public record SQLParameterizedQuery(String query, List<Object> arguments) {

	/**
	 * Create a new query
	 *
	 * @param query     the parameterized SQL query
	 * @param arguments the parameters
	 */
	public SQLParameterizedQuery {
	}

	/**
	 * @return the parameterized SQL query
	 */
	@Override
	public String query() {
		return query;
	}

	/**
	 * @return the parameters of the query
	 */
	@Override
	public List<Object> arguments() {
		return arguments;
	}

}
