package fr.boreal.storage.external.rdbms.layout;

import java.sql.SQLException;
import java.util.*;

import fr.boreal.model.logicalElements.api.Literal;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ArrayListHandler;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.storage.external.rdbms.SQLParameterizedQuery;
import fr.boreal.storage.external.rdbms.driver.RDBMSDriver;

/**
 * The NaturalSQLLayout stores atoms using table names that exactly match the predicate names.
 * No additional tables for terms or predicates are created. This layout is intended to be used
 * with a pre-existing database schema, or it can create tables dynamically if they don't exist.
 *
 * It assumes that predicate names are compatible with table names in the target database.
 */
public class NaturalSQLLayout implements RDBMSStorageLayout {

    protected RDBMSDriver driver;
    protected QueryRunner runner;
    private final Map<Predicate, String> predicateTableCache = new HashMap<>();

    public NaturalSQLLayout(RDBMSDriver driver) {
        this.driver = driver;
        this.runner = new QueryRunner(driver.getDatasource());
    }

    @Override
    public boolean canHandleFiltering() {
        return true;
    }

    @Override
    synchronized public String getTableName(Atom atom) throws SQLException {
        Predicate predicate = atom.getPredicate();

        if (predicateTableCache.containsKey(predicate)) {
            return predicateTableCache.get(predicate);
        }

        String tableName = predicate.label();
        if (!driver.hasTable(tableName)) {
            createTableForPredicate(predicate);
        }

        predicateTableCache.put(predicate, tableName);
        return tableName;
    }

    @Override
    public void handleTerms(Set<Term> all_terms) throws SQLException {
        // No-op: NaturalSQLLayout does not store terms separately
    }

    @Override
    public String getColumnName(String table, int termIndex) {
        // TODO: move query into the driver
        // TODO: avoid SQL injection
        String query = String.format("SELECT column_name FROM information_schema.columns " +
                "WHERE table_name = '%s' AND ordinal_position = '%s'", table, termIndex+1);
        try {
            List<Object[]> results = this.runner.query(driver.getConnection(), query, new ArrayListHandler());
            return results.getFirst()[0].toString();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public SQLParameterizedQuery addSpecificConditions(String sql_query, List<Object> arguments, FOQuery<?> q) {
        return new SQLParameterizedQuery(sql_query, arguments);
    }

    @Override
    public boolean hasCorrectDatabaseSchema(RDBMSDriver driver) {
        // No specific schema requirements for NaturalSQLLayout
        return true;
    }

    @Override
    public void createDatabaseSchema(RDBMSDriver driver) {
        // No-op: NaturalSQLLayout does not require any schema creation.
    }

    @Override
    public Collection<String> getAllTableNames() throws SQLException {
        Set<String> tableNames = new HashSet<>();
        // TODO: move query into the driver
        String query = "SELECT table_name FROM information_schema.tables WHERE table_type = 'BASE TABLE' AND table_schema = 'public'";
        List<Object[]> results = this.runner.query(driver.getConnection(), query, new ArrayListHandler());
        for (Object[] res : results) {
            tableNames.add(res[0].toString());
        }
        return tableNames;
    }

    @Override
    public Collection<Predicate> getAllPredicates(PredicateFactory factory) throws SQLException {
        Collection<String> tableNames = getAllTableNames();
        Set<Predicate> predicates = new HashSet<>();
        for (String tableName : tableNames) {
            int arity = getTableArity(tableName);
            predicates.add(factory.createOrGetPredicate(tableName, arity));
        }
        return predicates;
    }

    @Override
    public Term createTerm(String term, TermFactory factory) throws SQLException {
        // For NaturalSQLLayout, terms are used directly as literals or constants
        return factory.createOrGetLiteral(term);
    }

    @Override
    public Predicate createPredicate(String predicate, int arity, PredicateFactory factory) {
        return factory.createOrGetPredicate(predicate, arity);
    }

    @Override
    public String getTermsTableName() {
        throw new UnsupportedOperationException("NaturalSQLLayout does not use a terms table.");
    }

    private int getTableArity(String tableName) throws SQLException {
        String query = "SELECT COUNT(*) FROM information_schema.columns WHERE table_name = ?";
        List<Object[]> results = this.runner.query(this.driver.getConnection(), query, new ArrayListHandler(), tableName);
        return results.isEmpty() ? 0 : Integer.parseInt(results.get(0)[0].toString());
    }

    private void createTableForPredicate(Predicate predicate) throws SQLException {
        String tableName = predicate.label();
        int arity = predicate.arity();
        StringBuilder createTableQuery = new StringBuilder("CREATE TABLE " + tableName + " (");
        for (int i = 0; i < arity; i++) {
            createTableQuery.append("TERM").append(i).append(" ").append(driver.getTextFieldName());
            if (i < arity - 1) {
                createTableQuery.append(", ");
            }
        }
        createTableQuery.append(");");
        this.runner.update(this.driver.getConnection(), createTableQuery.toString());
    }
}
