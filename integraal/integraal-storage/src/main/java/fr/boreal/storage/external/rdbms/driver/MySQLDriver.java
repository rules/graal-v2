package fr.boreal.storage.external.rdbms.driver;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.mysql.cj.jdbc.MysqlDataSource;

/**
 * Driver for MySQL databases
 */
public class MySQLDriver implements RDBMSDriver {

	private final MysqlDataSource ds;
	private final Connection test_connection;
	private final String JDBCString;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new driver using the given parameters
	 * @param host the url of the database
	 * @param dbName the name of the database
	 * @param user the username used to connect to the database
	 * @param password the password used to connect to the database
	 * @throws SQLException iff some error occur
	 */
	public MySQLDriver(String host, String dbName, String user, String password) throws SQLException {
		this("jdbc:mysql://" + host + "/" + dbName + "?user=" + user + "&password=" + password);
	}

	/**
	 * Creates a new driver over the given jdbc string
	 * @param uri jdbc string
	 * @throws SQLException iff some error occur
	 */
	public MySQLDriver(String uri) throws SQLException {
		this.ds = new MysqlDataSource();
		this.ds.setUrl(uri);
		this.JDBCString = uri;
		try {
            this.test_connection = this.ds.getConnection();
		} catch (SQLException e) {
			throw new SQLException("[MySQLDriver] An error occurred while connecting to the MySQL database at " + uri + "\n"
					+ "Please make sure this connection path is correct and the database is accessible.", e);
		}
	}

	@Override
	public DataSource getDatasource() {
		return this.ds;
	}

	@Override
	public String getBaseSafeInsertQuery() {
		return "INSERT IGNORE INTO %t VALUES(%f);";
	}

	@Override
	public String getBaseSafeInsertSelectQuery() {
		return "INSERT IGNORE INTO %t %s;";
	}

	@Override
	public String getTextFieldName() {
		return "VARCHAR(256)";
	}

	@Override
	public String getNumberFieldName() {
		return "INTEGER";
	}

	@Override
	public Connection getConnection() {
		return this.test_connection;
	}

	@Override
	public String getJDBCString() {
		return this.JDBCString;
	}

	@Override
	public String getCSVCopyQuery(String tableName, String csvFilePath, char delimiter, int headerSize) {
		return "LOAD DATA INFILE '"+ csvFilePath + "'" +
				" INTO TABLE " + tableName +
				" FIELDS TERMINATED BY '" + delimiter + "'" +
				" LINES TERMINATED BY '\n'" +
				(headerSize > 0 ? "IGNORE " + headerSize + " ROWS" : "") + ";";
	}
}
