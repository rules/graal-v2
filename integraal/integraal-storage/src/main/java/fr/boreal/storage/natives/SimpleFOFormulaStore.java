package fr.boreal.storage.natives;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Stream;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.FactBaseType;
import fr.boreal.model.kb.impl.FactBaseDescription;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.lirmm.boreal.util.AtomType;
import fr.lirmm.boreal.util.stream.filter.FilterIteratorWithoutException;
import fr.lirmm.boreal.util.stream.filter.TypeFilter;

/**
 * This storage stores formulas in a collection
 */
public class SimpleFOFormulaStore implements FactBase {

	final Collection<FOFormula> facts;

	/**
	 * Default constructor
	 * By default, we use a HashSet&lt;Atom&gt;
	 */
	public SimpleFOFormulaStore() {
		this.facts = new HashSet<>();
	}

	/**
	 * A constructor initialized with a single formula
	 * @param formula the formula stored in this system
	 */
	public SimpleFOFormulaStore(FOFormula formula) {
		this();
		this.add(formula);
	}

	/**
	 * This constructor allows you to use any Collection
	 * @param facts the Collection that will contain all the atoms
	 */
	public SimpleFOFormulaStore(Collection<FOFormula> facts) {
		this.facts = facts;
	}

	@Override
    public Iterator<Atom> match(Atom a) {
		return this.match(a, new SubstitutionImpl());
	}

	@Override
	public Iterator<Atom> match(Atom a, Substitution s) {
		Iterator<Atom> atoms_by_predicate = this.getAtoms()
				.filter(other -> other.getPredicate().equals(a.getPredicate()))
				.iterator();

		final AtomType atomType = new AtomType(a, s);
		return new FilterIteratorWithoutException<>(atoms_by_predicate, new TypeFilter(atomType, s.createImageOf(a)));
	}

	@Override
	public boolean add(FOFormula atoms) {
		return this.facts.add(atoms);
	}

	@Override
	public boolean addAll(Collection<Atom> atoms) {
		return this.add(FOFormulaFactory.instance().createOrGetConjunction(atoms.toArray(new Atom[0])));
	}

	@Override
	public boolean add(Atom atom) {
		return this.addAll(Set.of(atom));
	}

	@Override
	public boolean remove(Atom atom) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean remove(FOFormula atoms) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection<Atom> atoms) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public Stream<Atom> getAtoms() {
		return this.facts.stream().flatMap(formula -> formula.asAtomSet().stream());
	}

	@Override
	public Iterator<Atom> getAtomsByPredicate(Predicate p) {
		return this.getAtoms()
				.filter(a -> a.getPredicate().equals(p))
				.iterator();
	}

	@Override
	public Iterator<Predicate> getPredicates() {
		return this.getAtoms()
				.map(Atom::getPredicate)
				.distinct()
				.iterator();
	}

	@Override
	public Iterator<Term> getTermsByPredicatePosition(Predicate p, int position) {
		return this.getAtoms()
				.filter(a -> a.getPredicate().equals(p))
				.map(a -> a.getTerm(position))
				.distinct()
				.iterator();
	}

	@Override
	public FactBaseDescription getDescription(Predicate viewPredicate) {
		return null;
	}

	@Override
	public FactBaseType getType(Predicate viewPredicate) {
		return FactBaseType.GRAAL;
	}

}
