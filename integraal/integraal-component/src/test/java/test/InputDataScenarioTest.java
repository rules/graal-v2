package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

import org.junit.Test;

import fr.boreal.component_builder.InputDataScenario;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.query.api.Query;
import fr.boreal.storage.builder.StorageBuilder;

/**
 * 
 */
public class InputDataScenarioTest {

    /**
     * Helper method to retrieve the absolute path of a resource in the test/resources folder.
     * 
     * @param relativePath The relative path to the resource.
     * @return The absolute Path object of the resource.
     * @throws URISyntaxException If the path syntax is incorrect.
     */
    private Path getAbsoluteResourcePath(String relativePath) throws URISyntaxException {
        return Paths.get(ClassLoader.getSystemResource(relativePath).toURI());
    }

    /**
     * 
     */
    @Test
    public void testSetFactbase() {
        InputDataScenario scenario = new InputDataScenario("TestScenario");
        FactBase factBase = StorageBuilder.defaultStorage();
        scenario.setFactbase(factBase);

        // Check that it is set correctly the first time
        assertEquals(Optional.of(factBase), scenario.getFactBase());

        // Try setting the factbase again
        FactBase anotherFactBase = StorageBuilder.defaultStorage();
        scenario.setFactbase(anotherFactBase);

        // Ensure the factbase has not been updated
        assertEquals(Optional.of(factBase), scenario.getFactBase());
    }

    /**
     * @throws URISyntaxException
     */
    @Test
    public void testSetFactbasePaths() throws URISyntaxException {
        InputDataScenario scenario = new InputDataScenario("TestScenario");

        // Get absolute paths using the helper method
        String[] paths = {
            getAbsoluteResourcePath("factbase/test_factbase.csv").toString(),
            getAbsoluteResourcePath("factbase/test_factbase.dlgp").toString()
        };

        scenario.setFactbasePaths(paths);

        // Verify that the paths were set correctly
        Collection<String> expectedPaths = Arrays.asList(paths);
        assertTrue(scenario.getFactbasePaths().isPresent());
        assertEquals(expectedPaths, scenario.getFactbasePaths().get());

        // Try setting the factbase paths again
        String[] newPaths = {
            getAbsoluteResourcePath("factbase/test_factbase2.csv").toString()
        };
        scenario.setFactbasePaths(newPaths);

        // Ensure the paths were not updated
        assertEquals(expectedPaths, scenario.getFactbasePaths().get());
    }

    /**
     * 
     */
    @Test
    public void testSetRulebase() {
        InputDataScenario scenario = new InputDataScenario("TestScenario");

        RuleBase ruleBase = new RuleBaseImpl();
        scenario.setRulebase(ruleBase);

        // Check that it is set correctly the first time
        assertEquals(Optional.of(ruleBase), scenario.getRuleBase());

        // Try setting the rulebase again
        RuleBase anotherRuleBase = new RuleBaseImpl();
        scenario.setRulebase(anotherRuleBase);

        // Ensure the rulebase has not been updated
        assertEquals(Optional.of(ruleBase), scenario.getRuleBase());
    }

    /**
     * @throws URISyntaxException
     */
    @Test
    public void testSetRulebasePaths() throws URISyntaxException {
        InputDataScenario scenario = new InputDataScenario("TestScenario");

        // Get absolute paths using the helper method
        String[] paths = {
            getAbsoluteResourcePath("rulebase/test_rulebase.dlgp").toString(),
            getAbsoluteResourcePath("rulebase/test_rulebase.owl").toString()
        };

        scenario.setRulebasePaths(paths);

        // Verify that the paths were set correctly
        Collection<String> expectedPaths = Arrays.asList(paths);
        assertTrue(scenario.getRulebasePath().isPresent());
        assertEquals(expectedPaths, scenario.getRulebasePath().get());

        // Try setting the rulebase paths again
        String[] newPaths = {
            getAbsoluteResourcePath("rulebase/test_rulebase2.owl").toString()
        };
        scenario.setRulebasePaths(newPaths);

        // Ensure the paths were not updated
        assertEquals(expectedPaths, scenario.getRulebasePath().get());
    }

    /**
     * 
     */
    @Test
    public void testSetQuerybase() {
        InputDataScenario scenario = new InputDataScenario("TestScenario");

        Collection<Query> queries = new ArrayList<>(); // Use mock or real Query objects
        scenario.setQuerybase(queries);

        // Check that it is set correctly the first time
        assertEquals(Optional.of(queries), scenario.getQueryBase());

        // Try setting the querybase again
        Collection<Query> newQueries = new ArrayList<>();
        scenario.setQuerybase(newQueries);

        // Ensure the querybase has not been updated
        assertEquals(Optional.of(queries), scenario.getQueryBase());
    }

    /**
     * @throws URISyntaxException
     */
    @Test
    public void testSetQuerybasePaths() throws URISyntaxException {
        InputDataScenario scenario = new InputDataScenario("TestScenario");

        // Get absolute paths using the helper method
        String[] paths = {
            getAbsoluteResourcePath("querybase/test_querybase.dlgp").toString(),
            getAbsoluteResourcePath("querybase/test_querybase2.dlgp").toString()
        };

        scenario.setQuerybasePaths(paths);

        // Verify that the paths were set correctly
        Collection<String> expectedPaths = Arrays.asList(paths);
        assertTrue(scenario.getQuerybasePaths().isPresent());
        assertEquals(expectedPaths, scenario.getQuerybasePaths().get());

        // Try setting the querybase paths again
        String[] newPaths = {
            getAbsoluteResourcePath("querybase/test_querybase3.dlgp").toString()
        };
        scenario.setQuerybasePaths(newPaths);

        // Ensure the paths were not updated
        assertEquals(expectedPaths, scenario.getQuerybasePaths().get());
    }

    /**
     * @throws URISyntaxException
     */
    @Test
    public void testSetMappingbasePaths() throws URISyntaxException {
        InputDataScenario scenario = new InputDataScenario("TestScenario");

        // Get absolute paths using the helper method
        String[] paths = {
            getAbsoluteResourcePath("mappingbase/test_mappingbase.json").toString(),
            getAbsoluteResourcePath("mappingbase/test_mappingbase.vd").toString()
        };

        scenario.setMappingbasePaths(paths);

        // Verify that the paths were set correctly
        Collection<String> expectedPaths = Arrays.asList(paths);
        assertTrue(scenario.getMappingbasePaths().isPresent());
        assertEquals(expectedPaths, scenario.getMappingbasePaths().get());

        // Try setting the mappingbase paths again
        String[] newPaths = {
            getAbsoluteResourcePath("mappingbase/test_mappingbase2.vd").toString()
        };
        scenario.setMappingbasePaths(newPaths);

        // Ensure the paths were not updated
        assertEquals(expectedPaths, scenario.getMappingbasePaths().get());
    }
}
