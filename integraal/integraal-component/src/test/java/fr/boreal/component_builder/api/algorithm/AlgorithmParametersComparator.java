package fr.boreal.component_builder.api.algorithm;

import fr.boreal.component_builder.AlgorithmParameters;
import fr.lirmm.boreal.util.enumerations.EnumUtils;

import java.util.Objects;

/**
 * Provides utility methods to compare {@link AlgorithmParameters} objects.
 */
public class AlgorithmParametersComparator {

    /**
     * Compares two {@link AlgorithmParameters} objects by checking whether
     * each corresponding getter returns the same value.
     *
     * @param left  the first {@code AlgorithmParameters} object
     * @param right the second {@code AlgorithmParameters} object
     * @return {@code true} if both objects have the same parameter values
     *         according to their public getters; {@code false} otherwise
     */
    public static boolean haveSameParameterValues(IAlgorithmParameters left, IAlgorithmParameters right) {
        // Quick check: same reference or both null
        if (left == right) {
            return true;
        }
        // If one is null and the other isn't
        if (left == null || right == null) {
            return false;
        }

        // Compare name and algorithm
        if (!Objects.equals(left.getName(), right.getName())) {
            return false;
        }
        if (!Objects.equals(left.getAlgorithm(), right.getAlgorithm())) {
            return false;
        }

        // Compare Answers (asksCountOnly, asksLists, asksSet)
        if (left.asksCountOnly() != right.asksCountOnly()) {
            return false;
        }
        if (left.asksLists() != right.asksLists()) {
            return false;
        }
        if (left.asksSet() != right.asksSet()) {
            return false;
        }

        if (!Objects.equals(left.getRuleApplier(), right.getRuleApplier())) {
            return false;
        }
        if (!Objects.equals(left.getScheduler(), right.getScheduler())) {
            return false;
        }
        if (!Objects.equals(left.getCriterion(), right.getCriterion())) {
            return false;
        }
        if (!Objects.equals(left.getComputer(), right.getComputer())) {
            return false;
        }
        if (!Objects.equals(left.getSkolemization(), right.getSkolemization())) {
            return false;
        }

        // Compare storage parameters (db type, driver, strategy)
        if (!Objects.equals(left.getStorageType(), right.getStorageType())) {
            return false;
        }
        if (!Objects.equals(left.getDBDriver(), right.getDBDriver())) {
            return false;
        }
        if (!Objects.equals(left.getDBStrategy(), right.getDBStrategy())) {
            return false;
        }

        // Compare compilation
        if (!Objects.equals(left.getCompilation(), right.getCompilation())) {
            return false;
        }

        // Compare integer and duration parameters (rank, max, timeout)
        if (!Objects.equals(left.getRank(), right.getRank())) {
            return false;
        }
        if (!Objects.equals(left.getMax(), right.getMax())) {
            return false;
        }
        if (!Objects.equals(left.getTimeout(), right.getTimeout())) {
            return false;
        }

        // Compare DBMS driver parameters (URL, PORT, etc.)
        if (!Objects.equals(left.getDBMSDriverParameters(), right.getDBMSDriverParameters())) {
            return false;
        }

        // Compare external halting conditions if needed
        if (!Objects.equals(left.getExternalHaltingConditions(), right.getExternalHaltingConditions())) {
            return false;
        }

        // If all checks pass, the parameter values match
        return true;
    }
}
