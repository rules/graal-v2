
package fr.boreal.component_builder.evaluators.query_evaluation.auxiliary;

import java.util.Collections;
import java.util.Iterator;
import java.util.function.Function;

import fr.boreal.model.logicalElements.api.Substitution;

/**
 * A class to encapsulate the function of evaluating a FOQuery and producing a
 * QueryEvaluationOutput.
 */
public class DefaultQueryEvaluationFunction implements Function<QueryEvaluationInput, QueryEvaluationOutput> {

	@Override
	public QueryEvaluationOutput apply(QueryEvaluationInput input) {

		
		if (input.count() && input.constantsOnly()) {
			long n_answers = input.queryEvaluator().countAnswers(input.query(), input.factBase());
			return new QueryEvaluationOutput(input.query(), Collections.emptyIterator(), n_answers, null);
		} else if (input.count()) {
			long n_answers = input.queryEvaluator().countHomomorphism(input.query(), input.factBase());
			return new QueryEvaluationOutput(input.query(), Collections.emptyIterator(), n_answers, null);
		}

		Iterator<Substitution> answers;
		if (input.constantsOnly()) {
			answers = input.queryEvaluator().evaluate(input.query(), input.factBase());
		} else {
			answers = input.queryEvaluator().homomorphism(input.query(), input.factBase());
		}
		return new QueryEvaluationOutput(input.query(), answers, null, null);
	}
}
