package fr.boreal.component_builder.api.scenario;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import fr.boreal.model.query.api.Query;

/**
 * The {@code IQueryBaseScenario} interface defines methods for managing the
 * query base in a knowledge base scenario.
 *
 */
interface IQueryBaseScenario {

	/**
	 * Retrieves the path(s) to the query base file(s).
	 *
	 * @return an {@link Optional} containing the file paths, or empty if not set
	 */
	Optional<Collection<String>> getQuerybasePaths();

	/**
	 * Sets the path to the query base file, if not already set.
	 *
	 * @param querybasepath the path to the query base file
	 */
	void setQuerybasePaths(String... querybasepath);

	/**
	 * Retrieves the query base object.
	 *
	 * @return an {@link Optional} containing the {@link Set} of {@link Query}
	 *         objects, or empty if not set
	 */
	Optional<Collection<Query>> getQueryBase();

	/**
	 * Sets the query base object, if not already set.
	 *
	 * @param queries the {@link Set} of {@link Query} objects to set
	 */
	void setQuerybase(Collection<Query> queries);

	/**
	 * Sets the query base object, if not already set.
	 * 
	 * @param query of {@link Query} object to set
	 */
	default void setQuerybase(Query query) {
		setQuerybase(Set.of(query));
	}
}
