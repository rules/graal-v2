package fr.boreal.component_builder.components;

import java.util.Collection;
import java.util.Objects;

import fr.boreal.component_builder.api.algorithm.IAlgorithmParameters;
import fr.boreal.component_builder.evaluators.query_evaluation.CountingQueryEvaluatorWithMultiEvaluator;
import fr.boreal.component_builder.evaluators.query_evaluation.QueryEvaluatorWithMultiEvaluator;
import fr.boreal.component_builder.externalHaltingConditions.ExternalAlgorithmHaltingConditions;
import fr.boreal.configuration.keywords.InteGraalKeywords;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.query.api.Query;

/**
 * A builder class for preparing and creating components for query answering
 * over fact bases. This class provides methods for setting up query evaluators
 * with optional external halting conditions.
 */
public class QueryAnsweringComponentBuilder {

	/**
	 * Prepares and returns a {@link QueryEvaluatorWithMultiEvaluator} for answering
	 * the given set of queries over the provided fact base, with optional external
	 * halting conditions to limit non-terminating algorithms.
	 *
	 * @param queries  a collection of {@link Query} objects to be evaluated over
	 *                 the fact base. This must not be {@code null}.
	 * @param factbase the {@link FactBase} that serves as the data source for
	 *                 answering the queries. This must not be {@code null}.
	 * @param ap       optional conditions used to limit the evaluation process,
	 *                 such as timeouts or maximum number of states, as well as the
	 *                 type of answers.
	 * @throws NullPointerException if either {@code queries} or {@code factbase} is
	 *                              {@code null}.
	 */
	public static QueryEvaluatorWithMultiEvaluator prepareAndGetQueryAnsweringFrom(Collection<Query> queries,
			FactBase factbase, IAlgorithmParameters ap) {

		Objects.requireNonNull(queries, "query must not be null");
		Objects.requireNonNull(factbase, "factBase must not be null");
		Objects.requireNonNull(ap, "algorithm parameters must not be null");

		var constantsOnly = !ap.getResultType().equals(InteGraalKeywords.Algorithms.Images.ALL);
		var hc = new ExternalAlgorithmHaltingConditions(ap.getRank().isEmpty() ? null : ap.getRank().get(),
				ap.getTimeout().isEmpty() ? null : ap.getTimeout().get());

		return new QueryEvaluatorWithMultiEvaluator(queries, factbase, null, constantsOnly, hc);
	}

	/**
	 * Prepares and returns a {@link CountingQueryEvaluatorWithMultiEvaluator} for answering the given
	 * set of queries over the provided fact base, with optional external halting
	 * conditions to limit non-terminating algorithms. The
	 * {@link CountingQueryEvaluatorWithMultiEvaluator} counts the number of answers in addition to
	 * performing the query evaluation.
	 *
	 * @param queries  a collection of {@link Query} objects to be evaluated over
	 *                 the fact base. This must not be {@code null}.
	 * @param factbase the {@link FactBase} that serves as the data source for
	 *                 answering the queries. This must not be {@code null}.
	 * @param ap       optional conditions used to limit the evaluation process,
	 *                 such as timeouts or maximum number of states, as well as the
	 *                 type of answers.
	 * @return a {@link CountingQueryEvaluatorWithMultiEvaluator} configured to answer the given
	 *         queries over the fact base with optional halting conditions and
	 *         counting the number of answers.
	 * @throws NullPointerException if either {@code queries} or {@code factbase} is
	 *                              {@code null}.
	 */
	public static CountingQueryEvaluatorWithMultiEvaluator prepareAndGetCountingQueryAnsweringFrom(Collection<Query> queries,
																								   FactBase factbase, IAlgorithmParameters ap) {

		Objects.requireNonNull(queries, "query must not be null");
		Objects.requireNonNull(factbase, "factBase must not be null");
		Objects.requireNonNull(ap, "algorithm parameters must not be null");

		var constantsOnly = !ap.getResultType().equals(InteGraalKeywords.Algorithms.Images.ALL);
		var hc = new ExternalAlgorithmHaltingConditions(ap.getRank().isEmpty() ? null : ap.getRank().get(),
				ap.getTimeout().isEmpty() ? null : ap.getTimeout().get());

		return new CountingQueryEvaluatorWithMultiEvaluator(queries, factbase, null, constantsOnly, hc);
	}

}
