package fr.boreal.component_builder.api.algorithm;

import java.util.Optional;

import fr.boreal.configuration.keywords.InteGraalKeywords;

/**
 * Interface for specifying the parameters related to a chase algorithm. The
 * chase algorithm is responsible for saturation, rule application, and
 * skolemization strategies.
 */
interface IForwardChainingParameters {

	/**
	 * Indicates whether the algorithm uses a saturation algorithm.
	 * 
	 * @return true if the algorithm uses a saturation algorithm, false otherwise
	 */
	boolean usesSaturationAlgorithm();

	/**
	 * Indicates whether the algorithm uses a saturation algorithm for OMQA.
	 * 
	 * @return true if the algorithm uses a saturation algorithm for OMQA, false otherwise
	 */

	boolean usesOMQASaturationAlgorithm();

	/**
	 * Gets the chase scheduler used in the algorithm.
	 * 
	 * @return an Optional containing the scheduler, or an empty Optional if not set
	 */
	Optional<InteGraalKeywords.Algorithms.Parameters.Chase.Scheduler> getScheduler();

	/**
	 * Sets the chase scheduler to be used in the algorithm.
	 * 
	 * @param schedulerName the scheduler to be set
	 * @return the configuration with settled value
	 */
	IAlgorithmParameters setScheduler(InteGraalKeywords.Algorithms.Parameters.Chase.Scheduler schedulerName);

	/**
	 * Gets the chase criterion used in the algorithm.
	 * 
	 * @return an Optional containing the chase criterion, or an empty Optional if
	 *         not set
	 */
	Optional<InteGraalKeywords.Algorithms.Parameters.Chase.Checker> getCriterion();

	/**
	 * Sets the chase criterion to be used in the algorithm.
	 * 
	 * @param criterionName the chase criterion to be set
	 * @return the configuration with settled value
	 */
	IAlgorithmParameters setCriterion(InteGraalKeywords.Algorithms.Parameters.Chase.Checker criterionName);

	/**
	 * Gets the chase computer used in the algorithm.
	 * 
	 * @return an Optional containing the chase computer, or an empty Optional if
	 *         not set
	 */
	Optional<InteGraalKeywords.Algorithms.Parameters.Chase.Computer> getComputer();

	/**
	 * Sets the chase computer to be used in the algorithm.
	 * 
	 * @param computerName the chase computer to be set
	 * @return the configuration with settled value
	 */
	IAlgorithmParameters setComputer(InteGraalKeywords.Algorithms.Parameters.Chase.Computer computerName);

	/**
	 * Gets the rule applier used in the chase algorithm.
	 * 
	 * @return an Optional containing the rule applier, or an empty Optional if not
	 *         set
	 */
	Optional<InteGraalKeywords.Algorithms.Parameters.Chase.Applier> getRuleApplier();

	/**
	 * Sets the rule applier to be used in the chase algorithm.
	 * 
	 * @param applierName the rule applier to be set
	 * @return the configuration with settled value
	 */
	IAlgorithmParameters setRuleApplier(InteGraalKeywords.Algorithms.Parameters.Chase.Applier applierName);

	/**
	 * Gets the skolemization strategy used in the chase algorithm.
	 * 
	 * @return an Optional containing the skolemization strategy, or an empty
	 *         Optional if not set
	 */
	Optional<InteGraalKeywords.Algorithms.Parameters.Chase.Namer> getSkolemization();

	/**
	 * Sets the skolemization strategy to be used in the chase algorithm.
	 * 
	 * @param skolemizationName the skolemization strategy to be set
	 * @return the configuration with settled value
	 */
	IAlgorithmParameters setSkolemization(InteGraalKeywords.Algorithms.Parameters.Chase.Namer skolemizationName);
	
	/**
	 * Gets the evaluator used in the chase algorithm.
	 * 
	 * @return an Optional containing the evaluator, or an empty
	 *         Optional if not set
	 */
	Optional<InteGraalKeywords.Algorithms.Parameters.Chase.Evaluator> getEvaluator();

	/**
	 * Sets the evaluator to be used in the chase algorithm.
	 * 
	 * @param evaluatorName the evaluator to be set
	 * @return the configuration with settled value
	 */
	IAlgorithmParameters setEvaluator(InteGraalKeywords.Algorithms.Parameters.Chase.Evaluator evaluatorName);

	/**
	 * Gets the transformer used in the chase algorithm.
	 * 
	 * @return an Optional containing the transformer, or an empty
	 *         Optional if not set
	 */
	Optional<InteGraalKeywords.Algorithms.Parameters.Chase.Transformer> getTransformer();

	/**
	 * Sets the transformer to be used in the chase algorithm.
	 * 
	 * @param transformerName the transformer to be set
	 * @return the configuration with settled value
	 */
	IAlgorithmParameters setTransformer(InteGraalKeywords.Algorithms.Parameters.Chase.Transformer transformerName);

}
