package fr.boreal.component_builder.components;

import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.boreal.component_builder.api.algorithm.IAlgorithmParameters;
import fr.boreal.configuration.keywords.InteGraalKeywords.InternalStorageConfiguration;
import fr.boreal.configuration.keywords.InteGraalKeywords.InternalStorageConfiguration.DBMSDriverParameters;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.storage.builder.StorageBuilder;
import fr.boreal.storage.natives.DefaultInMemoryAtomSet;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;

class StorageComponent {

	static final Logger LOG = LoggerFactory.getLogger(StorageComponent.class);

	static FactBase prepareStorage(IAlgorithmParameters integraalAlgorithmParameters) {

		if (integraalAlgorithmParameters.getStorageType().isEmpty()) {

			return new SimpleInMemoryGraphStore();

		}

		FactBase storage = switch (integraalAlgorithmParameters.getStorageType().get()) {

		case DefaultInMemoryAtomSet -> new DefaultInMemoryAtomSet();

		case SQL -> {
			StorageBuilder b = new StorageBuilder();
			if (integraalAlgorithmParameters.getDBDriver().isEmpty()) {
				b.useHSQLDB(integraalAlgorithmParameters.getName());
			} else {
				b = switch (integraalAlgorithmParameters.getDBDriver().get()) {
				case MySQL -> b.useMySQLDB(null);
				case PostgreSQL -> b.usePostgreSQLDB(getConnectionString(integraalAlgorithmParameters));

				case SQLite -> b.useSQLiteDB(null);
				default -> b.useHSQLDB(null);
				};
			}
			if (integraalAlgorithmParameters.getDBStrategy().isEmpty()) {
				b.useEncodingAdHocSQLStrategy();
			} else {
				b = switch (integraalAlgorithmParameters.getDBStrategy().get()) {
				case AdHocSQL -> b.useAdHocSQLStrategy();
				default -> b.useEncodingAdHocSQLStrategy();
				};
			}
			yield b.build().get();
		}
		case SPARQL -> {
			StorageBuilder b = new StorageBuilder();
			b.useSPARQLEndpoint(null);
			yield b.build().get(); // Use yield for consistency
		}
		default -> new SimpleInMemoryGraphStore();
		};

		if (mustClearDB(integraalAlgorithmParameters)) {
			storage.clear();
		}

		return storage;
	}

	private static boolean mustClearDB(IAlgorithmParameters integraalAlgorithmParameters) {

		if (missingDriverParams(integraalAlgorithmParameters)) {
			return false;
		}

		Map<InternalStorageConfiguration.DBMSDriverParameters, String> params = integraalAlgorithmParameters
				.getDBMSDriverParameters().get();

		if (params.get(DBMSDriverParameters.CLEAR_DB) != null) {
			return Boolean.parseBoolean(params.get(DBMSDriverParameters.CLEAR_DB));
		}

		return false;
	}

	private static String getConnectionString(IAlgorithmParameters integraalAlgorithmParameters) {

		String port = "5432";
		String databasename = "integraal";
		String username = "postgres";
		String userpassword = "admin";
		String template = "jdbc:postgresql://localhost:%s/%s?user=%s&password=%s";

		if(missingDriverParams(integraalAlgorithmParameters)) {
			// returning default connection
			return String.format(template, port, databasename, username, userpassword);
		}

		Map<InternalStorageConfiguration.DBMSDriverParameters, String> params = integraalAlgorithmParameters
				.getDBMSDriverParameters().get();

		if (params.get(DBMSDriverParameters.URL) != null) {
			return params.get(DBMSDriverParameters.URL);
		}

		if (params.get(DBMSDriverParameters.PORT) != null) {
			port = params.get(DBMSDriverParameters.PORT);
		}

		if (params.get(DBMSDriverParameters.DATABASE_NAME) != null) {
			databasename = params.get(DBMSDriverParameters.DATABASE_NAME);
		}

		if (params.get(DBMSDriverParameters.USER_NAME) != null) {
			username = params.get(DBMSDriverParameters.USER_NAME);
		}

		if (params.get(DBMSDriverParameters.USER_PASSWORD) != null) {
			userpassword = params.get(DBMSDriverParameters.USER_PASSWORD);
		}

		return String.format(template, port, databasename, username, userpassword);

	}

	/**
	 * Ensures the parameters exists
	 * 
	 * @param integraalAlgorithmParameters
	 */
	private static boolean missingDriverParams(IAlgorithmParameters integraalAlgorithmParameters) {

		Optional<Map<InternalStorageConfiguration.DBMSDriverParameters, String>> params = integraalAlgorithmParameters
				.getDBMSDriverParameters();

		if (params.isEmpty()) {

			LOG.debug("no parameters for the DBMS driver");

			return true;
		}

		return false;

	}
}
