package fr.boreal.component_builder.operations;

import java.io.Serializable;

import fr.boreal.component_builder.api.IOperationResult;
import fr.boreal.configuration.keywords.InteGraalKeywords;
import fr.boreal.configuration.keywords.InteGraalKeywords.Algorithms;

/**
 * records the operation which has not been performed
 * 
 * @param operation
 */
public record OperationNotPerformed(String operation) implements IOperationResult, Serializable {

	/**
	 * records the operation which has not been performed
	 * 
	 * @param operation
	 */
	public OperationNotPerformed(InteGraalKeywords.MonitoringOperations operation) {
		this(operation.toString());
	}

	/**
	 * records the algorithm which failed
	 * 
	 * @param algorithm
	 */
	public OperationNotPerformed(Algorithms algorithm) {
		this(algorithm.toString());
	}

	public String serializationString() {
		return "Operation non performed : " + operation;
	}

}
