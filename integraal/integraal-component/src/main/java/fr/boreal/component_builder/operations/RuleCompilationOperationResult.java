package fr.boreal.component_builder.operations;

import java.io.Serializable;

import fr.boreal.component_builder.api.IOperationResult;
import fr.boreal.model.ruleCompilation.api.RuleCompilationResult;

/**
 * records the size of the rule compilation
 * 
 * @param size
 */
public record RuleCompilationOperationResult(long size) implements IOperationResult, Serializable {

	/**
	 * records the size of the rule compilation
	 * 
	 * @param compilResult
	 */
	public RuleCompilationOperationResult(RuleCompilationResult compilResult) {
		this(compilResult.compilableRules().size());
	}

	public String serializationString() {
		return "Compilable rules found : " + size;
	}
}
