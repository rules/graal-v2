package fr.boreal.component_builder.api.algorithm;

import java.util.Optional;

import fr.boreal.configuration.keywords.InteGraalKeywords;

/**
 * Interface for compilation parameters.
 */
interface IBackwardChainingParameters {

	boolean usesQueryRewritingAlgorithm();

	boolean usesRuleCompilationAlgorithm();

	boolean usesOMQARewritingAlgorithm();

	Optional<InteGraalKeywords.Algorithms.Parameters.Compilation> getCompilation();

	IAlgorithmParameters setCompilation(InteGraalKeywords.Algorithms.Parameters.Compilation compilationName);
}