
package fr.boreal.component_builder.evaluators.query_evaluation.auxiliary;

import java.util.Collections;
import java.util.function.BiFunction;

import fr.lirmm.boreal.util.object_analyzer.ObjectAnalizer;

/**
 * Creates a query evaluation output where the timeout is indicated
 */
public class DefaultQueryEvaluationOutputIfTimeoutFunction
		implements BiFunction<QueryEvaluationInput, String, QueryEvaluationOutput> {

	@Override
	public QueryEvaluationOutput apply(QueryEvaluationInput t, String u) {

		return new QueryEvaluationOutput(t.query(), Collections.emptyIterator(), null,
				"Query " + ObjectAnalizer.digest(t.query()) + " TIMEOUT after " + u + " seconds");

	}

}
