package fr.boreal.component_builder.api.algorithm;

import fr.boreal.configuration.keywords.InteGraalKeywords.Algorithms.Answers;
import fr.boreal.configuration.keywords.InteGraalKeywords.Algorithms.Images;

/**
 * Interface for count-only queries.
 */
interface IAnswerType {

	boolean usesQueryAnsweringAlgorithm();

	/**
	 * @return true if only the result should be a list
	 */
	boolean asksLists();

	/**
	 * @return true if only the result should be a set
	 */
	boolean asksSet();

	/**
	 * @return the result type (list, set, count)
	 */
	Answers getResultType();

	/**
	 * @return the image type (constants, all)
	 */

	Images getImageType();

	/**
	 * @return true if only the count is requested.
	 */
	boolean asksCountOnly();

	/**
	 * sets the type of result
	 * 
	 * @param type expected
	 * 
	 */
	void setResultType(Answers type);

	/**
	 * sets the type of images
	 * 
	 * @param type expected
	 * 
	 */
	void setImageType(Images type);
}