package fr.boreal.component_builder.evaluators.query_evaluation;

import java.util.Collection;

import fr.boreal.component_builder.evaluators.query_evaluation.auxiliary.*;
import org.slf4j.LoggerFactory;

import fr.boreal.component_builder.externalHaltingConditions.ExternalAlgorithmHaltingConditions;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.queryEvaluation.api.QueryEvaluator;
import fr.boreal.component_builder.evaluators.generic.MultiEvaluator;

/**
 * A class that wraps around an FOQueryEvaluator to provide both batch and lazy
 * evaluation for a collection of FOQuery objects for which it only counts the
 * number of answers.
 *
 */
public class CountingQueryEvaluatorWithMultiEvaluator extends MultiEvaluator<QueryEvaluationInput, QueryEvaluationOutput> {

	static {
		LOG = LoggerFactory.getLogger(CountingQueryEvaluatorWithMultiEvaluator.class);
	}

	/**
	 * 
	 * @param queries        The collection of FOQuery objects to be evaluated.
	 * @param factBase       The FactBase against which the queries are evaluated.
	 * @param queryEvaluator The concrete evaluator to use
	 * @param constantsOnly  true iff variables must only map to constants
	 * @param externalHaltingConditions 
	 */
	public CountingQueryEvaluatorWithMultiEvaluator(Collection<Query> queries,
													FactBase factBase,
													QueryEvaluator<Query> queryEvaluator,
													boolean constantsOnly,
													ExternalAlgorithmHaltingConditions externalHaltingConditions) {

		super(new QueryInputWrapper(queries, factBase, queryEvaluator, constantsOnly, true).createQueryInputs(),
				new PreemptiveQueryEvaluationFunction(), new DefaultQueryEvaluationFunction(), externalHaltingConditions,
				new DefaultQueryEvaluationOutputIfTimeoutFunction());
	}

}
