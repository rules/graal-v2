package fr.boreal.component_builder.operations;

import java.io.Serializable;

import fr.boreal.component_builder.api.IOperationResult;
import fr.boreal.model.kb.api.RuleBase;

/**
 * records the size of the loaded rule base
 * 
 * @param size
 */
public record RuleBaseLoadingOperationResult(long size) implements IOperationResult, Serializable {

	/**
	 * records the size of the loaded rule base
	 * 
	 * @param rb
	 */
	public RuleBaseLoadingOperationResult(RuleBase rb) {
		this(rb.getRules().size());
	}

	public String serializationString() {
		return "Rulebase size : " + size;
	}

}
