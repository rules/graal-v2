package fr.boreal.component_builder.evaluators.query_evaluation.auxiliary;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.queryEvaluation.api.QueryEvaluator;
import fr.boreal.query_evaluation.generic.DefaultGenericQueryEvaluator;

public class QueryInputWrapper {

	private final Collection<Query> queries;
	private final FactBase factBase;
	private final QueryEvaluator<Query> queryEvaluator;
	private final boolean constantsOnly;
	private final boolean count;

	public QueryInputWrapper(Collection<Query> queries, FactBase factBase, QueryEvaluator<Query> queryEvaluator,
			boolean constantsOnly, boolean count) {
		Objects.requireNonNull(queries, "query must not be null");
		Objects.requireNonNull(factBase, "factBase must not be null");

		this.queries = queries;
		this.factBase = factBase;

		this.constantsOnly = constantsOnly;
		this.count = count;

		// if the evaluator is null use the default generic one
		this.queryEvaluator = (queryEvaluator != null ? queryEvaluator
				: DefaultGenericQueryEvaluator.defaultInstance());

	}

	public List<QueryEvaluationInput> createQueryInputs() {
		return queries.stream()
				.map(query -> new QueryEvaluationInput(query, factBase, queryEvaluator, constantsOnly, count))
				.collect(Collectors.toList());
	}
}
