package fr.boreal.component_builder.operations;

import java.io.Serializable;

import fr.boreal.component_builder.api.IOperationResult;
import fr.boreal.forward_chaining.chase.Chase;

/**
 * records the size of the chase result
 * 
 * @param size
 */
public record ChaseRunOperationResult(long size) implements IOperationResult, Serializable {

	/**
	 * 
	 * records the size of the chase result
	 * 
	 * @param chase
	 */
	public ChaseRunOperationResult(Chase chase) {
		this(chase.getFactBase().size());
	}

	public String serializationString() {
		return "Factbase size after chase : " + size;
	}

}
