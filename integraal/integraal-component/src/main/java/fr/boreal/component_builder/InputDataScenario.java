package fr.boreal.component_builder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.boreal.component_builder.api.scenario.IInputDataScenario;
import fr.boreal.component_builder.utils.StringUtils;
import fr.boreal.configuration.keywords.InteGraalKeywords;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.query.api.Query;
import fr.boreal.views.FederatedFactBase;
import radicchio.FileUtils;

/**
 * Represents a scenario for specifying the input of an InteGraal service (e.g.,
 * saturation). Provides mechanisms to set and retrieve data (facts, rules,
 * queries, mapping, and federation).
 */
public class InputDataScenario implements IInputDataScenario {

	private static final Logger LOG = LoggerFactory.getLogger(InputDataScenario.class);

	private final String name;

	// Note we use mutable wrappers for code concision.

	// KB elements from files
	private MW<Collection<String>> factbasePaths = new MW<>(null, false, "Factbase");
	private MW<Collection<String>> rulebasePaths = new MW<>(null, false, "Rulebase");
	private MW<Collection<String>> querybasePaths = new MW<>(null, false, "Querybase");
	private MW<Collection<String>> mappingbasePaths = new MW<>(null, false, "MappingBase");

	// KB elements from objects
	private MW<FactBase> factbase = new MW<>(null, false, "Factbase");
	private MW<RuleBase> rulebase = new MW<>(null, false, "Rulebase");
	private MW<Collection<Query>> querybase = new MW<>(null, false, "Querybase");

	// Hybrid RuleBase
	private MW<HybridRuleBase> hybridRulebase = new MW<>(null, false, "Hybrid Rulebase");
	private MW<HybridRuleBaseFilePath> hybridRulebaseFilePath = new MW<>(null, false, "Hybrid Rulebase");

	/**
	 * Constructs a new InputDataScenario with the specified name.
	 *
	 * @param scenarioName The name of the scenario.
	 */
	public InputDataScenario(String scenarioName) {
		this.name = scenarioName;
	}

	@Override
	public String getName() {
		return name;
	}

	public String toString() {
		return StringUtils.printObjectWithModifiableWrapper(this);
	}

	// FactBase Methods

	@Override
	public Optional<FactBase> getFactBase() {
		return Optional.ofNullable(factbase.getValue());
	}

	@Override
	public Optional<Collection<String>> getFactbasePaths() {
		return Optional.ofNullable(factbasePaths.getValue());
	}

	@Override
	public void setFactbase(FactBase fb) {
		setBase(factbase, fb);
	}

	@Override
	public void setFactbasePaths(String... paths) {
		setBasePaths(factbasePaths, paths, InteGraalKeywords.SupportedFileExtensions.Factbase.class);
	}

	// RuleBase Methods

	@Override
	public Optional<RuleBase> getRuleBase() {
		return Optional.ofNullable(rulebase.getValue());
	}

	@Override
	public Optional<Collection<String>> getRulebasePath() {
		return Optional.ofNullable(rulebasePaths.getValue());
	}

	@Override
	public void setRulebase(RuleBase rb) {
		setBase(rulebase, rb);
	}

	@Override
	public void setRulebasePaths(String... paths) {
		setBasePaths(rulebasePaths, paths, InteGraalKeywords.SupportedFileExtensions.Rulebase.class);
	}

	// QueryBase Methods

	@Override
	public Optional<Collection<Query>> getQueryBase() {
		return Optional.ofNullable(querybase.getValue());
	}

	@Override
	public Optional<Collection<String>> getQuerybasePaths() {
		return Optional.ofNullable(querybasePaths.getValue());
	}

	@Override
	public void setQuerybase(Collection<Query> qb) {
		setBase(querybase, qb);
	}

	@Override
	public void setQuerybasePaths(String... paths) {
		setBasePaths(querybasePaths, paths, InteGraalKeywords.SupportedFileExtensions.Querybase.class);
	}

	// MappingBase Methods

	@Override
	public Optional<Collection<String>> getMappingbasePaths() {
		return Optional.ofNullable(mappingbasePaths.getValue());
	}

	@Override
	public void setMappingbasePaths(String... paths) {
		setBasePaths(mappingbasePaths, paths, InteGraalKeywords.SupportedFileExtensions.Mappingbase.class);
	}

	// Federated Factbase Methods

	@Override
	public Optional<FederatedFactBase> getFederatedFactBase() throws Exception {
		return Optional.ofNullable(
				factbase.getValue() instanceof FederatedFactBase federatedFactBase ? federatedFactBase : null);
	}

	@Override
	public void setFederatedFactBase(FederatedFactBase fb) {
		setBase(this.factbase, fb);
	}

	// Hybrid RuleBase Methods

	@Override
	public Optional<RuleBase> getHybridRulebaseSat() {
		return Optional.ofNullable(hybridRulebase.getValue().saturationRules());
	}

	@Override
	public Optional<RuleBase> getHybridRulebaseRew() {
		return Optional.ofNullable(hybridRulebase.getValue().rewriteRules());
	}

	@Override
	public Optional<Collection<String>> getHybridRulebaseSatPaths() {
		return Optional.ofNullable(hybridRulebaseFilePath.getValue().saturationRulesFile());
	}

	@Override
	public Optional<Collection<String>> getHybridRulebaseRewPaths() {
		return Optional.ofNullable(hybridRulebaseFilePath.getValue().rewriteRulesFile());
	}

	@Override
	public void setHybridRulebase(RuleBase rbSat, RuleBase rbRew) {
		setBase(hybridRulebase, new HybridRuleBase(rbSat, rbRew));
	}

	@Override
	public void setHybridRulebasePaths(Collection<String> rulebasePathsSat, Collection<String> rulebasePathsRew) {
		setBase(hybridRulebaseFilePath, new HybridRuleBaseFilePath(rulebasePathsSat, rulebasePathsRew));
	}

	// Helper methods

	/**
	 * Helper method to set a base object (e.g., FactBase, RuleBase) and update the
	 * corresponding flag.
	 *
	 * @param <T>      The type of the base value (FactBase, RuleBase, etc.).
	 * @param wrapper  The wrapper containing the value and the flag.
	 * @param newValue The new value to set.
	 */
	private <T> void setBase(MW<T> wrapper, T newValue) {
		if (!checkAndWarn(wrapper)) {
			wrapper.setValue(newValue);
			wrapper.setFlagToTrue();
		}
	}

	/**
	 * Helper method to set base file paths.
	 *
	 * @param wrapper        The wrapper for the collection of paths.
	 * @param paths          The new paths to set.
	 * @param extensionClass The class for the file extension (e.g.,
	 *                       Rulebase.class).
	 */
	private void setBasePaths(MW<Collection<String>> wrapper, String[] paths, Class<? extends Enum<?>> extensionClass) {
		if (!checkAndWarn(wrapper)) {
			Collection<String> basePaths = new ArrayList<>();
			for (String path : paths) {
				basePaths.addAll(getFilesAndCheckExtensions(path, extensionClass));
			}
			if (!basePaths.isEmpty()) {
				wrapper.setValue(basePaths);
				wrapper.setFlagToTrue();
			}
		}
	}

	/**
	 * Helper method to check if a base has already been set, and log a warning if
	 * it has.
	 *
	 * @param wrapper The wrapper containing the value and flag.
	 * @return true if the base was already set, false otherwise.
	 */
	private boolean checkAndWarn(MW<?> wrapper) {
		if (wrapper.isSet()) {
			LOG.warn("{} was already set! Ignoring attempt to set {}.", wrapper.getBaseName(), wrapper.getBaseName());
			return true;
		}
		return false;
	}

	/**
	 * Auxiliary method to retrieve file names and check extensions.
	 *
	 * @param path           The file path.
	 * @param extensionClass The extension class used for validation.
	 * @return A collection of file names matching the extensions.
	 */
	private Collection<String> getFilesAndCheckExtensions(String path, Class<? extends Enum<?>> extensionClass) {
		return FileUtils.getFileNames(path, extensionClass);
	}

	// Inner classes for Hybrid RuleBase and its file paths

	private record HybridRuleBase(RuleBase saturationRules, RuleBase rewriteRules) {
	}

	private record HybridRuleBaseFilePath(Collection<String> saturationRulesFile, Collection<String> rewriteRulesFile) {
	}

	@Override
	public Collection<String> getAllReferencedFilePaths() {
		Collection<String> fileList = new ArrayList<>();
		getFactbasePaths().ifPresent(fileList::addAll);
		getQuerybasePaths().ifPresent(fileList::addAll);
		getRulebasePath().ifPresent(fileList::addAll);
		getMappingbasePaths().ifPresent(fileList::addAll);
		getHybridRulebaseSatPaths().ifPresent(fileList::addAll);
		getHybridRulebaseRewPaths().ifPresent(fileList::addAll);
		return fileList;
	}

	// MutableWrapper class

	public static class MW<T> {
		private T value;
		private boolean isSet;
		private final String baseName;

		public MW(T value, boolean isSet, String baseName) {
			this.value = value;
			this.isSet = isSet;
			this.baseName = baseName;
		}

		public T getValue() {
			return value;
		}

		public void setValue(T value) {
			this.value = value;
		}

		public boolean isSet() {
			return isSet;
		}

		public void setFlagToTrue() {
			this.isSet = true;
		}

		public String getBaseName() {
			return baseName;
		}
	}

}
