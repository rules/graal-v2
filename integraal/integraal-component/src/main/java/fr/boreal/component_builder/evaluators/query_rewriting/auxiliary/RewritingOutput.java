package fr.boreal.component_builder.evaluators.query_rewriting.auxiliary;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

import fr.boreal.model.query.api.Query;
import fr.boreal.model.query.impl.UnionFOQuery;
import fr.lirmm.boreal.util.object_analyzer.ObjectAnalizer;

public record RewritingOutput(Query query, UnionFOQuery rewritings, Optional<String> message)
		implements Serializable {

	public RewritingOutput {
		Objects.requireNonNull(query);
		Objects.requireNonNull(rewritings);
		Objects.requireNonNull(message);
	}

	public String printRewritingCount() {

		if (rewritings.getQueries().isEmpty()) {
			if (message.isEmpty()) {
				throw new IllegalStateException("no rewritings no error message");
			}
			return message.get();
		}

		String template = "Number of rewritings for %s : %s";
		return String.format(template, query, getInfos());
	}

	public String printOnlyRewritingCount() {

		if (rewritings.getQueries().isEmpty()) {
			if (message.isEmpty()) {
				throw new IllegalStateException("no rewritings no error message");
			}
			return message.get();
		}

		String template = "Number of rewritings : %s";

		return String.format(template, getInfos());
	}

	private String getInfos() {
        return ObjectAnalizer.info(rewritings) + "\t";
	}
}
