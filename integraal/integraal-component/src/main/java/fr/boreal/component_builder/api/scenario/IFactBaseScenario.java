package fr.boreal.component_builder.api.scenario;

import java.io.InputStream;
import java.io.Reader;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.storage.builder.StorageBuilder;
import fr.boreal.views.FederatedFactBase;

/**
 * The {@code IFactBaseScenario} interface defines methods for setting the fact
 * base in a knowledge base scenario where there is a single centralized
 * factbase. In case of a federation please use
 * {@code IFederatedFactBaseScenario}.
 */
interface IFactBaseScenario {

	/**
	 * Retrieves the path(s) to the fact base file(s).
	 *
	 * @return an {@link Optional} containing the file paths or empty if not set
	 */
	Optional<Collection<String>> getFactbasePaths();

	/**
	 * Sets the path(s) to the fact base file, if not already set.
	 *
	 * @param factbasepath the path to the fact base file
	 */
	void setFactbasePaths(String... factbasepath);

	/**
	 * Retrieves the fact base object.
	 *
	 * @return an {@link Optional} containing the {@link FactBase}, or empty if not
	 *         set
	 */
	Optional<FactBase> getFactBase();

	/**
	 * Sets the fact base object, if not already set.
	 *
	 * @param factbase the {@link FactBase} object to set
	 */
	void setFactbase(FactBase factbase);

	/**
	 * Gets the federated fact base.
	 * 
	 * @return FederatedFactBase
	 * @throws Exception
	 */
	Optional<FederatedFactBase> getFederatedFactBase() throws Exception;

	/**
	 * Sets the federated fact base object, if not already set.
	 *
	 * @param federatedfactbase the {@link FederatedFactBase} object to set
	 */
	void setFederatedFactBase(FederatedFactBase federatedfactbase);

	/**
	 * Sets the fact base object from an object that represents a DLGP string.
	 * 
	 * @param inputObject an object that can be turn into DLGP ; required not null
	 */
	default void setFactBaseFromInputDLGP(Object inputObject) {

		Objects.requireNonNull(inputObject);

		DlgpParser p =

				switch (inputObject) {
				case String is -> new DlgpParser(is);
				case InputStream is -> new DlgpParser(is);
				case Reader is -> new DlgpParser(is);
				default -> throw new IllegalArgumentException("Unsupported input type for DLGP : " + inputObject);

				};

		FactBase fb = StorageBuilder.defaultStorage();
		fb.addAll(p.streamParsedObjects(Atom.class));
		setFactbase(fb);
		p.close();
	}
}
