package fr.boreal.component_builder.evaluators.query_rewriting.auxiliary;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.ruleCompilation.api.RuleCompilation;

public class RewritingInputWrapper {
	private final Collection<Query> queries;
	private final RuleBase ruleBase;
	private final RuleCompilation compilation;

	public RewritingInputWrapper(Collection<Query> queries, RuleBase ruleBase, RuleCompilation compilation) {
		this.queries = queries;
		this.ruleBase = ruleBase;
		this.compilation = compilation;
	}

	public List<RewritingInput> createRewritingInputs() {
		return queries.stream()
				.map(query -> new RewritingInput(query, ruleBase, compilation))
				.collect(Collectors.toList());
	}
}
