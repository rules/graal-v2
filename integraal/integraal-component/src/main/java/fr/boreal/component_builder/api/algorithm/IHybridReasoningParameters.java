package fr.boreal.component_builder.api.algorithm;

interface IHybridReasoningParameters {

	boolean usesOMQAHybridAlgorithm();
}
