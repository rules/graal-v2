package fr.boreal.component_builder.evaluators.query_evaluation;

import java.util.Collection;

import fr.boreal.component_builder.evaluators.query_evaluation.auxiliary.*;
import org.slf4j.LoggerFactory;

import fr.boreal.component_builder.externalHaltingConditions.ExternalAlgorithmHaltingConditions;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.queryEvaluation.api.QueryEvaluator;
import fr.boreal.component_builder.evaluators.generic.MultiEvaluator;

/**
 * A class that wraps around an FOQueryEvaluator to provide both batch and lazy
 * evaluation for a collection of FOQuery objects.
 *
 */
public class QueryEvaluatorWithMultiEvaluator extends MultiEvaluator<QueryEvaluationInput, QueryEvaluationOutput> {

	static {
		LOG = LoggerFactory.getLogger(QueryEvaluatorWithMultiEvaluator.class);
	}

	/**
	 * Constructs a QueryEvaluator with the given collection of queries and fact
	 * base.
	 *
	 * @param queries                   The collection of FOQuery objects to be
	 *                                  evaluated.
	 * @param factBase                  The FactBase against which the queries are
	 *                                  evaluated.
	 * @param queryEvaluator            The concrete evaluator to use
	 * @param constantsOnly             true iff variables must only map to
	 *                                  constants
	 * @param externalHaltingConditions
	 */
	public QueryEvaluatorWithMultiEvaluator(Collection<Query> queries, FactBase factBase,
			QueryEvaluator<Query> queryEvaluator, boolean constantsOnly,
			ExternalAlgorithmHaltingConditions externalHaltingConditions) {

		super(new QueryInputWrapper(queries, factBase, queryEvaluator, constantsOnly, false).createQueryInputs(),
				new PreemptiveQueryEvaluationFunction(), new DefaultQueryEvaluationFunction(),
				externalHaltingConditions, new DefaultQueryEvaluationOutputIfTimeoutFunction());

	}

}
