package fr.boreal.component_builder.api.scenario;

import java.util.Collection;
import java.util.Optional;

/**
 * The {@code IFederatedFactBaseScenario} interface defines methods for managing
 * a federated factbase in a knowledge-based data management scenario.
 * 
 */
interface IMappingBaseScenario {

	/**
	 * Retrieves the path(s) to the mapping base file(s).
	 *
	 * @return an {@link Optional} containing the file paths, or empty if not set
	 */
	Optional<Collection<String>> getMappingbasePaths();

	/**
	 * Sets the path to the mapping base file, if not already set.
	 *
	 * @param mappingbasepath the path to the mapping base file
	 */
	void setMappingbasePaths(String... mappingbasepath);

}
