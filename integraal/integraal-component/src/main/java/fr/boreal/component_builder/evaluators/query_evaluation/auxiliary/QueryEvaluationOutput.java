package fr.boreal.component_builder.evaluators.query_evaluation.auxiliary;

import java.util.Iterator;
import java.util.Objects;

import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.Query;
import fr.lirmm.boreal.util.object_analyzer.ObjectAnalizer;

public record QueryEvaluationOutput(
		Query query,
		Iterator<Substitution> answers,
		Long result_size,
		String message) {

	public QueryEvaluationOutput {
		Objects.requireNonNull(query);
		if (result_size == null && answers == null) {
			throw new IllegalArgumentException("Either result_size or answers must be non-null.");
		}
	}

	public String getPrintQueryAnswersCount() {
		if (message != null) {
			return message;
		}
		return "Query " + ObjectAnalizer.digest(query)
				+ (result_size() != null ? "\tANSWERS : " + result_size : "\tEMPTY RESULT");
	}

	public String printOnlyAnswersCount() {
		if (message != null) {
			return message;
		}
		return (result_size() != null ? "\tANSWERS : " + result_size : "\tEMPTY RESULT");
	}

	public String getPrintQueryAnswers() {
		if (message != null) {
			return message;
		}
		StringBuilder resultBuilder = new StringBuilder("Query ").append(ObjectAnalizer.digest(query));
		answers.forEachRemaining(answer -> resultBuilder.append("\n").append(answer));
		return resultBuilder.toString();
	}
}
