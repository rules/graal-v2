package fr.boreal.component_builder.api.scenario;

import java.util.Collection;

import fr.boreal.component_builder.InputDataScenario;
import fr.boreal.component_builder.utils.FileHandler;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.query.api.Query;

/**
 * Knowledge base reasoning scenario as input data.
 */

public interface IInputDataScenario
		extends IFactBaseScenario, IMappingBaseScenario, IRuleBaseScenario, IQueryBaseScenario, FileHandler {

	/**
	 * Retrieves the name of the scenario.
	 *
	 * @return The name of the scenario.
	 */
	String getName();

	/////////////////////////////////
	//// STATIC METHODS
	/////////////////////////////////

	/**
	 * @param fb
	 * @param rb
	 * @return the KB scenario
	 */
	static IInputDataScenario KB(FactBase fb, RuleBase rb) {
		IInputDataScenario s = new InputDataScenario("KB");
		s.setFactbase(fb);
		s.setRulebase(rb);
		return s;
	}

	/**
	 * @param rb
	 * @param queries
	 * @return the OMQ scenario
	 */
	static IInputDataScenario OMQ(RuleBase rb, Collection<Query> queries) {
		IInputDataScenario s = new InputDataScenario("OMQ");
		s.setRulebase(rb);
		s.setQuerybase(queries);
		return s;

	}

	/**
	 * @param fb
	 * @param rb
	 * @param queries
	 * @return the OMQA scenario
	 */
	static IInputDataScenario OMQA(FactBase fb, RuleBase rb, Collection<Query> queries) {
		IInputDataScenario s = new InputDataScenario("OMQA");
		s.setFactbase(fb);
		s.setRulebase(rb);
		s.setQuerybase(queries);
		return s;
	}

	/**
	 * @param fb
	 * @param queries
	 * @return the QA scenario
	 */
	static IInputDataScenario QA(FactBase fb, Collection<Query> queries) {
		IInputDataScenario s = new InputDataScenario("QA");
		s.setFactbase(fb);
		s.setQuerybase(queries);
		return s;
	}

	/**
	 * @param ruleBase
	 * @return the Compilation scenario
	 */
	static IInputDataScenario Compilation(RuleBase ruleBase) {
		IInputDataScenario s = new InputDataScenario("Compilation");
		s.setRulebase(ruleBase);
		return s;
	}

	/**
	 * @param fb
	 * @param rb_sat
	 * @param rb_rew
	 * @param queries
	 * @return the hybrid OMQA scenario
	 */
	static IInputDataScenario OMQAHybrid(FactBase fb, RuleBase rb_sat, RuleBase rb_rew,
			Collection<Query> queries) {
		IInputDataScenario s = new InputDataScenario("Hybrid OMQA");
		s.setFactbase(fb);
		s.setHybridRulebase(rb_sat, rb_rew);
		s.setQuerybase(queries);
		return s;

	}

}
