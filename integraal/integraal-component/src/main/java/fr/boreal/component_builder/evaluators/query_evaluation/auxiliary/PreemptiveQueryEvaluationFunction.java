
package fr.boreal.component_builder.evaluators.query_evaluation.auxiliary;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.function.Function;

import fr.boreal.model.logicalElements.api.Substitution;

/**
 * A class to encapsulate the function of evaluating a FOQuery and producing a
 * QueryEvaluationOutput. It goes through all the answers to a query to force
 * its full evaluation. <br>
 * Use this if you need to force the full evaluation of the query e.g. for
 * experiments
 */
public class PreemptiveQueryEvaluationFunction implements Function<QueryEvaluationInput, QueryEvaluationOutput> {

	@Override
	public QueryEvaluationOutput apply(QueryEvaluationInput input) {


		if (input.count() && input.constantsOnly()) {
			long n_answers = input.queryEvaluator().countAnswers(input.query(), input.factBase());
			return new QueryEvaluationOutput(input.query(), Collections.emptyIterator(), n_answers, null);
		} else if (input.count()) {
			long n_answers = input.queryEvaluator().countHomomorphism(input.query(), input.factBase());
			return new QueryEvaluationOutput(input.query(), Collections.emptyIterator(), n_answers, null);
		}

		Iterator<Substitution> answers;
		if (input.constantsOnly()) {
			answers = input.queryEvaluator().evaluate(input.query(), input.factBase());
		} else {
			answers = input.queryEvaluator().homomorphism(input.query(), input.factBase());
		}
		return new QueryEvaluationOutput(input.query(), preemption(answers), null, null);
	}

	// go through all the answers to a query to force its full evaluation.
	private Iterator<Substitution> preemption(Iterator<Substitution> s) {
		ArrayList<Substitution> answersList = new ArrayList<>();
		while (s.hasNext()) {
			answersList.add(s.next());
		}
		return answersList.iterator();
	}
}
