package fr.boreal.component_builder.evaluators.query_rewriting;

import java.util.Collection;

import fr.boreal.component_builder.evaluators.query_rewriting.auxiliary.*;
import org.slf4j.LoggerFactory;

import fr.boreal.component_builder.externalHaltingConditions.ExternalAlgorithmHaltingConditions;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.ruleCompilation.NoRuleCompilation;
import fr.boreal.model.ruleCompilation.api.RuleCompilation;
import fr.boreal.component_builder.evaluators.generic.MultiEvaluator;

/**
 * A class for rewriting a collection of FOQuery objects.
 *
 */
public class QueryRewriterWitMultiEvaluator extends MultiEvaluator<RewritingInput, RewritingOutput> {

	static {

		LOG = LoggerFactory.getLogger(QueryRewriterWitMultiEvaluator.class);

	}

	/**
	 * Constructs a QueryRewriter with the given collection of queries, rules, and
	 * compilation. base. assumptions : if a compilation different from
	 * {@link NoRuleCompilation} is used, than the ruleset should contain only
	 * non-compilable rules.
	 * 
	 * @param queries  The collection of FOQuery objects to rewrite.
	 * @param ruleBase The RuleBase containing rules for rewriting. * @param
	 *                 compilation The RuleCompilation configuration for rewriting.
	 * @param compilation 
	 * @param externalHaltingConditions 
	 */

	public QueryRewriterWitMultiEvaluator(Collection<Query> queries, RuleBase ruleBase, RuleCompilation compilation,
										  ExternalAlgorithmHaltingConditions externalHaltingConditions) {

		super(new RewritingInputWrapper(queries, ruleBase, compilation).createRewritingInputs(),
				new DefaultRewritingFunction(), externalHaltingConditions,
				new DefaultQueryRewritingOutputIfTimeoutFunction());
	}

}
