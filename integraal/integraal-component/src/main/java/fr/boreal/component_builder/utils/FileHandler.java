package fr.boreal.component_builder.utils;

import java.util.Collection;

/**
 * A file handler is an object which manipulates files, and that can provide the
 * list of files it references.
 * 
 */
public interface FileHandler {

	/**
	 * @return the set of paths to files referenced by the object
	 */
	Collection<String> getAllReferencedFilePaths();

}
