package fr.boreal.io.api;

import java.io.Closeable;
import java.io.IOException;

/**
 * Write objects if possible
 * 
 * @author Florent Tornil
 *
 */
public interface Writer extends Closeable {
	
	/**
	 * Write the given object if possible
	 * @param object object to write
	 * @throws IOException
	 * 			 If an I/O error occurs
	 */
	void write(Object object) throws IOException;
	
	/**
	 * Flush the writer if possible
	 * @throws IOException
	 * 			 If an I/O error occurs
	 */
	void flush() throws IOException;
	
	/**
	 * Write the given objects if possible
	 * @param objects objects to write
	 * @throws IOException
	 * 			 If an I/O error occurs
	 */
	default void write(Object... objects) throws IOException {
		for(Object o : objects) {
			this.write(o);
		}
	}



}
