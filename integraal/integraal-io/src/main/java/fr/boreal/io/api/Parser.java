/*
 * Copyright (C) Inria Sophia Antipolis - Méditerranée / LIRMM
 * (Université de Montpellier & CNRS) (2014 - 2017)
 *
 * Contributors :
 *
 * Clément SIPIETER <clement.sipieter@inria.fr>
 * Mélanie KÖNIG
 * Swan ROCHER
 * Jean-François BAGET
 * Michel LECLÈRE
 * Marie-Laure MUGNIER <mugnier@lirmm.fr>
 *
 *
 * This file is part of Graal <https://graphik-team.github.io/graal/>.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.boreal.io.api;

import fr.boreal.io.dlgp.Directive;
import fr.boreal.io.dlgp.ParserResult;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.views.builder.ViewBuilder;
import fr.boreal.views.datasource.AbstractViewWrapper;
import fr.lirmm.boreal.util.stream.CloseableIterator;

import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * @author Clément Sipieter (INRIA) {@literal <clement@6pi.fr>}
 * @param <T> the type of objects returned by the parser
 *
 */
public interface Parser<T> extends CloseableIterator<T> {

	boolean hasNext() throws ParseException;

	T next() throws ParseException;

	/**
	 * 
	 * @return the parsed objects split by type into different collections
	 * @throws ParseException          iff a parsing error occur
	 * @throws ViewBuilder.ViewBuilderException iff a mapping parsing exception occur
	 * 
	 */
	default ParserResult parse() throws ParseException, ViewBuilder.ViewBuilderException {
		Collection<Atom> atoms = new ArrayList<>();
		Collection<FORule> rules = new ArrayList<>();
		Collection<Query> queries = new ArrayList<>();
		Collection<AbstractViewWrapper<String, ?>> views = new LinkedHashSet<>();

		while (this.hasNext()) {
			switch (this.next()) {
			case Atom a -> atoms.add(a);
			case FORule r -> rules.add(r);
			case Query q -> queries.add(q);
			case Directive d when d.type().equals(Directive.Type.VIEW) ->
				views.addAll(ViewBuilder.createFactBases(d.value().toString()));
			default -> {
			}
			}
        }
		return new ParserResult(atoms, rules, queries, views);
	}

	/**
	 * Streams parsed objects of a specific type.
	 * 
	 * @param <To>  The type of objects to stream (e.g., Atom or Rule).
	 * @param clazz The class object representing the type T.
	 * @return A stream of objects of type T.
	 */
	default <To> Stream<To> streamParsedObjects(Class<To> clazz) {
		Iterator<To> iterator = new Iterator<To>() {
			private To nextElement = null;
			private boolean hasFetchedNext = false;
			private boolean endReached = false;

			@Override
			public boolean hasNext() {
				if (endReached) {
					return false;
				}
				if (hasFetchedNext) {
					return true;
				}
				fetchNext();
				return nextElement != null;
			}

			@Override
			public To next() {
				if (!hasNext()) {
					throw new NoSuchElementException();
				}
				hasFetchedNext = false;
				return nextElement;
			}

			private void fetchNext() {
				hasFetchedNext = true;
				nextElement = null;
				while (true) {
					try {
						if (!Parser.this.hasNext()) {
							endReached = true;
							break;
						}
						Object obj = Parser.this.next();
						if (clazz.isInstance(obj)) {
							nextElement = clazz.cast(obj);
							break;
						}
					} catch (ParseException e) {
						throw new RuntimeException("Parsing failed", e);
					}
				}
			}
		};

		Spliterator<To> spliterator = Spliterators.spliteratorUnknownSize(iterator, Spliterator.ORDERED);
		return StreamSupport.stream(spliterator, false).onClose(() -> {
			try {
				Parser.this.close();
			} catch (Exception e) {
				// Handle close exception if necessary
			}
		});
	}

}
