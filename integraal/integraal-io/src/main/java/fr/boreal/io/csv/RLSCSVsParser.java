package fr.boreal.io.csv;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.boreal.io.api.ParseException;

import fr.boreal.io.api.Parser;
import fr.boreal.io.dlgp.ParserResult;
import fr.boreal.model.logicalElements.api.Atom;
import fr.lirmm.boreal.util.stream.ArrayBlockingStream;

/**
 * @author Florent Tornil
 * <p>
 *         This class parses a list of CSV file into atoms.
 * <p>
 *         Retrieves CSV files from a configuration file inspired from RLS
 *         syntax
 *         {@literal <a href="https://github.com/knowsys/rulewerk/wiki/Rule-syntax-grammar#source-declarations">...</a>}
 *         Each line of the configuration file defines a CSV file to be parsed
 *         as well as the predicate it defines.
 * <p>
 *         The {@link CSVParser} is used for each file. It is assumed that all
 *         the CSV file have the same separator, prefix and header size.
 *
 */
public class RLSCSVsParser implements Parser<Atom>, AutoCloseable {

	private final ArrayBlockingStream<Atom> buffer = new ArrayBlockingStream<>(512);
	private static final ExecutorService executor = Executors.newVirtualThreadPerTaskExecutor();


	/**
	 * @param filePath to RLS configuration file to parse with default configuration
	 * @param encode   a boolean true iff the CSV has to be encoded first
	 */
	public RLSCSVsParser(String filePath, boolean encode) {
		this(new File(filePath), CSVConstants.CSV_SEPARATOR, CSVConstants.CSV_PREFIX, CSVConstants.CSV_HEADER_SIZE_WHEN_RLS,
				encode);
	}

	/**
	 * @param file       RLS configuration file to parse
	 * @param separator  separator character of the CSV file
	 * @param prefix     prefix of the predicate name
	 * @param headerSize size of the header of the CSV file
	 * @param encode     a boolean true iff the CSV has to be encoded first
	 */
	public RLSCSVsParser(File file, char separator, String prefix, int headerSize, boolean encode) {
		
		new Thread(new Producer(file, buffer, separator, prefix, headerSize)).start();
	}

	@Override
	public boolean hasNext() {
		return buffer.hasNext();
	}

	@Override
	public Atom next() {
		return buffer.next();
	}

	@Override
	public void close() {
		this.buffer.close();
		executor.shutdownNow();
	}

	//
	// Private class Producer
	//

	static class Producer implements Runnable {

		private final File rls_file;
		private final ArrayBlockingStream<Atom> buffer;

		private final char separator;
		private final String prefix;
		private final int headerSize;

		public Producer(File file, ArrayBlockingStream<Atom> buffer, char separator, String prefix, int headerSize) {
			this.rls_file = file;
			this.buffer = buffer;
			this.separator = separator;
			this.prefix = prefix;
			this.headerSize = headerSize;
		}

		@Override
		public void run() {
			RLSCSVParser rlsParser = new RLSCSVParser(rls_file);
			while (rlsParser.hasNext()) {

				RLSCSVResult rls = rlsParser.next();
				CSVParser csvParser = new CSVParser(rls.predicateName(), rls.predicateArity(),
						new File(rls.csvFilepath()), this.separator, this.prefix, this.headerSize);
				while (csvParser.hasNext()) {
                    Atom a;
                    try {
                        a = csvParser.next();
                    } catch (ParseException e) {
                        throw new RuntimeException(e);
                    }
                    this.buffer.write(a);
				}
				csvParser.close();
			}
			rlsParser.close();
			this.buffer.close();
		}
	}

	public ParserResult parse() {
		Collection<Atom> atoms = new ArrayList<>();
		while (this.hasNext()) {
			atoms.add(this.next());
		}
		return new ParserResult(atoms, List.of(), List.of(), List.of());
	}

	/**
	 * @param configRLSFile to parse
	 * @return the lines in the configRLSFile
	 * @throws FileNotFoundException exception
	 */
	public static List<String> getRLSFileLines(String configRLSFile) throws FileNotFoundException {
		List<String> fileLines = new ArrayList<>();
		Scanner scanner = new Scanner(new File(configRLSFile));
		while (scanner.hasNextLine()) {
			fileLines.add(scanner.nextLine());
		}
		scanner.close();
		return fileLines;
	}
}
