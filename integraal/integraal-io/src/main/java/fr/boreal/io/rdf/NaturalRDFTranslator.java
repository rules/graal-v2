package fr.boreal.io.rdf;

import org.eclipse.rdf4j.model.Statement;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;

/**
 * Convert the given RDF statement into an atom
 * {@link RDFTranslationMode}
 */
public class NaturalRDFTranslator implements RDFTranslator {

	@Override
	public Atom statementToAtom(Statement st) {
		Predicate predicate = SameObjectPredicateFactory.instance().createOrGetPredicate(st.getPredicate().stringValue(), 2);
		Term subject = SameObjectTermFactory.instance().createOrGetLiteral(st.getSubject().stringValue());
		Term object = SameObjectTermFactory.instance().createOrGetLiteral(st.getObject().stringValue());
        return new AtomImpl(predicate, subject, object);
	}

}
