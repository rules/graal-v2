package fr.boreal.io.dlgp;

import java.util.HashSet;
import java.util.Set;

import fr.boreal.io.api.ParseException;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.rule.api.FORule;

/**
 * 
 * Util methods for DLGP parsing
 * 
 * @author Florent Tornil
 *
 */
public class DlgpUtil {

	/**
	 * Parses atoms from a dlgp String.
	 * 
	 * @param dlgp a dlgp-formatted String.
	 * @return the atoms.
	 * @throws ParseException if an exception occur while parsing DLGP input data
	 */
	public static Set<Atom> parseFacts(String dlgp) throws ParseException {
		Set<Atom> atoms = new HashSet<>();
		DlgpParser dlgp_parser = new DlgpParser(dlgp);
		while (dlgp_parser.hasNext()) {
			Object result = dlgp_parser.next();
			if (result instanceof Atom a) {
				atoms.add(a);
			}
		}
		dlgp_parser.close();
		return atoms;
	}

	/**
	 * Parses rules from a dlgp String.
	 * 
	 * @param dlgp a dlgp-formatted String.
	 * @return the rules.
	 * @throws ParseException if an exception occur while parsing DLGP input data
	 */
	public static Set<FORule> parseRules(String dlgp) throws ParseException {
		Set<FORule> rules = new HashSet<>();
		DlgpParser dlgp_parser = new DlgpParser(dlgp);
		while (dlgp_parser.hasNext()) {
			Object result = dlgp_parser.next();
			if (result instanceof FORule r) {
				rules.add(r);
			}
		}
		dlgp_parser.close();
		return rules;
	}

	/**
	 * Parses queries from a dlgp String.
	 * 
	 * @param dlgp a dlgp-formatted String.
	 * @return the queries.
	 * @throws ParseException if an exception occurs while parsing DLGP input data
	 */
	public static Set<Query> parseQueries(String dlgp) throws ParseException {
		Set<Query> queries = new HashSet<>();
		DlgpParser dlgp_parser = new DlgpParser(dlgp);
		while (dlgp_parser.hasNext()) {
			Object result = dlgp_parser.next();
			if (result instanceof FOQuery<?> q) {
				queries.add(q);
			}
		}
		dlgp_parser.close();
		return queries;
	}

}
