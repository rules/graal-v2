package fr.boreal.io.csv;

import fr.boreal.io.api.ParseException;
import fr.boreal.io.api.Parser;
import fr.boreal.io.dlgp.ParserResult;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.lirmm.boreal.util.stream.ArrayBlockingStream;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Florent Tornil
 * <p>
 *         This class parses a single CSV file into atoms.
 * <p>
 *         Each line of the file represents an atom, and every atom of the file
 *         has the same predicate either given to the constructor or deduced
 * <p>
 *         Each line of the file represents an atom and every atom of the file
 *         have the same predicate either given to the constructor or deduced
 *         from the file name.
 * 
 *         Please note that all the terms are seen as constants.
 */
public class CSVParser implements Parser<Atom>, AutoCloseable {

	private final ArrayBlockingStream<Object> buffer = new ArrayBlockingStream<>(512);
	private final ExecutorService executor = Executors.newVirtualThreadPerTaskExecutor();

	/**
	 * Parses the given CSV file using default values
	 * @param filePath path of the csv file to parse
	 */
	public CSVParser(String filePath) {
		this(new File(filePath), CSVConstants.CSV_SEPARATOR, CSVConstants.CSV_PREFIX, CSVConstants.CSV_HEADER_SIZE);
	}

	/**
	 * Parses the given CSV file using the given parsing arguments
	 * Uses the filename as predicate label
	 * @param file csv file to parse
	 * @param separator csv separator
	 * @param prefix (rdf) prefix
	 * @param headerSize size of the csv header
	 */
	public CSVParser(File file, char separator, String prefix, int headerSize) {
        executor.submit(new Producer(file, buffer, separator, prefix, headerSize));
	}

	/**
	 * Parses the given CSV file using the given predicate to create atoms
	 * @param predicateName label of the predicate
	 * @param arity arity of the predicate
	 * @param file csv file to parse
	 * @param separator csv separator
	 * @param prefix (rdf) prefix
	 * @param headerSize size of the csv header
	 */
	public CSVParser(String predicateName, int arity, File file, char separator, String prefix, int headerSize) {
        new Thread(new Producer(predicateName, arity, file, buffer, separator, prefix, headerSize)).start();
	}

	/**
	 * Parses the given CSV file using the given predicate to create atoms
	 * @param predicateName label of the predicate
	 * @param arity arity of the predicate
	 * @param file csv file to parse
	 * 
	 */
	public CSVParser(String predicateName, int arity, File file) {
		new Thread(new Producer(predicateName, arity, file, buffer, CSVConstants.CSV_SEPARATOR, CSVConstants.CSV_PREFIX, CSVConstants.CSV_HEADER_SIZE)).start();
	}

	@Override
	public boolean hasNext() {
		return buffer.hasNext();
	}

	@Override
	public Atom next() throws ParseException {
		Object val = buffer.next();
		if (val instanceof Throwable t) {
			if (t instanceof ParseException e) {
				throw e;
			}
			throw new ParseException("An error occurred while parsing.", t);
		}
		return (Atom) val;
	}

	@Override
	public void close() {
		this.buffer.close();
		executor.shutdownNow();
	}

	@Override
	public ParserResult parse()  {
		Collection<Atom> atoms = new ArrayList<>();
		while(this.hasNext()) {
            try {
                atoms.add(this.next());
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }
		return new ParserResult(atoms, List.of(), List.of(), List.of());
	}

	

	//
	// Private class Producer
	//

	static class Producer implements Runnable {

		private final PredicateFactory pf = SameObjectPredicateFactory.instance();
		private final TermFactory tf = SameObjectTermFactory.instance();

		private final File file;
		private final ArrayBlockingStream<Object> buffer;
		private final char separator;
		private final String prefix;
		private final int headerSize;

		private Predicate predicate = null;

		public Producer(File file, ArrayBlockingStream<Object> buffer, char separator, String prefix, int headerSize) {
			this.file = file;
			this.buffer = buffer;
			this.separator = separator;
			this.prefix = prefix;
			this.headerSize = headerSize;
		}

		public Producer(String predicateName, int arity, File file, ArrayBlockingStream<Object> buffer, char separator,
				String prefix, int headerSize) {
			this(file, buffer, separator, prefix, headerSize);
			this.predicate = this.pf.createOrGetPredicate(predicateName, arity);
			
		}

		@Override
		public void run() {
			CSVFormat format = CSVFormat.DEFAULT.builder()
					.setDelimiter(this.separator)
					.setSkipHeaderRecord(true)
					.setIgnoreSurroundingSpaces(true)
					.setAllowMissingColumnNames(true)
					.setQuote(null)
					.build();

			try (FileReader reader = new FileReader(this.file);
				 org.apache.commons.csv.CSVParser csvParser = new org.apache.commons.csv.CSVParser(reader, format)) {

				// Skip the header
				int skippedLines = 0;
				for (CSVRecord record : csvParser) {
					if (skippedLines < this.headerSize) {
						skippedLines++;
						continue;
					}

					if (this.predicate == null) {
						int arity = record.size();
						String predicateName = prefix + this.file.getName().split("\\.")[0].toLowerCase();
						this.predicate = this.pf.createOrGetPredicate(predicateName, arity);
					}

					List<Term> terms = new ArrayList<>(this.predicate.arity());
					for (String value : record) {
						Term t = this.tf.createOrGetLiteral(value);
						terms.add(t);
					}
					Atom a = new AtomImpl(this.predicate, terms);
					this.buffer.write(a);
				}
				this.buffer.close();
			} catch (IOException e) {
				this.buffer.write(e);
			}

		}
	}

}
