package fr.boreal.io.rdf;

/**
 * Translation mode for interpretation of RDF documents
 * A mode represent how an RDF triple (s, p, o) is translated into atoms
 */
public enum RDFTranslationMode {
	/**
	 * The raw translation uses the predicate "triple" of arity 3 and the 3 terms corresponding to s, p and o.
	 */
	Raw,
	
	/**
	 * The natural translation uses the predicate p of the triple as predicate of arity 2 and the 2 terms corresponding to s and o.
	 */
	Natural,
	/**
	 * The natural translation uses the predicate p of the triple as predicate of arity 2 and the 2 terms corresponding to s and o.
	 * In addition, if the predicate is rdf:type, then the object is used as the predicate of arity one instead with the 1 term corresponding to s.
	 */
	NaturalFull
}
