package fr.boreal.io.rdf;

import org.eclipse.rdf4j.model.Statement;

import fr.boreal.model.logicalElements.api.Atom;

/**
 * Translate (convert) RDF statements (triples) into atoms
 */
public interface RDFTranslator {
	
	/**
	 * Translate (convert) RDF statements (triples) into atoms
	 * @param st the rdf statement to convert
	 * @return the atom corresponding to the given statement according to the translator implementation
	 */
	Atom statementToAtom(Statement st);

}
