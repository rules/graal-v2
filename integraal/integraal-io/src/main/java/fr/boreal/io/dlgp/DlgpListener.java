package fr.boreal.io.dlgp;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.api.FOFormulaNegation;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.functions.Invoker;
import fr.boreal.model.functions.JavaMethodInvoker;
import fr.boreal.model.logicalElements.api.*;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.FactoryConstants;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.logicalElements.impl.ComputedAtomImpl;
import fr.boreal.model.partition.TermPartition;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.rule.api.Rule;
import fr.boreal.model.rule.impl.FORuleImpl;
import fr.lirmm.boreal.util.Prefix;
import fr.lirmm.boreal.util.stream.InMemoryStream;
import fr.lirmm.graphik.dlgp3.parser.ADlgpItemFactory.OBJECT_TYPE;
import fr.lirmm.graphik.dlgp3.parser.InvokeManager;
import fr.lirmm.graphik.dlgp3.parser.ParserListener;
import fr.boreal.model.functions.IntegraalInvokers;

import java.util.*;

class DlgpListener implements ParserListener {

	private final TermFactory termFactory = FactoryConstants.DEFAULT_TERM_FACTORY;
	private final PredicateFactory predicateFactory = FactoryConstants.DEFAULT_PREDICATE_FACTORY;
	private static final FOQueryFactory foQueryFactory = FOQueryFactory.instance();
	private static final FOFormulaFactory foFormulaFactory = FOFormulaFactory.instance();

	private List<Variable> answerVars;
	private Set<Atom> atomSet = null;
	private Set<Atom> atomSet2 = null;

	private TermPartition equalities = null;

	private Map<Variable, Variable> currentFactVariables = new HashMap<>();

	private boolean isNegativeMode = false;
	private Set<Set<Atom>> negative_parts = null;
	private Set<Atom> current_negative_part = null;
	private boolean isInFact = false;

	private Atom atom;
	private String label;

	private final InMemoryStream<Object> set;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	DlgpListener(InMemoryStream<Object> buffer) {
		this.set = buffer;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public void declarePrefix(String prefix, String ns) {
		this.set.write(new Prefix(prefix.substring(0, prefix.length() - 1), ns));
	}

	@Override
	public void declareBase(String base) {
		this.set.write(new Directive(Directive.Type.BASE, base));
	}

	@Override
	public void declareView(String viewDefinitionFile) {
		this.set.write(new Directive(Directive.Type.VIEW, viewDefinitionFile));
	}

	@Override
	public void declareTop(String top) {
		this.set.write(new Directive(Directive.Type.TOP, top));
	}

	@Override
	public void declareUNA() {
		this.set.write(new Directive(Directive.Type.UNA, null));
	}

	@Override
	public void directive(String text) {
		this.set.write(new Directive(Directive.Type.COMMENT, text));
	}

	@Override
	public void startsObject(OBJECT_TYPE objectType, String name) {
		this.label = (name == null) ? "" : name;

		this.atomSet = new LinkedHashSet<>();
		this.atomSet2 = null;
		this.negative_parts = new LinkedHashSet<>();
		this.current_negative_part = null;
		this.isInFact = false;

		if (OBJECT_TYPE.QUERY.equals(objectType)) {
			this.answerVars = new LinkedList<>();
			this.equalities = new TermPartition();
		} else if(OBJECT_TYPE.FACT.equals(objectType)) {
			this.currentFactVariables = new HashMap<>();
			this.isInFact = true;
		}
	}

	private void addAtomToCorrectSet(Atom a) {
		if (this.isNegativeMode) {
			this.current_negative_part.add(a);
		} else {
			this.atomSet.add(atom);
		}
	}

	@Override
	public void createsAtom(Object predicate, Object[] terms) {
		List<Term> list = new LinkedList<>();
		for (Object t : terms) {
			list.add(this.createTerm(t));
		}

		Predicate pred = this.createPredicate(predicate.toString(), terms.length);
		this.atom = new AtomImpl(pred, list);

		this.addAtomToCorrectSet(atom);
	}

	@Override
	public void createsEquality(Object term1, Object term2) {
		if(this.equalities == null) {
			throw new RuntimeException("Equalities are only supported in queries");
		}
		this.equalities.addClass(Set.of(this.createTerm(term1), this.createTerm(term2)));
	}

	@Override
	public void answerTermList(Object[] terms) {
		for (Object t : terms) {
			Term term = createTerm(t);
			if (term.isVariable()) {
				this.answerVars.add((Variable) term);
			} else {
				throw new RuntimeException("[Adding in answer variables : The term " + term + " is not a variable");
			}
		}
	}

	@Override
	public void endsConjunction(OBJECT_TYPE objectType) {
		switch (objectType) {
			case QUERY:
				this.createQueryConjunction(this.label, this.atomSet, this.negative_parts, this.answerVars, this.equalities);
				break;
			case NEG_CONSTRAINT:
				this.createNegativeConstraint(this.label, this.atomSet, this.negative_parts);
				break;
			case RULE:
				if (this.atomSet2 == null) {
					this.atomSet2 = this.atomSet;
					this.atomSet = new LinkedHashSet<>();
				} else {
					this.createRule(this.label, this.atomSet, this.negative_parts, this.atomSet2);
				}
				break;
			case FACT:
				this.createAtomSet(this.atomSet);
				break;
			default:
				break;
		}
	}

	/////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////

	private void createAtomSet(Set<Atom> atomset) {
		for (Atom atom : atomset) {
			this.set.write(atom);
		}
	}

	private Term createTerm(Object o) {
		if (o instanceof Term t) {
			if(this.isInFact && t instanceof Variable v) {
				Variable v2 = this.currentFactVariables.getOrDefault(v, this.createFreshVariable());
				this.currentFactVariables.put(v, v2);
				return v2;
			} else {
				return t;
			}

		} else {
			return this.createConstant(o.toString());
		}
	}

	private Predicate createPredicate(String label, int arity) {
		return this.predicateFactory.createOrGetPredicate(label, arity);
	}

	private Constant createConstant(String uri) {
		return this.termFactory.createOrGetConstant(uri);
	}

	private Variable createFreshVariable() {
		return this.termFactory.createOrGetFreshVariable();
	}

	private void createQueryConjunction(String label, Set<Atom> atomSet, Set<Set<Atom>> negParts,
										List<Variable> answerVars, TermPartition equalities) {
		List<FOFormula> subqueries = new ArrayList<>(atomSet);
		for (Set<Atom> negPart : negParts) {
			List<FOFormula> negsubqueries = new ArrayList<>(negPart);
			FOFormulaConjunction formula = foFormulaFactory.createOrGetConjunction(negsubqueries);
			FOFormulaNegation formulaNeg = foFormulaFactory.createOrGetNegation(formula);
			subqueries.add(formulaNeg);
		}
		FOFormula formula;
		if(subqueries.size() == 1) {
			formula = subqueries.getFirst();
		} else {
			formula = foFormulaFactory.createOrGetConjunction(subqueries);
		}
		this.set.write(foQueryFactory.createOrGetQuery(label, formula, answerVars, equalities));
	}

	private void createRule(String label, Set<Atom> body, Set<Set<Atom>> negParts, Set<Atom> head) {

		Collection<FOFormula> body_formulas = new ArrayList<>(body);
		for (Set<Atom> negPart : negParts) {
			List<FOFormula> negsubqueries = new ArrayList<>(negPart);
			FOFormulaConjunction formula = foFormulaFactory.createOrGetConjunction(negsubqueries);
			FOFormulaNegation formulaNeg = foFormulaFactory.createOrGetNegation(formula);
			body_formulas.add(formulaNeg);
		}
		FOFormulaConjunction body_formula = foFormulaFactory.createOrGetConjunction(body_formulas);

		Collection<FOFormula> head_subformulas = new ArrayList<>(head);
		FOFormulaConjunction head_formula = foFormulaFactory.createOrGetConjunction(head_subformulas);

		Rule rule = new FORuleImpl(label, body_formula, head_formula);
		this.set.write(rule);
	}

	private void createNegativeConstraint(String label, Set<Atom> body, Set<Set<Atom>> negParts) {
        Collection<FOFormula> body_formulas = new ArrayList<>(body);
		for (Set<Atom> negPart : negParts) {
            List<FOFormula> negsubqueries = new ArrayList<>(negPart);
			FOFormulaConjunction formula = foFormulaFactory.createOrGetConjunction(negsubqueries);
			FOFormulaNegation formulaNeg = foFormulaFactory.createOrGetNegation(formula);
			body_formulas.add(formulaNeg);
		}
		FOFormulaConjunction body_formula = foFormulaFactory.createOrGetConjunction(body_formulas);

		Rule rule = new FORuleImpl(label, body_formula, new AtomImpl(Predicate.BOTTOM));
		this.set.write(rule);
	}


	@Override
	public void declareComputed(String prefix, String ns) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createsComputedAtom(Object predicate, Object[] terms, Object invoker) {
		List<Term> list = new LinkedList<>();
		for (Object t : terms) {
			list.add(this.createTerm(t));
		}

		String fname = predicate.toString().split("#")[1];
		Predicate pred = this.createPredicate(fname, terms.length);

		Class<?> invokerClass = ((Class<?>) ((InvokeManager) invoker)
				.getInvokerObject(predicate.toString().split("#")[0] + "#"));

		if (invokerClass == null) {
			// standard functions
			IntegraalInvokers stdfct = new IntegraalInvokers(termFactory);
			Invoker graal_invoker = stdfct.getInvoker(pred.label());

			this.atom = new ComputedAtomImpl(graal_invoker, pred, list);
			this.addAtomToCorrectSet(atom);
		} else {
			String class_name = invokerClass.getName();
			try {
				Invoker graal_invoker = new JavaMethodInvoker(this.termFactory,
						InvokeManager.classToPath.get(class_name), class_name, fname);

				this.atom = new ComputedAtomImpl(graal_invoker, pred, list);
				this.addAtomToCorrectSet(atom);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void createsSpecialAtom(Object leftTerm, String predicate, Object rightTerm, Object invokerObject) {
		if(predicate.equals("http://www.lirmm.fr/dlgp/#eq")) {
			this.createsEquality(leftTerm, rightTerm);
		}
	}

	@Override
	public void beginNegation() {
		this.current_negative_part = new LinkedHashSet<>();
		this.isNegativeMode = true;
	}

	@Override
	public void endNegation() {
		this.negative_parts.add(this.current_negative_part);
		this.current_negative_part = null;
		this.isNegativeMode = false;
	}


}