package fr.boreal.test.io;

import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;

public class TestData {

	// Predicates
	public static final Predicate p1 = SameObjectPredicateFactory.instance().createOrGetPredicate("p1", 1);
	public static final Predicate q1 = SameObjectPredicateFactory.instance().createOrGetPredicate("q1", 1);
	public static final Predicate r1 = SameObjectPredicateFactory.instance().createOrGetPredicate("r1", 1);
	public static final Predicate q2 = SameObjectPredicateFactory.instance().createOrGetPredicate("q2", 2);
	public static final Predicate r2 = SameObjectPredicateFactory.instance().createOrGetPredicate("r2", 2);

	// Terms
	public static final Variable u = SameObjectTermFactory.instance().createOrGetVariable("u");
	public static final Variable v = SameObjectTermFactory.instance().createOrGetVariable("v");
	public static final Variable w = SameObjectTermFactory.instance().createOrGetVariable("w");
	public static final Variable x = SameObjectTermFactory.instance().createOrGetVariable("x");
	public static final Variable y = SameObjectTermFactory.instance().createOrGetVariable("y");
	public static final Variable z = SameObjectTermFactory.instance().createOrGetVariable("z");
	public static final Variable fb = SameObjectTermFactory.instance().createOrGetVariable("fb");

	public static final Constant a = SameObjectTermFactory.instance().createOrGetConstant("a");
	public static final Constant b = SameObjectTermFactory.instance().createOrGetConstant("b");
	public static final Constant c = SameObjectTermFactory.instance().createOrGetConstant("c");
	public static final Constant d = SameObjectTermFactory.instance().createOrGetConstant("d");

}
