package fr.boreal.test.io.dlgp;

import fr.boreal.io.api.ParseException;
import fr.boreal.io.api.Parser;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.views.builder.ViewBuilder;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.stream.Stream;

@RunWith(Parameterized.class)
class DlgpViewTest {

    @Parameterized.Parameters
    static Stream<Arguments> data() {
        return Stream.of(
                Arguments.of("@view v:<src/test/resources/v.vd>\n ?(X) :- v:view(X, Y).")
        );
    }

    @Parameterized.Parameters
    static Stream<Arguments> dataError() {
        return Stream.of(
                Arguments.of("@view v:<src/test/resources/w.vd>\n ?(X) :- v:view(X, Y)."),
                Arguments.of("@view v:<src/test/resources/v.vd>\n ?(X) :- v:v(X, Y)."),
                Arguments.of("@view v:<src/test/resources/v.vd>\n ?(X) :- v:view(X).")
        );
    }

    @DisplayName("Test DLGP import using view declaration")
    @ParameterizedTest(name = "{index}: DLGP import should work ...)")
    @MethodSource("data")
    public void dlgpViewImport(String dlgp) throws ViewBuilder.ViewBuilderException, ParseException {
        Parser<?> parser = new DlgpParser(dlgp);
        parser.parse();
        parser.close();
    }

    @DisplayName("Test DLGP import using view declaration with errors")
    @ParameterizedTest(name = "{index}: DLGP import should not work ...)")
    @MethodSource("dataError")
    public void dlgpViewImportError(String dlgp) throws ViewBuilder.ViewBuilderException {
        try (Parser<?> parser = new DlgpParser(dlgp)) {
            parser.parse();
        } catch (ParseException e) {
            return;
        }
        Assert.fail();
    }

}
