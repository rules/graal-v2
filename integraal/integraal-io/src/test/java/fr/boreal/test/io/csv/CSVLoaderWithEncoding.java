package fr.boreal.test.io.csv;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import fr.boreal.io.csv.CSVLoader;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.storage.builder.StorageBuilder;
import fr.boreal.storage.external.rdbms.RDBMSStore;

class CSVLoaderWithEncoding {

	private static final List<Arguments> testData = new ArrayList<>();
	private static final String postgresParams = "jdbc:postgresql://localhost:5432/p2?user=postgres&password=admin";

	@BeforeAll
	static void setupTestData() throws Exception {
		// Paths and expectations could be varied if there were multiple files/expected
		// results
		String rlsFile = "src/test/resources/rlsTestData.rls";
		int expectedAtoms = 10;

		FactBase inMemoryFactBase = StorageBuilder.defaultBuilder().build().get();
		FactBase hsqlDBFactBase = StorageBuilder.defaultBuilder().useHSQLDB("testDB").build().get();
		FactBase postgresFactBase = StorageBuilder.defaultBuilder().usePostgreSQLDB(postgresParams)
				.clearDBOnConstruction().build().get();

		testData.add(Arguments.of(inMemoryFactBase, true, rlsFile, expectedAtoms, 0));
		testData.add(Arguments.of(hsqlDBFactBase, true, rlsFile, expectedAtoms, 7));
		testData.add(Arguments.of(postgresFactBase, true, rlsFile, expectedAtoms, 7));

	}

	static Stream<Arguments> testDataProvider() {
		return testData.stream();
	}

	@DisplayName("Test CSV import with different configurations, encoding options, and expected outcomes")
	@ParameterizedTest(name = "{index}: FactBase={0}, Encode={1}, File={2}, ExpectedAtoms={3}")
	@MethodSource("testDataProvider")
	void csvHandling(FactBase factBase, boolean encode, String rlsFile, int expectedAtoms, int expectedTerms)
			throws Exception {
		try {

			CSVLoader.parseAndLoad(factBase, new File(rlsFile), encode);

			Assertions.assertEquals(expectedAtoms, factBase.size(), "The expected number of atoms does not match");

			long dicoSize = switch (factBase) {
			case RDBMSStore c -> {System.out.println(c.dictionaryList()); yield c.dictionarySize();}
			default -> 0;
			};

			//Assert.assertEquals("The expected number of terms does not match", expectedTerms, dicoSize);
		} catch (Exception ignored) {
		}
	}
}
