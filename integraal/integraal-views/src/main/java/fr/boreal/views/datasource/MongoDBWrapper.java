package fr.boreal.views.datasource;

import java.util.List;

import org.bson.Document;

import com.mongodb.client.MongoCursor;

import fr.boreal.storage.external.evaluator.MongoDBQueryEvaluator;
import fr.boreal.views.specializer.MandatoryParameterStringReplacement;
import fr.boreal.views.transformer.MongoDocumentTransformer;

/**
 * Access to a datasource using MongoDB queries
 * 
 * @author Florent Tornil
 *
 */
public class MongoDBWrapper extends AbstractViewWrapper<String, MongoCursor<Document>> {

	/**
	 * Create a new datasource over a mongodb server
	 * @param url the url of the mongodb server
	 * @param database the mongodb database
	 * @param collection the mongodb collection
	 * @param projectionPaths the paths to project results
	 */
	public MongoDBWrapper(String url, String database, String collection, List<String> projectionPaths) {
		super(new MandatoryParameterStringReplacement(), new MongoDBQueryEvaluator(url, database, collection), new MongoDocumentTransformer(projectionPaths));
	}

}
