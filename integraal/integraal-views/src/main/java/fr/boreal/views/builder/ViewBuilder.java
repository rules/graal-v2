package fr.boreal.views.builder;

import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.FactoryConstants;
import fr.boreal.storage.external.rdbms.driver.HSQLDBDriver;
import fr.boreal.storage.external.rdbms.driver.MySQLDriver;
import fr.boreal.storage.external.rdbms.driver.PostgreSQLDriver;
import fr.boreal.storage.external.rdbms.driver.SQLiteDriver;
import fr.boreal.views.datasource.*;
import fr.inria.integraal.view_parser.parser.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Creates view wrappers from a view definition file
 *
 * @author Florent Tornil
 *
 */
public class ViewBuilder {

	private static final PredicateFactory pf = FactoryConstants.DEFAULT_PREDICATE_FACTORY;

	/**
	 * Creates a collection of view wrappers from the given file
	 *
	 * @param filename a view configuration file
	 * @return the wrappers
	 * @throws ViewBuilderException if an exception occur
	 */
	public static Collection<AbstractViewWrapper<String, ?>> createFactBases(String filename) throws ViewBuilderException {
		Collection<AbstractViewWrapper<String, ?>> views = new ArrayList<>();
		try {
			ViewDocument document = ViewParser.parse(filename);

			for (DatasourceDescription d : document.getDatasources()) {
				Optional<DatasourceType> typeOpt = DatasourceType.getTypeByLabel(d.getType());
				if (typeOpt.isEmpty()) {
					throw new ViewBuilderException("Unknown datasource for type " + d.getType());
				}

				AbstractViewWrapper<String, ?> wrapper = switch (typeOpt.get()) {
					case HSQLDB -> {
						String url = d.getParameters().get("url");
						String database = d.getParameters().get("database");
						yield new SQLWrapper(new HSQLDBDriver(url, database));
					}
					case SQLITE -> {
						String url = d.getParameters().get("url");
						yield new SQLWrapper(new SQLiteDriver(url));
					}
					case MYSQL -> {
						String url = d.getParameters().get("url");
						String database = d.getParameters().get("database");
						String user = d.getParameters().get("user");
						String password = d.getParameters().get("password");
						yield new SQLWrapper(new MySQLDriver(url, database, user, password));
					}
					case POSTGRESQL -> {
						String url = d.getParameters().get("url");
						String database = d.getParameters().get("database");
						String user = d.getParameters().get("user");
						String password = d.getParameters().get("password");
						yield new SQLWrapper(new PostgreSQLDriver(url, database, user, password));
					}
					case SPARQLENDPOINT -> {
						String url = d.getParameters().get("url");
						yield new SPARQLWrapper(url);
					}
					case MONGODB -> {
						// TODO Get parameters
						yield new MongoDBWrapper("lot of params", "", "", List.of(""));
					}
					case WEBAPI -> {
						String user = d.getParameters().get("user");
						String password = d.getParameters().get("password");
						yield new WebAPIWrapper(user, password);
					}
				};

				wrapper.setName(d.getId());
				for (ViewDefinition m : document.getMappingsByDatasource(d)) {
					String query = m.getNativeQuery();
					Optional<String> position = m.getPositioning();

					List<ViewParameterSignature> parameterSignatures = new ArrayList<>();
					for(ViewSignature signature : m.getTemplates()) {
						parameterSignatures.add(new ViewParameterSignature(
								signature.getIfMissing(),
								signature.getMandatory(),
								signature.getSelection()));
					}
					ViewParameters<String> viewParameters = new ViewParameters<>(query, parameterSignatures, position);

					Predicate view = pf.createOrGetPredicate(m.getId(), parameterSignatures.size());
					wrapper.registerView(view, viewParameters);
					views.add(wrapper);
				}
			}
			return views;
		} catch (ViewParseException e) {
			throw new ViewBuilderException("An exception occurred while parsing the mapping file \n" + e);
		} catch (SQLException e) {
			throw new ViewBuilderException("An exception occurred while creating the SQL driver \n" + e);
		} catch (FileNotFoundException e) {
			throw new ViewBuilderException("The mapping file was not found " + filename + "\n" + e);
		} catch (IOException e) {
			throw new ViewBuilderException("An IO exception occurred while accessing the mapping file \n" + e);
		} catch (URISyntaxException e) {
			throw new ViewBuilderException("An URI syntax exception occurred while accessing the mapping file \n" + e);
		}
	}

	/**
	 * Creates a collection of view wrappers from a collection of mapping files
	 *
	 * @param filePaths a list of view definition files
	 * @return the wrappers
	 * @throws ViewBuilderException if an exception occur
	 */
	public static Collection<AbstractViewWrapper<String, ?>> createFactBases(Collection<String> filePaths) throws ViewBuilderException {
		Collection<AbstractViewWrapper<String, ?>> views = new ArrayList<>();
		for (String filename : filePaths) {
			views.addAll(ViewBuilder.createFactBases(filename));
		}
		return views;
	}

	/**
	 * Exception for creating datasources with views
	 *
	 * @author Florent Tornil
	 *
	 */
	public static class ViewBuilderException extends Exception {

		private static final long serialVersionUID = 1L;

		/**
		 * @param message the message of the exception
		 */
		public ViewBuilderException(String message) {
			super(message);
		}
	}

}
