package fr.boreal.views.transformer.missingValue;

import java.util.Optional;

import fr.boreal.model.logicalElements.api.Term;

/**
 * Handling strategy for missing values
 * 
 * @author Florent Tornil
 *
 */
public interface MissingValueHandler {

	/**
	 * @return the term to replace a missing value, empty if no term is replacing the missing value
	 */
	Optional<Term> handle();
	
}
