package fr.boreal.views.specializer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.views.datasource.ViewParameterSignature;
import fr.boreal.views.datasource.ViewParameters;

/**
 * This specializer replaces all occurrences of specifics substrings from the initial String query
 * by the frozen positions of the atom according to the mandatory parameters of the view.
 * <p>
 * All substrings equal to the mandatory value of the position will be replaced by the frozen term at this position.
 * If the term at this position is not frozen an error will happen, as a mandatory parameter was not given a value.
 * 
 */
public class MandatoryParameterStringReplacement implements Specializer<String> {

	@Override
	public ViewParameters<String> specialize(ViewParameters<String> parameters, Atom a, Substitution s) {
		String specializedQuery = parameters.nativeQuery();
		Optional<String> specializedPosition = parameters.position();
		List<ViewParameterSignature> specializedViewElements = new ArrayList<>();
		
		List<ViewParameterSignature> viewElements = parameters.viewElements();
		for(int i = 0; i < a.getPredicate().arity(); ++i) {
			ViewParameterSignature element = viewElements.get(i);
			Optional<String> specializedSelection = element.selection();
			if(element.isMandatory()) {
				if(s.createImageOf(a.getTerm(i)).isFrozen(s)) {
					String pattern = element.mandatoryAs().orElseThrow();
					String value = s.createImageOf(a.getTerm(i)).label();
					
					specializedQuery = specializedQuery.replaceAll(pattern, value);
					if(specializedPosition.isPresent()) {
						specializedPosition = Optional.of(specializedPosition.get().replaceAll(pattern, value));
					}
					if(specializedSelection.isPresent()) {
						specializedSelection = Optional.of(specializedSelection.get().replaceAll(pattern, value));
					}
				} else {
					throw new IllegalArgumentException("The view " + a.getPredicate() + " was called without a mandatory value for the parameter "
							+ element.mandatoryAs().orElseThrow());
				}
			}
			specializedViewElements.add(new ViewParameterSignature(
					element.missingValueHandling(),
					element.mandatoryAs(),
					specializedSelection));
		}
		return new ViewParameters<>(specializedQuery, specializedViewElements, specializedPosition);
	}

}
