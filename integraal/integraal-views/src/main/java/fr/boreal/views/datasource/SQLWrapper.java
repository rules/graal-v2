package fr.boreal.views.datasource;

import java.sql.SQLException;
import java.util.List;

import fr.boreal.storage.external.evaluator.SQLQueryEvaluator;
import fr.boreal.storage.external.rdbms.driver.RDBMSDriver;
import fr.boreal.views.specializer.MandatoryParameterStringReplacement;
import fr.boreal.views.transformer.SQLTuplesTransformer;

/**
 * Access to a datasource using web api queries
 * 
 * @author Florent Tornil
 *
 */
public class SQLWrapper extends AbstractViewWrapper<String, List<Object[]>> {

	/**
	 * Create a new datasource on a RDBMS
	 * @param driver the SQL driver
	 * @throws SQLException if an error occur
	 */
	public SQLWrapper(RDBMSDriver driver) throws SQLException {
		super(new MandatoryParameterStringReplacement(), new SQLQueryEvaluator(driver), new SQLTuplesTransformer());
	}

}
