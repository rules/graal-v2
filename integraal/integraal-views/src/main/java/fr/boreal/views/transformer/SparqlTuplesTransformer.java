package fr.boreal.views.transformer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQueryResult;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.views.datasource.ViewParameterSignature;

/**
 * Transform Sparql tuples (rdf4j {@link TupleQueryResult}) to Atoms
 * A tuple (t0, t1, ..., tn) applied to the atom of predicate p generates a new atom of arity n+1
 * p(f(t0), f(t1), ..., f(tn))
 * where f transform a {@link Value} into the correct type of {@link Term}
 */
public class SparqlTuplesTransformer extends AbstractTransformer<TupleQueryResult> {

	private final TermFactory tf;

	private List<String> ordered_binding_names;

	private static final String XSD = "http://www.w3.org/2001/XMLSchema#";
	private static final String XSD_INTEGER = XSD + "integer";

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new transformer using default factories
	 */
	public SparqlTuplesTransformer() {
		this(SameObjectTermFactory.instance());
	}

	/**
	 * Creates a new transformer using user-given factories
	 * @param tf the term factory
	 */
	public SparqlTuplesTransformer(TermFactory tf) {
		this.tf = tf;
	}

	/////////////////////////////////////////////////
	// Public method
	/////////////////////////////////////////////////

	@Override
	public Iterator<Atom> transform(TupleQueryResult nativeResults, List<ViewParameterSignature> signatures, Atom a, Substitution s) {
		Collection<Atom> atoms = new ArrayList<>();
		this.ordered_binding_names = nativeResults.getBindingNames();
		for(BindingSet nativeResult : nativeResults) {
			Optional<Atom> opt_viewAtom = this.transformAtom(nativeResult, signatures, a, s);
            opt_viewAtom.ifPresent(atoms::add);
		}
		return atoms.iterator();
	}

	@Override
	public boolean isMissingValue(Object o) {
        return !(o instanceof Value);
	}

	@Override
	// We get the correct term type according to the type of the value.
	// If the value is a java null (safety check, probably never used) we create a fresh existential
	// Blank nodes are represented as existential
	// IRIs are represented as constants
	// RDF-literals are converted to the correct literal type
	// TODO: For now we only handle String and Integer values
	// TODO: Other values will be handled as String
	// We can also compute a first filter here according to constants of the atom
	public Optional<Term> convertType(Object o, ViewParameterSignature signature, Atom a, Substitution s) {
		if(o instanceof Value rdf4j_value) {
			if(rdf4j_value.isBNode()) {
				return Optional.of(this.tf.createOrGetVariable(rdf4j_value.stringValue()));
			} else if(rdf4j_value.isIRI()) {
				return Optional.of(this.tf.createOrGetConstant(rdf4j_value.stringValue()));
			} else if(rdf4j_value.isLiteral()) {
				Literal rdf4j_literal = (Literal) rdf4j_value;
				String rdf4j_datatype = rdf4j_literal.getDatatype().stringValue();
				if(rdf4j_datatype.equals(XSD_INTEGER)) {
					return Optional.of(this.tf.createOrGetLiteral(rdf4j_literal.intValue()));
				} else {
					return Optional.of(this.tf.createOrGetLiteral(rdf4j_literal.stringValue()));
				}
			} else {
				return Optional.empty();
			}
		} else {
			return Optional.empty();
		}
	}

	@Override
	public Object getObjectAtIndex(Object nativeResult, int nativeResultIndex, ViewParameterSignature signature) {
		return ((BindingSet)nativeResult).getBinding(this.ordered_binding_names.get(nativeResultIndex)).getValue();
	}
}
