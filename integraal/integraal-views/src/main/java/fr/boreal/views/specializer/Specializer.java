package fr.boreal.views.specializer;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.views.datasource.ViewParameters;

/**
 * Specialize the native query in order to take into account the given parameters.
 *
 * @param <NativeQueryType> the type of the query to specialize
 */
public interface Specializer<NativeQueryType> {

	/**
	 * There is no guaranty that the query will be specialized to take into account all the parameters.
	 * This is specific to the implementation and the possibilities given by the native language.
	 * @param parameters of the view to take into account
	 * @param a atom that may contain constants or equal variables
	 * @param s substitution that specializes the given atom
	 * @return the specialized view parameters
	 */
	ViewParameters<NativeQueryType> specialize(ViewParameters<NativeQueryType> parameters, Atom a, Substitution s);
}
