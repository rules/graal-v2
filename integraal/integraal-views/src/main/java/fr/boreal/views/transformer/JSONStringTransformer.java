package fr.boreal.views.transformer;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.views.datasource.ViewParameterSignature;
import fr.boreal.views.datasource.ViewParameters;

import java.util.*;

/**
 * Transform a String representing a JSON object into atoms.
 * <p>
 * The first step is a positioning query on the JSON object
 * The second step is to retrieve an ordered list of elements, which will represent our terms using selection queries.
 */
public class JSONStringTransformer extends AbstractTransformer<String> {

	private final TermFactory tf;

	/**
	 * Creates a new transformer using default factories
	 */
	public JSONStringTransformer() {
		this(SameObjectTermFactory.instance());
	}

	/**
	 * Creates a new transformer using user-given factories
	 * @param tf the term factory
	 */
	public JSONStringTransformer(TermFactory tf) {
		this.tf = tf;
	}

	/////////////////////////////////////////////////
	// Public method
	/////////////////////////////////////////////////

	@Override
	public Iterator<Atom> transform(String nativeResults, ViewParameters<?> parameters, Atom a, Substitution s) {

		Collection<Atom> atoms = new ArrayList<>();
		Object document = Configuration.defaultConfiguration().jsonProvider().parse(nativeResults);
		List<Object> jsonEntries = JsonPath.read(document, parameters.position().orElseThrow());
		List<ViewParameterSignature> signatures = parameters.viewElements();
		for(Object jsonEntry : jsonEntries) {
			Optional<Atom> opt_viewAtom = this.transformAtom(jsonEntry, signatures, a, s);
			opt_viewAtom.ifPresent(atoms::add);
		}
		return atoms.iterator();
	}

	@Override
	public Iterator<Atom> transform(String nativeResults, List<ViewParameterSignature> viewElements, Atom a,
									Substitution s) {
		throw new UnsupportedOperationException("[JSON Transformer] This transform method should not be called,"
				+ " please use the one with the whole parameters object");
	}

	@Override
	public boolean isMissingValue(Object o) {
		return o == null;
	}

	@Override
	public Optional<Term> convertType(Object o, ViewParameterSignature signature, Atom a, Substitution s) {
		return Optional.of(this.tf.createOrGetLiteral(o));
	}

	@Override
	public Object getObjectAtIndex(Object nativeResult, int nativeResultIndex, ViewParameterSignature signature) {
		return JsonPath.read(nativeResult, signature.selection().orElseThrow());
	}
}
