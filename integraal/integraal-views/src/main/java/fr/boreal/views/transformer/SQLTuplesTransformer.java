package fr.boreal.views.transformer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.views.datasource.ViewParameterSignature;

/**
 * Transform SQL tuples (java.net List&lt;Object[]&lt;) to Atoms
 * A tuple (t0, t1, ..., tn) applied to the atom of predicate p generates a new atom of arity n+1
 * p(f(t0), f(t1), ..., f(tn))
 * where f transform an Object into the correct type of {@link Term}
 */
public class SQLTuplesTransformer extends AbstractTransformer<List<Object[]>> {

	private final TermFactory tf;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new transformer using default factories
	 */
	public SQLTuplesTransformer() {
		this(SameObjectTermFactory.instance());
	}

	/**
	 * Creates a new transformer using user-given factories
	 * @param tf the term factory
	 */
	public SQLTuplesTransformer(TermFactory tf) {
		this.tf = tf;
	}

	/////////////////////////////////////////////////
	// Public method
	/////////////////////////////////////////////////

	@Override
	public Iterator<Atom> transform(List<Object[]> nativeResults, List<ViewParameterSignature> signatures, Atom a, Substitution s) {
		Collection<Atom> atoms = new ArrayList<>();
		for(Object[] nativeResult : nativeResults) {
			Optional<Atom> opt_viewAtom = this.transformAtom(nativeResult, signatures, a, s);
            opt_viewAtom.ifPresent(atoms::add);
		}
		return atoms.iterator();
	}

	@Override
	public boolean isMissingValue(Object o) {
		return o == null;
	}

	@Override
	public Optional<Term> convertType(Object o, ViewParameterSignature signature, Atom a, Substitution s) {
		return Optional.of(this.tf.createOrGetLiteral(o));
	}

	@Override
	public Object getObjectAtIndex(Object nativeResult, int nativeResultIndex, ViewParameterSignature signature) {
		return ((Object[])nativeResult)[nativeResultIndex];
	}
}
