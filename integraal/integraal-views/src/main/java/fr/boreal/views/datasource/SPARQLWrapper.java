package fr.boreal.views.datasource;

import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;

import fr.boreal.storage.external.evaluator.SparqlQueryEvaluator;
import fr.boreal.views.specializer.MandatoryParameterStringReplacement;
import fr.boreal.views.transformer.SparqlTuplesTransformer;

/**
 * Access to a datasource using SPARQL queries
 * 
 * @author Florent Tornil
 *
 */
public class SPARQLWrapper extends AbstractViewWrapper<String, TupleQueryResult> {

	/**
	 * Create a new datasource on the endpoint
	 * @param endpointUrl the url of the SPARQL endpoint
	 */
	public SPARQLWrapper(String endpointUrl) {		
		super(new MandatoryParameterStringReplacement(), new SparqlQueryEvaluator(new SPARQLRepository(endpointUrl)), new SparqlTuplesTransformer());
	}

}
