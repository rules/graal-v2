/**
 * Module for view elements of InteGraal
 *
 * @author Florent Tornil
 *
 */
module fr.boreal.views {

	requires transitive fr.boreal.model;
	requires fr.inria.integraal.view_parser.parser;
	requires fr.lirmm.boreal.util;
	
	requires fr.boreal.storage;
	
	requires com.google.common;
	requires json.path;
	requires org.mongodb.driver.sync.client;
	requires rdf4j.query;
	requires rdf4j.repository.sparql;
	requires rdf4j.model.api;

    exports fr.boreal.views;
	exports fr.boreal.views.builder;
	exports fr.boreal.views.datasource;
}