package fr.lirmm.boreal.util.validator.query;


import fr.boreal.model.query.api.Query;
import fr.boreal.model.query.impl.UnionFOQuery;
import fr.lirmm.boreal.util.validator.Validator;


/**
 * @author Michel Leclère
 * Checks that the query is a UCQ (i.e. a (possibly empty) set of CQ FOQueries)
 */
public class UCQValidator implements Validator<Query>{

	@Override
	public boolean check(Query element) {
		if(element instanceof UnionFOQuery uq) { 
			Validator<Query> cq_checker = new CQValidator();
			return cq_checker.check(uq.getQueries());
		}
		else return false;
	}

}
