/*
 * Copyright (C) Inria Sophia Antipolis - Méditerranée / LIRMM
 * (Université de Montpellier & CNRS) (2014 - 2017)
 *
 * Contributors :
 *
 * Clément SIPIETER <clement.sipieter@inria.fr>
 * Mélanie KONIG
 * Swan ROCHER
 * Jean-François BAGET
 * Michel LECLERE
 * Marie-Laure MUGNIER <mugnier@lirmm.fr>
 *
 *
 * This file is part of Graal <https://graphik-team.github.io/graal/>.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lirmm.boreal.util;

import java.util.HashMap;
import java.util.Map;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;

/**
 * Computes the signature (or type) of an atom. More precisely, for each
 * position i of atom p(x1,..,xn), it is determined whether <br/>
 * (1) x_i is a constant or a variable and <br/>
 * (2) there are multiple occurrences of the same variable.
 */
public class AtomType {

	/**
	 * Mask for a Variable
	 */
	public static final int VARIABLE = -1;

	/**
	 * Mask for a constant
	 */
	public static final int CONSTANT_OR_FROZEN_VAR_OR_GROUND_FUNCTIONAL_TERM = -2;

	public static final int FUNCTIONAL_TERM_WITH_VARS = -3;

	private boolean theAtomContainsAConstant;
	private boolean theAtomContainsAFunctionalTermWithVariables;
	private boolean multipleOccurrencesOfTheSameVariable; // actually never set to false

	private final int[] type; // to store the type of each term of the atom

    /**
	 * Creates the mask of the atom
	 * @param atom atom to mask
	 */
	public AtomType(Atom atom) {
		this(atom, new SubstitutionImpl());
	}

	/**
	 * Creates the mask of the atom
	 * @param atom atom to mask
	 * @param s substitution
	 */
	public AtomType(Atom atom, Substitution s) {
		theAtomContainsAConstant = false;
		theAtomContainsAFunctionalTermWithVariables=false;
        int size = atom.getPredicate().arity();

		type = new int[size];

		// we also account for multiple occurrences
		Map<Term, Integer> firstPositionMap = new HashMap<>();

		for (int i = 0; i < atom.getPredicate().arity(); i++) {
			Term t = atom.getTerm(i);
			if(t.isFunctionalTerm()&&!t.isFrozen(s)){
				type[i] = FUNCTIONAL_TERM_WITH_VARS;
				theAtomContainsAFunctionalTermWithVariables = true;
				multipleOccurrencesOfTheSameVariable = true; //TODO check if we can remove this
			}
			if (t.isFrozen(s)) {
				// the term is a constant
				type[i] = CONSTANT_OR_FROZEN_VAR_OR_GROUND_FUNCTIONAL_TERM;
				theAtomContainsAConstant = true;
				multipleOccurrencesOfTheSameVariable = true; //TODO check if we can remove this, why should we set info for variables?
			} else {
				// the term is a variable
				Integer firstPos = firstPositionMap.get(t);
				if (firstPos == null) {
					// first occurrence of the variable
					firstPositionMap.put(t, i);
					firstPos = VARIABLE;
				} else {
					// two occurrences of the same variables
					multipleOccurrencesOfTheSameVariable = true;
				}
				type[i] = firstPos;
			}
		}
	}

	/**
	 * @param index position in the atom
	 * @return the type of the term at the given position
	 */
	public int getType(int index) {
		return this.type[index];
	}

	/**
	 * @return true iff there is a constant in the atom
	 */
	public boolean isThereConstant() {
		return this.theAtomContainsAConstant;
	}

	/**
	 * @return true iff there is a functional term with variables in the atom
	 */
	public boolean isThereFunctionalTermWithVars() {
		return this.theAtomContainsAFunctionalTermWithVariables;
	}

	/**
	 * @return true iff there is a constraint in the atom
	 */
	public boolean isThereConstraint() {
		return this.multipleOccurrencesOfTheSameVariable;
	}

}
