package fr.lirmm.boreal.util.stream.filter;

import fr.boreal.model.logicalElements.api.Substitution;

/**
 * @author Florent Tornil
 *
 * Tests if the values of the substitution are constants
 */
public class ConstantFilter implements Filter<Substitution> {

	/**
	 * Default constructor
	 */
	public ConstantFilter() { }

	@Override
	public boolean filter(Substitution s) {
		return s.keys().stream().allMatch(t -> s.createImageOf(t).isFrozen(null));
	}

}
