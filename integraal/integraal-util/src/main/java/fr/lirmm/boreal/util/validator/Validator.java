package fr.lirmm.boreal.util.validator;

import java.util.Collection;

/**
 * @author Florent Tornil
 * <p>
 * Validate a specific condition over the given elements
 * @param <T> the type of the elements to be checked
 */
public interface Validator<T> {

	/**
	 * @param elements elements to check
	 * @return true iff all the elements respect a implementation defined property
	 */
	default boolean check(Collection<? extends T> elements) {
		return elements.stream().allMatch(this::check);
	}
	
	/**
	 * @param element element to check
	 * @return true iff the element respect a implementation defined property
	 */
	boolean check(T element);
	
}
