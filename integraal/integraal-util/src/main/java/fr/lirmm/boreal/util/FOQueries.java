package fr.lirmm.boreal.util;

import fr.boreal.model.formula.FOFormulas;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.partition.TermPartition;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;

import java.util.Set;

/**
 * Utility class for First Order Queries
 */
public class FOQueries {

	/**
	 * constants or literals affected to answer variables by the given substitution are put in the equalities
	 * variable renaming is directly applied
	 * @param query query to create the image from
	 * @param s substitution to apply
	 * @return a new FOFormula corresponding to the given FOFormula in which the
	 *         given substitution have been applied
	 */
	public static <Formula extends FOFormula> FOQuery<Formula> createImageWith(FOQuery<Formula> query, Substitution s) {

		// Update variable equalities to take into account the substitution
		TermPartition allEqualities = new TermPartition(query.getVariableEqualities());
		for(Variable v : s.keys()) {
			if(v != s.createImageOf(v)) {
				allEqualities.addClass(Set.of(v, s.createImageOf(v)));
			}
		}

		// Aggregate the substitution with the equalities, preferring the domain of the substitution
		s = s.aggregated(allEqualities.getAssociatedSubstitution(query).orElseThrow()).orElseThrow();

		// rename according to the equalities
		Formula formula = FOFormulas.createImageWith(query.getFormula(), s);

		// Keep only the equalities concerning answer variables
		// and only with constants or variables that appear in the query's renamed formula
		TermPartition newEqualities = new TermPartition();
		Set<Variable> formulaVariables = formula.getVariables();
		for(Variable v : query.getAnswerVariables()) {
			Term rep = s.createImageOf(v);
			if(rep != v) {
				if(rep.isFrozen(null) || (rep.isVariable() && formulaVariables.contains(rep))) {
					newEqualities.addClass(Set.of(v, rep));
				}
			}
		}

		return FOQueryFactory.instance().createOrGetQuery(formula, query.getAnswerVariables(), newEqualities);
	}

	/**
	 * A query is consistent with a given substitution iff the query equalities and the substitution are compatible.
	 * <br>
	 * They are said compatible if one cannot infer equality between two constants.
	 *
	 * <p>
	 * This means that :
	 * <ul>
	 *     <li>the equalities are valid (no 2 constant on the same equality-class)</li>
	 *     <li>a variable equal to a constant by the equalities cannot be affected to another term by the substitution</li>
	 *     <li>two variables equal by the equalities cannot be affected to two different terms by the substitution</li>
	 * </ul>
	 *
	 * <p>
	 * The variables in the equalities and domain of the substitution belong to the query.
	 * <br>
	 * The variables in the range of the substitution are different from the variables of the query (even if they have the same String representation)
	 *
	 * @param query the query
	 * @param s the substitution
	 * @return true iff the query is consistent with the substitution
	 */
	public static boolean isConsistent(FOQuery<?> query, Substitution s) {
		TermPartition equalities = query.getVariableEqualities();

		for(Set<Term> classs : equalities.getClasses()) {
			Term constraint = null;
			for(Term t : classs) {
				if(t.isFrozen(s)) {
					Term v = s.createImageOf(t);
					if(constraint != null && !v.equals(constraint)) {
						return false;
					} else {
						constraint = v;
					}
				}
			}
		}
		return true;
	}

}
