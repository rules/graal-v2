package fr.lirmm.boreal.util.stream;
import java.util.function.Function;

/**
 * This functional interface allows to not catch an Exception in a
 * lambda expression : use it at your own risk, the exception
 * becomes unchecked.
 * It is recommended to use it only in a method that can throw the 
 * Exception type that is unchecked. So the exception can be
 * checked when using the method.
 * 
 * Source : http://4comprehension.com/sneakily-throwing-exceptions-in-lambda-expressions-in-java/
 * 
 * @author Guillaume Pérution-Kihli
 * @param <T> type of the input element
 * @param <R> type of the result element
 */

@FunctionalInterface
public interface ThrowingFunction<T, R> {

    /**
     * Redefine the apply method to be able to declare that an exception is thrown
     * @param t the element
     * @return the result of the function
     * @throws Exception if an exception is thrown by the inner method
     */
    R apply(T t) throws Exception;

    /**
     * @param <T> type of the exception to throw
     * @param <R> type of the result element
     * @param t the exception
     * @return nothing
     * @throws T the initial exception
     */
    @SuppressWarnings("unchecked")
    static <T extends Exception, R> R sneakyThrow(Exception t) throws T {
        throw (T) t;
    }

    /**
     * @param <T> type of the input element
     * @param <R> type of the result element
     * @param f the function to call
     * @return the result of the function
     */
    static <T, R> Function<T, R> unchecked(ThrowingFunction<T, R> f) {
        return t -> {
            try {
                return f.apply(t);
            } catch (Exception ex) {
                return ThrowingFunction.sneakyThrow(ex);
            }
        };
    }
}
