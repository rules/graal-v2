package fr.lirmm.boreal.util.validator.query;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.query.api.FOQuery;
import fr.lirmm.boreal.util.validator.Validator;
import fr.lirmm.boreal.util.validator.rule.ConjunctionFormulaValidator;

/**
 * @author Michel Leclère
 * Checks that the query is a CQ (i.e. a FOQuery whose FOFormula only contains conjunctions and atoms)
 */
public class CQValidator implements Validator<Query> {

	@Override
	public boolean check(Query element) {
		if(element instanceof FOQuery<?> q) {
			return ConjunctionFormulaValidator.instance().check(q.getFormula());
		}
		else return false;
	}

}



