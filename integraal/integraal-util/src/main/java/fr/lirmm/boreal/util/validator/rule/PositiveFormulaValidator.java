package fr.lirmm.boreal.util.validator.rule;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.api.FOFormulaDisjunction;
import fr.lirmm.boreal.util.validator.Validator;

/**
 * @author Florent Tornil
 * Checks that the formula is positive (i.e.: contains no negation)
 */
public class PositiveFormulaValidator implements Validator<FOFormula> {
	private static final PositiveFormulaValidator INSTANCE = new PositiveFormulaValidator();

	/**
	 * Returns the singleton instance of PositiveFormulaValidator.
	 * @return the instance of PositiveFormulaValidator
	 */
	public static PositiveFormulaValidator instance() {
		return INSTANCE;
	}

	@Override
	public boolean check(FOFormula element) {
		if(element.isConjunction()) {
			return this.check(((FOFormulaConjunction) element).getSubElements());
		} else if(element.isDisjunction()) {
			return this.check(((FOFormulaDisjunction) element).getSubElements());
		} else if(element.isNegation()) {
			return false;
		} else return element.isAtomic();

    }

}
