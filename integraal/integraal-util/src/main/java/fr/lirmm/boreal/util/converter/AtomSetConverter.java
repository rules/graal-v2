package fr.lirmm.boreal.util.converter;

import java.util.HashSet;
import java.util.Set;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.logicalElements.api.Atom;

/**
 * Converts FOFormula to and from AtomSet
 * <br/>
 * This is used for compatibility with Graal 1.3 version.
 * 
 * @author Florent Tornil
 *
 */
public class AtomSetConverter {

	/**
	 * Converts the given FOFormula into a Graal AtomSet
	 * @param f the formula to convert
	 * @return the converted atomset
	 */
	public static fr.lirmm.graphik.integraal.api.core.InMemoryAtomSet convert(FOFormula f) {
		fr.lirmm.graphik.integraal.api.core.InMemoryAtomSet as = new fr.lirmm.graphik.integraal.core.atomset.DefaultInMemoryAtomSet();
		f.asAtomSet().forEach(a -> as.add(AtomConverter.convert(a)));
		return as;
	}

	/**
	 * Converts the given FOFormula into a Graal AtomSet
	 * @param f the formula to convert
	 * @return the converted atomset
	 */
	public static fr.lirmm.graphik.graal.api.core.InMemoryAtomSet convert2(FOFormula f) {
		fr.lirmm.graphik.graal.api.core.InMemoryAtomSet as = new fr.lirmm.graphik.graal.core.atomset.graph.DefaultInMemoryGraphStore();
		f.asAtomSet().forEach(a -> as.add(AtomConverter.convert2(a)));
		return as;
	}

	/**
	 * Converts the given atomset into a FOFormula
	 * @param as the atomset to convert
	 * @return the converted formula
	 */
	public static FOFormula reverse(fr.lirmm.graphik.integraal.api.core.InMemoryAtomSet as) {
		Set<Atom> atoms = new HashSet<>();
        for (fr.lirmm.graphik.integraal.api.core.Atom a : as) {
            atoms.add(AtomConverter.reverse(a));
        }
		return FOFormulaFactory.instance().createOrGetConjunction(atoms.toArray(new Atom[0]));
	}

}
