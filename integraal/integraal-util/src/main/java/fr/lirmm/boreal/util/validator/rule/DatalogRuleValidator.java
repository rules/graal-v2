package fr.lirmm.boreal.util.validator.rule;

import fr.boreal.model.rule.api.FORule;
import fr.lirmm.boreal.util.validator.Validator;

/**
 * @author Florent Tornil
 * Checks that the rule is datalog
 */
public class DatalogRuleValidator implements Validator<FORule> {

	@Override
	public boolean check(FORule element) {
		return element.getExistentials().isEmpty();
	}

}
