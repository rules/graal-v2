package fr.lirmm.boreal.util.validator.rule;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.rule.api.FORule;
import fr.lirmm.boreal.util.validator.Validator;

/**
 * @author Florent Tornil
 * Checks that the head of the rule is only a (possibly multi-level) conjunction
 */
public class ConjunctiveHeadRuleValidator implements Validator<FORule> {

	@Override
	public boolean check(FORule rule) {
		return ConjunctionFormulaValidator.instance().check(rule.getHead());
	}

}
