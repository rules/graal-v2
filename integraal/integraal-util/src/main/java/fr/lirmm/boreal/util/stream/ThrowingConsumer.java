package fr.lirmm.boreal.util.stream;

import java.util.function.Consumer;

/**
 * This functional interface allows to not catch an Exception in a
 * lambda expression : use it at your own risk, the exception
 * becomes unchecked.
 * It is recommended to use it only in a method that can throw the
 * Exception type that is unchecked. So the exception can be
 * checked when using the method.
 *
 * Source : http://4comprehension.com/sneakily-throwing-exceptions-in-lambda-expressions-in-java/
 *
 * @param <T> type of the input element
 */

@FunctionalInterface
public interface ThrowingConsumer<T> {

    /**
     * Redefine the accept method to be able to declare that an exception is thrown
     * @param t the element
     * @throws Exception if an exception is thrown by the inner method
     */
    void accept(T t) throws Exception;

    /**
     * @param <T> type of the input element
     * @param c the consumer to call
     * @return the consumer result
     */
    static <T> Consumer<T> unchecked(ThrowingConsumer<T> c) {
        return t -> {
            try {
                c.accept(t);
            } catch (Exception ex) {
                ThrowingFunction.sneakyThrow(ex);
            }
        };
    }
}