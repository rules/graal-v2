package fr.lirmm.boreal.util.converter;

import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.lirmm.graphik.integraal.core.factory.DefaultPredicateFactory;

/**
 * Converts Predicate to and from Graal's Predicate
 * <br/>
 * This is used for compatibility with Graal 1.3 version.
 * 
 * @author Florent Tornil
 *
 */
public class PredicateConverter {
	
	/**
	 * Converts the given Predicate into a Graal Predicate
	 * @param p the predicate to convert
	 * @return the converted predicate
	 */
	public static fr.lirmm.graphik.integraal.api.core.Predicate convert(Predicate p) {
		return DefaultPredicateFactory.instance().create(p.label(), p.arity());
	}
	
	/**
	 * Converts the given Predicate into a Graal Predicate
	 * @param p the predicate to convert
	 * @return the converted predicate
	 */
	public static fr.lirmm.graphik.graal.api.core.Predicate convert2(Predicate p) {
		return fr.lirmm.graphik.integraal.core.factory.DefaultPredicateFactory.instance().create2(p.label(), p.arity());
	}
	
	/**
	 * Converts the given Graal Predicate into a Predicate
	 * @param p the predicate to convert
	 * @return the converted predicate
	 */
	public static Predicate reverse(fr.lirmm.graphik.integraal.api.core.Predicate p) {
		return SameObjectPredicateFactory.instance().createOrGetPredicate(p.getIdentifier().toString(), p.getArity());
	}

}
