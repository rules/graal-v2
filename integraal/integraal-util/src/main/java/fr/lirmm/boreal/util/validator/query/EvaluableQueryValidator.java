package fr.lirmm.boreal.util.validator.query;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.query.impl.UnionFOQuery;
import fr.lirmm.boreal.util.validator.Validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public interface EvaluableQueryValidator  {

    /**
     * @param q query
     * @return true iff the query is evaluable
     */
    static boolean check(Query q) {
        return switch (q) {
            case FOQuery<?> cq when (cq.getFormula() instanceof Atom a) -> checkAtomicQuery((FOQuery<Atom>) cq);
            case FOQuery<?> cq -> checkAtomSet(cq.getFormula().asAtomSet());
            case UnionFOQuery ucq -> ucq.getQueries().stream().allMatch(cq->check(cq));
            default -> throw new IllegalStateException("Unsupported query type " + q);
        };
    }


    /**
     * @param atomicQuery atomic query
     * @return true iff the atomic query is supported
     */
    static boolean checkAtomicQuery(FOQuery<Atom> atomicQuery) {
        if (Arrays.stream(atomicQuery.getFormula().getTerms()).noneMatch(t -> t.isFunctionalTerm() || t.isEvaluableFunction())) {
            return true;
        }
        // if there are functional terms of evaluable functions then the query must be boolean because we cannot reconstruct the answer set (iterator of substitutions) from an iterator of atoms
        return atomicQuery.getAnswerVariables().isEmpty();
    }

    /**
     * @param atomset atomset to be checked as a whole ; it must not contain a functional term in more than one atom because the backtrack cannot support that
     * @return true iff the atomet is evaluable
     */
    static boolean checkAtomSet(Collection<Atom> atomset) {
        List<Atom> atomsWithNonEvaluableFunctionalTerms = new ArrayList<>();
        for (var a : atomset) {
            for (Term t : a.getTerms()) {
                if (t.isFunctionalTerm()) {
                    atomsWithNonEvaluableFunctionalTerms.add(a);
                }
            }
        }
        return atomsWithNonEvaluableFunctionalTerms.size() <= 1;
    }

}
