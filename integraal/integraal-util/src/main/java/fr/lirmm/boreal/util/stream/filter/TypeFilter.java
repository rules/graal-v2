/*
 * Copyright (C) Inria Sophia Antipolis - Méditerranée / LIRMM
 * (Université de Montpellier & CNRS) (2014 - 2017)
 *
 * Contributors :
 *
 * Clément SIPIETER <clement.sipieter@inria.fr>
 * Mélanie KONIG
 * Swan ROCHER
 * Jean-François BAGET
 * Michel LECLERE
 * Marie-Laure MUGNIER <mugnier@lirmm.fr>
 *
 *
 * This file is part of Graal <https://graphik-team.github.io/graal/>.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lirmm.boreal.util.stream.filter;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.LogicalFunctionalTerm;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.logicalElements.impl.functionalTerms.FunctionalTermHomomorphism;
import fr.lirmm.boreal.util.AtomType;

import java.util.Optional;

/**
 * @author Florent Tornil
 * Tests if atom e is isomorphic (ie, a simple variable renaming) to this.atom
 */
public class TypeFilter implements Filter<Atom> {

    private final AtomType type;
    private final Atom atom;

    /**
     * @param type type filter
     * @param atom atom filter
     */
    public TypeFilter(AtomType type, Atom atom) {
        this.type = type;
        this.atom = atom;
    }

    @Override
    public boolean filter(Atom e) {
        Substitution currentHom = new SubstitutionImpl();

        for (int i = 0; i < e.getPredicate().arity(); ++i) {
            if (atom.getTerm(i).isFunctionalTerm()) {
                if (e.getTerm(i).isFunctionalTerm()) {
                    Optional<Substitution> hom = FunctionalTermHomomorphism.homomorphism((LogicalFunctionalTerm) atom.getTerm(i), (LogicalFunctionalTerm) e.getTerm(i), new SubstitutionImpl());
                    // at this stage, we must verify that the homomorphism is compatible throughout all terms
                    if (hom.isEmpty()) {
                        return false;
                    } else {
                        Optional<Substitution> mergedHom = currentHom.merged(hom.get());
                        if (mergedHom.isEmpty()) {
                            return false;
                        }
                        else{
                            currentHom=mergedHom.get();
                        }
                    }
                } else {
                    // the second term is not a functional term
                    return false;
                }
            } else if (type.getType(i) >= 0) {
                // variable
                if (!e.getTerm(i).equals(e.getTerm(type.getType(i)))) {
                    return false;
                }
            } else if (type.getType(i) == AtomType.CONSTANT_OR_FROZEN_VAR_OR_GROUND_FUNCTIONAL_TERM) {
                // constant
                if (!e.getTerm(i).equals(atom.getTerm(i))) {
                    return false;
                }
            }
        }
        return true;
    }

}
