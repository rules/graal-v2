package fr.lirmm.boreal.util.time;

/**
 * Manipulates objects representing time.
 */
public class TimeUtils {

	/**
	 * 
	 * @param time_ms interval duration in milliseconds
	 * @return a human understandable representation 
	 */
	public static String displayTimeInComprehensibleFormat(Long time_ms) {

		// note abuse of switch notation
		return switch (time_ms) {
		case Long o when o < 1000 -> time_ms + " ms";
		case Long o when o < 60 * 1000 -> time_ms / 1000 + " seconds";
		case Long o when o < 60 * 60 * 1000 -> time_ms / (60 * 1000) + " minutes";
		default -> time_ms / (60 * 60 * 1000) + " hours";

		};
	}

}
