package timeout;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Duration;
import java.util.concurrent.Callable;

import fr.lirmm.boreal.util.timeout.CallableWithTimeout;
import org.junit.Test;


public class CallableWithTimeoutTest {

    @Test
    public void testCallableCompletesBeforeTimeout() {
        // Define a task that completes within the timeout
        Callable<Boolean> task = () -> {
            Thread.sleep(500); // Simulates a short delay
            return true; // Returns true when completed
        };

        // Execute the task with a timeout of 1 second
        Boolean result = CallableWithTimeout.execute(task, Duration.ofSeconds(1));

        // Assert that the result is true (task completed successfully)
        assertNotNull(result);
        assertTrue(result);
    }

    @Test
    public void testCallableTimesOut() {
        // Define a task that exceeds the timeout
        Callable<Boolean> task = () -> {
            Thread.sleep(2000); // Simulates a long delay
            return true; // This will not be reached within the timeout
        };

        // Execute the task with a timeout of 1 second
        Boolean result = CallableWithTimeout.execute(task, Duration.ofSeconds(1));

        // Assert that the result is null (task timed out)
        assertNull(result);
    }

    @Test
    public void testCallableThrowsException() {
        // Define a task that throws an exception
        Callable<Boolean> task = () -> {
            throw new IllegalStateException("Simulated exception");
        };

        // Expect a RuntimeException to be thrown when executing the task
        assertThrows(RuntimeException.class, () -> {
            CallableWithTimeout.execute(task, Duration.ofSeconds(1));
        });
    }
}
