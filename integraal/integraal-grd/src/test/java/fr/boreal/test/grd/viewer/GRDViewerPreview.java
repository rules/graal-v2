package fr.boreal.test.grd.viewer;
import java.util.Set;

import fr.boreal.grd.impl.GRDImpl;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.impl.FORuleImpl;

/**
 * @author Florent Tornil
 *
 * Preview on how to use the GRDViewer class to display a GRD
 */
public class GRDViewerPreview {

	private static final TermFactory tf = SameObjectTermFactory.instance();
	private static final PredicateFactory pf = SameObjectPredicateFactory.instance();

	private static final Predicate p = pf.createOrGetPredicate("p", 1);
	private static final Predicate q = pf.createOrGetPredicate("q", 1);
	private static final Predicate r = pf.createOrGetPredicate("r", 2);

	private static final Variable x = tf.createOrGetVariable("x");
	private static final Variable y = tf.createOrGetVariable("y");

	private static final Atom px = new AtomImpl(p, x);
	private static final Atom py = new AtomImpl(p, y);
	private static final Atom qx = new AtomImpl(q, x);
	private static final Atom rxy = new AtomImpl(r, x, y);

	private static final FORule Rpx_qx = new FORuleImpl(px, qx);
	private static final FORule Rpx_py = new FORuleImpl(px, py);
	private static final FORule Rqx_rxy = new FORuleImpl(qx, rxy);
	
	/**
	 * Displays a predefined GRD
	 * @param args unused parameter
	 */
	public static void main(String[] args) {
		GRDImpl grd = new GRDImpl(new RuleBaseImpl(Set.of(Rpx_py, Rpx_qx, Rqx_rxy)));
		GRDViewer.instance().display(grd);
	}
}
