package fr.boreal.test.grd;

import fr.boreal.grd.impl.GRDImpl;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.io.dlgp.ParserResult;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.rule.api.FORule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runners.Parameterized.Parameters;

import java.util.Set;
import java.util.stream.Stream;

class GRDImplFunctionsTest {

    @Parameters
	static Stream<Arguments> grdFunctionsData() {
        String baseFolder = "src/test/resources/";
        return Stream.of(
				Arguments.of(baseFolder + "functions_grd.dlgp")
				);
	}

	@DisplayName("Test GRD with functions")
	@ParameterizedTest(name = "{index}: Creating GRD with functions should work")
	@MethodSource("grdFunctionsData")
	public void grdWithFunctionsTest(String input) {

		ParserResult inputObjects = DlgpParser.parseFile(input);

		RuleBase rb = new RuleBaseImpl(inputObjects.rules());
		GRDImpl grd = new GRDImpl(rb);

		for(FORule r : rb.getRules()) {
			// position at the root of the GRD (done in the dlgp label of the rule for the unit test)
			if(r.getLabel().equals("ROOT")) {
				Set<FORule> computed_dependencies = grd.getTriggeredRules(r);

				// Cycle
				Assertions.assertTrue(computed_dependencies.contains(r), "The cycle dependency is detected");

				// Start of the path
				FORule startPath = null;
				for(FORule r2 : computed_dependencies) {
					if(r2 != r) {
						startPath = r2;
						break;
					}
				}
				Assertions.assertTrue(computed_dependencies.contains(startPath), "The dependency toward the other rule is detected");
				Assertions.assertEquals(2, computed_dependencies.size(), "Exactly 2 dependencies should be computed");
				
				// Follow the path
				computed_dependencies = grd.getTriggeredRules(startPath);
				Assertions.assertEquals(1, computed_dependencies.size(), "Exactly 1 dependency should be computed");
				
				computed_dependencies = grd.getTriggeredRules(computed_dependencies.iterator().next());
				Assertions.assertEquals(0, computed_dependencies.size(), "No dependency should be found at the end of the path");

			} else if(r.getLabel().equals("ALONE")) {
				Set<FORule> computed_dependencies = grd.getTriggeredRules(r);
				Assertions.assertEquals(0, computed_dependencies.size(), "No dependencies should be computed");
			}
		}
	}
}
