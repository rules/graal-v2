/**
 * Module for grd elements of InteGraal
 * 
 * @author Florent Tornil
 * 
 */
module fr.boreal.grd {
	requires transitive fr.boreal.model;
	requires fr.lirmm.boreal.util;
	requires fr.boreal.unifiers;
	requires fr.boreal.storage;
	requires fr.boreal.query_evaluation;
	
	requires org.jgrapht.core;
	requires gs.core;
	requires com.google.common;
	requires fr.boreal.io;

	exports fr.boreal.grd.api;
	exports fr.boreal.grd.impl;
	exports fr.boreal.grd.impl.dependency_checker;

}