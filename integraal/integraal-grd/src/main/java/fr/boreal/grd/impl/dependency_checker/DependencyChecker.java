package fr.boreal.grd.impl.dependency_checker;

import fr.boreal.model.rule.api.FORule;
import fr.boreal.unifier.QueryUnifier;

/**
 * Checks if a dependency between two rules is valid according to an implementation-specific criteria
 *
 * @author Florent Tornil
 */
public interface DependencyChecker {

	/**
	 * @param r1 the source rule
	 * @param r2 the target rule
	 * @param u the unifier between r1 and r2
	 * @return true iff the dependency is valid
	 */
	boolean isValidPositiveDependency(FORule r1, FORule r2, QueryUnifier u);
	
	/**
	 * @param r1 the source rule
	 * @param r2 the target rule
	 * @param u the unifier between r1 and r2
	 * @return true iff the dependency is valid
	 */
	boolean isValidNegativeDependency(FORule r1, FORule r2, QueryUnifier u);
	
	
}
