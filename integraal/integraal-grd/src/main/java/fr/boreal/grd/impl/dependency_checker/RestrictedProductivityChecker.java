package fr.boreal.grd.impl.dependency_checker;

import fr.boreal.model.formula.FOFormulas;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;
import fr.boreal.unifier.QueryUnifier;
import fr.lirmm.boreal.util.Rules;

/**
 * Checks that the dependency will produce something
 * <p>
 * Let r1, r2 two rules (B+, B-, H), potentially with (safe) negation <br>
 * We say that r1 may trigger r2 iff there exists a unifier u between B+(r2) and H(r1) such that <br>
 * (i)   u is productive <br>
 * (ii)  the new fact is not foldable on the facts used to produce it
 * 
 * @author Florent Tornil
 *
 */
public class RestrictedProductivityChecker extends ProductivityChecker {
	
	private static final RestrictedProductivityChecker INSTANCE = new RestrictedProductivityChecker();

	/**
	 * @return the default instance of this checker
	 */
	public static synchronized RestrictedProductivityChecker instance() {
		return INSTANCE;
	}
	
	@Override
	public boolean isValidPositiveDependency(FORule r1, FORule r2, QueryUnifier u) {
		if(super.isValidPositiveDependency(r1, r2, u)) {
			Substitution s = u.getAssociatedSubstitution();
			FactBase fb = new SimpleInMemoryGraphStore();
			fb.addAll(FOFormulas.createImageWith(Rules.getPositiveBodyPart(r1), s).asAtomSet());
			fb.addAll(FOFormulas.createImageWith(r1.getHead(), s).asAtomSet());
			fb.addAll(FOFormulas.createImageWith(Rules.getPositiveBodyPart(r1), s).asAtomSet());

			FOQuery<? extends FOFormula> q = FOQueryFactory.instance().createOrGetQuery(r2.getHead(), r2.getFrontier());
			return !GenericFOQueryEvaluator.defaultInstance().existHomomorphism(q, fb, s);
		} else {
			return false;
		}
	}

}
