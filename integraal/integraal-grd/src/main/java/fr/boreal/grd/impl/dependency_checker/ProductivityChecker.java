package fr.boreal.grd.impl.dependency_checker;

import java.util.Set;

import com.google.common.collect.Sets;

import fr.boreal.model.formula.FOFormulas;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.unifier.QueryUnifier;
import fr.lirmm.boreal.util.Rules;

/**
 * Checks that the dependency will produce something
 * <p>
 * Let r1, r2 two rules (B+, B-, H), potentially with (safe) negation <br>
 * We say that r1 may trigger r2 iff there exists a unifier u between B+(r2) and H(r1) such that <br>
 * (i)   μ(B+(r1)) ∩ μ(B-(r1)) = ∅ <br>
 * (ii)  μ(B+(r1)) ∩ μ(B-(r2)) = ∅ <br>
 * (iii) μ(B+(r2)) ∩ μ(B-(r2)) = ∅ <br>
 * (iv)  μ(B+(r2)) ∩ [μ(B-(r1))\μ(H(r1))] = ∅ <br>
 * (v)   μ(H(r2)) ∉ [μ(B+(r1)) u μ(H(r1)) u μ(B+(r2))] <br>
 * (vi)  μ(B-(r2)) ∩ μ(H(r1)) = ∅ <br>
 * (vii) μ(B+(r2)) ∉ μ(B+(r1))
 * <p>
 * Let r1, r2 two rules (B+, B-, H) with (safe) negation <br>
 * We say that r1 may prevent the triggering of r2 iff there exists a unifier u between Bi-(r2) and H(r1) such that <br>
 * (i)  [μ(B+(r1)) U μ(B+(r2))] ∩ [μ(B-(r1)) U μ(B-(r2))] = ∅ <br>
 * 
 * @author Florent Tornil
 *
 */
public class ProductivityChecker implements DependencyChecker {

	private static final ProductivityChecker INSTANCE = new ProductivityChecker();

	/**
	 * @return the default instance of this checker
	 */
	public static synchronized ProductivityChecker instance() {
		return INSTANCE;
	}

	@Override
	public boolean isValidPositiveDependency(FORule r1, FORule r2, QueryUnifier u) {
		Substitution s = u.getAssociatedSubstitution();

		Set<Atom> bpr1 = FOFormulas.createImageWith(Rules.getPositiveBodyPart(r1), s).asAtomSet();
		Set<Atom> bnr1 = FOFormulas.createImageWith(Rules.getNegativeBodyParts(r1), s).asAtomSet();
		Set<Atom> hr1 = FOFormulas.createImageWith(r1.getHead(), s).asAtomSet();

		Set<Atom> bpr2 = FOFormulas.createImageWith(Rules.getPositiveBodyPart(r2), s).asAtomSet();
		Set<Atom> bnr2 = FOFormulas.createImageWith(Rules.getNegativeBodyParts(r2), s).asAtomSet();
		Set<Atom> hr2 = FOFormulas.createImageWith(r2.getHead(), s).asAtomSet();

		return Sets.intersection(bpr1, bnr1).isEmpty() &&
				Sets.intersection(bpr1, bnr2).isEmpty() &&
				Sets.intersection(bpr2, bnr2).isEmpty() &&
				Sets.intersection(bpr2, Sets.difference(bnr1, hr1)).isEmpty() &&
				!Sets.union(Sets.union(bpr1, hr1), bpr2).containsAll(hr2) &&
				Sets.intersection(bnr2, hr1).isEmpty() &&
				!bpr1.containsAll(bpr2);
	}

	@Override
	public boolean isValidNegativeDependency(FORule r1, FORule r2, QueryUnifier u) {
		Substitution s = u.getAssociatedSubstitution();

		Set<Atom> bpr1 = FOFormulas.createImageWith(Rules.getPositiveBodyPart(r1), s).asAtomSet();
		Set<Atom> bnr1 = FOFormulas.createImageWith(Rules.getNegativeBodyParts(r1), s).asAtomSet();

		Set<Atom> bpr2 = FOFormulas.createImageWith(Rules.getPositiveBodyPart(r2), s).asAtomSet();
		Set<Atom> bnr2 = FOFormulas.createImageWith(Rules.getNegativeBodyParts(r2), s).asAtomSet();

		return Sets.intersection(Sets.union(bpr1, bpr2), Sets.union(bnr1, bnr2)).isEmpty();
	}
}
