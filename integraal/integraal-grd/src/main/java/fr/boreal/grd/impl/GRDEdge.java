package fr.boreal.grd.impl;

import org.jgrapht.graph.DefaultEdge;

import java.io.Serial;

/**
 * An edge of the GRD graph
 * <p>
 * This is mainly used to add metadata to the edges.
 * 
 * @author Florent Tornil
 */
public class GRDEdge extends DefaultEdge {

	@Serial
	private static final long serialVersionUID = 8327000815421028181L;
	
	/**
	 * True iff this edge is a positive dependency
	 */
	private boolean positive = true;
	
	/**
	 * Constructor using default values
	 */
	public GRDEdge() {
		// do nothing
	}
	
	/**
	 * Constructor using a given value for the type of dependency
	 * @param positive true iff the edge is a positive dependency
	 */
	public GRDEdge(boolean positive) {
		this.positive = positive;
	}
	
	/**
	 * Check if this edge is positive
	 * 
	 * @return true iff this edge represents a positive dependency
	 */
	public boolean isPositive() {
		return this.positive;
	}

}
