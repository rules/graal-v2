/*
 * Copyright (C) Inria Sophia Antipolis - Méditerranée / LIRMM
 * (Université de Montpellier & CNRS) (2014 - 2017)
 *
 * Contributors :
 *
 * Clément SIPIETER <clement.sipieter@inria.fr>
 * Mélanie KONIG
 * Swan ROCHER
 * Jean-François BAGET
 * Michel LECLERE
 * Marie-Laure MUGNIER <mugnier@lirmm.fr>
 *
 *
 * This file is part of Graal <https://graphik-team.github.io/graal/>.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.boreal.grd.api;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.rule.api.FORule;

/**
 * A graph of rule dependencies represent the possibility that an application of a rule triggers the linked rules in the graph.
 * 
 * @author Florent Tornil
 *
 */
public interface GraphOfFORuleDependencies {

	/**
	 * Return all the rules in the GRD
	 * @return all the rules in the GRD
	 */
	Collection<FORule> getRules();
	
	/**
	 * Returns all rules that can be triggered by the specified one.
	 *
	 * @param src a Rule.
	 * @return all rules that can be triggered by the specified one.
	 */
	Set<FORule> getTriggeredRules(FORule src);
	
	/**
	 * Returns all rules that can be prevented by the specified one.
	 *
	 * @param src a Rule.
	 * @return all rules that can be prevented by the specified one.
	 */
	Set<FORule> getPreventedRules(FORule src);

	/**
	 * Check if this GRD is stratifiable
	 * 
	 * @return true iff the ruleset represented by this GRD is stratifiable
	 */
	boolean isStratifiable();

	/**
	 * The computed order of the stratification must guaranty a correct rule application
	 * but no additional assumption is done at this level on the computed stratum
	 * @return the stratification as an ordered list of rulebases
	 */
	List<RuleBase> getStratification();
	
	/**
	 * The computed order of the stratification must guaranty a correct rule application.
	 * This method should try to minimize the number of stratum while keeping a correct order but no guaranty is given on the minimality of the result.
	 * An empty optional should be returned if the rules are not stratifiable
	 * @return the stratification as an ordered list of rulebases
	 */
	Optional<List<RuleBase>> getPseudoMinimalStratification();

	/**
	 * Computes a stratification of the rules for single evaluation.
	 * This method ensures that each rule is evaluated exactly once and attempts to minimize the number of strata.
	 * This is only possible for an acyclic graph; if the graph contains a cycle, an empty Optional is returned.
	 *
	 * @return an Optional containing the stratification as an ordered list of rule bases if the graph is acyclic,
	 *         otherwise an empty Optional.
	 */
	Optional<List<RuleBase>> getSingleEvaluationStratification();
}
