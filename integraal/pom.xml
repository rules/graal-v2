<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>fr.lirmm.graphik</groupId>
    <artifactId>integraal</artifactId>
    <version>${revision}</version>
    <packaging>pom</packaging>

    <name>InteGraal : Knowledge-Representation and Reasoning for Data
        Integration
    </name>
    <description>
        InteGraal has been designed in a modular way, in order to facilitate
        software reuse and extension.
        It should make it easy to test new scenarios and techniques, in
        particular by combining algorithms.
        The main features of Graal are currently the following:
        (1) internal storage to store data by using a SQL or RDF representation
        (Postgres, MySQL, HSQL, SQLite, Remote SPARQL endpoints, Local in-memory
        triplestores) as well as a native in-memory representation
        (2) data-integration capabilities for exploiting federated heterogeneous
        data-sources through mappings able to target systems such as SQL, RDF,
        and black-box (e.g. Web-APIs)
        (3) algorithms for query-answering over heterogeneous and federated data
        based on query rewriting and/or forward chaining (or chase)
    </description>
    <url>https://gitlab.inria.fr/rules/integraal</url>

    <licenses>
        <license>
            <name>Apache License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0</url>
        </license>
    </licenses>

    <developers>
        <developer>
            <id>jfbaget</id>
            <name>Jean-François Baget</name>
            <email>jean-francois.baget@inria.fr</email>
            <roles>
                <role>Modeling</role>
            </roles>
            <timezone>1</timezone>
            <organization>INRIA</organization>
            <organizationUrl>http://www.inria.fr/</organizationUrl>
        </developer>
        <developer>
            <id>pbisquert</id>
            <name>Pierre Bisquert</name>
            <email>pierre.bisquert@inrae.fr</email>
            <roles>
                <role>Modeling</role>
                <role>Developer</role>
                <role>Testing</role>
            </roles>
            <timezone>1</timezone>
            <organization>INRAE</organization>
            <organizationUrl>http://www.inria.fr/</organizationUrl>
        </developer>
        <developer>
            <id>mlecler</id>
            <name>Michel Leclère</name>
            <email>michel.leclere@umontpellier.fr</email>
            <roles>
                <role>Modeling</role>
                <role>Developer</role>
                <role>Testing</role>
            </roles>
            <timezone>1</timezone>
            <organization>Montpellier University</organization>
            <organizationUrl>http://www.lirmm.fr/</organizationUrl>
        </developer>
        <developer>
            <id>mmugnier</id>
            <name>Marie-Laure Mugnier</name>
            <email>marie-laure.mugnier@umontpellier.fr</email>
            <roles>
                <role>Modeling</role>
            </roles>
            <timezone>1</timezone>
            <organization>Montpellier University</organization>
            <organizationUrl>http://www.lirmm.fr/</organizationUrl>
        </developer>
        <developer>
            <id>gperution</id>
            <name>Guillaume Pérution</name>
            <email>guillaume.perution-kihli@inria.fr</email>
            <roles>
                <role>Modeling</role>
                <role>Developer</role>
                <role>Testing</role>
            </roles>
            <timezone>1</timezone>
            <organization>INRIA</organization>
            <organizationUrl>http://www.inria.fr/</organizationUrl>
        </developer>
        <developer>
            <id>ftornil</id>
            <name>Florent Tornil</name>
            <email>florent.tornil@inria.fr</email>
            <roles>
                <role>Modeling</role>
                <role>Developer</role>
                <role>Testing</role>
            </roles>
            <timezone>1</timezone>
            <organization>INRIA</organization>
            <organizationUrl>http://www.inria.fr/</organizationUrl>
        </developer>
        <developer>
            <id>fulliana</id>
            <name>Federico Ulliana</name>
            <email>federico.ulliana@inria.fr</email>
            <roles>
                <role>Modeling</role>
                <role>Developer</role>
                <role>Testing</role>
            </roles>
            <timezone>1</timezone>
            <organization>INRIA and Montpellier University</organization>
            <organizationUrl>http://www.inria.fr/</organizationUrl>
        </developer>
    </developers>

    <modules>
        <module>integraal-model</module>
        <module>integraal-util</module>
        <module>integraal-storage</module>
        <module>integraal-io</module>
        <module>integraal-query-evaluation</module>
        <module>integraal-unifiers</module>
        <module>integraal-grd</module>
        <module>integraal-forward-chaining</module>
        <module>integraal-backward-chaining</module>
        <module>integraal-graal-ruleset-analysis</module>
        <module>integraal-redundancy</module>
        <module>integraal-forgetting</module>
        <module>integraal-views</module>
        <module>integraal-api</module>
        <module>integraal-all</module>
        <module>integraal-component</module>
        <module>integraal-core</module>
        <module>integraal-configuration</module>
    </modules>

    <scm>
        <url>https://gitlab.inria.fr/rules/integraal.git</url>
    </scm>

    <properties>
        <groupId>fr.lirmm.graphik</groupId>
        <!-- Unique entry point for version number management -->
        <!-- pattern to follow: x.y.z[-q]-b -->
        <!-- where x = major version,
            y = minor version,
            z = incremental version,
            q (optional) = qualifier,
            b = build number.
            For more details:
        https://docs.oracle.com/middleware/1212/core/MAVEN/maven_version.htm#MAVEN400
         -->
        <revision>1.6.4</revision>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.release>21</maven.compiler.release>

        <!-- Dependencies version management -->

        <!-- Plugins -->
        <maven-compiler-plugin-version>3.13.0</maven-compiler-plugin-version>
        <maven-surefire-plugin-version>3.2.5</maven-surefire-plugin-version>
        <maven-shade-plugin-version>3.5.2</maven-shade-plugin-version>
        <maven-pmd-plugin-version>3.21.2</maven-pmd-plugin-version>
        <flatten-maven-plugin-version>1.6.0</flatten-maven-plugin-version>
        <findbugs-maven-plugin-version>3.0.5</findbugs-maven-plugin-version>
        <jacoco-maven-plugin-version>0.8.11</jacoco-maven-plugin-version>
        <maven-enforcer-plugin-version>3.5.0</maven-enforcer-plugin-version>

        <nexus-staging-maven-plugin-version>1.6.13</nexus-staging-maven-plugin-version>
        <maven-source-plugin-version>3.3.0</maven-source-plugin-version>
        <maven-javadoc-plugin-version>3.10.0</maven-javadoc-plugin-version>
        <maven-gpg-plugin-version>3.2.1</maven-gpg-plugin-version>

        <!-- Tool utilities -->
        <jcommander-version>1.82</jcommander-version>
        <picocli-version>4.7.5</picocli-version>
        <jline-version>3.24.0</jline-version>
        <jansi-version>2.4.0</jansi-version>

        <!-- Utils -->
        <guava-version>33.1.0-jre</guava-version>
        <commons-lang3-version>3.14.0</commons-lang3-version>
        <commons-collections4-version>4.4</commons-collections4-version>
        <jgrapht-version>1.5.2</jgrapht-version>
        <graphstream-version>1.3</graphstream-version>
        <radicchio-version>0.1.5</radicchio-version>
        <jackson-version>2.17.2</jackson-version>

        <!-- Graal -->
        <graal-version>1.3.1</graal-version>

        <!-- Unit test -->
        <junit-version>4.13.2</junit-version>
        <junit-jupiter-version>5.10.2</junit-jupiter-version>
        <mockito-version>5.3.1</mockito-version>

        <!-- Databases -->
        <commons-dbutils-version>1.8.1</commons-dbutils-version>
        <rdf4j-version>5.0.2</rdf4j-version>
        <opencsv-version>5.9</opencsv-version>
        <commons-csv-version>1.9.0</commons-csv-version>
        <dlgp-parser-version>1.2.4</dlgp-parser-version>
        <view-parser-version>1.0.0</view-parser-version>
        <sqlite-version>3.45.2.0</sqlite-version>
        <postgresql-version>42.7.3</postgresql-version>
        <hsqldb-version>2.7.2</hsqldb-version>
        <mysql-version>9.0.0</mysql-version>
        <json-version>20240303</json-version>
        <json-path-version>2.9.0</json-path-version>
        <mongodb-version>5.0.0</mongodb-version>

        <!-- Logging -->
        <slf4j-version>2.0.12</slf4j-version>
        <logback-version>1.5.4</logback-version>
    </properties>

    <dependencyManagement>
        <!-- Defines the version that are used by default in subprojects.
        Does not automatically import the dependencies: subprojects must
        explicitly declare their dependencies.-->
        <dependencies>
            <dependency>
                <groupId>${groupId}</groupId>
                <artifactId>integraal-api</artifactId>
                <version>${revision}</version>
            </dependency>
            <dependency>
                <groupId>${groupId}</groupId>
                <artifactId>integraal-backward-chaining</artifactId>
                <version>${revision}</version>
            </dependency>
            <dependency>
                <groupId>${groupId}</groupId>
                <artifactId>integraal-component</artifactId>
                <version>${revision}</version>
            </dependency>
            <dependency>
                <groupId>${groupId}</groupId>
                <artifactId>integraal-core</artifactId>
                <version>${revision}</version>
            </dependency>
            <dependency>
                <groupId>${groupId}</groupId>
                <artifactId>integraal-forgetting</artifactId>
                <version>${revision}</version>
            </dependency>
            <dependency>
                <groupId>${groupId}</groupId>
                <artifactId>integraal-forward-chaining</artifactId>
                <version>${revision}</version>
            </dependency>
            <dependency>
                <groupId>${groupId}</groupId>
                <artifactId>integraal-graal-ruleset-analysis</artifactId>
                <version>${revision}</version>
            </dependency>
            <dependency>
                <groupId>${groupId}</groupId>
                <artifactId>integraal-grd</artifactId>
                <version>${revision}</version>
            </dependency>
            <dependency>
                <groupId>${groupId}</groupId>
                <artifactId>integraal-io</artifactId>
                <version>${revision}</version>
            </dependency>
            <dependency>
                <groupId>${groupId}</groupId>
                <artifactId>integraal-model</artifactId>
                <version>${revision}</version>
            </dependency>
            <dependency>
                <groupId>${groupId}</groupId>
                <artifactId>integraal-query-evaluation</artifactId>
                <version>${revision}</version>
            </dependency>
            <dependency>
                <groupId>${groupId}</groupId>
                <artifactId>integraal-redundancy</artifactId>
                <version>${revision}</version>
            </dependency>
            <dependency>
                <groupId>${groupId}</groupId>
                <artifactId>integraal-storage</artifactId>
                <version>${revision}</version>
            </dependency>
            <dependency>
                <groupId>${groupId}</groupId>
                <artifactId>integraal-unifiers</artifactId>
                <version>${revision}</version>
            </dependency>
            <dependency>
                <groupId>${groupId}</groupId>
                <artifactId>integraal-util</artifactId>
                <version>${revision}</version>
            </dependency>
            <dependency>
                <groupId>${groupId}</groupId>
                <artifactId>integraal-views</artifactId>
                <version>${revision}</version>
            </dependency>
            <dependency>
                <groupId>${groupId}</groupId>
                <artifactId>integraal-configuration</artifactId>
                <version>${revision}</version>
            </dependency>

            <dependency>
                <groupId>${groupId}</groupId>
                <artifactId>view-parser</artifactId>
                <version>${view-parser-version}</version>
            </dependency>
            <dependency>
                <groupId>${groupId}</groupId>
                <artifactId>dlgp-parser</artifactId>
                <version>${dlgp-parser-version}</version>
            </dependency>
            <dependency>
                <groupId>fr.lirmm.graphik</groupId>
                <artifactId>graal-api</artifactId>
                <version>${graal-version}</version>
            </dependency>
            <dependency>
                <groupId>fr.lirmm.graphik</groupId>
                <artifactId>graal-core</artifactId>
                <version>${graal-version}</version>
            </dependency>
            <dependency>
                <groupId>fr.lirmm.graphik</groupId>
                <artifactId>graal-util</artifactId>
                <version>${graal-version}</version>
            </dependency>

            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>${guava-version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-lang3</artifactId>
                <version>${commons-lang3-version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-collections4</artifactId>
                <version>${commons-collections4-version}</version>
            </dependency>
            <dependency>
                <groupId>fr.lirmm.graphik</groupId>
                <artifactId>radicchio</artifactId>
                <version>${radicchio-version}</version>
            </dependency>
            <dependency>
                <groupId>org.jgrapht</groupId>
                <artifactId>jgrapht-core</artifactId>
                <version>${jgrapht-version}</version>
            </dependency>
            <dependency>
                <groupId>org.graphstream</groupId>
                <artifactId>gs-core</artifactId>
                <version>${graphstream-version}</version>
            </dependency>
            <dependency>
                <groupId>org.graphstream</groupId>
                <artifactId>gs-ui</artifactId>
                <version>${graphstream-version}</version>
            </dependency>

            <dependency>
                <groupId>com.beust</groupId>
                <artifactId>jcommander</artifactId>
                <version>${jcommander-version}</version>
            </dependency>
            <dependency>
                <groupId>info.picocli</groupId>
                <artifactId>picocli</artifactId>
                <version>${picocli-version}</version>
            </dependency>
            <dependency>
                <groupId>org.jline</groupId>
                <artifactId>jline</artifactId>
                <version>${jline-version}</version>
            </dependency>
            <dependency>
                <groupId>info.picocli</groupId>
                <artifactId>picocli-shell-jline3</artifactId>
                <version>${picocli-version}</version>
            </dependency>
            <dependency>
                <groupId>org.fusesource.jansi</groupId>
                <artifactId>jansi</artifactId>
                <version>${jansi-version}</version>
            </dependency>

            <dependency>
                <groupId>com.jayway.jsonpath</groupId>
                <artifactId>json-path</artifactId>
                <version>${json-path-version}</version>
            </dependency>
            <dependency>
                <groupId>com.opencsv</groupId>
                <artifactId>opencsv</artifactId>
                <version>${opencsv-version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-csv</artifactId>
                <version>${commons-csv-version}</version>
            </dependency>
            <dependency>
                <groupId>commons-dbutils</groupId>
                <artifactId>commons-dbutils</artifactId>
                <version>${commons-dbutils-version}</version>
            </dependency>
            <dependency>
                <groupId>org.xerial</groupId>
                <artifactId>sqlite-jdbc</artifactId>
                <version>${sqlite-version}</version>
            </dependency>
            <dependency>
                <groupId>org.postgresql</groupId>
                <artifactId>postgresql</artifactId>
                <version>${postgresql-version}</version>
            </dependency>
            <dependency>
                <groupId>org.hsqldb</groupId>
                <artifactId>hsqldb</artifactId>
                <version>${hsqldb-version}</version>
            </dependency>
            <dependency>
                <groupId>com.mysql</groupId>
                <artifactId>mysql-connector-j</artifactId>
                <version>${mysql-version}</version>
            </dependency>
            <dependency>
                <groupId>org.eclipse.rdf4j</groupId>
                <artifactId>rdf4j-rio-api</artifactId>
                <version>${rdf4j-version}</version>
            </dependency>
            <dependency>
                <groupId>org.eclipse.rdf4j</groupId>
                <artifactId>rdf4j-model-api</artifactId>
                <version>${rdf4j-version}</version>
            </dependency>
            <dependency>
                <groupId>org.eclipse.rdf4j</groupId>
                <artifactId>rdf4j-query</artifactId>
                <version>${rdf4j-version}</version>
            </dependency>
            <dependency>
                <groupId>org.eclipse.rdf4j</groupId>
                <artifactId>rdf4j-repository-api</artifactId>
                <version>${rdf4j-version}</version>
            </dependency>
            <dependency>
                <groupId>org.eclipse.rdf4j</groupId>
                <artifactId>rdf4j-sail-memory</artifactId>
                <version>${rdf4j-version}</version>
            </dependency>
            <dependency>
                <groupId>org.eclipse.rdf4j</groupId>
                <artifactId>rdf4j-repository-sail</artifactId>
                <version>${rdf4j-version}</version>
            </dependency>
            <dependency>
                <groupId>org.eclipse.rdf4j</groupId>
                <artifactId>rdf4j-repository-sparql</artifactId>
                <version>${rdf4j-version}</version>
            </dependency>
            <dependency>
                <groupId>org.json</groupId>
                <artifactId>json</artifactId>
                <version>${json-version}</version>
            </dependency>
            <dependency>
                <groupId>org.mongodb</groupId>
                <artifactId>mongodb-driver-sync</artifactId>
                <version>${mongodb-version}</version>
            </dependency>
            <dependency>
                <groupId>org.mongodb</groupId>
                <artifactId>bson</artifactId>
                <version>${mongodb-version}</version>
            </dependency>

            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>${junit-version}</version>
            </dependency>
            <dependency>
                <groupId>org.junit.jupiter</groupId>
                <artifactId>junit-jupiter-api</artifactId>
                <version>${junit-jupiter-version}</version>
            </dependency>
            <dependency>
                <groupId>org.junit.jupiter</groupId>
                <artifactId>junit-jupiter-params</artifactId>
                <version>${junit-jupiter-version}</version>
            </dependency>
            <dependency>
                <groupId>org.mockito</groupId>
                <artifactId>mockito-core</artifactId>
                <version>${mockito-version}</version>
            </dependency>
            <dependency>
                <groupId>org.mockito</groupId>
                <artifactId>mockito-junit-jupiter</artifactId>
                <version>${mockito-version}</version>
            </dependency>

            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-api</artifactId>
                <version>${slf4j-version}</version>
            </dependency>
            <dependency>
                <groupId>ch.qos.logback</groupId>
                <artifactId>logback-classic</artifactId>
                <version>${logback-version}</version>
            </dependency>

            <!-- Specific dependency version to ensure dependency convergence -->
            <dependency>
                <groupId>commons-codec</groupId>
                <artifactId>commons-codec</artifactId>
                <version>1.17.1</version>
            </dependency>
            <dependency>
                <groupId>com.fasterxml.jackson.core</groupId>
                <artifactId>jackson-databind</artifactId>
                <version>${jackson-version}</version>
            </dependency>
            <dependency>
                <groupId>com.fasterxml.jackson.core</groupId>
                <artifactId>jackson-core</artifactId>
                <version>${jackson-version}</version>
            </dependency>
            <dependency>
                <groupId>com.fasterxml.jackson.core</groupId>
                <artifactId>jackson-annotations</artifactId>
                <version>${jackson-version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-text</artifactId>
                <version>1.12.0</version>
            </dependency>
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-math3</artifactId>
                <version>3.6.1</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <plugins>
            <plugin>
                <!-- Maven Compiler Plugin -->
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven-compiler-plugin-version}</version>
                <configuration>
                    <release>${maven.compiler.release}</release>
                </configuration>
            </plugin>
            <plugin>
                <!-- Maven Surefire Plugin -->
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>${maven-surefire-plugin-version}</version>
            </plugin>
            <plugin>
                <!-- Maven pom flatten -->
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>flatten-maven-plugin</artifactId>
                <version>${flatten-maven-plugin-version}</version>
                <configuration>
                    <flattenMode>oss</flattenMode>
                </configuration>
                <executions>
                    <execution>
                        <id>flatten</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>flatten</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>flatten.clean</id>
                        <phase>clean</phase>
                        <goals>
                            <goal>clean</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <!-- Maven Javadoc Plugin -->
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>${maven-javadoc-plugin-version}</version>
                <executions>
                    <execution>
                        <id>attach-javadocs</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <!-- Maven Enforce Plugin -->
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <version>${maven-enforcer-plugin-version}</version>
                <executions>
                    <execution>
                        <id>dependency-convergence</id>
                        <configuration>
                            <rules>
                                <dependencyConvergence/>
                            </rules>
                        </configuration>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <distributionManagement>
        <snapshotRepository>
            <id>ossrh</id>
            <name>Sonatype Nexus Snapshots</name>
            <url>https://oss.sonatype.org/content/repositories/snapshots</url>
        </snapshotRepository>
        <repository>
            <id>ossrh</id>
            <url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
        </repository>
    </distributionManagement>
    <profiles>
        <profile>
            <id>maven-central-release</id>
            <build>
                <plugins>
                    <plugin>
                        <!-- Maven pom flatten -->
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>flatten-maven-plugin</artifactId>
                        <version>${flatten-maven-plugin-version}</version>
                        <configuration>
                            <flattenMode>oss</flattenMode>
                        </configuration>
                        <executions>
                            <execution>
                                <id>flatten</id>
                                <phase>process-resources</phase>
                                <goals>
                                    <goal>flatten</goal>
                                </goals>
                            </execution>
                            <execution>
                                <id>flatten.clean</id>
                                <phase>clean</phase>
                                <goals>
                                    <goal>clean</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <!-- Maven central publish -->
                        <groupId>org.sonatype.plugins</groupId>
                        <artifactId>nexus-staging-maven-plugin</artifactId>
                        <version>${nexus-staging-maven-plugin-version}</version>
                        <extensions>true</extensions>
                        <configuration>
                            <serverId>ossrh</serverId>
                            <nexusUrl>https://oss.sonatype.org/</nexusUrl>
                            <autoReleaseAfterClose>false</autoReleaseAfterClose>
                        </configuration>
                    </plugin>
                    <plugin>
                        <!-- Attach source jar -->
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-source-plugin</artifactId>
                        <version>${maven-source-plugin-version}</version>
                        <executions>
                            <execution>
                                <id>attach-sources</id>
                                <goals>
                                    <goal>jar-no-fork</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <!-- Attach javadoc jar -->
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-javadoc-plugin</artifactId>
                        <version>${maven-javadoc-plugin-version}</version>
                        <executions>
                            <execution>
                                <id>attach-javadocs</id>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <!-- GPG key validation -->
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <version>${maven-gpg-plugin-version}</version>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
