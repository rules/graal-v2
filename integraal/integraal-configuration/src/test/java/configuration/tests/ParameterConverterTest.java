package configuration.tests;

import java.time.Duration;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runners.Parameterized.Parameters;

import fr.boreal.configuration.keywords.InteGraalKeywords;
import fr.boreal.configuration.parameters.IGParameter;
import fr.boreal.configuration.parameters.IGParameterConverter;
import fr.boreal.configuration.parameters.IGParameterException;

/*
 * Checks that parameters are correctly converted
 */
public class ParameterConverterTest {


    // Test Integer parameters
    private static final String max = "MAX";
    private static final String rank = "Rank";
    private static final String val0 = "0";
    private static final String valNeg = "-3";
    private static final String val5 = "5";

    //Test Duration parameters
    private static final String timeout = "TimeOut";
    private static final String val5s = "PT5s";
    private static final String val0s = "pt0s";

    // Test Enum parameters
    private static final String checker = "CHECKER";
    private static final String images = "IMAGES";
    private static final String valChecker = "oblivious";
    private static final String valImages = "ALL";

    // Test Enum parameters open values
    private static final String url = InteGraalKeywords.URL.toString();
    private static final String urlVal = "/u/r/l";

    // Test unknown, empty or null parameters
    private static final String unknown = "Unknown";
    private static final String empty = "";
    private static final String nullPara = null;


    @Parameters
    static Stream<Arguments> data() {
        return Stream.of(Arguments.of(max, val5, new IGParameter(InteGraalKeywords.MAX, 5)),
                Arguments.of(max, val0, new IGParameter(InteGraalKeywords.MAX, 0)),
                Arguments.of(max, valNeg, null),
                Arguments.of(max, valChecker, null),
                Arguments.of(rank, val5, new IGParameter(InteGraalKeywords.RANK, 5)),
                Arguments.of(timeout, val5s, new IGParameter(InteGraalKeywords.TIMEOUT, Duration.parse("PT5s"))),
                Arguments.of(timeout, val5, null),
                Arguments.of(timeout, val0s, new IGParameter(InteGraalKeywords.TIMEOUT, Duration.ZERO)),
                Arguments.of(checker, valChecker, new IGParameter(InteGraalKeywords.CHECKER, InteGraalKeywords.Algorithms.Parameters.Chase.Checker.OBLIVIOUS)),
                Arguments.of(checker, valImages, null),
                Arguments.of(images, valImages, new IGParameter(InteGraalKeywords.IMAGES, InteGraalKeywords.Algorithms.Images.ALL)),
                Arguments.of(unknown, valImages, null),
                Arguments.of(empty, valImages, null),
                Arguments.of(nullPara, valImages, null)
        );
    }

    @Parameters
    static Stream<Arguments> negativeExamples() {
        return Stream.of(
                Arguments.of(checker, valImages)
        );
    }

    @Parameters
    static Stream<Arguments> openParameters() {
        return Stream.of(
                Arguments.of(url, urlVal, new IGParameter(InteGraalKeywords.URL, urlVal))
                );
    }

    @Parameters
    static Stream<Arguments> caseSensitiveExamples() {
        return Stream.of(
                Arguments.of("url", urlVal, new IGParameter(InteGraalKeywords.URL, urlVal))
        );
    }

    @DisplayName("Test converter ")
    @ParameterizedTest(name = "{index}: test conversion <{0},{1}> should give {2}")
    @MethodSource({"data", "openParameters", "caseSensitiveExamples"})
    public void testConverterTest(String name, String value, IGParameter<InteGraalKeywords, ?> expectedigp) {
        IGParameter<String, String> para = new IGParameter(name, value);
        try {
            IGParameter<InteGraalKeywords, ?> igp = IGParameterConverter.convert(para);
            Assertions.assertEquals(expectedigp, igp);
        } catch (IGParameterException e) {
            Assertions.assertEquals(expectedigp, null);
        }
    }

    @DisplayName("Test converter Negative Examples ")
    @ParameterizedTest(name = "{index}: test conversion <{0},{1}> should raise IGParameterException")
    @MethodSource("negativeExamples")
    public void testConverterTestNegativeExamples(String name, String value) {
        IGParameter<String, String> para = new IGParameter(name, value);
        try {
            IGParameterConverter.convert(para);
            Assertions.fail();
        } catch (IGParameterException e) {
            //test ok
        }
    }


}
