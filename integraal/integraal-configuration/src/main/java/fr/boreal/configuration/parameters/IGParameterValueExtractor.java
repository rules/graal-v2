package fr.boreal.configuration.parameters;

import java.time.Duration;

import fr.boreal.configuration.keywords.InteGraalKeywords;
import fr.boreal.configuration.keywords.InteGraalKeywords.Algorithms;

public class IGParameterValueExtractor {

	/**
	 *
	 * @param params
	 * @return the first occurrence of a timeout in the params
	 */
	public static Duration getTimeout(IGParameter<InteGraalKeywords, ?>... params) {

		return (Duration) search(params, InteGraalKeywords.TIMEOUT);

	}

	/**
	 *
	 * @param params
	 * @return the first occurrence of a rank in the params
	 */
	public static Integer getRank(IGParameter<InteGraalKeywords, ?>... params) {

		return (Integer) search(params, InteGraalKeywords.RANK);

	}

	/**
	 *
	 * @param params
	 * @return the first occurrence of a max in the params
	 */
	public static Integer getMax(IGParameter<InteGraalKeywords, ?>... params) {

		return (Integer) search(params, InteGraalKeywords.MAX);

	}

	/**
	 *
	 * @param params
	 * @return the first occurrence of the type of query image in the params
	 */
	public static Algorithms.Images getImage(IGParameter<InteGraalKeywords, ?>... params) {

		return (Algorithms.Images) search(params, InteGraalKeywords.IMAGES);

	}

	/**
	 *
	 * @param params
	 * @return returns true if the image type is not explicitly set as ALL
	 */
	public static boolean onlyConstants(IGParameter<InteGraalKeywords, ?>... params) {

		return !getImage(params).equals(InteGraalKeywords.Algorithms.Images.ALL);

	}

	private static Object search(IGParameter<InteGraalKeywords, ?>[] params, InteGraalKeywords name) {
		for (var p : params) {
			if (p.name().equals(name)) {
				return p.value();
			}
		}
		return null;
	}

	public static InteGraalKeywords.Algorithms.Parameters.HybridTypes getHybridType(
			IGParameter<InteGraalKeywords, ?>[] params) {
		
		return (InteGraalKeywords.Algorithms.Parameters.HybridTypes) search(params, InteGraalKeywords.HYBRIDTYPES);


	}

}
