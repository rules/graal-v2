package fr.boreal.configuration.parameters;


import fr.boreal.configuration.keywords.InteGraalKeywords;
import fr.lirmm.boreal.util.enumerations.EnumUtils;

/**
 * Structure used to describe an InteGraal API service parameter
 *
 * When used with InteGraalKeywords it runs a sanity check on the parameter value.
 *
 * @param name  identifies a service parameter
 * @param value provides a value for this parameter if required
 * @param <K>   a generic type for the name
 * @param <V>   a generic type for the value
 */
public record IGParameter<K, V>(K name, V value) {

    public IGParameter(K name, V value) {
        this.name = name;
        this.value = value;

        // sanity check wrt to InteGraalKeywords
        if (name instanceof InteGraalKeywords enumParam) {
            if (enumParam.isEnumeration() && value instanceof InteGraalKeywords valueKW) {
                InteGraalKeywords.checkAndGetEnumerationValue(valueKW.name(), enumParam);
            }
            InteGraalKeywords.checkValueType(value, enumParam);
        }
    }
}
