package fr.boreal.configuration.parameters;

import fr.boreal.configuration.keywords.InteGraalKeywords;

public class IGParameterConverter {

    /**
     * Converts an "external" IGParameter (a tuple of String values) to an "internal" IGParameter
     * (i.e. a tuple (InteGraalKeywords, Type)).
     * <br/>
     * The conversion is based on the parameter's name and value. The name must match one of the
     * values in the {@link InteGraalKeywords} enumeration. The value must be one of the values
     * associated with the type of this enum value (see {@link InteGraalKeywords}).
     *
     * @param parameter The external IGParameter to be converted.
     * @return An internal IGParameter with its associated InteGraalKeywords and the appropriate value type.
     * @throws IGParameterException If the parameter name is unknown in {@link InteGraalKeywords},
     *                              or if the value format is incorrect wrt its expected type
     *                              or if the type of the InteGraalKeyword is not specified.
     * @author Pierre Bisquert, Michel Leclère
     */
    public static IGParameter<InteGraalKeywords, ?> convert(IGParameter<String, String> parameter) throws IGParameterException {
        if (parameter.name() == null || parameter.name().equals(""))
            throw new IGParameterException("an IGParameter name must not be null.");
        try {
            InteGraalKeywords igk = InteGraalKeywords.findKeyword(parameter.name());
            Object value = igk.findValue(parameter.value());
            return new IGParameter(igk, value);
        } catch (IllegalArgumentException e) {
            throw new IGParameterException(e.getMessage());
        }
    }


    /**
     * Converts multiple "external" IGParameters (tuples of String values) with String values
     * to an array of "internal" IGParameters (i.e. tuples (InteGraalKeywords, Type)
     * <br/>
     * This method processes each parameter individually and converts it using the convert method.
     *
     * @param parameters The IGParameters with String values to be converted.
     * @return An array of IGParameters with InteGraalKeywords and the appropriate value types.
     * @throws IGParameterException If any parameter name is unknown, the value format is incorrect,
     *                              or the type of the parameter is not specified.
     */
    @SafeVarargs
    public static IGParameter<InteGraalKeywords, ?>[] convert(IGParameter<String, String>... parameters) throws IGParameterException {
        @SuppressWarnings("unchecked")
        IGParameter<InteGraalKeywords, ?>[] igParameters = new IGParameter[parameters.length];
        int i = 0;
        for (IGParameter<String, String> parameter : parameters) {
            igParameters[i++] = IGParameterConverter.convert(parameter);
        }
        return igParameters;
    }


}
