package fr.boreal.model.formula.impl;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaNegation;

/**
 * Default implementation of a FOFormulaNegation
 *
 * @author Florent Tornil
 */
public record FONegationImpl(FOFormula element) implements FOFormulaNegation {

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Constructor with a sub element
	 *
	 * @param element the sub element
	 */
	public FONegationImpl {
	}

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////


	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof FOFormulaNegation other) {
            return this.element().equals(other.element());
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return "not( " + this.element().toString() + ")";
	}

}
