package fr.boreal.model.logicalElements.api;

import java.util.List;
import java.util.Set;

/**
 * @author Florent Tornil, Federico Ulliana
 */
public interface LogicalFunctionalTerm extends Term {

    @Override
    default boolean isFunctionalTerm() {
        return true;
    }

    /**
     * @return true iff the term contains no variables
     */
    default boolean isGround() {
        return getVariables().isEmpty();
    }

    /**
     * @return a string representing the function name
     */
    String getFunctionName();

    /**
     * @return the list of first level terms.
     * For instance, for f(a,g(b)), it returns a and g(b).
     */
    List<Term> getFirstLevelTerms();

    /**
     * @return the list of all nested terms.
     * For instance, for f(a,g(b)), it returns a, g(b), and b.
     */
    List<Term> getAllNestedTerms();

    /**
     * Recursively enumerate all variable terms
     *
     * @return the set of variables included in this term
     */
    Set<Variable> getVariables();

    /**
     * Recursively enumerate all literal terms
     *
     * @return the set of literals included in this term
     */
    Set<Literal<?>> getLiterals();

    /**
     * Recursively enumerate all constant terms
     *
     * @return the set of literals included in this term
     */
    Set<Constant> getConstants();
}
