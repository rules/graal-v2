package fr.boreal.model.formula.impl;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Class that allows to wrap a fact base into a FOConjunction
 *
 * @author Guillaume Pérution-Kihli
 */
public class FOConjunctionFactBaseWrapper implements FOFormulaConjunction {
    final FactBase factBase;

    public FOConjunctionFactBaseWrapper(FactBase factBase) {
        this.factBase = factBase;
    }

    @Override
    public Collection<? extends FOFormula> getSubElements() {
        return factBase.getAtomsInMemory();
    }

    @Override
    public Set<Atom> asAtomSet() {
        return factBase.getAtomsInMemory();
    }

    @Override
    public Set<Predicate> getPredicates() {
        Set<Predicate> predicates = new HashSet<>();
        factBase.getPredicates().forEachRemaining(predicates::add);
        return predicates;
    }

    @Override
    public Set<Variable> getVariables() {
        return factBase.getVariables().collect(Collectors.toSet());
    }

    @Override
    public Set<Constant> getConstants() {
        return factBase.getConstants().collect(Collectors.toSet());
    }

    @Override
    public Set<Literal<?>> getLiterals() {
        return factBase.getLiterals().collect(Collectors.toSet());
    }

    @Override
    public String toString() {
        return factBase.toString();
    }
}
