package fr.boreal.model.logicalElements.factory.impl;

import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;

public class FactoryConstants {
	public static final TermFactory DEFAULT_TERM_FACTORY = SameObjectTermFactory.instance();
	public static final PredicateFactory DEFAULT_PREDICATE_FACTORY = SameObjectPredicateFactory.instance();
}
