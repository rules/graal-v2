package fr.boreal.model.formula.api;

import java.util.Set;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.Literal;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Variable;

/**
 * This interface represents a first order formula of atoms.
 * 
 * @author Florent Tornil
 */
public sealed interface FOFormula permits Atom, FOFormulaConjunction, FOFormulaDisjunction, FOFormulaNegation, CompoundFOFormula {

	/**
	 * @return true iff this node of the formula is a conjunction
	 */
	default boolean isConjunction() {
		return false;
	}

	/**
	 * @return true iff this node of the formula is a disjunction
	 */
	default boolean isDisjunction() {
		return false;
	}

	/**
	 * @return true iff this node of the formula is a negation
	 */
	default boolean isNegation() {
		return false;
	}

	/**
	 * @return true iff this node of the formula is a leaf
	 */
	default boolean isAtomic() {
		return false;
	}

	/**
	 * The given set does not represent any meaning on the formula as conjunctions,
	 * disjunctions and negations will be merged together.
	 * <br/>
	 * This should only be used to prepare some indexes
	 *
	 * @return the set of all the atoms of this formula
	 */
	Set<Atom> asAtomSet();

	/**
	 * @return the set of all the predicates of the atoms of this formula
	 */
	Set<Predicate> getPredicates();

	/**
	 * @return the set of all the variables of the atoms of this formula
	 */
	Set<Variable> getVariables();

	/**
	 * @return the set of all the constant of the atoms of this formula
	 */
	Set<Constant> getConstants();

	/**
	 * @return the set of all the constant of the atoms of this formula
	 */
	Set<Literal<?>> getLiterals();
}
