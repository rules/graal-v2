package fr.boreal.model.logicalElements.impl;

import java.util.List;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;

/**
 * Default implementation of Atom
 * @author Florent Tornil
 *
 */
public class AtomImpl implements Atom {

	private final Predicate predicate;
	private final Term[] terms;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Constructor with a list of terms
	 * @param predicate the predicate
	 * @param terms the list of terms
	 */
	public AtomImpl(Predicate predicate, List<Term> terms) {
		this.predicate = predicate;
		int n = predicate.arity();

		if (n != terms.size()) {
			throw new IllegalArgumentException("Cannot create an atom with predicate " + predicate + " of arity " + n
					+ " with " + terms.size() + " terms.");
		}

		this.terms = new Term[n];
		int i = 0;
		for (Term t : terms) {
			this.terms[i++] = t;
		}
	}

	/**
	 * Constructor with an array of terms
	 * @param predicate the predicate
	 * @param terms the array of terms
	 */
	public AtomImpl(Predicate predicate, Term... terms) {
		this.predicate = predicate;
		this.terms = terms;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public int indexOf(Term term) {
		for (int i = 0; i < terms.length; ++i) {
			if (terms[i].equals(term)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public boolean contains(Term term) {
		return this.indexOf(term) >= 0;
	}

	@Override
	public int[] indexesOf(Term term) {
		int[] result;
		int resultCounter = 0;
		int termsSize = terms.length;
        for (Term value : terms) {
            if (value.equals(term)) {
                resultCounter++;
            }
        }
		result = new int[resultCounter];
		if (resultCounter != 0) {
			int pos = 0;
			for (int i = 0; i < termsSize; i++) {
				if (terms[i].equals(term)) {
					result[pos] = i;
					pos++;
				}
			}
		}
		return result;
	}

	@Override
	public Term getTerm(int index) {
		return this.terms[index];
	}

	@Override
	public Term[] getTerms() {
		return this.terms;
	}
	
	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	@Override
	public Predicate getPredicate() {
		return this.predicate;
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.getPredicate().hashCode();
		for (Term t : this.terms) {
			result = prime * result + t.hashCode();
		}
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof Atom other) {
            if (this.getPredicate().equals(other.getPredicate())) {
				for (int i = 0; i < this.terms.length; i++) {
					if (!this.getTerm(i).equals(other.getTerm(i))) {
						return false;
					}
				}
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * @return a string representation of this atom of the form p(t1,...,tn)
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getPredicate().toString());
		sb.append('(');
		boolean first = true;
		for (Term t : this.terms) {
			if (!first) {
				sb.append(',');
			}
			sb.append(t.toString());
			first = false;
		}
		sb.append(')');
		return sb.toString();
	}
}
