package fr.boreal.model.logicalElements.impl;

import fr.boreal.model.logicalElements.api.Constant;

/**
 * Default implementation of Constant
 *
 * @author Florent Tornil
 */
public record ConstantImpl(String label) implements Constant {

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Constructor using a simple label
	 *
	 * @param label the string representation of the constant
	 */
	public ConstantImpl {
	}


	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof Constant other) {
			return this.label().equals(other.label());
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return this.label;
	}
}
