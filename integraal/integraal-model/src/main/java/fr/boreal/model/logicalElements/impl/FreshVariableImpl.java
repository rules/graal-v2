package fr.boreal.model.logicalElements.impl;

/**
 * A fresh variable is a variable that is not equals to another variable
 * @author Florent Tornil
 *
 */
public class FreshVariableImpl extends VariableImpl {

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * @param label the label
	 */
	public FreshVariableImpl(String label) {
		super(label);
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public int hashCode() {
		return super.hashCode() * 31;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof FreshVariableImpl other) {
            return this.label().equals(other.label());
		} else {
			return false;
		}
	}

}
