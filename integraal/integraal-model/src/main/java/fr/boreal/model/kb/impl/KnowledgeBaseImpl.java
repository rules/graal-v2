package fr.boreal.model.kb.impl;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.KnowledgeBase;
import fr.boreal.model.kb.api.RuleBase;

import java.util.Objects;

/**
 * Default implementation of a KnowledgeBase
 * 
 * @author Florent Tornil
 *
 */
public class KnowledgeBaseImpl implements KnowledgeBase {

	private final FactBase factbase;
	private final RuleBase rulebase;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Default constructor
	 * @param factbase the facts
	 * @param rulebase the rules
	 */
	public KnowledgeBaseImpl(FactBase factbase, RuleBase rulebase) {
		this.factbase = factbase;
		this.rulebase = rulebase;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	@Override
	public FactBase getFactBase() {
		return this.factbase;
	}

	@Override
	public RuleBase getRuleBase() {
		return this.rulebase;
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public boolean equals(Object o) {
		if (o instanceof KnowledgeBase kb) {
			return this.getRuleBase().equals(kb.getRuleBase()) && this.getFactBase().equals(kb.getFactBase());
		}

		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.getRuleBase(), this.getFactBase());
	}

    @Override
	public String toString() {
        return String.format("<KB>[Factbase: %s Rulebase: %s]",
				this.factbase,
                this.rulebase);
	}

}
