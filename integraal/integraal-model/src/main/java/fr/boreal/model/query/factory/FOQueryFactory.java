package fr.boreal.model.query.factory;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.partition.TermPartition;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.impl.FOQueryImpl;

import java.util.Collection;

/**
 * Factory for queries
 * 
 * @author Florent Tornil
 *
 */
public class FOQueryFactory {

	private static final FOQueryFactory INSTANCE = new FOQueryFactory();

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * @return the default instance of this factory
	 */
	public static FOQueryFactory instance() {
		return INSTANCE;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	/**
	 * Create a new query with the given arguments
	 * @param formula the query atoms as a FOFormula
	 * @param answer_variables the list of answer variables
	 * @return a query or an empty optional if an error occur
	 */
	public <Formula extends FOFormula> FOQuery<Formula> createOrGetQuery(Formula formula, Collection<Variable> answer_variables) {
		return this.createOrGetQuery(formula, answer_variables, new TermPartition());
	}

	/**
	 * Create a new query with the given arguments
	 * @param label the label of the query
	 * @param formula the query atoms as a FOFormula
	 * @param answer_variables the list of answer variables
	 * @return a query or an empty optional if an error occur
	 */
	public <Formula extends FOFormula> FOQuery<Formula> createOrGetQuery(String label, Formula formula, Collection<Variable> answer_variables) {
		return this.createOrGetQuery(label, formula, answer_variables, new TermPartition());
	}

	/**
	 * Create a new query with the given arguments
	 * @param formula the query atoms as a FOFormula
	 * @param answer_variables the list of answer variables
	 * @param equalities the equalities of the query variables
	 * @return a query or an empty optional if an error occur
	 */
	public <Formula extends FOFormula> FOQuery<Formula> createOrGetQuery(Formula formula, Collection<Variable> answer_variables, TermPartition equalities) {
		return this.createOrGetQuery("", formula, answer_variables, equalities);
	}

	/**
	 * Create a new query with the given arguments
	 * @param label the label of the query
	 * @param formula the query atoms as a FOFormula
	 * @param answer_variables the list of answer variables
	 * @param equalities the equalities of the query variables
	 * @return a query or an empty optional if an error occur
	 */
	public <Formula extends FOFormula> FOQuery<Formula> createOrGetQuery(String label, Formula formula, Collection<Variable> answer_variables, TermPartition equalities) {
		return new FOQueryImpl<>(label, formula, answer_variables, equalities);
	}

}
