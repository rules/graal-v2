package fr.boreal.model.ruleCompilation;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.tuple.Pair;

import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.partition.TermPartition;
import fr.boreal.model.partition.TermPartitionFactory;
import fr.boreal.model.ruleCompilation.api.RuleCompilationResult;
import fr.boreal.model.ruleCompilation.api.RuleCompilation;

/**
 * Does not compile anything
 * 
 * @author Florent Tornil
 */
public class NoRuleCompilation implements RuleCompilation {
	
	private static NoRuleCompilation instance;

	/**
	 * Do not instance. Use default instance.
	 */
	private NoRuleCompilation() {	}

	/**
	 * @return the default instance of this class
	 */
	public static synchronized NoRuleCompilation instance() {
		if (instance == null)
			instance = new NoRuleCompilation();

		return instance;
	}

	@Override
	public void compile(RuleBase rb) {
		// Do nothing
	}


	@Override
	public RuleCompilationResult compileAndGet(RuleBase rb) {
		// Do nothing
		return null;
	}
	
	@Override
	public boolean isMoreSpecificThan(Atom a, Atom b) {
		throw new NotImplementedException("This method is not implemented as it is not used anywhere");
	}

	@Override
	public Set<Pair<Atom, Substitution>> unfold(Atom a) {
		return Set.of(Pair.of(a, new SubstitutionImpl()));
	}

	@Override
	public boolean isCompatible(Predicate p, Predicate q) {
		return p.equals(q);
	}

	@Override
	public Set<Predicate> getCompatiblePredicates(Predicate p) {
		return Set.of(p);
	}

	@Override
	public Set<Substitution> getHomomorphisms(Atom a, Atom b, Substitution s) {
		Set<Substitution> res = new HashSet<>();
		if (a.getPredicate().equals(b.getPredicate())) {
			Substitution sub = new SubstitutionImpl();
			Iterator<Term> fatherTermsIt = List.of(a.getTerms()).iterator();
			Iterator<Term> sonTermsIt = List.of(b.getTerms()).iterator();

			Term fatherTerm, sonTerm;
			while (fatherTermsIt.hasNext() && sonTermsIt.hasNext()) {
				fatherTerm = fatherTermsIt.next();
				sonTerm = sonTermsIt.next();
				
				if (fatherTerm.isFrozen(s)) {
					if (!s.createImageOf(fatherTerm).equals(sonTerm)) {
						return res;
					}
				} else if (!sub.keys().contains(fatherTerm))
					sub.add((Variable) fatherTerm, sonTerm);
				else if (!sub.createImageOf(fatherTerm).equals(sonTerm))
					return res;
			}
			res.add(sub);
		}
		return res;
	}

	@Override
	public Set<TermPartition> getUnifications(Atom a, Atom b) {
		if (a.getPredicate().equals(b.getPredicate())) {
			return Set.of(TermPartitionFactory.instance().getByPositionPartition(List.of(a, b)));
		} else {
			return Set.of();
		}
	}

}
