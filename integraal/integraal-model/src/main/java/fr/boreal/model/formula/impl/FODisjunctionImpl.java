package fr.boreal.model.formula.impl;

import java.util.Collection;

import fr.boreal.model.formula.api.CompoundFOFormula;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaDisjunction;

/**
 * Default implementation of FOFormulaDisjunction
 * @author Florent Tornil
 */
public class FODisjunctionImpl extends CompoundFOFormula implements FOFormulaDisjunction {

	private static final String connector_representation = "v";

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Constructor using sub formulas
	 * @param subformulas the sub formulas
	 */
	public FODisjunctionImpl(Collection<? extends FOFormula> subformulas) {
		super(subformulas);
	}

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	@Override
	public String getConnectorRepresentation() {
		return connector_representation;
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof FOFormulaDisjunction other) {
            return this.getSubElements().equals(other.getSubElements());
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return this.getSubElements().stream().map(f -> f.hashCode() * this.getSubElements().size() * 31).reduce(0,
				Integer::sum);
	}
}
