package fr.boreal.model.queryEvaluation.api;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.functional.AllVariablesInSubstitutionMapToConstant;
import fr.boreal.model.logicalElements.functional.SpecificVariablesInSubstitutionMapToConstant;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.Query;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.iterators.FilterIterator;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

/**
 * A Query evaluator is one possible algorithm to evaluate a {@link Query}.
 * <br/>
 * Evaluating a Query means retrieving all the {@link Substitution} of the
 * answer variables from the query with respect to the atoms from the
 * {@link FactBase}.
 *
 * @param <Q> The type of {@link Query} that the evaluator can evaluate
 *
 */
public interface QueryEvaluator<Q extends Query> {

    /**
     * @param query query to evaluate
     * @param factbase factbase in which atoms are stored
     * @param variablesThatMustBeMappedToConstants the set of variables that must be mapped to constants
     * @param preHomomorphism a partial homomorphism from var(query) to terms(factbase) to extend
     * @return an {@link Iterator} of {@link Substitution} over all the answers of
     *         the given query in the given factbase with respect to the query
     *         answer variables.
     */
    Iterator<Substitution> evaluate(Q query, FactBase factbase, Collection<Variable> variablesThatMustBeMappedToConstants, Substitution preHomomorphism);

    /**
     * @param query query to evaluate
     * @param factbase factbase in which atoms are stored
     * @return an {@link Iterator} of {@link Substitution} over all the
     *         homomorphisms of the given query in the given factbase with respect
     *         to the query answer variables.
     */
    default Iterator<Substitution> homomorphism(Q query, FactBase factbase) {
        return evaluate(query, factbase, Set.of(), new SubstitutionImpl());
    }

    /**
     * @param query query to evaluate
     * @param factbase factbase in which atoms are stored
     * @param preHomomorphism a partial homomorphism to extend
     * @return an {@link Iterator} of {@link Substitution} over all the
     *         homomorphisms of the given query in the given factbase with respect
     *         to the query answer variables.
     */
    default Iterator<Substitution> homomorphism(Q query, FactBase factbase, Substitution preHomomorphism) {
        return evaluate(query, factbase, Set.of(), preHomomorphism);
    }

    /**
     * @param query query to evaluate
     * @param factbase factbase in which atoms are stored
     * @return an {@link Iterator} of {@link Substitution} over all the
     *         answers of the given query in the given factbase with respect
     *         to the query answer variables.
     */
    default Iterator<Substitution> evaluate(Q query, FactBase factbase) {
        return evaluate(query, factbase, query.getAnswerVariables(), new SubstitutionImpl());
    }

    /**
     * @param query query to evaluate
     * @param factbase factbase in which atoms are stored
     * @param preHomomorphism a partial answer to extend
     * @return an {@link Iterator} of {@link Substitution} over all the
     *         answers of the given query in the given factbase with respect
     *         to the query answer variables.
     */
    default Iterator<Substitution> evaluate(Q query, FactBase factbase, Substitution preHomomorphism) {
        return evaluate(query, factbase, query.getAnswerVariables(), preHomomorphism);
    }

    /**
     * @param query query to evaluate
     * @param factbase factbase in which atoms are stored
     * @return true iff there exist a substitution s that is a homomorphism of the query on the factbase
     */
    default boolean existHomomorphism(Q query, FactBase factbase) {
        return this.homomorphism(query, factbase).hasNext();
    }

    /**
     * @param query query to evaluate
     * @param factbase factbase in which atoms are stored
     * @param preHomomorphism a partial homomorphism to extend
     * @return true iff there exist a substitution s that is a homomorphism of the query on the factbase
     */
    default boolean existHomomorphism(Q query, FactBase factbase, Substitution preHomomorphism) {
        return this.homomorphism(query, factbase, preHomomorphism).hasNext();
    }

    /**
     * @param query query to evaluate
     * @param factbase factbase in which atoms are stored
     * @return true iff there exist a substitution s that is an answer to the query on the factbase
     */
    default boolean existAnswer(Q query, FactBase factbase) {
        return this.evaluate(query, factbase).hasNext();
    }

    /**
     * @param query query to evaluate
     * @param factbase factbase in which atoms are stored
     * @param preHomomorphism a partial answer to extend
     * @return true iff there exist a substitution s that is an answer to the query on the factbase
     */
    default boolean existAnswer(Q query, FactBase factbase, Substitution preHomomorphism) {
        return this.evaluate(query, factbase, preHomomorphism).hasNext();
    }

    /**
     * @param query query to evaluate
     * @param factbase factbase in which atoms are stored
     * @return the number of answers to the query
     */
    default long countAnswers(Q query, FactBase factbase) {
        AtomicLong count = new AtomicLong(0);
        this.evaluate(query, factbase).forEachRemaining(x -> count.getAndIncrement());
        return count.get();
    }

    /**
     * @param query query to evaluate
     * @param factbase factbase in which atoms are stored
     * @return the number of substitution that are homomorphisms of the query on the factbase
     */
    default long countHomomorphism(Q query, FactBase factbase) {
        AtomicLong count = new AtomicLong(0);
        this.homomorphism(query, factbase).forEachRemaining(x -> count.getAndIncrement());
        return count.get();
    }

    /**
     * Filters the result of a query by removing substitutions which map a variable
     * to some other variable of the active domain.
     *
     * @param unfilteredSubstitutions the unfiltered substitution
     * @param variablesThatMustBeMappedToConstants the set of variables that must be mapped to constants
     * @return the post-processed query result where results have been possibly
     *         filtered by retaining only substitutions mapping to constants if
     *         required
     */
    default Iterator<Substitution> postprocessResult(Iterator<Substitution> unfilteredSubstitutions, Collection<Variable> variablesThatMustBeMappedToConstants) {
        if (variablesThatMustBeMappedToConstants.isEmpty()) {
            return unfilteredSubstitutions;
        } else {
            var predicate = new SpecificVariablesInSubstitutionMapToConstant(variablesThatMustBeMappedToConstants);
            return postprocessResult(unfilteredSubstitutions, predicate);
        }
    }

    /**
     * Filters the result of a query by removing substitutions which map a variable
     * to some other variable of the active domain.
     *
     * @param unfilteredSubstitutions the unfiltered substitution
     * @param constantsOnly true iff all variables must be mapped to constants
     * @return the post-processed query result where results have been possibly
     *         filtered by retaining only substitutions mapping to constants if
     *         required
     */
    default Iterator<Substitution> postprocessResult(Iterator<Substitution> unfilteredSubstitutions, boolean constantsOnly) {
        if (constantsOnly) {
            return postprocessResult(unfilteredSubstitutions, new AllVariablesInSubstitutionMapToConstant());
        } else {
            return unfilteredSubstitutions;
        }
    }

    /**
     * Filters the result of a query according to a predicate.
     *
     * @param unfilteredSubstitutions the unfiltered substitution
     * @param predicate the condition to be evaluated
     * @return the post-processed query result where results have been possibly
     *         filtered by retaining only substitutions mapping to constants if
     *         required
     */
    default Iterator<Substitution> postprocessResult(Iterator<Substitution> unfilteredSubstitutions, Predicate<Substitution> predicate) {
        return new FilterIterator<>(unfilteredSubstitutions, predicate);
    }
}
