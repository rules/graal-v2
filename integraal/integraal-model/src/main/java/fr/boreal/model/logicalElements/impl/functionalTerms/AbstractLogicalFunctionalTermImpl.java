package fr.boreal.model.logicalElements.impl.functionalTerms;

import fr.boreal.model.logicalElements.api.*;

import java.util.*;

/**
 * Common methods for the implementation of a FunctionalTerm
 *
 * @author Federico Ulliana
 */
public abstract class AbstractLogicalFunctionalTermImpl implements LogicalFunctionalTerm {

    protected final String function_name;
    protected final List<Term> sub_terms;

    /////////////////////////////////////////////////
    // Constructors
    /////////////////////////////////////////////////

    /**
     * Default constructor
     *
     * @param function_name the name of the function to call
     * @param sub_terms     the parameters of the function
     */
    public AbstractLogicalFunctionalTermImpl(String function_name, List<Term> sub_terms) {
        this.function_name = function_name;
        this.sub_terms = sub_terms;
        sanityCheck(function_name, sub_terms);
    }

    /**
     * this method can be used to check the well-formedness of the functional term
     *
     * @param functionName
     * @param subTerms
     */
    abstract void sanityCheck(String functionName, List<Term> subTerms);


    /////////////////////////////////////////////////
    // Public methods

    /// //////////////////////////////////////////////
    @Override
    public String label() {
        throw new IllegalArgumentException(
                "Cannot get the label for a functional term. To get a String representation, please use toString");
    }

    @Override
    public String getFunctionName() {
        return this.function_name;
    }

    @Override
    public List<Term> getFirstLevelTerms() {
        return this.sub_terms;
    }

    @Override
    public List<Term> getAllNestedTerms() {
        List<Term> res = new ArrayList<>(List.of());
        for (Term t : sub_terms) {
            res.add(t);
            if (t.isFunctionalTerm() && t instanceof LogicalFunctionalTerm ft) {
                res.addAll(ft.getAllNestedTerms());
            }
        }
        return res;
    }

    @Override
    public Set<Variable> getVariables() {
        Set<Variable> variables = new HashSet<>();
        for (Term t : sub_terms) {
            if (t.isVariable()) {
                variables.add((Variable) t);
            } else if (t.isFunctionalTerm()) {
                variables.addAll(((LogicalFunctionalTerm) t).getVariables());
            }
        }
        return variables;
    }

    public Set<Constant> getConstants() {
        Set<Constant> constants = new HashSet<>();
        for (Term t : sub_terms) {
            if (t.isConstant()) {
                constants.add((Constant) t);
            } else if (t.isFunctionalTerm()) {
                constants.addAll(((LogicalFunctionalTerm) t).getConstants());
            }
        }
        return constants;
    }

    @Override
    public Set<Literal<?>> getLiterals() {
        Set<Literal<?>> literals = new HashSet<>();
        for (Term t : sub_terms) {
            if (t.isLiteral()) {
                literals.add((Literal<?>) t);
            } else if (t.isFunctionalTerm()) {
                literals.addAll(((LogicalFunctionalTerm) t).getLiterals());
            }
        }
        return literals;
    }

    /////////////////////////////////////////////////
    // Object methods
    /// //////////////////////////////////////////////

    @Override
    public int hashCode() {
        return Objects.hash(this.function_name, sub_terms);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o == null) {
            return false;
        } else if (o instanceof AbstractLogicalFunctionalTermImpl other) {
            boolean staticEqual = this.function_name.equals(other.function_name);
            if (staticEqual) {
                Iterator<Term> thisTermIt = this.sub_terms.iterator();
                Iterator<Term> otherTermIt = other.sub_terms.iterator();
                boolean termEqual = true;
                while (thisTermIt.hasNext()) {
                    if (!otherTermIt.hasNext() || !termEqual) {
                        return false;
                    } else {
                        termEqual = thisTermIt.next().equals(otherTermIt.next());
                    }
                }
                return termEqual && !otherTermIt.hasNext();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        boolean first = true;
        StringBuilder res = new StringBuilder(this.function_name + "(");

        for (Term t : this.sub_terms) {
            if (!first) {
                res.append(", ");
            } else {
                first = false;
            }
            res.append(t);
        }
        return res + ")";
    }

}
