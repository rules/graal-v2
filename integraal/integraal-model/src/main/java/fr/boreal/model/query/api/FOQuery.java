package fr.boreal.model.query.api;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.partition.TermPartition;

/**
 * This interface represents a first order Query.
 * <br/>
 * It is represented with a formula, the body of the query.
 * Associated to this formula is answer variables.
 * <br/>
 * Additionally, a query is linked to variable equalities.
 */
public interface FOQuery<Formula extends FOFormula> extends Query {

	/**
	 * @return the first order formula that represents this first order query
	 */
	Formula getFormula();

	/**
	 * The variable equalities represent equalities between variables of this query.
	 * <br/>
	 * These equalities are represented by a partition of the terms of the query.
	 * An empty partition mean that no equalities are expressed.
	 *
	 * @return the partition of the terms of this query representing variable equalities.
	 */
	TermPartition getVariableEqualities();

}
