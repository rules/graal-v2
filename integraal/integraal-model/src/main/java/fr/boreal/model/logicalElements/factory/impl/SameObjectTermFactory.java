package fr.boreal.model.logicalElements.factory.impl;

import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.Literal;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.impl.identityObjects.IdentityConstantImpl;
import fr.boreal.model.logicalElements.impl.identityObjects.IdentityFreshVariableImpl;
import fr.boreal.model.logicalElements.impl.identityObjects.IdentityLiteralImpl;
import fr.boreal.model.logicalElements.impl.identityObjects.IdentityVariableImpl;

import java.util.HashMap;
import java.util.Map;

/**
 * This factory creates Terms For each call at a same method with the same
 * parameters, the same java object will be returned. Therefore, the equality
 * test between those objects can be restricted to java object reference
 * comparison (==). Please make sure the terms implementations used are
 * immutable otherwise there could be unwanted side effects.
 * 
 * @author Florent Tornil
 */
public class SameObjectTermFactory implements TermFactory {

	private static final TermFactory INSTANCE = new SameObjectTermFactory();
	private static final String FRESH_PREFIX = "Graal:EE";

	private final Map<String, Constant> constants = new HashMap<>();
	private final Map<Object, Literal<?>> literals = new HashMap<>();
	private final Map<String, Variable> variables = new HashMap<>();

	private int fresh_counter = 0;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * @return the default instance of this factory
	 */
	public static TermFactory instance() {
		return INSTANCE;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public synchronized Constant createOrGetConstant(String label) {
        return this.constants.computeIfAbsent(label, IdentityConstantImpl::new);
	}

	@Override
	public synchronized <T> Literal<T> createOrGetLiteral(T value) {
		@SuppressWarnings("unchecked")
		Literal<T> result = (Literal<T>) this.literals.get(value);
		if (result == null) {
			result = new IdentityLiteralImpl<>(value);
			this.literals.put(value, result);
		}
		return result;
	}

	@Override
	public synchronized Variable createOrGetVariable(String label) {
        return this.variables.computeIfAbsent(label, IdentityVariableImpl::new);
	}

	@Override
	public synchronized Variable createOrGetFreshVariable() {
		return new IdentityFreshVariableImpl(FRESH_PREFIX + ++fresh_counter);
	}

	@Override
	public boolean forgetConstant(String label) {
		if (this.constants.get(label) != null) {
			this.constants.remove(label);
			return true;
		}
		else return false; 
	}

}
