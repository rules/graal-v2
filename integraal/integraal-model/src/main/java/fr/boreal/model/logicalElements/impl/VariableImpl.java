package fr.boreal.model.logicalElements.impl;

import fr.boreal.model.logicalElements.api.Variable;

/**
 * Default implementation of Variable
 * 
 * @author Florent Tornil
 *
 */
public class VariableImpl implements Variable {

	private final String label;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Constructor using a label
	 * @param label string representation of the variable
	 */
	public VariableImpl(String label) {
		this.label = label;
	}

	@Override
	public String label() {
		return this.label;
	}

	@Override
	public int hashCode() {
		return this.label.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof FreshVariableImpl) {
			return false;
		} else if (o instanceof Variable other) {
            return this.label().equals(other.label());
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return this.label;
	}
}
