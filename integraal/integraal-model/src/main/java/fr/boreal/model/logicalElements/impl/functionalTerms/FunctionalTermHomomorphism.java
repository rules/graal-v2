package fr.boreal.model.logicalElements.impl.functionalTerms;

import fr.boreal.model.logicalElements.api.*;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;

import java.util.Iterator;
import java.util.Optional;
import java.util.stream.Collectors;

public class FunctionalTermHomomorphism {

    /**
     * Computes the homomorphism from the source term to the target term with respect to the given substitution
     *
     * @param source            source functional term
     * @param target            target functional term
     * @param inputSubstitution initial substitution for source
     * @return the homomorphism of this to o
     */
    public static Optional<Substitution> homomorphism(LogicalFunctionalTerm source, LogicalFunctionalTerm target, Substitution inputSubstitution) {

        if (source.getFunctionName().equals(target.getFunctionName()) // Same name
                && source.getFirstLevelTerms().size() == target.getFirstLevelTerms().size()) { // Same number of arguments

            Iterator<Term> sourceTermsIt = source.getFirstLevelTerms().iterator();
            Iterator<Term> targetTermsIt = target.getFirstLevelTerms().iterator();

            // Add to res the variables already mapped by inputSubstitution to some term
            Substitution res = new SubstitutionImpl(
                    source.getVariables().stream()
                            .filter(v -> inputSubstitution.keys().contains(v))
                            .collect(Collectors.toMap(v -> v, inputSubstitution::createImageOf)));

            while (sourceTermsIt.hasNext() && targetTermsIt.hasNext()) {
                Term sourceTerm = sourceTermsIt.next();
                Term targetTerm = targetTermsIt.next();

                if (sourceTerm.isFrozen(inputSubstitution)) {
					/* If the target term is functional and evaluable, we need to evaluate it to compare it with
					// the image of the source term by s.
					// For example, if we have sourceTerm = Variable(x), s = {x -> Literal<2>} and
					// targetTerm = sum(1, 1), we want  to evaluate sum(1, 1) so that we have sum(1, 1) = Literal<2>
					// that is equal to s(x) = Literal<2>.
					*/
                    sourceTerm =
                            switch (sourceTerm) {
                                case EvaluableFunctionImpl et ->
                                        ((EvaluableFunctionImpl) et).eval(inputSubstitution);
                                case SpecializableLogicalFunctionalTerm st ->
                                        ((SpecializableLogicalFunctionalTerm) st).specialize(inputSubstitution);
                                default -> sourceTerm;
                            };

                    if (targetTerm instanceof EvaluableFunctionImpl otherTerm) {
                        targetTerm = otherTerm.eval(new SubstitutionImpl());
                    }

                    if (!inputSubstitution.createImageOf(sourceTerm).equals(targetTerm)) {
                        return Optional.empty(); // s does not map sourceTerm to targetTerm
                    }
                } else if (sourceTerm.isFunctionalTerm()) { // If we have a source functional term, that is not frozen
                    if (targetTerm.isFunctionalTerm()) {
                        Optional<Substitution> innerRes =
                                FunctionalTermHomomorphism.homomorphism(((LogicalFunctionalTerm) sourceTerm), (LogicalFunctionalTerm) targetTerm, inputSubstitution);
                        if (innerRes.isEmpty()) {
                            return Optional.empty(); // There is no homomorphism from sourceTerm to targetTerm
                        }
                        res = res.merged(innerRes.get()).orElse(null);
                        if (res == null) {
                            return Optional.empty(); // Merging failed
                        }
                    } else {
                        return Optional.empty();
                    }
                } else if (!res.keys().contains(sourceTerm)) {
                    if (targetTerm instanceof EvaluableFunctionImpl otherTerm) {
                        targetTerm = otherTerm.eval(new SubstitutionImpl());
                    }

                    res.add((Variable) sourceTerm, targetTerm); // extend the substitution res
                } else {
                    if (targetTerm instanceof EvaluableFunctionImpl otherTerm) {
                        targetTerm = otherTerm.eval(new SubstitutionImpl());
                    }

                    if (!res.createImageOf(sourceTerm).equals(targetTerm)) {
                        return Optional.empty(); // res does not map sourceTerm to targetTerm
                    }
                }
            }
            return Optional.of(res);
        }
        return Optional.empty();
    }

}
