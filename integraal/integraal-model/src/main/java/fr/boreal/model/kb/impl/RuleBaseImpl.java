package fr.boreal.model.kb.impl;

import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.rule.api.FORule;

import java.util.*;

/**
 * Default implementation of a RuleBase
 * @author Florent Tornil
 *
 */
public class RuleBaseImpl implements RuleBase {

	private final Collection<FORule> rules;
	private Map<Predicate, Collection<FORule>> rules_by_head_predicate;
	private Map<Predicate, Collection<FORule>> rules_by_body_predicate;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Default constructor, uses a HashSet to store rules
	 */
	public RuleBaseImpl() {
		this.rules = new HashSet<>();
	}

	/**
	 * Default constructor
	 * Also initializes the indexes
	 * @param rules the rules
	 */
	public RuleBaseImpl(Collection<FORule> rules) {
		this.rules = rules;
		this.rules_by_head_predicate = new HashMap<>();
		this.rules_by_body_predicate = new HashMap<>();

		for(FORule rule : rules) {
			for(Atom a : rule.getHead().asAtomSet()) {
				Collection<FORule> rulesWithAPredicate = this.rules_by_head_predicate.getOrDefault(a.getPredicate(), new HashSet<>());
				rulesWithAPredicate.add(rule);
				this.rules_by_head_predicate.put(a.getPredicate(), rulesWithAPredicate);
			}
			for(Atom a : rule.getBody().asAtomSet()) {
				Collection<FORule> rulesWithAPredicate = this.rules_by_body_predicate.getOrDefault(a.getPredicate(), new HashSet<>());
				rulesWithAPredicate.add(rule);
				this.rules_by_body_predicate.put(a.getPredicate(), rulesWithAPredicate);
			}
		}
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	@Override
	public Collection<FORule> getRules() {
		return this.rules;
	}

	@Override
	public Collection<FORule> getRulesByHeadPredicate(Predicate p) {
		if(this.rules_by_head_predicate == null) {
			this.rules_by_head_predicate = new HashMap<>();
			for(FORule rule : this.rules) {
				for(Atom a : rule.getHead().asAtomSet()) {
					Collection<FORule> rulesWithAPredicate = this.rules_by_head_predicate.getOrDefault(a.getPredicate(), new HashSet<>());
					rulesWithAPredicate.add(rule);
					this.rules_by_head_predicate.put(a.getPredicate(), rulesWithAPredicate);
				}
			}
		}
		return this.rules_by_head_predicate.getOrDefault(p, Collections.emptySet());
	}

	@Override
	public Collection<FORule> getRulesByBodyPredicate(Predicate p) {
		if(this.rules_by_body_predicate == null) {
			this.rules_by_body_predicate = new HashMap<>();
			for(FORule rule : this.rules) {
				for(Atom a : rule.getBody().asAtomSet()) {
					Collection<FORule> rulesWithAPredicate = this.rules_by_body_predicate.getOrDefault(a.getPredicate(), new HashSet<>());
					rulesWithAPredicate.add(rule);
					this.rules_by_body_predicate.put(a.getPredicate(), rulesWithAPredicate);
				}
			}
		}
		return this.rules_by_body_predicate.getOrDefault(p, Collections.emptySet());
	}

	@Override
	public void add(FORule r) {
		this.rules.add(r);
		this.rules_by_body_predicate = null;
		this.rules_by_head_predicate = null;
	}

	@Override
	public void remove(FORule r) {
		this.rules.remove(r);
		this.rules_by_body_predicate = null;
		this.rules_by_head_predicate = null;
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public int hashCode() {
		return this.rules.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof RuleBase) {
			Collection<FORule> c1 = ((RuleBase) o).getRules();
			Collection<FORule> c2 = this.getRules();
			if (c1.size() == c2.size()) {
				return c1.parallelStream().allMatch(c2::contains);
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return this.rules.toString();
	}

}
