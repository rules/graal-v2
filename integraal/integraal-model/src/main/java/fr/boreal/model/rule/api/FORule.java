package fr.boreal.model.rule.api;

import fr.boreal.model.formula.api.FOFormula;

/**
 * A Rule with FOFormula representation
 * @author Florent Tornil
 *
 */
public interface FORule extends Rule {

	@Override
	FOFormula getBody();

	@Override
	FOFormula getHead();

}
