package fr.boreal.model.formula.api;

import java.util.Collection;

/**
 * Representation of conjunctions of sub formulas
 * 
 * @author Florent Tornil
 */
public non-sealed interface FOFormulaConjunction extends FOFormula {

	@Override
	default boolean isConjunction() {
		return true;
	}

	/**
	 * @return the sub formulas
	 */
	Collection<? extends FOFormula> getSubElements();

}
