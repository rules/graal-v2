package fr.boreal.model.logicalElements.impl.functionalTerms;

import fr.boreal.model.logicalElements.api.*;

import java.util.*;

/**
 * Default implementation of FunctionalTerm
 *
 * @author Florent Tornil
 */
public class SpecializableLogicalFunctionalTermImpl extends AbstractLogicalFunctionalTermImpl implements SpecializableLogicalFunctionalTerm {

    /////////////////////////////////////////////////
    // Constructors
    /////////////////////////////////////////////////

    /**
     * Default constructor
     *
     * @param function_name the name of the function to call
     * @param sub_terms     the parameters of the function
     */
    public SpecializableLogicalFunctionalTermImpl(String function_name, List<Term> sub_terms) {
        super(function_name,sub_terms);
    }

    /////////////////////////////////////////////////
    // Public methods
    /////////////////////////////////////////////////

    public void sanityCheck(String functionName, List<Term> subTerms) {}

    @Override
    public LogicalFunctionalTerm specialize(Substitution s) {
        List<Term> sub_terms_images = new ArrayList<>();

        // apply the substution
        for (Term t : this.getFirstLevelTerms()) {
            Term image = s.createImageOf(t);
            sub_terms_images.add(image);
        }

        try {
            // this will succeed if the specialized term is ground
            return new GroundFunctionalTermImpl(this.getFunctionName(), sub_terms_images);
        }catch (Exception e){
            return new SpecializableLogicalFunctionalTermImpl(this.getFunctionName(), sub_terms_images);
        }
    }

}
