package fr.boreal.model.logicalElements.api;

import fr.boreal.model.logicalElements.impl.identityObjects.IdentityPredicateImpl;

/**
 * A Predicate represents a relation between terms. It's arity determines the
 * number of terms allowed in the relation. For example, a Predicate with name
 * {@code P} and arity {@code n} allows atomic formulas of the form
 * {@code P(t1,...,tn)}.
 *
 */
public interface Predicate {

	/**
	 * Bottom predicate : always false
	 */
	Predicate BOTTOM = new IdentityPredicateImpl("⊥", 0);
	/**
	 * Top predicate : always true
	 */
	Predicate TOP = new IdentityPredicateImpl("⊤", 0);

	/**
	 * @return the arity of this predicate
	 */
	int arity();

	/**
	 * @return a String representation of this predicate name
	 */
	String label();

}
