package fr.boreal.model.formula.factory;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.api.FOFormulaDisjunction;
import fr.boreal.model.formula.api.FOFormulaNegation;
import fr.boreal.model.formula.impl.FOConjunctionFactBaseWrapper;
import fr.boreal.model.formula.impl.FOConjunctionImpl;
import fr.boreal.model.formula.impl.FODisjunctionImpl;
import fr.boreal.model.formula.impl.FONegationImpl;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.impl.AtomImpl;

/**
 * Factory for formulas
 * 
 * @author Florent Tornil
 *
 */
public class FOFormulaFactory {

	private static final FOFormulaFactory INSTANCE = new FOFormulaFactory();

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * @return the default instance of this factory
	 */
	public static FOFormulaFactory instance() {
		return INSTANCE;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	/**
	 * @param sub_formula the sub formulas
	 * @return the negation of the given formula
	 */
	public final FOFormulaNegation createOrGetNegation(FOFormula sub_formula) {
		return new FONegationImpl(sub_formula);
	}

	/**
	 * @param sub_formulas the sub formulas
	 * @return a conjunction of the given formulas
	 */
	public final FOFormulaConjunction createOrGetConjunction(Collection<? extends FOFormula> sub_formulas) {
		return new FOConjunctionImpl(sub_formulas);
	}

	/**
	 * @param sub_formulas the sub formulas
	 * @return a conjunction of the given formulas
	 */
	public final FOFormulaConjunction createOrGetConjunction(FOFormula... sub_formulas) {
		Collection<FOFormula> set = new LinkedHashSet<>();
        Collections.addAll(set, sub_formulas);
		return this.createOrGetConjunction(set);
	}

	/**
	 * @param fact_base the fact base to convert
	 * @return the conjunction of all the atoms in the fact base
	 */
	public final FOFormulaConjunction createOrGetConjunction(FactBase fact_base) {
		return new FOConjunctionFactBaseWrapper(fact_base);
	}

	/**
	 * @param sub_formulas the sub formulas
	 * @return a disjunction of the given formulas
	 */
	public final FOFormulaDisjunction createOrGetDisjunction(Collection<? extends FOFormula> sub_formulas) {
		return new FODisjunctionImpl(sub_formulas);
	}

	/**
	 * @param sub_formulas the sub formulas
	 * @return a disjunction of the given formulas
	 */
	public final FOFormulaDisjunction createOrGetDisjunction(FOFormula... sub_formulas) {
		Collection<FOFormula> set = new LinkedHashSet<>();
        Collections.addAll(set, sub_formulas);
		return this.createOrGetDisjunction(set);
	}

	/**
	 * @param formula the formula to copy
	 * @return a deep copy of the given formula
	 */
	public FOFormula copy(FOFormula formula) {
		if (formula.isAtomic()) {
			return this.copyAtom((Atom) formula);
		}
		if (formula.isConjunction()) {
			return this.copyConjunction((FOFormulaConjunction) formula);
		}
		if (formula.isDisjunction()) {
			return this.copyDisjunction((FOFormulaDisjunction) formula);
		}
		if (formula.isNegation()) {
			return this.copyNegation((FOFormulaNegation) formula);
		}
		return null;
	}

	/**
	 * @param formula the formula to copy
	 * @return a deep copy of the given conjunction
	 */
	public FOFormulaConjunction copyConjunction(FOFormulaConjunction formula) {
		Collection<FOFormula> new_sub_formulas = new LinkedHashSet<>();
		for (FOFormula old_sub_formula : formula.getSubElements()) {
			FOFormula new_sub_formula = this.copy(old_sub_formula);
			if (new_sub_formula != null) {
				new_sub_formulas.add(new_sub_formula);
			} else {
				return null;
			}
		}
        return new FOConjunctionImpl(new_sub_formulas);
	}

	/**
	 * @param formula the formula to copy
	 * @return a deep copy of the given disjunction
	 */
	public FOFormulaDisjunction copyDisjunction(FOFormulaDisjunction formula) {
		Collection<FOFormula> new_sub_formulas = new LinkedHashSet<>();
		for (FOFormula old_sub_formula : formula.getSubElements()) {
			FOFormula new_sub_formula = this.copy(old_sub_formula);
			if (new_sub_formula != null) {
				new_sub_formulas.add(new_sub_formula);
			} else {
				return null;
			}
		}
        return new FODisjunctionImpl(new_sub_formulas);
	}

	/**
	 * @param formula the formula to copy
	 * @return a deep copy of the given negation
	 */
	public FOFormulaNegation copyNegation(FOFormulaNegation formula) {
		FOFormula sub_formula = this.copy(formula.element());
		if (sub_formula != null) {
			return new FONegationImpl(sub_formula);
		} else {
			return null;
		}
	}

	/**
	 * @param atom the atom to copy
	 * @return a deep copy of the given atom
	 */
	public Atom copyAtom(Atom atom) {
		return new AtomImpl(atom.getPredicate(), atom.getTerms());
	}
}
