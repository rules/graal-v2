package fr.boreal.model.ruleCompilation;

import java.util.*;
import java.util.Map.Entry;

import fr.boreal.model.logicalElements.api.*;
import fr.boreal.model.logicalElements.impl.functionalTerms.SpecializableLogicalFunctionalTermImpl;
import org.apache.commons.lang3.tuple.Pair;

import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.partition.TermPartition;
import fr.boreal.model.partition.TermPartitionFactory;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.ruleCompilation.api.RuleCompilationResult;
import fr.boreal.model.ruleCompilation.api.RuleCompilation;
import fr.boreal.model.logicalElements.impl.functionalTerms.FunctionalTermHomomorphism;

/**
 * Version as implemented by Melanie in graal
 * <br/>
 * Compilation for : p(X, Y, ...) -> q(X, Y, ...)
 * - atomic rules (head and body)
 * - no existential variables
 * - no constants
 * - body and head atom have the same arity
 * - body and head atom have exactly the same variables at the same position
 *
 * @author Florent Tornil
 */
public class HierarchicalRuleCompilation implements RuleCompilation {

    private final Map<Predicate, Set<Predicate>> order;

    /**
     * Create a new empty compilation
     */
    public HierarchicalRuleCompilation() {
        this.order = new HashMap<>();
    }

    @Override
    public void compile(RuleBase rb) {
        List<FORule> compilable = this.extractCompilable(rb);
        this.computeOrder(compilable);
    }

    /**
     * Creates a record with the partition of the compiled rules.
     */

    @Override
    public RuleCompilationResult compileAndGet(RuleBase rb) {

        List<FORule> snapshot_original_rulebase = new ArrayList<>(rb.getRules());

        List<FORule> compilable_rules = this.extractCompilable(rb);

        List<FORule> non_compilable_rules = new ArrayList<>(snapshot_original_rulebase);
        non_compilable_rules.removeAll(compilable_rules);

        this.computeOrder(compilable_rules);

        return new RuleCompilationResult(this, compilable_rules, compilable_rules, non_compilable_rules);
    }


    @Override
    public boolean isMoreSpecificThan(Atom a, Atom b) {
        if (this.isCompatible(a.getPredicate(), b.getPredicate())) {
            for (int i = 0; i < a.getTerms().length; i++) {
                if (!a.getTerm(i).equals(b.getTerm(i))) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Set<Pair<Atom, Substitution>> unfold(Atom a) {
        Set<Pair<Atom, Substitution>> res = new HashSet<>();
        res.add(Pair.of(a, new SubstitutionImpl()));
        for (Predicate p : this.getCompatiblePredicates(a.getPredicate())) {
            Atom u = new AtomImpl(p, a.getTerms());
            res.add(Pair.of(u, new SubstitutionImpl()));
        }
        return res;
    }

    @Override
    public boolean isCompatible(Predicate p, Predicate q) {
        return p.equals(q) || this.order.getOrDefault(p, new HashSet<>()).contains(q);
    }

    @Override
    public Set<Predicate> getCompatiblePredicates(Predicate p) {
        Set<Predicate> res = new HashSet<>();
        res.add(p);
        res.addAll(this.order.getOrDefault(p, new HashSet<>()));
        return res;
    }

    @Override
    public Set<Substitution> getHomomorphisms(Atom a, Atom b, Substitution s) {
        Set<Substitution> res = new HashSet<>();
        if (this.isCompatible(a.getPredicate(), b.getPredicate())) {
            Substitution homomorphism = new SubstitutionImpl();
            Iterator<Term> fatherTermsIt = List.of(a.getTerms()).iterator();
            Iterator<Term> sonTermsIt = List.of(b.getTerms()).iterator();

            while (fatherTermsIt.hasNext() && sonTermsIt.hasNext()) {
                Term fatherTerm = fatherTermsIt.next();
                Term sonTerm = sonTermsIt.next();

                if (fatherTerm.isFrozen(s)) {
                    if (!s.createImageOf(fatherTerm).equals(sonTerm)) {
                        return res;
                    }
                } else if (fatherTerm.isFunctionalTerm()) {
                    if (sonTerm.isFunctionalTerm()) {
                        Optional<Substitution> innerRes = FunctionalTermHomomorphism.homomorphism(((LogicalFunctionalTerm) fatherTerm), (LogicalFunctionalTerm) sonTerm, s);
                        if (innerRes.isEmpty()) {
                            return res;
                        } else {
                            homomorphism = homomorphism.merged(innerRes.get()).orElse(null);
                            if (homomorphism == null) {
                                return res;
                            }
                        }
                    } else {
                        return res;
                    }
                } else if (fatherTerm.isEvaluableFunction()) {
                    if (sonTerm.isEvaluableFunction()) {
                        // TODO : not sure we want this here
                        var fatherSpecializable = new SpecializableLogicalFunctionalTermImpl(((EvaluableFunction)fatherTerm).getFunctionName(),((EvaluableFunction)fatherTerm).getTerms());
                        var sonSpecializable = new SpecializableLogicalFunctionalTermImpl(((EvaluableFunction)sonTerm).getFunctionName(),((EvaluableFunction)sonTerm).getTerms());

                        Optional<Substitution> innerRes = FunctionalTermHomomorphism.homomorphism(( fatherSpecializable),  sonSpecializable, s);
                        if (innerRes.isEmpty()) {
                            return res;
                        } else {
                            homomorphism = homomorphism.merged(innerRes.get()).orElse(null);
                            if (homomorphism == null) {
                                return res;
                            }
                        }
                    } else {
                        return res;
                    }
                } else if (!homomorphism.keys().contains(fatherTerm)) {
                    homomorphism.add((Variable) fatherTerm, sonTerm);
                } else if (!homomorphism.createImageOf(fatherTerm).equals(sonTerm)) {
                    return res;
                }
            }
            res.add(homomorphism);
        }
        return res;
    }

    @Override
    public Set<TermPartition> getUnifications(Atom a, Atom b) {
        Set<TermPartition> res = new HashSet<>();
        if (this.isCompatible(b.getPredicate(), a.getPredicate())) {
            res.add(TermPartitionFactory.instance().getByPositionPartition(List.of(a, b)));
        }
        return res;
    }

    private List<FORule> extractCompilable(RuleBase rb) {
        Iterator<FORule> ruleSet = rb.getRules().iterator();
        List<FORule> compilable = new ArrayList<>();
        Set<FORule> toRemove = new HashSet<>();
        while (ruleSet.hasNext()) {
            FORule r = ruleSet.next();
            if (this.isCompilable(r)) {
                compilable.add(r);
                toRemove.add(r);
            }
        }
        for (FORule r : toRemove) {
            rb.remove(r);
        }
        return compilable;
    }

    private boolean isCompilable(FORule r) {
        Collection<Atom> bodyAtoms = r.getBody().asAtomSet();
        if (bodyAtoms.size() != 1) {
            return false;
        }
        Collection<Atom> headAtoms = r.getBody().asAtomSet();
        if (headAtoms.size() != 1) {
            return false;
        }

        Term[] body = r.getBody().asAtomSet().iterator().next().getTerms();
        Term[] head = r.getHead().asAtomSet().iterator().next().getTerms();
        if (body.length != head.length) {
            return false;
        }

        // Check if each variable is different
        Set<Term> variables = new HashSet<>();
        for (Term t : body) {
            if (!variables.add(t)) {
                return false;
            }
        }
        for (int i = 0; i < body.length; i++) {
            // Check for constants and same variables (include existential check)
            if (!body[i].isVariable() || !body[i].equals(head[i])) {
                return false;
            }
        }
        return true;
    }

    private void computeOrder(List<FORule> compilable) {
        for (FORule rule : compilable) {
            Predicate body = rule.getBody().asAtomSet().iterator().next().getPredicate();
            Predicate head = rule.getHead().asAtomSet().iterator().next().getPredicate();

            Set<Predicate> hierarchie = this.order.getOrDefault(head, new HashSet<>());
            hierarchie.add(body);
            this.order.put(head, hierarchie);

            this.updateTransitiveClosure(body, head);
        }
    }

    private void updateTransitiveClosure(Predicate body, Predicate head) {
        // Added rule  body -> head
        // Add transitivity for X -> body -> head
        // As X -> head
        Set<Predicate> hierarchieBody = this.order.getOrDefault(body, new HashSet<>());
        Set<Predicate> hierarchieHead = this.order.get(head);
        hierarchieHead.addAll(hierarchieBody);
        this.order.put(head, hierarchieHead);

        // Added rule body -> head
        // Add transitivity X -> head -> Y
        // As X -> Y
        for (Entry<Predicate, Set<Predicate>> entry : this.order.entrySet()) {
            if (entry.getValue().contains(head)) {
                Set<Predicate> newHierarchieX = entry.getValue();
                newHierarchieX.addAll(hierarchieHead);
                this.order.put(entry.getKey(), newHierarchieX);
            }
        }
    }
}
