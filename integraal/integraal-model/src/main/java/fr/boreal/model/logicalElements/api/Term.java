package fr.boreal.model.logicalElements.api;

/**
 * In analogy to natural language, where a noun phrase refers to an object,
 * a term denotes a mathematical object referring to someone or something.
 * A term is either a {@link Constant}, a {@link Variable}, a {@link Literal} or a {@link LogicalFunctionalTerm}
 */
public interface Term {

	/**
	 * @return a string representation of this object
	 */
	String label();

	/**
	 * @return true iff this object is a {@link Constant}
	 */
	default boolean isConstant() {
		return false;
	}

	/**
	 * @return true iff this object is a {@link Variable}
	 */
	default boolean isVariable() {
		return false;
	}

	/**
	 * @return true iff this object is a {@link Literal}
	 */
	default boolean isLiteral() {
		return false;
	}

	/**
	 * @return true iff this object is a {@link LogicalFunctionalTerm}
	 */
	default boolean isFunctionalTerm() {
		return false;
	}
	
	/**
	 * @return true iff this object and its nested terms do not contain any variable
	 */
	default boolean isGround() {
		return false;
	}
	
	/**
	 * A term is frozen if it is a constant or literal or if it is affected by the given substitution
	 * @param s a substitution freezing some variables
	 * @return true iff this term is frozen
	 */
	default boolean isFrozen(Substitution s) {
		return this.isConstant() ||
				this.isLiteral() ||
				(this.isFunctionalTerm() && s != null && s.keys().containsAll(((LogicalFunctionalTerm)this).getVariables())) ||
				((s != null) && s.keys().contains(this));
	}

	default boolean isEvaluableFunction()  {
		return false;
	}

}
