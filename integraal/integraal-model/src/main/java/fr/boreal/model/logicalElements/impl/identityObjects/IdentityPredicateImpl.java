package fr.boreal.model.logicalElements.impl.identityObjects;

import fr.boreal.model.logicalElements.api.Predicate;

/**
 * Implementation of a Predicate where the equality test is redefined to only test java object equality on reference.
 * <p>
 * It is recommended to use a factory to create these predicates.
 *
 * @author Florent Tornil
 */
public record IdentityPredicateImpl(String label, int arity) implements Predicate {

	@Override
	public String toString() {
		return this.label;
	}

}
