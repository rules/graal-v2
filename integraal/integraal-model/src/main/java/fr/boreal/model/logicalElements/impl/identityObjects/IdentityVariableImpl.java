package fr.boreal.model.logicalElements.impl.identityObjects;

import fr.boreal.model.logicalElements.api.Variable;

/**
 * Implementation of a Variable where the equality test is redefined to only test java object equality on reference.
 * <br/>
 * It is recommended to use a factory to create these terms.
 * 
 * @author Florent Tornil
 *
 */
public class IdentityVariableImpl implements Variable {

	private final String label;

	/**
	 * Constructor using a label
	 * @param label string representation
	 */
	public IdentityVariableImpl(String label) {
		this.label = label;
	}

	@Override
	public String label() {
		return this.label;
	}

	@Override
	public String toString() {
		return this.label;
	}
}
