package fr.boreal.model.kb.impl;

/**
 * A description of a FactBase for data access using mappings
 *
 * @author Florent Tornil
 */
public record FactBaseDescription(String url, String query) {

	/**
	 * Constructor using an url and a query
	 *
	 * @param url   a url as string
	 * @param query a query as string
	 */
	public FactBaseDescription {
	}

	/**
	 * @return the query
	 */
	@Override
	public String query() {
		return this.query;
	}

	/**
	 * @return the url
	 */
	@Override
	public String url() {
		return this.url;
	}

}
