package fr.boreal.model.logicalElements.api;

import java.util.HashSet;
import java.util.Set;

import fr.boreal.model.formula.api.FOFormula;

/**
 * Interface for Atoms. An atom is an atomic query of the form P(t1, ..., tn)
 * where P is a {@link Predicate} of arity n and t1, ..., tn are {@link Term}s.
 */
public non-sealed interface Atom extends FOFormula {

	/**
	 * @return the {@link Predicate} associated to this atom.
	 */
	Predicate getPredicate();

	/**
	 * @param index the position of the term in the atom
	 * @return the {@link Term} at the index position in the atom.
	 */
	Term getTerm(int index);

	/**
	 * @return the {@link Term} at the index position in the atom.
	 */
	Term[] getTerms();

	/**
	 * @param term the term to check
	 * @return true iff the {@link Term} term is present in this atom.
	 */
	boolean contains(Term term);

	/**
	 * @param term the term to find
	 * @return the index of the first occurrence of {@link Term} term in this atom. Any
	 *         negative non-zero value is returned if this atom does not contain the term.
	 */
	int indexOf(Term term);

	/**
	 * @param term the term to find all occurrences of
	 * @return the indexes of all the occurrence of {@link Term} term in this atom. An
	 *         empty array is returned if this atom does not contain the term.
	 */
	int[] indexesOf(Term term);

	@Override
	default Set<Variable> getVariables() {
		final int arity = this.getPredicate().arity();
		Set<Variable> variables = new HashSet<>();
		for (int i = 0; i < arity; i++) {
			Term term_i = this.getTerm(i);
			if(term_i.isVariable()) {
				variables.add((Variable) term_i);
			} else if(term_i.isFunctionalTerm()) {
				variables.addAll(((LogicalFunctionalTerm) term_i).getVariables());
			} else if(term_i.isEvaluableFunction()) {
				variables.addAll(((EvaluableFunction) term_i).getVariables());
			}
		}
		return variables;
	}
	
	@Override
	default Set<Constant> getConstants() {
		final int arity = this.getPredicate().arity();
		Set<Constant> constants = new HashSet<>();
		for (int i = 0; i < arity; i++) {
			Term term_i = this.getTerm(i);
			if (term_i.isConstant()) {
				constants.add((Constant) term_i);
			}
			else if(term_i.isFunctionalTerm()) {
				constants.addAll(((LogicalFunctionalTerm)term_i).getConstants());
			} else if(term_i.isEvaluableFunction()) {
				constants.addAll(((EvaluableFunction)term_i).getTerms(Constant.class));
			}
		}
		return constants;
	}

	@Override
	default Set<Literal<?>> getLiterals() {
		final int arity = this.getPredicate().arity();
		Set<Literal<?>> literals = new HashSet<>();
		for (int i = 0; i < arity; i++) {
			Term term_i = this.getTerm(i);
			if (term_i.isLiteral()) {
				literals.add((Literal<?>) term_i);
			} else if(term_i.isFunctionalTerm()) {
				literals.addAll(((LogicalFunctionalTerm) term_i).getLiterals());
			} else if(term_i.isEvaluableFunction()) {
				for(var lit : ((EvaluableFunction) term_i).getLiterals()){
					literals.add(lit);
				}
			}
		}
		return literals;
	}
	
	@Override
	default boolean isAtomic() {
		return true;
	}
	
	@Override
	default Set<Atom> asAtomSet() {
		return Set.of(this);
	}

	@Override
	default Set<Predicate> getPredicates() {
		return Set.of(this.getPredicate());
	}

    default boolean isComputedAtom() {
		return false;
	}
}
