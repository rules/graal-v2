package fr.boreal.model.logicalElements.functional;

import java.util.Collection;

import org.apache.commons.collections4.Predicate;

import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;

/**
 * Predicate used to filter substitutions that do not map only to constants.
 */

public class SpecificVariablesInSubstitutionMapToConstant implements Predicate<Substitution> {

	private final Collection<Variable> variables;

	public SpecificVariablesInSubstitutionMapToConstant(Collection<Variable> vars) {
		this.variables = vars;
	}

	@Override
	public boolean evaluate(Substitution s) {
		return s.mapsToConstantsOnly(variables);
	}

}
