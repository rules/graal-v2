package fr.boreal.model.query.impl;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.api.Query;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * The UnionFOQuery represents a union of queries.
 * Each query in the union is independent of the others ;
 * except for the answer variable's tuple that must be exactly the same.
 * <br/>
 * This constraint is ensured during the object creation and
 * an assertion error is thrown iff this constraint is not respected.
 * <br/>
 * An empty union can be created and the answer variables will be an empty list.
 * The result of the evaluation of an empty union should always be false.
 */
public class UnionFOQuery implements Query {

    private String label = "";

    private final Collection<FOQuery<? extends FOFormula>> queries;
    private Collection<Variable> answerVariables;

    public UnionFOQuery(Collection<FOQuery<? extends FOFormula>> queries) {
        this.queries = queries;
        Optional<FOQuery<? extends FOFormula>> opt_firstQuery = queries.stream().findFirst();
        if(opt_firstQuery.isPresent()) {
            this.answerVariables = opt_firstQuery.get().getAnswerVariables();
        } else {
            this.answerVariables = List.of();
        }

        for(FOQuery<?> query : queries) {
            if(this.answerVariables == null) {
                this.answerVariables = query.getAnswerVariables();
            } else {
                assert query.getAnswerVariables().equals(this.answerVariables);
            }
        }
    }

    public UnionFOQuery(String label, Collection<FOQuery<? extends FOFormula>> queries) {
        this(queries);
        this.label = label;
    }

    public Collection<FOQuery<? extends FOFormula>> getQueries() {
        return this.queries;
    }

    @Override
    public Collection<Variable> getAnswerVariables() {
        return this.answerVariables;
    }

    @Override
    public String getLabel() {
        return this.label;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UnionFOQuery that)) return false;
        return Objects.equals(queries, that.queries) && Objects.equals(answerVariables, that.answerVariables);
    }

    @Override
    public int hashCode() {
        return Objects.hash(queries, answerVariables);
    }

    @Override
    public String toString() {
        return "UnionFOQuery{" +
                "queries=" + queries +
                ", answerVariables=" + answerVariables +
                '}';
    }
}
