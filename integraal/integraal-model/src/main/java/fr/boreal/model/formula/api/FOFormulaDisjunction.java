package fr.boreal.model.formula.api;

import java.util.Collection;

/**
 * Representation of disjunctions of sub formulas
 * 
 * @author Florent Tornil
 */
public non-sealed interface FOFormulaDisjunction extends FOFormula {

	@Override
	default boolean isDisjunction() {
		return true;
	}

	/**
	 * @return the sub formulas
	 */
	Collection<? extends FOFormula> getSubElements();

}
