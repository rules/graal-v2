package fr.boreal.model.logicalElements.api;

/**
 * A constant is a symbol referring to something
 * 
 * @author Florent Tornil
 *
 */
public interface Constant extends Term {
	
	@Override
	default boolean isConstant() {
		return true;
	}@Override
	
	default boolean isGround() {
		return true;
	}

}
