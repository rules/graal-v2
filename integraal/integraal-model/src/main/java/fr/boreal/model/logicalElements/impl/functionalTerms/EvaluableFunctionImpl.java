package fr.boreal.model.logicalElements.impl.functionalTerms;

import java.util.*;

import fr.boreal.model.functions.Invoker;
import fr.boreal.model.logicalElements.api.*;

/**
 * Default implementation of FunctionalTerm
 *
 * @author Florent Tornil, Federico Ulliana
 */
public record EvaluableFunctionImpl(String function_name, Invoker invoker,
                                    List<Term> sub_terms) implements EvaluableFunction {

    /////////////////////////////////////////////////
    // Public methods

    /// //////////////////////////////////////////////
    public EvaluableFunctionImpl {
        Objects.requireNonNull(invoker, String.format("invoker cannot be null - name: %s - sub_terms: %s", function_name, sub_terms));
        Objects.requireNonNull(function_name, "function name cannot be null ");
        Objects.requireNonNull(sub_terms, "sub_terms cannot be null ");
    }

    public void sanityCheck(String functionName, List<Term> subTerms) {
    }

    @Override
    public EvaluableFunctionImpl setFunctionParameters(Substitution s) {
        List<Term> sub_terms_images = new ArrayList<>();

        // apply the substitution
        for (Term t : this.sub_terms()) {
            Term image = s.createImageOf(t);
            sub_terms_images.add(image);
        }

        return new EvaluableFunctionImpl(function_name, invoker, sub_terms_images);
    }

    @Override
    public String getFunctionName() {
        return this.function_name;
    }

    @Override
    public List<Term> getTerms() {
        return this.sub_terms;
    }

    @Override
    public Term eval(Substitution s) {
        /*
         * A functional term is evaluable IFF all of its arguments -- after applying the substitution -- are literals
         */
        List<Term> sub_terms_images = new ArrayList<>();

        for (Term t : this.sub_terms) {
            boolean forceEvaluation = true;
            Term image = s.createImageOf(t, forceEvaluation);
            if (!image.isLiteral()) {
                throw new RuntimeException("evaluation failed for functional term " + this + " because " + image + " (is not a literal) is of type " + image.getClass());
            }
            sub_terms_images.add(image);
        }
        return this.invoker.invoke(sub_terms_images.toArray(new Term[0]));
    }

    @Override
    public EvaluableFunction evaluateNestedFunctions(Substitution s) {
        List<Term> sub_terms_images = new ArrayList<>();

        for (Term t : this.sub_terms) {
            Term image = null;
            if (t.isEvaluableFunction()) {
                // the function has a nested function
                EvaluableFunction partiallyEvaluated = ((EvaluableFunction) t).evaluateNestedFunctions(s);
                if (partiallyEvaluated.isEvaluableWith(s)) {
                    boolean forceEvaluation = true;
                    image = s.createImageOf(t, forceEvaluation);
                }
            } else {
                image = s.createImageOf(t);
            }
            sub_terms_images.add(image);
        }


        return new EvaluableFunctionImpl(function_name, invoker, sub_terms_images);

    }

    public boolean isEvaluableFunction() {
        return true;
    }

    public boolean isFunctionalTerm() {
        return false;
    }

    @Override
    public Set<Variable> getVariables() {
        Set<Variable> variables = new LinkedHashSet<>();
        for (Term t : sub_terms) {
            if (t.isVariable()) {
                variables.add((Variable) t);
            } else if (t.isFunctionalTerm()) {
                variables.addAll(((LogicalFunctionalTerm) t).getVariables());
            } else if (t.isEvaluableFunction()) {
                variables.addAll(((EvaluableFunction) t).getVariables());
            }
        }
        return variables;
    }

    @Override
    public <T extends Term> Set<T> getTerms(Class<T> type) {
        Set<T> terms = new LinkedHashSet<>();
        for (Term t : sub_terms) {
            if (t.getClass().equals(type)) {
                terms.add((T) t);
            }
            if (t.isFunctionalTerm()) {
                for (var nested : ((LogicalFunctionalTerm) t).getAllNestedTerms()) {
                    if (nested.getClass().equals(type)) {
                        terms.add((T) nested);
                    }
                }
            }
            if (t.isEvaluableFunction()) {
                terms.addAll((Collection<? extends T>) ((EvaluableFunction) t).getTerms(type));
            }
        }
        return terms;
    }
    /////////////////////////////////////////////////
    // Object methods

    /// //////////////////////////////////////////////

    @Override
    public int hashCode() {
        return Objects.hash(this.function_name, sub_terms, invoker);
    }

    @Override
    public String label() {
        throw new IllegalArgumentException(
                "Cannot get the label for a functional term. To get a String representation, please use toString");
    }

    public String toString() {
        boolean first = true;
        StringBuilder res = new StringBuilder(this.function_name + "(");

        for (Term t : this.sub_terms) {
            if (!first) {
                res.append(", ");
            } else {
                first = false;
            }
            res.append(t);
        }
        return res + ")";
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o == null) {
            return false;
        } else if (o instanceof EvaluableFunctionImpl other) {
            boolean staticEqual = this.function_name.equals(other.function_name);
            if (staticEqual) {
                Iterator<Term> thisTermIt = this.sub_terms.iterator();
                Iterator<Term> otherTermIt = other.sub_terms.iterator();
                boolean termEqual = true;
                while (thisTermIt.hasNext()) {
                    if (!otherTermIt.hasNext() || !termEqual) {
                        return false;
                    } else {
                        termEqual = thisTermIt.next().equals(otherTermIt.next());
                    }
                }
                return termEqual && !otherTermIt.hasNext();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
