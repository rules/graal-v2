package fr.boreal.model.logicalElements.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import fr.boreal.model.functions.Invoker;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.ComputedAtom;
import fr.boreal.model.logicalElements.api.Literal;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;

/**
 * Default implementation of a ComputedAtom
 * 
 * @author Florent Tornil
 *
 */
public class ComputedAtomImpl extends AtomImpl implements ComputedAtom {

	private final Invoker invoker;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Constructor using a list of terms
	 * @param invoker the invoker
	 * @param predicate the predicate
	 * @param terms the list of terms
	 */
	public ComputedAtomImpl(Invoker invoker, Predicate predicate, List<Term> terms) {
		super(predicate, terms);
		this.invoker = invoker;
		this.checkInvoker();
	}

	/**
	 * Constructor using an array of terms
	 * @param invoker the invoker
	 * @param predicate the predicate
	 * @param terms the array of terms
	 */
	public ComputedAtomImpl(Invoker invoker, Predicate predicate, Term... terms) {
		super(predicate, terms);
		this.invoker = invoker;
		this.checkInvoker();
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public Atom eval(Substitution s, boolean forceFunctionEvaluation) {
		boolean evaluable = true;
		ArrayList<Term> sub_terms_images = new ArrayList<>();
		for (Term t : this.getTerms()) {
			// force the evaluation of functions in terms
			Term image = s.createImageOf(t,forceFunctionEvaluation);
			if(!image.isLiteral()) {
				evaluable = false;
			}
			sub_terms_images.add(image);
		}
		if(!evaluable) {
			return new ComputedAtomImpl(this.invoker, this.getPredicate(), sub_terms_images);
		}

		Term result = this.invoker.invoke(sub_terms_images.toArray(new Term[sub_terms_images.size()]));

		// Ensure the result is a Literal<Boolean>
		if (!(result instanceof Literal<?> literal) || !(literal.value() instanceof Boolean value)) {
			throw new RuntimeException(
					String.format(
							"[%s::invoke] Invoker result is not a Literal<Boolean>: %s " +
									"(in computed atom %s with substitution %s)",
							this.getClass(), result, this, s));
		}

		// Return TOP or BOTTOM based on the boolean value
		return value ? new AtomImpl(Predicate.TOP) : new AtomImpl(Predicate.BOTTOM);

	}

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public int hashCode() {
		return Objects.hash(Arrays.hashCode(this.getTerms()), this.getPredicate(), this.invoker);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof ComputedAtom) {
			ComputedAtom c = (ComputedAtom)o;
			if (this.getPredicate().equals(c.getPredicate())) {
				for (int i = 0; i < this.getTerms().length; i++) {
					if (!this.getTerm(i).equals(c.getTerm(i))) {
						return false;
					}
				}
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	private void checkInvoker() {
		if (this.invoker == null) {
			throw new IllegalStateException(
					String.format(
							"[%s::invoke] The invoker in ComputedAtomImpl is null: [predicate: %s, terms: %s]",
							this.getClass(),
							this.getPredicate(),
							Arrays.toString(this.getTerms())));
		}
	}

}
