package fr.boreal.model.logicalElements.api;

public interface SpecializableLogicalFunctionalTerm extends LogicalFunctionalTerm {
    /**
     *
     * @param s substitution
     * @return a new functional term where the substitution has been applied
     */
    LogicalFunctionalTerm specialize(Substitution s);
}
