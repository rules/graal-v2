package fr.boreal.model.kb.api;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Atom;

/**
 * @author Florent Tornil
 *
 */
public interface Writeable {

	/**
	 * Stores the given atom
	 * 
	 * @param atom to add
	 * @return true iff the atom is new
	 */
	boolean add(Atom atom);

	/**
	 * Stores the given atoms
	 * 
	 * @param atoms to add
	 * @return true iff at least one atom is new
	 */
	boolean add(FOFormula atoms);

	/**
	 * Stores the given atoms
	 * 
	 * @param atoms to add
	 * @return true iff at least one atom is new
	 */
	boolean addAll(Collection<Atom> atoms);

	/**
	 * Stores the given atoms
	 * 
	 * @param atoms to add
	 * @return true iff at least one atom is new
	 */
	default boolean addAll(Stream<Atom> atoms) {
			return atoms.map(this::add).reduce(Boolean::logicalOr).orElse(false);
	}

	/**
	 * Removes the given atom
	 * 
	 * @param atom to remove
	 * @return true iff the atom is removed
	 */
	boolean remove(Atom atom);

	/**
	 * Removes the given atoms
	 * 
	 * @param atoms to remove
	 * @return true iff at least one atom is removed
	 */
	boolean remove(FOFormula atoms);

	/**
	 * Removes the given atoms
	 * 
	 * @param atoms to remove
	 * @return true iff at least one atom is removed
	 */
	boolean removeAll(Collection<Atom> atoms);

	/**
	 * Removes the given atoms
	 * 
	 * @param atoms to remove
	 * @return true iff at least one atom is removed
	 */
	default boolean removeAll(Stream<Atom> atoms) {
		return atoms.map(this::remove).reduce(Boolean::logicalOr).orElse(false);
	}

	/**
	 * Deletes all atoms
	 */
	default void clear() {
		throw new UnsupportedOperationException("Clear is not supported on this writeable.");
	}
}
