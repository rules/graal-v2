package fr.boreal.model.ruleCompilation.api;

import java.util.Collection;

import fr.boreal.model.rule.api.FORule;

public record RuleCompilationResult(RuleCompilation compilation, Collection<FORule> originalRuleSet, Collection<FORule> compilableRules, Collection<FORule> nonCompilableRules) {

}
