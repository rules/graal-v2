package fr.boreal.model.kb.api;

/**
 * Types of FactBases that can be handled with InteGraal
 * 
 * @author Florent Tornil
 */
public enum FactBaseType {
	/**
	 * Native Graal storage
	 */
	GRAAL,
	/**
	 * Database storage
	 */
	RDBMS,
	/**
	 * Triple Store storage
	 */
	ENDPOINT,
	/**
	 * MongoDB storage
	 */
	MONGO,
	/**
	 * API Web access
	 */
	WEBAPI
}
