package fr.boreal.model.kb.api;

import java.util.*;

import fr.boreal.model.logicalElements.api.*;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;

/**
 * This interface describe storage systems that can be accessed with read rights.
 * <br/>
 * The base operation is the match that return all the atoms matching with the given atom.
 * An atom is said to match with another atom if it has
 * - the same predicate,
 * - the same constants at the same position and
 * - there exist a homomorphism from the variables of the first into the second
 * <br/>
 * A match can be computed with regard to an initial substitution, which act as a freeze for some variables of the initial atom
 *
 * @author Florent Tornil
 */
public interface Readable {

    /**
     * @param a atom to match
     * @return all the atoms that match the given one
     */
    default Iterator<Atom> match(Atom a) {
        return match(a, new SubstitutionImpl());
    }

    /**
     * The substitution is used to freeze some variables of the atom
     *
     * @param a atom to match
     * @param s a substitution
     * @return all the atom that match the given one according to the given substitution
     */
    Iterator<Atom> match(Atom a, Substitution s);

}
