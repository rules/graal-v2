package fr.boreal.model.query.api;

import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.partition.TermPartition;

import java.util.Collection;

/**
 * This interface represents a general Query
 */
public interface Query {

	/**
	 * @return the label of the query
	 */
	String getLabel();

	/**
	 * @return an ordered collection of variable representing the variables of this query that are used as answers.
	 */
	Collection<Variable> getAnswerVariables();

}