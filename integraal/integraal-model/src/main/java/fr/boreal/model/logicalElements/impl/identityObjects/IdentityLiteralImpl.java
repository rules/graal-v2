package fr.boreal.model.logicalElements.impl.identityObjects;

import fr.boreal.model.logicalElements.api.Literal;

/**
 * Implementation of a Literal where the equality test is redefined to only test java object equality on reference.
 * <p>
 * It is recommended to use a factory to create these terms.
 *
 * @param <T> the native type of the Literal
 * @author Florent Tornil
 */
public record IdentityLiteralImpl<T>(T value) implements Literal<T> {

	@Override
	public String label() {
		return this.value.toString();
	}

	@Override
	public String toString() {
		return this.value == null ? "null" : this.value.toString();
	}

	public boolean equals(Object o){
		if (this == o) {
			return true;
		};
		return false;
	}
}
