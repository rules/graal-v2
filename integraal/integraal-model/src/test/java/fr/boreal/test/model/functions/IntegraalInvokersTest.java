package fr.boreal.test.model.functions;

import fr.boreal.model.functions.IntegraalInvokers;
import fr.boreal.model.functions.Invoker;
import fr.boreal.model.logicalElements.api.*;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.logicalElements.impl.ComputedAtomImpl;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.functions.IntegraalInvokers.InvokerException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class IntegraalInvokersTest {
    private static final IntegraalInvokers invokers = new IntegraalInvokers(SameObjectTermFactory.instance());
    private static final List<String> stringLiterals = List.of("1021", "409", "150", "146");
    private static final Map<String, Literal<String>> stringToLiteral = stringLiterals.stream().collect(Collectors.toMap(s -> s, s -> SameObjectTermFactory.instance().createOrGetLiteral(s)));
    private static final List<String> stringVariables = List.of("X", "Y", "Z", "U", "V");
    private static final Map<String, Variable> stringToVariable = stringVariables.stream().collect(Collectors.toMap(s -> s, s -> SameObjectTermFactory.instance().createOrGetVariable(s)));
    private static final Literal<Boolean> literalFalse = SameObjectTermFactory.instance().createOrGetLiteral(false);
    private static final Literal<Boolean> literalTrue = SameObjectTermFactory.instance().createOrGetLiteral(true);
    private static final Atom top = new AtomImpl(Predicate.TOP);
    private static final Atom bottom = new AtomImpl(Predicate.BOTTOM);

    static Stream<Object[]> isSmallerOrEqualsToData () {
        return Arrays.stream(new Object[][]{
                {stringToLiteral.get("1021"), stringToLiteral.get("409"), literalFalse},
                {stringToLiteral.get("1021"), stringToLiteral.get("150"), literalFalse},
                {stringToLiteral.get("1021"), stringToLiteral.get("146"), literalFalse},
                {stringToLiteral.get("1021"), stringToLiteral.get("1021"), literalTrue},

                {stringToLiteral.get("409"), stringToLiteral.get("1021"), literalTrue},
                {stringToLiteral.get("409"), stringToLiteral.get("409"), literalTrue},
                {stringToLiteral.get("409"), stringToLiteral.get("150"), literalFalse},
                {stringToLiteral.get("409"), stringToLiteral.get("146"), literalFalse},

                {stringToLiteral.get("150"), stringToLiteral.get("1021"), literalTrue},
                {stringToLiteral.get("150"), stringToLiteral.get("409"), literalTrue},
                {stringToLiteral.get("150"), stringToLiteral.get("150"), literalTrue},
                {stringToLiteral.get("150"), stringToLiteral.get("146"), literalFalse},

                {stringToLiteral.get("146"), stringToLiteral.get("1021"), literalTrue},
                {stringToLiteral.get("146"), stringToLiteral.get("409"), literalTrue},
                {stringToLiteral.get("146"), stringToLiteral.get("150"), literalTrue},
                {stringToLiteral.get("146"), stringToLiteral.get("146"), literalTrue},
        });
    }

    static Stream<Object[]> sumData() {
        return Arrays.stream(new Object[][]{
                {new Term[]{stringToLiteral.get("1021"), stringToLiteral.get("409")},
                        SameObjectTermFactory.instance().createOrGetLiteral(1430L)},
                {new Term[]{SameObjectTermFactory.instance().createOrGetLiteral("10.5"),
                        SameObjectTermFactory.instance().createOrGetLiteral("20.1")},
                        SameObjectTermFactory.instance().createOrGetLiteral(30.6)}
        });
    }

    static Stream<Object[]> minusData() {
        return Arrays.stream(new Object[][]{
                {new Term[]{stringToLiteral.get("1021"), stringToLiteral.get("409")},
                        SameObjectTermFactory.instance().createOrGetLiteral(612L)},
                {new Term[]{SameObjectTermFactory.instance().createOrGetLiteral("100.5"),
                        SameObjectTermFactory.instance().createOrGetLiteral("50.25")},
                        SameObjectTermFactory.instance().createOrGetLiteral(50.25)}
        });
    }

    static Stream<Object[]> productData() {
        return Arrays.stream(new Object[][]{
                {new Term[]{stringToLiteral.get("1021"), stringToLiteral.get("409")},
                        SameObjectTermFactory.instance().createOrGetLiteral(417589L)},
                {new Term[]{SameObjectTermFactory.instance().createOrGetLiteral("10.5"),
                        SameObjectTermFactory.instance().createOrGetLiteral("20.1")},
                        SameObjectTermFactory.instance().createOrGetLiteral(211.05)}
        });
    }

    static Stream<Object[]> divideData() {
        return Arrays.stream(new Object[][]{
                {new Term[]{stringToLiteral.get("1021"), stringToLiteral.get("409")},
                        SameObjectTermFactory.instance().createOrGetLiteral(1021.0 / 409)},
                {new Term[]{SameObjectTermFactory.instance().createOrGetLiteral("100.5"),
                        SameObjectTermFactory.instance().createOrGetLiteral("50.25")},
                        SameObjectTermFactory.instance().createOrGetLiteral(2.0)}
        });
    }

    static Stream<Object[]> medianData() {
        return Arrays.stream(new Object[][]{
                {new Term[]{stringToLiteral.get("150"), stringToLiteral.get("146"), stringToLiteral.get("1021"), stringToLiteral.get("409")}, SameObjectTermFactory.instance().createOrGetLiteral(279.5)},
                {new Term[]{stringToLiteral.get("1021"), stringToLiteral.get("409"), stringToLiteral.get("150")},
                        SameObjectTermFactory.instance().createOrGetLiteral(409.0)},
        });
    }

    @ParameterizedTest
    @MethodSource("isSmallerOrEqualsToData")
    public void isSmallerOrEqualsToTest (Term t1, Term t2, Literal<Boolean> expected) {
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SMALLER_OR_EQUALS_TO).invoke(t1, t2));

        ComputedAtomImpl a = new ComputedAtomImpl(
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SMALLER_OR_EQUALS_TO),
                SameObjectPredicateFactory.instance().createOrGetPredicate("p2", 2),
                stringToVariable.get("X"), stringToVariable.get("Y"));
        Substitution s = new SubstitutionImpl();
        s.add(stringToVariable.get("X"), t1);
        s.add(stringToVariable.get("Y"), t2);

        Assertions.assertEquals(expected.value() ? top : bottom, a.eval(s));
    }

    @ParameterizedTest
    @MethodSource("sumData")
    public void sumTest(Term[] terms, Literal<Number> expected) {
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.SUM).invoke(terms));
    }

    @ParameterizedTest
    @MethodSource("minusData")
    public void minusTest(Term[] terms, Literal<Number> expected) {
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.MINUS).invoke(terms));
    }

    @ParameterizedTest
    @MethodSource("productData")
    public void productTest(Term[] terms, Literal<Number> expected) {
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.PRODUCT).invoke(terms));
    }

    @ParameterizedTest
    @MethodSource("divideData")
    public void divideTest(Term[] terms, Literal<Double> expected) {
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.DIVIDE).invoke(terms));
    }

    @ParameterizedTest
    @MethodSource("medianData")
    public void medianTest(Term[] terms, Literal<Double> expected) {
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.MEDIAN).invoke(terms));
    }
    // Test for exception cases
    @Test
    public void testSumWithInvalidArguments() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.SUM).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("invalid"),
                    SameObjectTermFactory.instance().createOrGetLiteral("data"));
        });
    }

    @Test
    public void testMinusWithInvalidArguments() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.MINUS).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("invalid"),
                    SameObjectTermFactory.instance().createOrGetLiteral("data"));
        });
    }

    @Test
    public void testDivideByZero() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.DIVIDE).invoke(new Term[]{
                    SameObjectTermFactory.instance().createOrGetLiteral(100),
                    SameObjectTermFactory.instance().createOrGetLiteral(0)
            });
        });
    }

    @Test
    public void testEvalWithInvalidResult() {
        ComputedAtomImpl computedAtom = new ComputedAtomImpl(
                invokers.getInvoker(IntegraalInvokers.StdInvoker.SUM),
                SameObjectPredicateFactory.instance().createOrGetPredicate("p", 1),
                stringToVariable.get("X")
        );

        Substitution s = new SubstitutionImpl();
        s.add(stringToVariable.get("X"), SameObjectTermFactory.instance().createOrGetLiteral("invalid"));

        Assertions.assertThrows(RuntimeException.class, () -> computedAtom.eval(s));
    }

    @Test
    public void testSumWithNoArguments() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.SUM).invoke();
        });
    }

    @Test
    public void testMinusWithNoArguments() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.MINUS).invoke();
        });
    }
    @Test
    public void testProductWithNoArguments() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.PRODUCT).invoke();
        });
    }

    @Test
    public void testProductWithInvalidArguments() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.PRODUCT).invoke(new Term[]{
                    SameObjectTermFactory.instance().createOrGetLiteral("invalid"),
                    SameObjectTermFactory.instance().createOrGetLiteral("data")
            });
        });
    }
    @Test
    public void testDivideWithNoArguments() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.DIVIDE).invoke();
        });
    }

    @Test
    public void testDivideWithInvalidArguments() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.DIVIDE).invoke(new Term[]{
                    SameObjectTermFactory.instance().createOrGetLiteral("invalid"),
                    SameObjectTermFactory.instance().createOrGetLiteral("data")
            });
        });
    }

    private static final Literal<Double> literalDouble1 = SameObjectTermFactory.instance().createOrGetLiteral(15.5);
    private static final Literal<Double> literalDouble2 = SameObjectTermFactory.instance().createOrGetLiteral(22.1);
    private static final Literal<Double> literalDouble3 = SameObjectTermFactory.instance().createOrGetLiteral(8.3);
    private static final Literal<Integer> literalInt1 = SameObjectTermFactory.instance().createOrGetLiteral(5);
    private static final Literal<Integer> literalInt2 = SameObjectTermFactory.instance().createOrGetLiteral(10);
    private static final Literal<Integer> literalInt3 = SameObjectTermFactory.instance().createOrGetLiteral(2);

    static Stream<Object[]> maxData() {
        return Arrays.stream(new Object[][]{
                {new Term[]{literalInt1, literalInt2, literalInt3}, literalInt2}, // max is 10
                {new Term[]{literalDouble1, literalDouble2, literalDouble3}, literalDouble2}, // max is 22.1
                {new Term[]{literalInt1, literalDouble1}, literalDouble1} // max is 15.5
        });
    }

    static Stream<Object[]> minData() {
        return Arrays.stream(new Object[][]{
                {new Term[]{literalInt1, literalInt2, literalInt3}, literalInt3}, // min is 2
                {new Term[]{literalDouble1, literalDouble2, literalDouble3}, literalDouble3}, // min is 8.3
                {new Term[]{literalInt1, literalDouble1}, literalInt1} // min is 5
        });
    }

    @ParameterizedTest
    @MethodSource("maxData")
    public void maxTest(Term[] terms, Literal<Number> expected) {
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.MAX).invoke(terms));
    }

    // Tests for exceptions in max
    @Test
    public void testMaxWithNoArguments() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.MAX).invoke();
        });
    }

    @Test
    public void testMaxWithInvalidArguments() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.MAX).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("invalid"),
                    SameObjectTermFactory.instance().createOrGetLiteral("data"));
        });
    }

    // Tests for exceptions in min
    @Test
    public void testMinWithNoArguments() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.MIN).invoke();
        });
    }

    @Test
    public void testMinWithInvalidArguments() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.MIN).invoke(new Term[]{
                    SameObjectTermFactory.instance().createOrGetLiteral("invalid"),
                    SameObjectTermFactory.instance().createOrGetLiteral("data")
            });
        });
    }

    private static final Literal<Double> literalDouble4 = SameObjectTermFactory.instance().createOrGetLiteral(50.0);
    private static final Literal<Double> literalDouble5 = SameObjectTermFactory.instance().createOrGetLiteral(30.0);
    private static final Literal<Double> literalDouble6 = SameObjectTermFactory.instance().createOrGetLiteral(70.0);

    static Stream<Object[]> averageData() {
        return Arrays.stream(new Object[][]{
                {new Term[]{literalDouble1, literalDouble2}, SameObjectTermFactory.instance().createOrGetLiteral(18.8)},
                {new Term[]{literalInt1, literalInt2, literalInt3}, SameObjectTermFactory.instance().createOrGetLiteral(5.666666666666667)},
                {new Term[]{literalDouble4, literalDouble5, literalDouble6},
                        SameObjectTermFactory.instance().createOrGetLiteral(50.0)}
        });
    }

    @ParameterizedTest
    @MethodSource("averageData")
    public void averageTest(Term[] terms, Literal<Double> expected) {
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.AVERAGE).invoke(terms));
    }

    // Tests for exceptions in average
    @Test
    public void testAverageWithNoArguments() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.AVERAGE).invoke();
        });
    }

    @Test
    public void testAverageWithInvalidArguments() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.AVERAGE).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("invalid"),
                    SameObjectTermFactory.instance().createOrGetLiteral("data"));
        });
    }

    // Tests for exceptions in median
    @Test
    public void testMedianWithNoArguments() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.MEDIAN).invoke();
        });
    }

    @Test
    public void testMedianWithInvalidArguments() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.MEDIAN).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("invalid"),
                    SameObjectTermFactory.instance().createOrGetLiteral("data"));
        });
    }

    static Stream<Object[]> isEvenData() {
        return Stream.of(
                new Object[]{SameObjectTermFactory.instance().createOrGetLiteral(2), SameObjectTermFactory.instance().createOrGetLiteral(true)},
                new Object[]{SameObjectTermFactory.instance().createOrGetLiteral(3), SameObjectTermFactory.instance().createOrGetLiteral(false)}
        );
    }

    static Stream<Object[]> isOddData() {
        return Stream.of(
                new Object[]{SameObjectTermFactory.instance().createOrGetLiteral(2), SameObjectTermFactory.instance().createOrGetLiteral(false)},
                new Object[]{SameObjectTermFactory.instance().createOrGetLiteral(3), SameObjectTermFactory.instance().createOrGetLiteral(true)}
        );
    }

    // Add a data source for invalid terms
    static Stream<Object[]> invalidTermsData() {
        return Stream.of(
                new Object[]{SameObjectTermFactory.instance().createOrGetLiteral("notANumber")},
                new Object[]{stringToVariable.get("X")} // Assuming X is not defined as a literal number
        );
    }

    // Exception Test for max
    @ParameterizedTest
    @MethodSource("invalidTermsData")
    public void maxExceptionTest(Term term) {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.MAX).invoke(term);
        });
    }

    @ParameterizedTest
    @MethodSource("minData")
    public void minTest(Term[] terms, Literal<Number> expected) {
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.MIN).invoke(terms));
    }

    // Exception Test for min
    @ParameterizedTest
    @MethodSource("invalidTermsData")
    public void minExceptionTest(Term term) {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.MIN).invoke(term);
        });
    }

    @ParameterizedTest
    @MethodSource("isEvenData")
    public void isEvenTest(Term term, Literal<Boolean> expected) {
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_EVEN).invoke(term));
    }

    // Exception Test for isEven
    @ParameterizedTest
    @MethodSource("invalidTermsData")
    public void isEvenExceptionTest(Term term) {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_EVEN).invoke(term);
        });
    }

    @ParameterizedTest
    @MethodSource("isOddData")
    public void isOddTest(Term term, Literal<Boolean> expected) {
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_ODD).invoke(term));
    }

    // Exception Test for isOdd
    @ParameterizedTest
    @MethodSource("invalidTermsData")
    public void isOddExceptionTest(Term term) {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_ODD).invoke(term);
        });
    }

    @Test
    public void testIsEvenWithNoArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_EVEN).invoke();
        });
    }

    @Test
    public void testIsEvenWithInvalidArgument() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_EVEN).invoke(
                    stringToVariable.get("X") // Assuming "X" is a variable, not a literal
            );
        });
    }

    @Test
    public void testIsOddWithNoArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_ODD).invoke();
        });
    }

    @Test
    public void testIsOddWithInvalidArgument() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_ODD).invoke(
                    stringToVariable.get("X") // Assuming "X" is a variable, not a literal
            );
        });
    }

    static Stream<Object[]> isGreaterThanData() {
        return Stream.of(
                new Object[]{SameObjectTermFactory.instance().createOrGetLiteral(5), SameObjectTermFactory.instance().createOrGetLiteral(3), SameObjectTermFactory.instance().createOrGetLiteral(true)},
                new Object[]{SameObjectTermFactory.instance().createOrGetLiteral(3), SameObjectTermFactory.instance().createOrGetLiteral(5), SameObjectTermFactory.instance().createOrGetLiteral(false)},
                new Object[]{SameObjectTermFactory.instance().createOrGetLiteral(5), SameObjectTermFactory.instance().createOrGetLiteral(5), SameObjectTermFactory.instance().createOrGetLiteral(false)},
                new Object[]{SameObjectTermFactory.instance().createOrGetLiteral(7.5), SameObjectTermFactory.instance().createOrGetLiteral(2.5), SameObjectTermFactory.instance().createOrGetLiteral(true)}
        );
    }

    @ParameterizedTest
    @MethodSource("isGreaterThanData")
    public void isGreaterThanTest(Term t1, Term t2, Literal<Boolean> expected) {
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_GREATER_THAN).invoke(t1, t2));
    }

    @Test
    public void testIsGreaterThanWithNoArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_GREATER_THAN).invoke();
        });
    }

    @Test
    public void testIsGreaterThanWithOneArgument() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_GREATER_THAN).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(5)
            );
        });
    }

    @Test
    public void testIsGreaterThanWithInvalidArgument() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_GREATER_THAN).invoke(
                    stringToVariable.get("X"), // Assuming "X" is a variable, not a literal
                    SameObjectTermFactory.instance().createOrGetLiteral(5)
            );
        });
    }

    @Test
    public void testIsGreaterThanWithNonNumericLiteral() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_GREATER_THAN).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("notANumber"), // Non-numeric literal
                    SameObjectTermFactory.instance().createOrGetLiteral(5)
            );
        });
    }

    @Test
    public void testIsGreaterThanWithNonLiteralSecondArgument() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_GREATER_THAN).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(10), // First argument is a valid literal
                    stringToVariable.get("X") // Second argument is a variable, not a literal
            );
        });
    }

    static Stream<Object[]> isGreaterOrEqualsToData() {
        return Stream.of(
                new Object[]{SameObjectTermFactory.instance().createOrGetLiteral(10), SameObjectTermFactory.instance().createOrGetLiteral(5), SameObjectTermFactory.instance().createOrGetLiteral(true)},
                new Object[]{SameObjectTermFactory.instance().createOrGetLiteral(5), SameObjectTermFactory.instance().createOrGetLiteral(5), SameObjectTermFactory.instance().createOrGetLiteral(true)},
                new Object[]{SameObjectTermFactory.instance().createOrGetLiteral(5), SameObjectTermFactory.instance().createOrGetLiteral(10), SameObjectTermFactory.instance().createOrGetLiteral(false)}
        );
    }

    @ParameterizedTest
    @MethodSource("isGreaterOrEqualsToData")
    public void isGreaterOrEqualsToTest(Term term1, Term term2, Literal<Boolean> expected) {
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_GREATER_OR_EQUALS_TO).invoke(term1, term2));
    }

    @Test
    public void testIsGreaterOrEqualsToWithNonLiteralArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_GREATER_OR_EQUALS_TO).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(10), // First argument is valid
                    stringToVariable.get("X") // Second argument is not a literal
            );
        });
    }

    @Test
    public void testIsGreaterOrEqualsToWithNonNumberLiterals() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_GREATER_OR_EQUALS_TO).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("non-number"), // Invalid number literal
                    SameObjectTermFactory.instance().createOrGetLiteral(5)
            );
        });

        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_GREATER_OR_EQUALS_TO).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(10),
                    SameObjectTermFactory.instance().createOrGetLiteral("non-number") // Invalid number literal
            );
        });
    }

    @Test
    public void testIsGreaterOrEqualsToWithNonLiteralFirstArgument() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_GREATER_OR_EQUALS_TO).invoke(
                    stringToVariable.get("X"), // First argument is not a literal
                    SameObjectTermFactory.instance().createOrGetLiteral(10) // Second argument is valid
            );
        });
    }

    @Test
    public void testIsGreaterOrEqualsToWithTooFewArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_GREATER_OR_EQUALS_TO).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(10) // Only one argument
            );
        });
    }

    @Test
    public void testIsGreaterOrEqualsToWithTooManyArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_GREATER_OR_EQUALS_TO).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(10),
                    SameObjectTermFactory.instance().createOrGetLiteral(5),
                    SameObjectTermFactory.instance().createOrGetLiteral(1) // Three arguments
            );
        });
    }

    @Test
    public void testIsSmallerThanWithValidArguments() {
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral(5);
        Term t2 = SameObjectTermFactory.instance().createOrGetLiteral(10);

        Literal<Boolean> expected = SameObjectTermFactory.instance().createOrGetLiteral(true);
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SMALLER_THAN).invoke(t1, t2));
    }

    @Test
    public void testIsSmallerThanWithEqualNumbers() {
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral(10);
        Term t2 = SameObjectTermFactory.instance().createOrGetLiteral(10);

        Literal<Boolean> expected = SameObjectTermFactory.instance().createOrGetLiteral(false);
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SMALLER_THAN).invoke(t1, t2));
    }

    @Test
    public void testIsSmallerThanWithTooFewArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SMALLER_THAN).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(5) // Only one argument
            );
        });
    }

    @Test
    public void testIsSmallerThanWithTooManyArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SMALLER_THAN).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(5),
                    SameObjectTermFactory.instance().createOrGetLiteral(10),
                    SameObjectTermFactory.instance().createOrGetLiteral(1) // Three arguments
            );
        });
    }

    @Test
    public void testIsSmallerThanWithNonLiteralFirstArgument() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SMALLER_THAN).invoke(
                    stringToVariable.get("X"), // Non-literal argument
                    SameObjectTermFactory.instance().createOrGetLiteral(10)
            );
        });
    }

    @Test
    public void testIsSmallerThanWithNonLiteralSecondArgument() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SMALLER_THAN).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(5),
                    stringToVariable.get("Y") // Non-literal argument
            );
        });
    }

    @Test
    public void testIsSmallerThanWithNonNumberLiterals() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SMALLER_THAN).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("stringLiteral"),
                    SameObjectTermFactory.instance().createOrGetLiteral(10)
            );
        });
    }

    @Test
    public void testIsSmallerOrEqualsToWithValidArguments() {
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral(5);
        Term t2 = SameObjectTermFactory.instance().createOrGetLiteral(10);

        Literal<Boolean> expected = SameObjectTermFactory.instance().createOrGetLiteral(true);
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SMALLER_OR_EQUALS_TO).invoke(t1, t2));
    }

    @Test
    public void testIsSmallerOrEqualsToWithEqualNumbers() {
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral(10);
        Term t2 = SameObjectTermFactory.instance().createOrGetLiteral(10);

        Literal<Boolean> expected = SameObjectTermFactory.instance().createOrGetLiteral(true);
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SMALLER_OR_EQUALS_TO).invoke(t1, t2));
    }

    @Test
    public void testIsSmallerOrEqualsToWithTooFewArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SMALLER_OR_EQUALS_TO).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(5) // Only one argument
            );
        });
    }

    @Test
    public void testIsSmallerOrEqualsToWithTooManyArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SMALLER_OR_EQUALS_TO).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(5),
                    SameObjectTermFactory.instance().createOrGetLiteral(10),
                    SameObjectTermFactory.instance().createOrGetLiteral(1) // Three arguments
            );
        });
    }

    @Test
    public void testIsSmallerOrEqualsToWithNonLiteralFirstArgument() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SMALLER_OR_EQUALS_TO).invoke(
                    stringToVariable.get("X"), // Non-literal argument
                    SameObjectTermFactory.instance().createOrGetLiteral(10)
            );
        });
    }

    @Test
    public void testIsSmallerOrEqualsToWithNonLiteralSecondArgument() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SMALLER_OR_EQUALS_TO).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(5),
                    stringToVariable.get("Y") // Non-literal argument
            );
        });
    }

    @Test
    public void testIsSmallerOrEqualsToWithNonNumberLiterals() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SMALLER_OR_EQUALS_TO).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("stringLiteral"),
                    SameObjectTermFactory.instance().createOrGetLiteral(10)
            );
        });
    }

    @Test
    public void testIsLexicographicallyGreaterThanWithValidStrings_FirstGreater() {
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral("zebra");
        Term t2 = SameObjectTermFactory.instance().createOrGetLiteral("apple");

        Literal<Boolean> expected = SameObjectTermFactory.instance().createOrGetLiteral(true);
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_GREATER_THAN).invoke(t1, t2));
    }

    @Test
    public void testIsLexicographicallyGreaterThanWithEqualStrings() {
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral("apple");
        Term t2 = SameObjectTermFactory.instance().createOrGetLiteral("apple");

        Literal<Boolean> expected = SameObjectTermFactory.instance().createOrGetLiteral(false);
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_GREATER_THAN).invoke(t1, t2));
    }

    @Test
    public void testIsLexicographicallyGreaterThanWithValidStrings_FirstSmaller() {
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral("apple");
        Term t2 = SameObjectTermFactory.instance().createOrGetLiteral("zebra");

        Literal<Boolean> expected = SameObjectTermFactory.instance().createOrGetLiteral(false);
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_GREATER_THAN).invoke(t1, t2));
    }

    @Test
    public void testIsLexicographicallyGreaterThanWithTooFewArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_GREATER_THAN).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("zebra") // Only one argument
            );
        });
    }

    @Test
    public void testIsLexicographicallyGreaterThanWithTooManyArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_GREATER_THAN).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("zebra"),
                    SameObjectTermFactory.instance().createOrGetLiteral("apple"),
                    SameObjectTermFactory.instance().createOrGetLiteral("extra") // Three arguments
            );
        });
    }

    @Test
    public void testIsLexicographicallyGreaterThanWithNonLiteralFirstArgument() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_GREATER_THAN).invoke(
                    stringToVariable.get("X"), // Non-literal argument
                    SameObjectTermFactory.instance().createOrGetLiteral("apple")
            );
        });
    }

    @Test
    public void testIsLexicographicallyGreaterThanWithNonLiteralSecondArgument() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_GREATER_THAN).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("zebra"),
                    stringToVariable.get("Y") // Non-literal argument
            );
        });
    }

    @Test
    public void testIsLexicographicallyGreaterThanWithNonStringLiterals() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_GREATER_THAN).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(123), // Non-string literal
                    SameObjectTermFactory.instance().createOrGetLiteral("apple")
            );
        });
    }

    @Test
    public void testIsLexicographicallyGreaterOrEqualsToWithFirstStringGreater() {
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral("zebra");
        Term t2 = SameObjectTermFactory.instance().createOrGetLiteral("apple");

        Literal<Boolean> expected = SameObjectTermFactory.instance().createOrGetLiteral(true);
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_GREATER_OR_EQUALS_TO).invoke(t1, t2));
    }

    @Test
    public void testIsLexicographicallyGreaterOrEqualsToWithEqualStrings() {
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral("apple");
        Term t2 = SameObjectTermFactory.instance().createOrGetLiteral("apple");

        Literal<Boolean> expected = SameObjectTermFactory.instance().createOrGetLiteral(true);
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_GREATER_OR_EQUALS_TO).invoke(t1, t2));
    }

    @Test
    public void testIsLexicographicallyGreaterOrEqualsToWithFirstStringSmaller() {
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral("apple");
        Term t2 = SameObjectTermFactory.instance().createOrGetLiteral("zebra");

        Literal<Boolean> expected = SameObjectTermFactory.instance().createOrGetLiteral(false);
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_GREATER_OR_EQUALS_TO).invoke(t1, t2));
    }

    @Test
    public void testIsLexicographicallyGreaterOrEqualsToWithTooFewArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_GREATER_OR_EQUALS_TO).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("zebra") // Only one argument
            );
        });
    }

    @Test
    public void testIsLexicographicallyGreaterOrEqualsToWithTooManyArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_GREATER_OR_EQUALS_TO).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("zebra"),
                    SameObjectTermFactory.instance().createOrGetLiteral("apple"),
                    SameObjectTermFactory.instance().createOrGetLiteral("extra") // Three arguments
            );
        });
    }

    @Test
    public void testIsLexicographicallyGreaterOrEqualsToWithNonLiteralFirstArgument() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_GREATER_OR_EQUALS_TO).invoke(
                    stringToVariable.get("X"), // Non-literal argument
                    SameObjectTermFactory.instance().createOrGetLiteral("apple")
            );
        });
    }

    @Test
    public void testIsLexicographicallyGreaterOrEqualsToWithNonLiteralSecondArgument() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_GREATER_OR_EQUALS_TO).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("zebra"),
                    stringToVariable.get("Y") // Non-literal argument
            );
        });
    }

    @Test
    public void testIsLexicographicallyGreaterOrEqualsToWithNonStringLiterals() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_GREATER_OR_EQUALS_TO).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(123), // Non-string literal
                    SameObjectTermFactory.instance().createOrGetLiteral("apple")
            );
        });
    }

    @Test
    public void testIsLexicographicallySmallerThanWithFirstStringSmaller() {
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral("apple");
        Term t2 = SameObjectTermFactory.instance().createOrGetLiteral("zebra");

        Literal<Boolean> expected = SameObjectTermFactory.instance().createOrGetLiteral(true);
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_SMALLER_THAN).invoke(t1, t2));
    }

    @Test
    public void testIsLexicographicallySmallerThanWithFirstStringGreater() {
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral("zebra");
        Term t2 = SameObjectTermFactory.instance().createOrGetLiteral("apple");

        Literal<Boolean> expected = SameObjectTermFactory.instance().createOrGetLiteral(false);
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_SMALLER_THAN).invoke(t1, t2));
    }

    @Test
    public void testIsLexicographicallySmallerThanWithTooFewArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_SMALLER_THAN).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("apple") // Only one argument
            );
        });
    }

    @Test
    public void testIsLexicographicallySmallerThanWithTooManyArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_SMALLER_THAN).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("apple"),
                    SameObjectTermFactory.instance().createOrGetLiteral("zebra"),
                    SameObjectTermFactory.instance().createOrGetLiteral("extra") // Three arguments
            );
        });
    }

    @Test
    public void testIsLexicographicallySmallerThanWithNonLiteralFirstArgument() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_SMALLER_THAN).invoke(
                    stringToVariable.get("X"), // Non-literal argument
                    SameObjectTermFactory.instance().createOrGetLiteral("zebra")
            );
        });
    }

    @Test
    public void testIsLexicographicallySmallerThanWithNonLiteralSecondArgument() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_SMALLER_THAN).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("apple"),
                    stringToVariable.get("Y") // Non-literal argument
            );
        });
    }

    @Test
    public void testIsLexicographicallySmallerThanWithNonStringLiterals() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_SMALLER_THAN).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(123), // Non-string literal
                    SameObjectTermFactory.instance().createOrGetLiteral("apple")
            );
        });
    }

    @Test
    public void testIsLexicographicallySmallerOrEqualsToWithFirstStringSmaller() {
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral("apple");
        Term t2 = SameObjectTermFactory.instance().createOrGetLiteral("zebra");

        Literal<Boolean> expected = SameObjectTermFactory.instance().createOrGetLiteral(true);
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_SMALLER_OR_EQUALS_TO).invoke(t1, t2));
    }

    @Test
    public void testIsLexicographicallySmallerOrEqualsToWithEqualStrings() {
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral("apple");
        Term t2 = SameObjectTermFactory.instance().createOrGetLiteral("apple");

        Literal<Boolean> expected = SameObjectTermFactory.instance().createOrGetLiteral(true);
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_SMALLER_OR_EQUALS_TO).invoke(t1, t2));
    }

    @Test
    public void testIsLexicographicallySmallerOrEqualsToWithFirstStringGreater() {
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral("zebra");
        Term t2 = SameObjectTermFactory.instance().createOrGetLiteral("apple");

        Literal<Boolean> expected = SameObjectTermFactory.instance().createOrGetLiteral(false);
        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_SMALLER_OR_EQUALS_TO).invoke(t1, t2));
    }

    @Test
    public void testIsLexicographicallySmallerOrEqualsToWithTooFewArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_SMALLER_OR_EQUALS_TO).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("apple") // Only one argument
            );
        });
    }

    @Test
    public void testIsLexicographicallySmallerOrEqualsToWithTooManyArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_SMALLER_OR_EQUALS_TO).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("apple"),
                    SameObjectTermFactory.instance().createOrGetLiteral("zebra"),
                    SameObjectTermFactory.instance().createOrGetLiteral("extra") // Three arguments
            );
        });
    }

    @Test
    public void testIsLexicographicallySmallerOrEqualsToWithNonLiteralFirstArgument() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_SMALLER_OR_EQUALS_TO).invoke(
                    stringToVariable.get("X"), // Non-literal argument
                    SameObjectTermFactory.instance().createOrGetLiteral("zebra")
            );
        });
    }

    @Test
    public void testIsLexicographicallySmallerOrEqualsToWithNonLiteralSecondArgument() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_SMALLER_OR_EQUALS_TO).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("apple"),
                    stringToVariable.get("Y") // Non-literal argument
            );
        });
    }

    @Test
    public void testIsLexicographicallySmallerOrEqualsToWithNonStringLiterals() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_LEXICOGRAPHICALLY_SMALLER_OR_EQUALS_TO).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(123), // Non-string literal
                    SameObjectTermFactory.instance().createOrGetLiteral("apple")
            );
        });
    }

    @Test
    public void testIsPrimeWithPrimeNumbers() {
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral(2);
        Term t2 = SameObjectTermFactory.instance().createOrGetLiteral(11);
        Term t3 = SameObjectTermFactory.instance().createOrGetLiteral(13);

        Literal<Boolean> expectedTrue = SameObjectTermFactory.instance().createOrGetLiteral(true);

        Assertions.assertEquals(expectedTrue, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_PRIME).invoke(t1));
        Assertions.assertEquals(expectedTrue, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_PRIME).invoke(t2));
        Assertions.assertEquals(expectedTrue, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_PRIME).invoke(t3));
    }

    @Test
    public void testIsPrimeWithNonPrimeNumbers() {
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral(4);
        Term t2 = SameObjectTermFactory.instance().createOrGetLiteral(15);
        Term t3 = SameObjectTermFactory.instance().createOrGetLiteral(100);

        Literal<Boolean> expectedFalse = SameObjectTermFactory.instance().createOrGetLiteral(false);

        Assertions.assertEquals(expectedFalse, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_PRIME).invoke(t1));
        Assertions.assertEquals(expectedFalse, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_PRIME).invoke(t2));
        Assertions.assertEquals(expectedFalse, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_PRIME).invoke(t3));
    }

    @Test
    public void testIsPrimeWithEdgeCases() {
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral(-1);
        Term t2 = SameObjectTermFactory.instance().createOrGetLiteral(0);
        Term t3 = SameObjectTermFactory.instance().createOrGetLiteral(1);

        Literal<Boolean> expectedFalse = SameObjectTermFactory.instance().createOrGetLiteral(false);

        Assertions.assertEquals(expectedFalse, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_PRIME).invoke(t1));
        Assertions.assertEquals(expectedFalse, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_PRIME).invoke(t2));
        Assertions.assertEquals(expectedFalse, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_PRIME).invoke(t3));
    }

    @Test
    public void testIsPrimeWithNonIntegerArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_PRIME).invoke(SameObjectTermFactory.instance().createOrGetLiteral(2.5));
        });
    }

    @Test
    public void testIsPrimeWithNonLiteralArgument() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_PRIME).invoke(stringToVariable.get("X"));
        });
    }

    @Test
    public void testIsPrimeWithTooFewOrTooManyArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_PRIME).invoke();
        });

        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_PRIME).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(2),
                    SameObjectTermFactory.instance().createOrGetLiteral(3)
            );
        });
    }

    @Test
    public void testIsPrimeWithLargerNonPrimes() {
        // 25 is divisible by 5 (i in the loop)
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral(25);
        // 49 is divisible by 7 (i + 2 in the loop)
        Term t2 = SameObjectTermFactory.instance().createOrGetLiteral(49);
        // 121 is divisible by 11 (i in the loop)
        Term t3 = SameObjectTermFactory.instance().createOrGetLiteral(121);

        Literal<Boolean> expectedFalse = SameObjectTermFactory.instance().createOrGetLiteral(false);

        Assertions.assertEquals(expectedFalse, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_PRIME).invoke(t1));
        Assertions.assertEquals(expectedFalse, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_PRIME).invoke(t2));
        Assertions.assertEquals(expectedFalse, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_PRIME).invoke(t3));
    }

    @Test
    public void testEqualsWithLessThanTwoArguments() {
        // Should throw an exception because fewer than two arguments are passed
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.EQUALS).invoke(SameObjectTermFactory.instance().createOrGetLiteral("singleTerm"));
        });
    }

    @Test
    public void testEqualsWithAllEqualArguments() {
        // Should return true because all terms are equal
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral("sameTerm");
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.EQUALS).invoke(t1, t1, t1));
    }

    @Test
    public void testEqualsWithNotAllEqualArguments() {
        // Should return false because terms are not all equal
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral("term1");
        Term t2 = SameObjectTermFactory.instance().createOrGetLiteral("term2");
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.EQUALS).invoke(t1, t2));
    }

    @Test
    public void testEqualsWithMixedArguments() {
        // Should return false because terms are of different types
        Term t1 = SameObjectTermFactory.instance().createOrGetLiteral("stringTerm");
        Term t2 = SameObjectTermFactory.instance().createOrGetLiteral(123);
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.EQUALS).invoke(t1, t2));
    }

    @Test
    public void testConcatWithStrings() {
        Term string1 = SameObjectTermFactory.instance().createOrGetLiteral("Hello, ");
        Term string2 = SameObjectTermFactory.instance().createOrGetLiteral("World!");

        Literal<?> result = (Literal<?>) invokers.getInvoker(IntegraalInvokers.StdInvoker.CONCAT).invoke(string1, string2);
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral("Hello, World!"), result);
    }

    @Test
    public void testConcatWithLists() {
        Term list1 = SameObjectTermFactory.instance().createOrGetLiteral(List.of(1, 2, 3));
        Term list2 = SameObjectTermFactory.instance().createOrGetLiteral(List.of(4, 5, 6));

        Literal<?> result = (Literal<?>) invokers.getInvoker(IntegraalInvokers.StdInvoker.CONCAT).invoke(list1, list2);
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(List.of(1, 2, 3, 4, 5, 6)), result);
    }

    @Test
    public void testConcatWithInvalidTypes() {
        Term string1 = SameObjectTermFactory.instance().createOrGetLiteral("Hello, ");
        Term list1 = SameObjectTermFactory.instance().createOrGetLiteral(List.of(1, 2, 3));

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.CONCAT).invoke(string1, list1);
        });
    }

    @Test
    public void testConcatWithInvalidArgumentLength() {
        Term string1 = SameObjectTermFactory.instance().createOrGetLiteral("Hello");

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.CONCAT).invoke(string1);
        });
    }

    @Test
    public void testConcatWithUnsupportedTypes() {
        // Passing an unsupported type such as a number to the concat method
        Term numberTerm = SameObjectTermFactory.instance().createOrGetLiteral(123);
        Term string1 = SameObjectTermFactory.instance().createOrGetLiteral("Hello");

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.CONCAT).invoke(numberTerm, string1);
        });
    }

    @Test
    public void testConcatWithNonLiteralArguments() {
        Term nonLiteralTerm = stringToVariable.get("X"); // Using a variable instead of a literal to force non-literal case
        Term literalString = SameObjectTermFactory.instance().createOrGetLiteral("Hello");

        // Should throw an exception because the first argument is not a literal
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.CONCAT).invoke(nonLiteralTerm, literalString);
        });

        // Should throw an exception because the second argument is not a literal
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.CONCAT).invoke(literalString, nonLiteralTerm);
        });
    }

    @Test
    public void testConcatWithListArguments() {
        Term list1 = SameObjectTermFactory.instance().createOrGetLiteral(List.of(1, 2, 3));
        Term list2 = SameObjectTermFactory.instance().createOrGetLiteral(List.of(4, 5, 6));

        // This should cover the branch where both value1 and value2 are lists
        Literal<?> result = (Literal<?>) invokers.getInvoker(IntegraalInvokers.StdInvoker.CONCAT).invoke(list1, list2);
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(List.of(1, 2, 3, 4, 5, 6)), result);
    }

    @Test
    public void testConcatWithFirstListSecondNonList() {
        Term list1 = SameObjectTermFactory.instance().createOrGetLiteral(List.of(1, 2, 3));
        Term nonListLiteral = SameObjectTermFactory.instance().createOrGetLiteral("Not a list");

        // Should throw an exception because the second argument is not a list, but the first one is
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.CONCAT).invoke(list1, nonListLiteral);
        });
    }

    @Test
    public void testConcatWithSecondListFirstNonList() {
        Term nonListLiteral = SameObjectTermFactory.instance().createOrGetLiteral("Not a list");
        Term list2 = SameObjectTermFactory.instance().createOrGetLiteral(List.of(4, 5, 6));

        // Should throw an exception because the first argument is not a list, but the second one is
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.CONCAT).invoke(nonListLiteral, list2);
        });
    }

    @Test
    public void testToLowerCaseValidArgument() {
        // Test with a valid string literal
        Literal<String> input = SameObjectTermFactory.instance().createOrGetLiteral("HELLO");
        Literal<String> expected = SameObjectTermFactory.instance().createOrGetLiteral("hello");

        Literal<String> result = (Literal<String>) invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_LOWER_CASE).invoke(input);

        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testToLowerCaseWithEmptyString() {
        // Test with an empty string literal
        Literal<String> input = SameObjectTermFactory.instance().createOrGetLiteral("");
        Literal<String> expected = SameObjectTermFactory.instance().createOrGetLiteral("");

        Literal<String> result = (Literal<String>) invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_LOWER_CASE).invoke(input);

        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testToLowerCaseWithNonLiteralArgument() {
        // Test with a non-literal argument (e.g., a variable) to trigger exception
        Term nonLiteralTerm = stringToVariable.get("X");  // Assuming X is not a literal

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_LOWER_CASE).invoke(nonLiteralTerm);
        });
    }

    @Test
    public void testToLowerCaseWithNonStringLiteral() {
        // Test with a literal that is not a string (e.g., a number)
        Literal<Integer> input = SameObjectTermFactory.instance().createOrGetLiteral(123);

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_LOWER_CASE).invoke(input);
        });
    }

    @Test
    public void testToLowerCaseWithNoArguments() {
        // Test with no arguments to trigger exception
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_LOWER_CASE).invoke();
        });
    }

    @Test
    public void testToLowerCaseWithTooManyArguments() {
        // Test with more than one argument to trigger exception
        Literal<String> input1 = SameObjectTermFactory.instance().createOrGetLiteral("HELLO");
        Literal<String> input2 = SameObjectTermFactory.instance().createOrGetLiteral("WORLD");

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_LOWER_CASE).invoke(input1, input2);
        });
    }

    @Test
    public void testToUpperCaseValidArgument() {
        // Test with a valid string literal
        Literal<String> input = SameObjectTermFactory.instance().createOrGetLiteral("hello");
        Literal<String> expected = SameObjectTermFactory.instance().createOrGetLiteral("HELLO");

        Literal<String> result = (Literal<String>) invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_UPPER_CASE).invoke(input);

        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testToUpperCaseWithEmptyString() {
        // Test with an empty string literal
        Literal<String> input = SameObjectTermFactory.instance().createOrGetLiteral("");
        Literal<String> expected = SameObjectTermFactory.instance().createOrGetLiteral("");

        Literal<String> result = (Literal<String>) invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_UPPER_CASE).invoke(input);

        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testToUpperCaseWithNonLiteralArgument() {
        // Test with a non-literal argument (e.g., a variable) to trigger exception
        Term nonLiteralTerm = stringToVariable.get("X");  // Assuming X is not a literal

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_UPPER_CASE).invoke(nonLiteralTerm);
        });
    }

    @Test
    public void testToUpperCaseWithNonStringLiteral() {
        // Test with a literal that is not a string (e.g., a number)
        Literal<Integer> input = SameObjectTermFactory.instance().createOrGetLiteral(123);

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_UPPER_CASE).invoke(input);
        });
    }

    @Test
    public void testToUpperCaseWithNoArguments() {
        // Test with no arguments to trigger exception
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_UPPER_CASE).invoke();
        });
    }

    @Test
    public void testToUpperCaseWithTooManyArguments() {
        // Test with more than one argument to trigger exception
        Literal<String> input1 = SameObjectTermFactory.instance().createOrGetLiteral("hello");
        Literal<String> input2 = SameObjectTermFactory.instance().createOrGetLiteral("world");

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_UPPER_CASE).invoke(input1, input2);
        });
    }

    @Test
    public void testReplaceValidArguments() {
        // Test with valid string replacement
        Literal<String> target = SameObjectTermFactory.instance().createOrGetLiteral("hello world");
        Literal<String> oldStr = SameObjectTermFactory.instance().createOrGetLiteral("world");
        Literal<String> newStr = SameObjectTermFactory.instance().createOrGetLiteral("there");

        Literal<String> expected = SameObjectTermFactory.instance().createOrGetLiteral("hello there");
        Term result = invokers.getInvoker(IntegraalInvokers.StdInvoker.REPLACE).invoke(target, oldStr, newStr);

        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testReplaceWithEmptyStrings() {
        // Test replacing an empty string with another string
        Literal<String> target = SameObjectTermFactory.instance().createOrGetLiteral("hello");
        Literal<String> oldStr = SameObjectTermFactory.instance().createOrGetLiteral("");
        Literal<String> newStr = SameObjectTermFactory.instance().createOrGetLiteral("X");

        Literal<String> expected = SameObjectTermFactory.instance().createOrGetLiteral("XhXeXlXlXoX");
        Term result = invokers.getInvoker(IntegraalInvokers.StdInvoker.REPLACE).invoke(target, oldStr, newStr);

        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testReplaceWithNoReplacement() {
        // Test replacing a string that is not found in the target
        Literal<String> target = SameObjectTermFactory.instance().createOrGetLiteral("hello world");
        Literal<String> oldStr = SameObjectTermFactory.instance().createOrGetLiteral("earth");
        Literal<String> newStr = SameObjectTermFactory.instance().createOrGetLiteral("planet");

        Literal<String> expected = SameObjectTermFactory.instance().createOrGetLiteral("hello world"); // no replacement
        Term result = invokers.getInvoker(IntegraalInvokers.StdInvoker.REPLACE).invoke(target, oldStr, newStr);

        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testReplaceWithNonStringArguments() {
        // Test with non-string arguments (e.g., numbers) to trigger exception
        Literal<Integer> target = SameObjectTermFactory.instance().createOrGetLiteral(123);
        Literal<String> oldStr = SameObjectTermFactory.instance().createOrGetLiteral("2");
        Literal<String> newStr = SameObjectTermFactory.instance().createOrGetLiteral("9");

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.REPLACE).invoke(target, oldStr, newStr);
        });
    }

    @Test
    public void testReplaceWithNoArguments() {
        // Test with no arguments to trigger exception
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.REPLACE).invoke();
        });
    }

    @Test
    public void testReplaceWithTooFewArguments() {
        // Test with fewer than three arguments to trigger exception
        Literal<String> target = SameObjectTermFactory.instance().createOrGetLiteral("hello world");
        Literal<String> oldStr = SameObjectTermFactory.instance().createOrGetLiteral("world");

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.REPLACE).invoke(target, oldStr);
        });
    }

    @Test
    public void testReplaceWithTooManyArguments() {
        // Test with more than three arguments to trigger exception
        Literal<String> target = SameObjectTermFactory.instance().createOrGetLiteral("hello world");
        Literal<String> oldStr = SameObjectTermFactory.instance().createOrGetLiteral("world");
        Literal<String> newStr = SameObjectTermFactory.instance().createOrGetLiteral("there");
        Literal<String> extraArg = SameObjectTermFactory.instance().createOrGetLiteral("extra");

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.REPLACE).invoke(target, oldStr, newStr, extraArg);
        });
    }

    @Test
    public void testReplaceWithNonLiteralFirstArgument() {
        // Test case where the first argument is not a literal
        Variable nonLiteralArg = SameObjectTermFactory.instance().createOrGetVariable("X");
        Literal<String> oldStr = SameObjectTermFactory.instance().createOrGetLiteral("world");
        Literal<String> newStr = SameObjectTermFactory.instance().createOrGetLiteral("there");

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.REPLACE).invoke(nonLiteralArg, oldStr, newStr);
        });
    }

    @Test
    public void testReplaceWithNonLiteralSecondArgument() {
        // Test case where the second argument is not a literal
        Literal<String> target = SameObjectTermFactory.instance().createOrGetLiteral("hello world");
        Variable nonLiteralArg = SameObjectTermFactory.instance().createOrGetVariable("Y");
        Literal<String> newStr = SameObjectTermFactory.instance().createOrGetLiteral("there");

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.REPLACE).invoke(target, nonLiteralArg, newStr);
        });
    }

    @Test
    public void testReplaceWithNonLiteralThirdArgument() {
        // Test case where the third argument is not a literal
        Literal<String> target = SameObjectTermFactory.instance().createOrGetLiteral("hello world");
        Literal<String> oldStr = SameObjectTermFactory.instance().createOrGetLiteral("world");
        Variable nonLiteralArg = SameObjectTermFactory.instance().createOrGetVariable("Z");

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.REPLACE).invoke(target, oldStr, nonLiteralArg);
        });
    }

    @Test
    public void testLengthWithValidString() {
        // Test case where a valid string is passed
        Literal<String> validString = SameObjectTermFactory.instance().createOrGetLiteral("hello world");

        Literal<Integer> expectedLength = SameObjectTermFactory.instance().createOrGetLiteral(11); // "hello world" has 11 characters

        Assertions.assertEquals(expectedLength, invokers.getInvoker(IntegraalInvokers.StdInvoker.LENGTH).invoke(validString));
    }

    @Test
    public void testLengthWithEmptyString() {
        // Test case where an empty string is passed
        Literal<String> emptyString = SameObjectTermFactory.instance().createOrGetLiteral("");

        Literal<Integer> expectedLength = SameObjectTermFactory.instance().createOrGetLiteral(0); // Empty string has length 0

        Assertions.assertEquals(expectedLength, invokers.getInvoker(IntegraalInvokers.StdInvoker.LENGTH).invoke(emptyString));
    }

    @Test
    public void testLengthWithNonLiteralArgument() {
        // Test case where the argument is not a literal
        Variable nonLiteralArg = SameObjectTermFactory.instance().createOrGetVariable("X");

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.LENGTH).invoke(nonLiteralArg);
        });
    }

    @Test
    public void testLengthWithNonStringLiteral() {
        // Test case where the literal is not a string
        Literal<Integer> nonStringLiteral = SameObjectTermFactory.instance().createOrGetLiteral(12345);

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.LENGTH).invoke(nonStringLiteral);
        });
    }

    @Test
    public void testLengthWithNoArguments() {
        // Test case where no arguments are passed
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.LENGTH).invoke();
        });
    }

    @Test
    public void testWeightedAverageWithValidTuples() {
        // Test case with tuples of (value, weight) represented as Lists
        Literal<?> tuple1 = SameObjectTermFactory.instance().createOrGetLiteral(
                List.of(SameObjectTermFactory.instance().createOrGetLiteral(3),
                        SameObjectTermFactory.instance().createOrGetLiteral(1))
        );
        Literal<?> tuple2 = SameObjectTermFactory.instance().createOrGetLiteral(
                List.of(SameObjectTermFactory.instance().createOrGetLiteral(6),
                        SameObjectTermFactory.instance().createOrGetLiteral(3))
        );

        Literal<Double> expected = SameObjectTermFactory.instance().createOrGetLiteral(5.25);

        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_AVERAGE).invoke(tuple1, tuple2));
    }

    @Test
    public void testWeightedAverageWithAlternatingArguments() {
        // Test case with alternating value and weight literals
        Literal<?> value1 = SameObjectTermFactory.instance().createOrGetLiteral(3);
        Literal<?> weight1 = SameObjectTermFactory.instance().createOrGetLiteral(1);
        Literal<?> value2 = SameObjectTermFactory.instance().createOrGetLiteral(6);
        Literal<?> weight2 = SameObjectTermFactory.instance().createOrGetLiteral(3);

        Literal<Double> expected = SameObjectTermFactory.instance().createOrGetLiteral(5.25);

        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_AVERAGE).invoke(value1, weight1, value2, weight2));
    }

    @Test
    public void testWeightedAverageWithCollectionOfTuples() {
        // Test case with a collection of tuples
        Literal<?> tuple1 = SameObjectTermFactory.instance().createOrGetLiteral(
                List.of(SameObjectTermFactory.instance().createOrGetLiteral(3),
                        SameObjectTermFactory.instance().createOrGetLiteral(1))
        );
        Literal<?> tuple2 = SameObjectTermFactory.instance().createOrGetLiteral(
                List.of(SameObjectTermFactory.instance().createOrGetLiteral(6),
                        SameObjectTermFactory.instance().createOrGetLiteral(3))
        );

        Literal<?> collection = SameObjectTermFactory.instance().createOrGetLiteral(List.of(tuple1, tuple2));

        Literal<Double> expected = SameObjectTermFactory.instance().createOrGetLiteral(5.25);

        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_AVERAGE).invoke(collection));
    }

    @Test
    public void testWeightedAverageWithZeroWeight() {
        // Test case where a weight is zero
        Literal<?> value1 = SameObjectTermFactory.instance().createOrGetLiteral(3);
        Literal<?> weight1 = SameObjectTermFactory.instance().createOrGetLiteral(0);
        Literal<?> value2 = SameObjectTermFactory.instance().createOrGetLiteral(6);
        Literal<?> weight2 = SameObjectTermFactory.instance().createOrGetLiteral(3);

        Literal<Double> expected = SameObjectTermFactory.instance().createOrGetLiteral(6.0);

        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_AVERAGE).invoke(value1, weight1, value2, weight2));
    }

    @Test
    public void testWeightedAverageWithNoArguments() {
        // Test case where no arguments are passed
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_AVERAGE).invoke();
        });
    }

    @Test
    public void testWeightedAverageWithOddNumberOfArguments() {
        // Test case where there is an odd number of arguments
        Literal<?> value1 = SameObjectTermFactory.instance().createOrGetLiteral(3);
        Literal<?> weight1 = SameObjectTermFactory.instance().createOrGetLiteral(1);
        Literal<?> value2 = SameObjectTermFactory.instance().createOrGetLiteral(6);

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_AVERAGE).invoke(value1, weight1, value2);
        });
    }

    @Test
    public void testWeightedAverageWithInvalidArguments() {
        // Test case where the arguments are not valid numbers
        Literal<?> invalidValue = SameObjectTermFactory.instance().createOrGetLiteral("invalid");
        Literal<?> weight = SameObjectTermFactory.instance().createOrGetLiteral(1);

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_AVERAGE).invoke(invalidValue, weight);
        });
    }

    @Test
    public void testWeightedAverageWithZeroTotalWeight() {
        // Test case where the total weight is zero
        Literal<?> value1 = SameObjectTermFactory.instance().createOrGetLiteral(3);
        Literal<?> weight1 = SameObjectTermFactory.instance().createOrGetLiteral(0);
        Literal<?> value2 = SameObjectTermFactory.instance().createOrGetLiteral(6);
        Literal<?> weight2 = SameObjectTermFactory.instance().createOrGetLiteral(0);

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_AVERAGE).invoke(value1, weight1, value2, weight2);
        });
    }

    @Test
    public void testWeightedMedianWithValidTuples() {
        // Test case with tuples of (value, weight) represented as Lists
        Literal<?> tuple1 = SameObjectTermFactory.instance().createOrGetLiteral(
                List.of(SameObjectTermFactory.instance().createOrGetLiteral(3),
                        SameObjectTermFactory.instance().createOrGetLiteral(1))
        );
        Literal<?> tuple2 = SameObjectTermFactory.instance().createOrGetLiteral(
                List.of(SameObjectTermFactory.instance().createOrGetLiteral(6),
                        SameObjectTermFactory.instance().createOrGetLiteral(1))
        );

        Literal<Double> expected = SameObjectTermFactory.instance().createOrGetLiteral(4.5);

        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_MEDIAN).invoke(tuple1, tuple2));
    }

    @Test
    public void testWeightedMedianWithAlternatingArguments() {
        // Test case with alternating value and weight literals
        Literal<?> value1 = SameObjectTermFactory.instance().createOrGetLiteral(3);
        Literal<?> weight1 = SameObjectTermFactory.instance().createOrGetLiteral(1);
        Literal<?> value2 = SameObjectTermFactory.instance().createOrGetLiteral(6);
        Literal<?> weight2 = SameObjectTermFactory.instance().createOrGetLiteral(1);

        Literal<Double> expected = SameObjectTermFactory.instance().createOrGetLiteral(4.5);

        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_MEDIAN).invoke(value1, weight1, value2, weight2));
    }

    @Test
    public void testWeightedMedianWithCollectionOfTuples() {
        // Test case with a collection of tuples
        Literal<?> tuple1 = SameObjectTermFactory.instance().createOrGetLiteral(
                List.of(SameObjectTermFactory.instance().createOrGetLiteral(3),
                        SameObjectTermFactory.instance().createOrGetLiteral(1))
        );
        Literal<?> tuple2 = SameObjectTermFactory.instance().createOrGetLiteral(
                List.of(SameObjectTermFactory.instance().createOrGetLiteral(6),
                        SameObjectTermFactory.instance().createOrGetLiteral(3))
        );

        Literal<?> collection = SameObjectTermFactory.instance().createOrGetLiteral(List.of(tuple1, tuple2));

        Literal<Double> expected = SameObjectTermFactory.instance().createOrGetLiteral(6.0);

        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_MEDIAN).invoke(collection));
    }

    @Test
    public void testWeightedMedianWithZeroTotalWeight() {
        // Test case where the total weight is zero
        Literal<?> value1 = SameObjectTermFactory.instance().createOrGetLiteral(3);
        Literal<?> weight1 = SameObjectTermFactory.instance().createOrGetLiteral(0);
        Literal<?> value2 = SameObjectTermFactory.instance().createOrGetLiteral(6);
        Literal<?> weight2 = SameObjectTermFactory.instance().createOrGetLiteral(0);

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_MEDIAN).invoke(value1, weight1, value2, weight2);
        });
    }

    @Test
    public void testWeightedMedianWithNegativeWeight() {
        Literal<?> value1 = SameObjectTermFactory.instance().createOrGetLiteral(3);
        Literal<?> weight1 = SameObjectTermFactory.instance().createOrGetLiteral(-1);  // Negative weight
        Literal<?> value2 = SameObjectTermFactory.instance().createOrGetLiteral(6);
        Literal<?> weight2 = SameObjectTermFactory.instance().createOrGetLiteral(3);

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_MEDIAN).invoke(value1, weight1, value2, weight2);
        });
    }

    @Test
    public void testWeightedMedianWithNegativeWeightInTuples() {
        Literal<?> tuple1 = SameObjectTermFactory.instance().createOrGetLiteral(
                List.of(SameObjectTermFactory.instance().createOrGetLiteral(3),
                        SameObjectTermFactory.instance().createOrGetLiteral(-1))  // Negative weight
        );
        Literal<?> tuple2 = SameObjectTermFactory.instance().createOrGetLiteral(
                List.of(SameObjectTermFactory.instance().createOrGetLiteral(6),
                        SameObjectTermFactory.instance().createOrGetLiteral(3))
        );

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_MEDIAN).invoke(tuple1, tuple2);
        });
    }

    @Test
    public void testWeightedMedianWithOddNumberOfArguments() {
        Literal<?> value1 = SameObjectTermFactory.instance().createOrGetLiteral(3);
        Literal<?> weight1 = SameObjectTermFactory.instance().createOrGetLiteral(1);
        Literal<?> value2 = SameObjectTermFactory.instance().createOrGetLiteral(6);

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_MEDIAN).invoke(value1, weight1, value2);
        });
    }

    @Test
    public void testWeightedMedianLoopTermination() {
        Literal<?> value1 = SameObjectTermFactory.instance().createOrGetLiteral(1);
        Literal<?> weight1 = SameObjectTermFactory.instance().createOrGetLiteral(1);
        Literal<?> value2 = SameObjectTermFactory.instance().createOrGetLiteral(2);
        Literal<?> weight2 = SameObjectTermFactory.instance().createOrGetLiteral(1);
        Literal<?> value3 = SameObjectTermFactory.instance().createOrGetLiteral(3);
        Literal<?> weight3 = SameObjectTermFactory.instance().createOrGetLiteral(1);

        Assertions.assertDoesNotThrow(() -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_MEDIAN).invoke(
                    value1, weight1, value2, weight2, value3, weight3
            );
        });
    }

    @Test
    public void testWeightedMedianWithAverageOfMiddleValues() {
        Literal<?> value1 = SameObjectTermFactory.instance().createOrGetLiteral(3);
        Literal<?> weight1 = SameObjectTermFactory.instance().createOrGetLiteral(1);
        Literal<?> value2 = SameObjectTermFactory.instance().createOrGetLiteral(6);
        Literal<?> weight2 = SameObjectTermFactory.instance().createOrGetLiteral(1);

        // The median should be the average of 3 and 6 since their weights are equal
        Literal<?> expectedMedian = SameObjectTermFactory.instance().createOrGetLiteral(4.5);

        Assertions.assertEquals(expectedMedian, invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_MEDIAN).invoke(
                value1, weight1, value2, weight2
        ));
    }

    @Test
    public void testWeightedMedianImpossibleCalculation() {
        // Use an empty set of arguments, which should cause the computation to fail.
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_MEDIAN).invoke();
        });
    }

    @Test
    public void testSetWithValidTerms() {
        Literal<?> term1 = SameObjectTermFactory.instance().createOrGetLiteral(3);
        Literal<?> term2 = SameObjectTermFactory.instance().createOrGetLiteral(6);
        Literal<?> term3 = SameObjectTermFactory.instance().createOrGetLiteral(3);  // Duplicate term

        Set<Term> expectedSet = new HashSet<>(List.of(term1, term2));

        Literal<Set<Term>> result = (Literal<Set<Term>>) invokers.getInvoker(IntegraalInvokers.StdInvoker.SET).invoke(term1, term2, term3);

        Assertions.assertEquals(expectedSet, result.value());
    }

    @Test
    public void testSetWithNoArguments() {
        Set<Term> expectedSet = new HashSet<>();

        Literal<Set<Term>> result = (Literal<Set<Term>>) invokers.getInvoker(IntegraalInvokers.StdInvoker.SET).invoke();

        Assertions.assertEquals(expectedSet, result.value());
    }

    @Test
    public void testSetWithDuplicateTerms() {
        Literal<?> term1 = SameObjectTermFactory.instance().createOrGetLiteral(5);
        Literal<?> term2 = SameObjectTermFactory.instance().createOrGetLiteral(5);  // Duplicate term

        Set<Term> expectedSet = new HashSet<>(List.of(term1));

        Literal<Set<Term>> result = (Literal<Set<Term>>) invokers.getInvoker(IntegraalInvokers.StdInvoker.SET).invoke(term1, term2);

        Assertions.assertEquals(expectedSet, result.value());
    }

    @Test
    public void testSetWithDifferentTypes() {
        Literal<?> term1 = SameObjectTermFactory.instance().createOrGetLiteral(1);
        Literal<?> term2 = SameObjectTermFactory.instance().createOrGetLiteral("Hello");
        Literal<?> term3 = SameObjectTermFactory.instance().createOrGetLiteral(true);

        Set<Term> expectedSet = new HashSet<>(List.of(term1, term2, term3));

        Literal<Set<Term>> result = (Literal<Set<Term>>) invokers.getInvoker(IntegraalInvokers.StdInvoker.SET).invoke(term1, term2, term3);

        Assertions.assertEquals(expectedSet, result.value());
    }

    @Test
    public void testSizeWithValidCollection() {
        Literal<Collection<Integer>> collectionLiteral = SameObjectTermFactory.instance().createOrGetLiteral(List.of(1, 2, 3));

        Literal<Integer> result = (Literal<Integer>) invokers.getInvoker(IntegraalInvokers.StdInvoker.SIZE).invoke(collectionLiteral);

        Assertions.assertEquals(3, result.value());
    }

    @Test
    public void testSizeWithValidMap() {
        Map<String, Integer> map = Map.of("key1", 1, "key2", 2);
        Literal<Map<String, Integer>> mapLiteral = SameObjectTermFactory.instance().createOrGetLiteral(map);

        Literal<Integer> result = (Literal<Integer>) invokers.getInvoker(IntegraalInvokers.StdInvoker.SIZE).invoke(mapLiteral);

        Assertions.assertEquals(2, result.value());
    }

    @Test
    public void testSizeWithEmptyCollection() {
        Literal<Collection<Integer>> collectionLiteral = SameObjectTermFactory.instance().createOrGetLiteral(List.of());

        Literal<Integer> result = (Literal<Integer>) invokers.getInvoker(IntegraalInvokers.StdInvoker.SIZE).invoke(collectionLiteral);

        Assertions.assertEquals(0, result.value());
    }

    @Test
    public void testSizeWithEmptyMap() {
        Literal<Map<String, Integer>> mapLiteral = SameObjectTermFactory.instance().createOrGetLiteral(Map.of());

        Literal<Integer> result = (Literal<Integer>) invokers.getInvoker(IntegraalInvokers.StdInvoker.SIZE).invoke(mapLiteral);

        Assertions.assertEquals(0, result.value());
    }

    @Test
    public void testSizeWithInvalidArgument() {
        Literal<String> invalidLiteral = SameObjectTermFactory.instance().createOrGetLiteral("Not a collection or map");

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.SIZE).invoke(invalidLiteral);
        });
    }

    @Test
    public void testSizeWithIncorrectNumberOfArguments() {
        Literal<Collection<Integer>> collectionLiteral = SameObjectTermFactory.instance().createOrGetLiteral(List.of(1, 2, 3));
        Literal<String> extraLiteral = SameObjectTermFactory.instance().createOrGetLiteral("Extra argument");

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.SIZE).invoke(collectionLiteral, extraLiteral);
        });
    }

    @Test
    public void testSizeWithNonLiteralArgument() {
        Term nonLiteralTerm = SameObjectTermFactory.instance().createOrGetVariable("X");

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.SIZE).invoke(nonLiteralTerm);
        });
    }

    static Stream<Object[]> intersectionData() {
        return Stream.of(
                new Object[]{
                        new Term[]{
                                SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2)),
                                SameObjectTermFactory.instance().createOrGetLiteral(Set.of(2, 3))
                        },
                        SameObjectTermFactory.instance().createOrGetLiteral(Set.of(2))  // Expected result
                },
                new Object[]{
                        new Term[]{
                                SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2)),
                                SameObjectTermFactory.instance().createOrGetLiteral(Set.of(2, 3)),
                                SameObjectTermFactory.instance().createOrGetLiteral(Set.of(2, 4))
                        },
                        SameObjectTermFactory.instance().createOrGetLiteral(Set.of(2))  // Expected result
                },
                new Object[]{
                        new Term[]{
                                SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2)),
                                SameObjectTermFactory.instance().createOrGetLiteral(Set.of(3, 4))
                        },
                        SameObjectTermFactory.instance().createOrGetLiteral(Set.of())  // Expected result (no intersection)
                }
        );
    }

    @ParameterizedTest
    @MethodSource("intersectionData")
    public void testIntersection(Term[] terms, Literal<Set<Object>> expected) {
        Literal<Set<Object>> result = (Literal<Set<Object>>) invokers.getInvoker(IntegraalInvokers.StdInvoker.INTERSECTION).invoke(terms);
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testIntersectionWithCollection() {
        Literal<List<Literal<Set<Object>>>> collection = SameObjectTermFactory.instance().createOrGetLiteral(
                List.of(
                        SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2)),
                        SameObjectTermFactory.instance().createOrGetLiteral(Set.of(2, 3)),
                        SameObjectTermFactory.instance().createOrGetLiteral(Set.of(2, 4))
                )
        );
        Literal<Set<Object>> expected = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(2));

        Literal<Set<Object>> result = (Literal<Set<Object>>) invokers.getInvoker(IntegraalInvokers.StdInvoker.INTERSECTION).invoke(collection);
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testIntersectionNoArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.INTERSECTION).invoke();
        });
    }

    @Test
    public void testIntersectionWithNonSetArgument() {
        Literal<Integer> nonSet = SameObjectTermFactory.instance().createOrGetLiteral(42);
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.INTERSECTION).invoke(nonSet);
        });
    }

    @Test
    public void testIntersectionWithMixedTypes() {
        Literal<Set<Object>> validSet = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2));
        Literal<Integer> nonSet = SameObjectTermFactory.instance().createOrGetLiteral(42);

        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.INTERSECTION).invoke(validSet, nonSet);
        });
    }

    @Test
    public void testIntersectionWithNonLiteralArgument() {
        Term nonLiteralTerm = SameObjectTermFactory.instance().createOrGetVariable("X"); // A non-literal term

        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.INTERSECTION).invoke(nonLiteralTerm);
        });
    }

    // Test when all arguments are valid sets
    @Test
    public void testUnionWithValidSets() {
        Literal<Set<Object>> set1 = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2));
        Literal<Set<Object>> set2 = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(3, 4));

        Literal<Set<Object>> expected = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2, 3, 4));

        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.UNION).invoke(set1, set2));
    }

    // Test when one argument is not a Literal
    @Test
    public void testUnionWithNonLiteralArgument() {
        Term nonLiteralTerm = SameObjectTermFactory.instance().createOrGetVariable("X"); // A non-literal term

        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.UNION).invoke(nonLiteralTerm);
        });
    }

    // Test when one argument is not a set (not a collection)
    @Test
    public void testUnionWithNonSetArgument() {
        Literal<String> nonSetLiteral = SameObjectTermFactory.instance().createOrGetLiteral("notASet");

        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.UNION).invoke(nonSetLiteral);
        });
    }

    // Test with an empty set
    @Test
    public void testUnionWithEmptySet() {
        Literal<Set<Object>> emptySet = SameObjectTermFactory.instance().createOrGetLiteral(Set.of());
        Literal<Set<Object>> expected = SameObjectTermFactory.instance().createOrGetLiteral(Set.of());

        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.UNION).invoke(emptySet));
    }

    // Test when both arguments are valid sets and one is a subset of the other
    @Test
    public void testIsSubsetValid() {
        Literal<Set<Object>> set1 = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2));
        Literal<Set<Object>> set2 = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2, 3, 4));

        Literal<Boolean> expected = SameObjectTermFactory.instance().createOrGetLiteral(true);

        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SUBSET).invoke(set1, set2));
    }

    // Test when both arguments are valid sets but not a subset
    @Test
    public void testIsNotSubset() {
        Literal<Set<Object>> set1 = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 5));
        Literal<Set<Object>> set2 = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2, 3, 4));

        Literal<Boolean> expected = SameObjectTermFactory.instance().createOrGetLiteral(false);

        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SUBSET).invoke(set1, set2));
    }

    // Test when the first argument is not a set
    @Test
    public void testIsSubsetFirstArgumentNotSet() {
        Literal<String> notASet = SameObjectTermFactory.instance().createOrGetLiteral("notASet");
        Literal<Set<Object>> validSet = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2, 3));

        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SUBSET).invoke(notASet, validSet);
        });
    }

    // Test when the second argument is not a set
    @Test
    public void testIsSubsetSecondArgumentNotSet() {
        Literal<Set<Object>> validSet = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2));
        Literal<String> notASet = SameObjectTermFactory.instance().createOrGetLiteral("notASet");

        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SUBSET).invoke(validSet, notASet);
        });
    }

    // Test when there are not exactly two arguments
    @Test
    public void testIsSubsetWrongNumberOfArguments() {
        Literal<Set<Object>> set1 = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2));

        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SUBSET).invoke(set1);
        });
    }

    // Test when the first argument is not a Literal
    @Test
    public void testIsSubsetFirstArgumentNotLiteral() {
        Term notLiteral = SameObjectTermFactory.instance().createOrGetVariable("X");  // A variable is not a literal
        Literal<Set<Object>> validSet = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2, 3));

        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SUBSET).invoke(notLiteral, validSet);
        });
    }

    // Test when the second argument is not a Literal
    @Test
    public void testIsSubsetSecondArgumentNotLiteral() {
        Literal<Set<Object>> validSet = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2));
        Term notLiteral = SameObjectTermFactory.instance().createOrGetVariable("Y");  // A variable is not a literal

        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SUBSET).invoke(validSet, notLiteral);
        });
    }

    // Test when set1.size() > set2.size() to ensure the size condition is checked
    @Test
    public void testIsSubsetSizeCondition() {
        Literal<Set<Object>> set1 = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2, 3, 4));
        Literal<Set<Object>> set2 = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2));  // Smaller set

        Literal<Boolean> expected = SameObjectTermFactory.instance().createOrGetLiteral(false);

        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_SUBSET).invoke(set1, set2));
    }

    // Test when the first argument is not a Literal
    @Test
    public void testIsStrictSubsetFirstArgumentNotLiteral() {
        Term notLiteral = SameObjectTermFactory.instance().createOrGetVariable("X");  // A variable is not a literal
        Literal<Set<Object>> validSet = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2, 3));

        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_STRICT_SUBSET).invoke(notLiteral, validSet);
        });
    }

    // Test when the second argument is not a Literal
    @Test
    public void testIsStrictSubsetSecondArgumentNotLiteral() {
        Literal<Set<Object>> validSet = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2));
        Term notLiteral = SameObjectTermFactory.instance().createOrGetVariable("Y");  // A variable is not a literal

        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_STRICT_SUBSET).invoke(validSet, notLiteral);
        });
    }

    // Test when set1 is not a set (invalid argument)
    @Test
    public void testIsStrictSubsetFirstArgumentNotSet() {
        Literal<String> notSet = SameObjectTermFactory.instance().createOrGetLiteral("notASet");
        Literal<Set<Object>> validSet = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2, 3));

        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_STRICT_SUBSET).invoke(notSet, validSet);
        });
    }

    // Test when set2 is not a set (invalid argument)
    @Test
    public void testIsStrictSubsetSecondArgumentNotSet() {
        Literal<Set<Object>> validSet = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2));
        Literal<String> notSet = SameObjectTermFactory.instance().createOrGetLiteral("notASet");

        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_STRICT_SUBSET).invoke(validSet, notSet);
        });
    }

    // Test strict subset: valid case (set1 is a strict subset of set2)
    @Test
    public void testIsStrictSubsetValid() {
        Literal<Set<Object>> set1 = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2));
        Literal<Set<Object>> set2 = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2, 3));

        Literal<Boolean> expected = SameObjectTermFactory.instance().createOrGetLiteral(true);

        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_STRICT_SUBSET).invoke(set1, set2));
    }

    // Test strict subset: invalid case (set1 is not a strict subset of set2, same size)
    @Test
    public void testIsStrictSubsetSameSize() {
        Literal<Set<Object>> set1 = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2, 3));
        Literal<Set<Object>> set2 = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2, 3));

        Literal<Boolean> expected = SameObjectTermFactory.instance().createOrGetLiteral(false);

        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_STRICT_SUBSET).invoke(set1, set2));
    }

    // Test strict subset: invalid case (set1 is not a strict subset of set2, set1 has more elements)
    @Test
    public void testIsStrictSubsetSet1LargerThanSet2() {
        Literal<Set<Object>> set1 = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2, 3, 4));
        Literal<Set<Object>> set2 = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2, 3));

        Literal<Boolean> expected = SameObjectTermFactory.instance().createOrGetLiteral(false);

        Assertions.assertEquals(expected, invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_STRICT_SUBSET).invoke(set1, set2));
    }

    // Test when number of arguments is not equal to 2
    @Test
    public void testIsStrictSubsetInvalidNumberOfArguments() {
        Literal<Set<Object>> set1 = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2, 3));
        Literal<Set<Object>> set2 = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2));
        Literal<Set<Object>> set3 = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2, 3, 4));

        // Passing only one argument
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_STRICT_SUBSET).invoke(set1);
        });

        // Passing more than two arguments
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_STRICT_SUBSET).invoke(set1, set2, set3);
        });
    }

    @Test
    public void testContainsValid() {
        Literal<Set<Literal<Integer>>> setLiteral = SameObjectTermFactory.instance().createOrGetLiteral(
                Set.<Literal<Integer>>of(SameObjectTermFactory.instance().createOrGetLiteral(1),
                        SameObjectTermFactory.instance().createOrGetLiteral(2),
                        SameObjectTermFactory.instance().createOrGetLiteral(3)));
        Literal<Integer> elementInSet = SameObjectTermFactory.instance().createOrGetLiteral(2);
        Literal<Integer> elementNotInSet = SameObjectTermFactory.instance().createOrGetLiteral(4);

        // Test when the element is in the set
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.CONTAINS).invoke(setLiteral, elementInSet)
        );

        // Test when the element is not in the set
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.CONTAINS).invoke(setLiteral, elementNotInSet)
        );
    }

    @Test
    public void testContainsInvalidFirstArgumentNotCollection() {
        Literal<String> stringLiteral = SameObjectTermFactory.instance().createOrGetLiteral("NotACollection");
        Literal<Integer> element = SameObjectTermFactory.instance().createOrGetLiteral(1);

        // Test when the first argument is not a collection
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.CONTAINS).invoke(stringLiteral, element);
        });
    }

    @Test
    public void testContainsInvalidNumberOfArguments() {
        Literal<Set<Object>> setLiteral = SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2, 3));
        Literal<Integer> element = SameObjectTermFactory.instance().createOrGetLiteral(2);

        // Test when only one argument is passed
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.CONTAINS).invoke(setLiteral);
        });

        // Test when more than two arguments are passed
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.CONTAINS).invoke(setLiteral, element, element);
        });
    }

    @Test
    public void testContainsInvalidFirstArgument() {
        // Create an invalid first argument that is not a Literal
        Term invalidTerm = SameObjectTermFactory.instance().createOrGetVariable("X"); // Variable instead of Literal
        Literal<Integer> element = SameObjectTermFactory.instance().createOrGetLiteral(2);

        // Test when the first argument is not a Literal
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.CONTAINS).invoke(invalidTerm, element);
        });
    }


    @Test
    public void testContainsValidStringCases() {
        Literal<String> stringLiteral = SameObjectTermFactory.instance().createOrGetLiteral("Hello, world!");
        Literal<String> substringPresent = SameObjectTermFactory.instance().createOrGetLiteral("world");
        Literal<String> substringAbsent = SameObjectTermFactory.instance().createOrGetLiteral("goodbye");

        // Test when the substring is present in the string
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.CONTAINS).invoke(stringLiteral, substringPresent)
        );

        // Test when the substring is not present in the string
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.CONTAINS).invoke(stringLiteral, substringAbsent)
        );
    }

    @Test
    public void testContainsInvalidFirstArgumentNotCollectionOrString() {
        Literal<Integer> invalidFirstArg = SameObjectTermFactory.instance().createOrGetLiteral(123);
        Literal<Integer> element = SameObjectTermFactory.instance().createOrGetLiteral(1);

        // Test when the first argument is neither a collection nor a string
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.CONTAINS).invoke(invalidFirstArg, element);
        });
    }

    @Test
    public void testContainsInvalidSecondArgumentForString() {
        Literal<String> stringLiteral = SameObjectTermFactory.instance().createOrGetLiteral("Hello");
        Literal<Integer> nonStringElement = SameObjectTermFactory.instance().createOrGetLiteral(123);

        // Test when the second argument is not a string for a string containment check
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.CONTAINS).invoke(stringLiteral, nonStringElement);
        });
    }

    @Test
    public void testContainsInvalidSecondArgumentNotLiteral() {
        Literal<String> stringLiteral = SameObjectTermFactory.instance().createOrGetLiteral("Hello");
        Term nonLiteralElement = SameObjectTermFactory.instance().createOrGetVariable("X"); // Variable instead of Literal

        // Test when the second argument is not a Literal
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.CONTAINS).invoke(stringLiteral, nonLiteralElement);
        });
    }

    @Test
    public void testDictWithValidTuples() {
        // Valid key-value pairs (tuples of size 2)
        Literal<List<Literal<String>>> tuple1 = SameObjectTermFactory.instance().createOrGetLiteral(
                List.of(SameObjectTermFactory.instance().createOrGetLiteral("key1"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value1")));

        Literal<List<Literal<String>>> tuple2 = SameObjectTermFactory.instance().createOrGetLiteral(
                List.of(SameObjectTermFactory.instance().createOrGetLiteral("key2"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value2")));

        Map<Term, Term> expectedMap = Map.of(
                SameObjectTermFactory.instance().createOrGetLiteral("key1"),
                SameObjectTermFactory.instance().createOrGetLiteral("value1"),
                SameObjectTermFactory.instance().createOrGetLiteral("key2"),
                SameObjectTermFactory.instance().createOrGetLiteral("value2")
        );

        Literal<Map<Term, Term>> expected = SameObjectTermFactory.instance().createOrGetLiteral(expectedMap);

        // Invoke dict invoker
        Assertions.assertEquals(expected,
                invokers.getInvoker(IntegraalInvokers.StdInvoker.DICT).invoke(tuple1, tuple2));
    }

    @Test
    public void testDictWithInvalidArguments() {
        // Invalid argument: not a tuple
        Literal<String> invalidLiteral = SameObjectTermFactory.instance().createOrGetLiteral("notATuple");

        // Test with invalid non-tuple argument
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.DICT).invoke(invalidLiteral);
        });

        // Invalid argument: tuple of size 1
        Literal<List<Literal<String>>> invalidTuple = SameObjectTermFactory.instance().createOrGetLiteral(
                List.of(SameObjectTermFactory.instance().createOrGetLiteral("key1")));

        // Test with invalid tuple size
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.DICT).invoke(invalidTuple);
        });
    }

    @Test
    public void testDictWithNonLiteralArgument() {
        // Non-literal argument (e.g., a Term that is not a Literal)
        Term nonLiteral = SameObjectTermFactory.instance().createOrGetVariable("X");

        // Test with a non-literal argument
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.DICT).invoke(nonLiteral);
        });
    }

    @Test
    public void testMergeDictsValid() {
        // Create two dictionaries
        Literal<Map<Term, Term>> dict1 = SameObjectTermFactory.instance().createOrGetLiteral(
                Map.of(SameObjectTermFactory.instance().createOrGetLiteral("key1"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value1"))
        );
        Literal<Map<Term, Term>> dict2 = SameObjectTermFactory.instance().createOrGetLiteral(
                Map.of(SameObjectTermFactory.instance().createOrGetLiteral("key2"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value2"))
        );

        // Expected result after merging
        Literal<Map<Term, Term>> expectedMergedDict = SameObjectTermFactory.instance().createOrGetLiteral(
                Map.of(
                        SameObjectTermFactory.instance().createOrGetLiteral("key1"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value1"),
                        SameObjectTermFactory.instance().createOrGetLiteral("key2"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value2")
                )
        );

        // Test merging two valid dictionaries
        Assertions.assertEquals(
                expectedMergedDict,
                invokers.getInvoker(IntegraalInvokers.StdInvoker.MERGE_DICTS).invoke(dict1, dict2)
        );
    }

    @Test
    public void testMergeDictsWithInvalidFirstArgument() {
        // First argument is not a dictionary
        Literal<Set<Literal<Integer>>> nonDict1 = SameObjectTermFactory.instance().createOrGetLiteral(
                Set.of(SameObjectTermFactory.instance().createOrGetLiteral(1),
                        SameObjectTermFactory.instance().createOrGetLiteral(2))
        );
        Literal<Map<Term, Term>> dict2 = SameObjectTermFactory.instance().createOrGetLiteral(
                Map.of(SameObjectTermFactory.instance().createOrGetLiteral("key2"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value2"))
        );

        // Test merging with an invalid first argument (non-dictionary)
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.MERGE_DICTS).invoke(nonDict1, dict2);
        });
    }

    @Test
    public void testMergeDictsWithInvalidSecondArgument() {
        // Second argument is not a dictionary
        Literal<Map<Term, Term>> dict1 = SameObjectTermFactory.instance().createOrGetLiteral(
                Map.of(SameObjectTermFactory.instance().createOrGetLiteral("key1"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value1"))
        );
        Literal<Set<Literal<Integer>>> nonDict2 = SameObjectTermFactory.instance().createOrGetLiteral(
                Set.of(SameObjectTermFactory.instance().createOrGetLiteral(1),
                        SameObjectTermFactory.instance().createOrGetLiteral(2))
        );

        // Test merging with an invalid second argument (non-dictionary)
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.MERGE_DICTS).invoke(dict1, nonDict2);
        });
    }

    @Test
    public void testMergeDictsWithInvalidArgumentsCount() {
        Literal<Map<Term, Term>> dict1 = SameObjectTermFactory.instance().createOrGetLiteral(
                Map.of(SameObjectTermFactory.instance().createOrGetLiteral("key1"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value1"))
        );

        // Test with only one argument (less than required)
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.MERGE_DICTS).invoke(dict1);
        });

        // Test with more than two arguments (more than required)
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.MERGE_DICTS).invoke(dict1, dict1, dict1);
        });
    }

    @Test
    public void testMergeDictsWithNonLiteralFirstArgument() {
        // First argument is a non-literal (e.g., a variable)
        Term nonLiteral = SameObjectTermFactory.instance().createOrGetVariable("X");
        Literal<Map<Term, Term>> dict2 = SameObjectTermFactory.instance().createOrGetLiteral(
                Map.of(SameObjectTermFactory.instance().createOrGetLiteral("key2"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value2"))
        );

        // Test merging with a non-literal first argument
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.MERGE_DICTS).invoke(nonLiteral, dict2);
        });
    }

    @Test
    public void testMergeDictsWithNonLiteralSecondArgument() {
        // Second argument is a non-literal (e.g., a variable)
        Literal<Map<Term, Term>> dict1 = SameObjectTermFactory.instance().createOrGetLiteral(
                Map.of(SameObjectTermFactory.instance().createOrGetLiteral("key1"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value1"))
        );
        Term nonLiteral = SameObjectTermFactory.instance().createOrGetVariable("Y");

        // Test merging with a non-literal second argument
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.MERGE_DICTS).invoke(dict1, nonLiteral);
        });
    }

    // Test when the input is a valid dictionary
    @Test
    public void testDictKeysWithValidDictionary() {
        Literal<Map<Literal<String>, Literal<String>>> dict = SameObjectTermFactory.instance().createOrGetLiteral(
                Map.of(SameObjectTermFactory.instance().createOrGetLiteral("key1"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value1"),
                        SameObjectTermFactory.instance().createOrGetLiteral("key2"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value2"))
        );

        Literal<Set<?>> expectedKeys = SameObjectTermFactory.instance().createOrGetLiteral(
                Set.of(SameObjectTermFactory.instance().createOrGetLiteral("key1"),
                        SameObjectTermFactory.instance().createOrGetLiteral("key2"))
        );

        Assertions.assertEquals(expectedKeys,
                invokers.getInvoker(IntegraalInvokers.StdInvoker.DICT_KEYS).invoke(dict));
    }

    // Test when the input is an empty dictionary
    @Test
    public void testDictKeysWithEmptyDictionary() {
        Literal<Map<Literal<String>, Literal<String>>> emptyDict = SameObjectTermFactory.instance().createOrGetLiteral(
                Map.of()  // Empty dictionary
        );

        Literal<Set<?>> expectedEmptySet = SameObjectTermFactory.instance().createOrGetLiteral(Set.of());

        Assertions.assertEquals(expectedEmptySet,
                invokers.getInvoker(IntegraalInvokers.StdInvoker.DICT_KEYS).invoke(emptyDict));
    }

    // Test when the argument is not a literal
    @Test
    public void testDictKeysWithNonLiteralArgument() {
        Term nonLiteral = SameObjectTermFactory.instance().createOrGetVariable("X");

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.DICT_KEYS).invoke(nonLiteral);
        });
    }

    // Test when the argument is not a dictionary (e.g., a string)
    @Test
    public void testDictKeysWithNonDictionaryLiteral() {
        Literal<String> nonDictLiteral = SameObjectTermFactory.instance().createOrGetLiteral("NotADictionary");

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.DICT_KEYS).invoke(nonDictLiteral);
        });
    }

    // Test when no arguments are provided
    @Test
    public void testDictKeysWithNoArguments() {
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.DICT_KEYS).invoke();
        });
    }

    // Test when more than one argument is provided
    @Test
    public void testDictKeysWithMultipleArguments() {
        Literal<Map<Literal<String>, Literal<String>>> dict1 = SameObjectTermFactory.instance().createOrGetLiteral(
                Map.of(SameObjectTermFactory.instance().createOrGetLiteral("key1"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value1"))
        );

        Literal<Map<Literal<String>, Literal<String>>> dict2 = SameObjectTermFactory.instance().createOrGetLiteral(
                Map.of(SameObjectTermFactory.instance().createOrGetLiteral("key2"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value2"))
        );

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.DICT_KEYS).invoke(dict1, dict2);
        });
    }

    // Test when no arguments are provided
    @Test
    public void testDictValuesWithNoArguments() {
        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.DICT_VALUES).invoke();
        });
    }

    // Test when more than one argument is provided
    @Test
    public void testDictValuesWithMultipleArguments() {
        Literal<Map<Literal<String>, Literal<String>>> dict1 = SameObjectTermFactory.instance().createOrGetLiteral(
                Map.of(SameObjectTermFactory.instance().createOrGetLiteral("key1"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value1"))
        );

        Literal<Map<Literal<String>, Literal<String>>> dict2 = SameObjectTermFactory.instance().createOrGetLiteral(
                Map.of(SameObjectTermFactory.instance().createOrGetLiteral("key2"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value2"))
        );

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.DICT_VALUES).invoke(dict1, dict2);
        });
    }

    // Test when argument is not a dictionary
    @Test
    public void testDictValuesWithNonDictionaryArgument() {
        Literal<String> nonDictLiteral = SameObjectTermFactory.instance().createOrGetLiteral("This is not a dictionary");

        Assertions.assertThrows(IntegraalInvokers.InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.DICT_VALUES).invoke(nonDictLiteral);
        });
    }

    // Test with a valid dictionary
    @Test
    public void testDictValuesWithValidDictionary() {
        Literal<Map<Literal<String>, Literal<String>>> dict = SameObjectTermFactory.instance().createOrGetLiteral(
                Map.of(SameObjectTermFactory.instance().createOrGetLiteral("key1"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value1"),
                        SameObjectTermFactory.instance().createOrGetLiteral("key2"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value2"))
        );

        Collection<?> actualValues = ((Literal<Collection<?>>) invokers.getInvoker(IntegraalInvokers.StdInvoker.DICT_VALUES).invoke(dict)).value();

        // Assert that the collection contains the expected values, regardless of order
        Assertions.assertEquals(
                Set.of(
                        SameObjectTermFactory.instance().createOrGetLiteral("value1"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value2")),
                new HashSet<>(actualValues));
    }

    @Test
    public void testDictValuesWithNonLiteralArgument() {
        Term nonLiteral = SameObjectTermFactory.instance().createOrGetVariable("X"); // This is not a Literal

        InvokerException exception = Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.DICT_VALUES).invoke(nonLiteral);
        });

        Assertions.assertTrue(exception.getMessage().contains("Argument must be a literal containing a dictionary (map)."));
    }

    @Test
    public void testGetWithInvalidArgumentCount() {
        Literal<List<Literal<Integer>>> listLiteral = SameObjectTermFactory.instance().createOrGetLiteral(List.of(
                SameObjectTermFactory.instance().createOrGetLiteral(1),
                SameObjectTermFactory.instance().createOrGetLiteral(2),
                SameObjectTermFactory.instance().createOrGetLiteral(3)
        ));

        InvokerException exception = Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.GET).invoke(listLiteral);
        });

        Assertions.assertTrue(exception.getMessage().contains("Must provide a literal container and an index or key."));
    }

    @Test
    public void testGetWithNonLiteralArgument() {
        Term nonLiteral = SameObjectTermFactory.instance().createOrGetVariable("X"); // This is not a Literal

        Literal<Integer> index = SameObjectTermFactory.instance().createOrGetLiteral(1);

        InvokerException exception = Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.GET).invoke(nonLiteral, index);
        });

        Assertions.assertTrue(exception.getMessage().contains("Must provide a literal container and an index or key."));
    }

    @Test
    public void testGetWithIndexOutOfBounds() {
        Literal<List<Literal<Integer>>> listLiteral = SameObjectTermFactory.instance().createOrGetLiteral(List.of(
                SameObjectTermFactory.instance().createOrGetLiteral(1),
                SameObjectTermFactory.instance().createOrGetLiteral(2),
                SameObjectTermFactory.instance().createOrGetLiteral(3)
        ));

        Literal<Integer> index = SameObjectTermFactory.instance().createOrGetLiteral(5); // Out of bounds

        InvokerException exception = Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.GET).invoke(listLiteral, index);
        });

        Assertions.assertTrue(exception.getMessage().contains("Index out of bounds."));
    }

    @Test
    public void testGetWithInvalidIndex() {
        Literal<List<Literal<Integer>>> listLiteral = SameObjectTermFactory.instance().createOrGetLiteral(List.of(
                SameObjectTermFactory.instance().createOrGetLiteral(1),
                SameObjectTermFactory.instance().createOrGetLiteral(2),
                SameObjectTermFactory.instance().createOrGetLiteral(3)
        ));

        Literal<String> invalidIndex = SameObjectTermFactory.instance().createOrGetLiteral("invalidIndex");

        InvokerException exception = Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.GET).invoke(listLiteral, invalidIndex);
        });
    }

    @Test
    public void testGetWithNonListOrMapArgument() {
        Literal<Integer> nonListOrMap = SameObjectTermFactory.instance().createOrGetLiteral(123); // Not a list or map
        Literal<Integer> index = SameObjectTermFactory.instance().createOrGetLiteral(0);

        InvokerException exception = Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.GET).invoke(nonListOrMap, index);
        });

        Assertions.assertTrue(exception.getMessage().contains("First argument must be a list or a map."));
    }

    @Test
    public void testGetFromList() {
        Literal<List<Literal<Integer>>> listLiteral = SameObjectTermFactory.instance().createOrGetLiteral(
                List.of(SameObjectTermFactory.instance().createOrGetLiteral(1),
                        SameObjectTermFactory.instance().createOrGetLiteral(2),
                        SameObjectTermFactory.instance().createOrGetLiteral(3)));

        // Test getting a valid index
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(2),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.GET).invoke(listLiteral, SameObjectTermFactory.instance().createOrGetLiteral(1))
        );

        // Test getting index 0
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(1),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.GET).invoke(listLiteral, SameObjectTermFactory.instance().createOrGetLiteral(0))
        );

        // Test getting index 2 (last element)
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(3),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.GET).invoke(listLiteral, SameObjectTermFactory.instance().createOrGetLiteral(2))
        );
    }

    @Test
    public void testGetFromMap() {
        Literal<Map<Literal<String>, Literal<String>>> mapLiteral = SameObjectTermFactory.instance().createOrGetLiteral(
                Map.of(SameObjectTermFactory.instance().createOrGetLiteral("key1"), SameObjectTermFactory.instance().createOrGetLiteral("value1"),
                        SameObjectTermFactory.instance().createOrGetLiteral("key2"), SameObjectTermFactory.instance().createOrGetLiteral("value2")));

        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral("value1"),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.GET).invoke(mapLiteral, SameObjectTermFactory.instance().createOrGetLiteral("key1"))
        );

        // Test getting another valid key
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral("value2"),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.GET).invoke(mapLiteral, SameObjectTermFactory.instance().createOrGetLiteral("key2"))
        );
    }

    @Test
    public void testGetInvalidIndex() {
        Literal<List<Literal<Integer>>> listLiteral = SameObjectTermFactory.instance().createOrGetLiteral(
                List.of(SameObjectTermFactory.instance().createOrGetLiteral(1),
                        SameObjectTermFactory.instance().createOrGetLiteral(2),
                        SameObjectTermFactory.instance().createOrGetLiteral(3)));

        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.GET).invoke(listLiteral, SameObjectTermFactory.instance().createOrGetLiteral(3)); // Out of bounds
        });
    }

    @Test
    public void testGetFromInvalidContainer() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.GET).invoke(SameObjectTermFactory.instance().createOrGetLiteral("not_a_list_or_map"), SameObjectTermFactory.instance().createOrGetLiteral(0));
        });
    }

    @Test
    public void testGetWithInvalidArguments() {
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.GET).invoke();
        });
    }

    @Test
    public void testGetNegativeIndex() {
        Literal<List<Literal<Integer>>> listLiteral = SameObjectTermFactory.instance().createOrGetLiteral(
                List.of(SameObjectTermFactory.instance().createOrGetLiteral(1),
                        SameObjectTermFactory.instance().createOrGetLiteral(2),
                        SameObjectTermFactory.instance().createOrGetLiteral(3)));

        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.GET).invoke(listLiteral, SameObjectTermFactory.instance().createOrGetLiteral(-1)); // Negative index
        });
    }

    @Test
    public void testGetInvalidIndexFormat() {
        Literal<List<Literal<Integer>>> listLiteral = SameObjectTermFactory.instance().createOrGetLiteral(
                List.of(SameObjectTermFactory.instance().createOrGetLiteral(1),
                        SameObjectTermFactory.instance().createOrGetLiteral(2),
                        SameObjectTermFactory.instance().createOrGetLiteral(3)));

        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.GET).invoke(listLiteral, SameObjectTermFactory.instance().createOrGetLiteral("not_a_number")); // Invalid index format
        });
    }

    @Test
    public void testGetIndexWithNonNumericValue() {
        Literal<List<Literal<Integer>>> listLiteral = SameObjectTermFactory.instance().createOrGetLiteral(
                List.of(SameObjectTermFactory.instance().createOrGetLiteral(1),
                        SameObjectTermFactory.instance().createOrGetLiteral(2),
                        SameObjectTermFactory.instance().createOrGetLiteral(3)));

        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.GET).invoke(listLiteral, SameObjectTermFactory.instance().createOrGetLiteral("invalid_index")); // Non-numeric index
        });
    }

    @Test
    public void testCreateTupleWithValidArguments() {
        // Create valid terms to pass to the tuple invoker
        Literal<String> strLiteral = SameObjectTermFactory.instance().createOrGetLiteral("a");
        Literal<Integer> intLiteral1 = SameObjectTermFactory.instance().createOrGetLiteral(1);
        Literal<Integer> intLiteral2 = SameObjectTermFactory.instance().createOrGetLiteral(2);
        Literal<String> strLiteral2 = SameObjectTermFactory.instance().createOrGetLiteral("b");

        // Invoke the tuple method
        Literal<List<Term>> result = (Literal<List<Term>>) invokers.getInvoker(IntegraalInvokers.StdInvoker.TUPLE)
                .invoke(strLiteral, intLiteral1, strLiteral2, intLiteral2);

        // Verify the result is as expected
        List<Term> expectedTuple = List.of(strLiteral, intLiteral1, strLiteral2, intLiteral2);
        Assertions.assertEquals(expectedTuple, result.value());
    }

    @Test
    public void testCreateTupleWithSingleArgument() {
        // Create a single valid term to pass to the tuple invoker
        Literal<String> strLiteral = SameObjectTermFactory.instance().createOrGetLiteral("single");

        // Invoke the tuple method
        Literal<List<Term>> result = (Literal<List<Term>>) invokers.getInvoker(IntegraalInvokers.StdInvoker.TUPLE)
                .invoke(strLiteral);

        // Verify the result contains the single element
        List<Term> expectedTuple = List.of(strLiteral);
        Assertions.assertEquals(expectedTuple, result.value());
    }

    @Test
    public void testContainsKeyWithValidKey() {
        // Create a valid dictionary (map) literal
        Map<Literal<String>, Literal<String>> map = new HashMap<>();
        map.put(SameObjectTermFactory.instance().createOrGetLiteral("key1"), SameObjectTermFactory.instance().createOrGetLiteral("value1"));
        map.put(SameObjectTermFactory.instance().createOrGetLiteral("key2"), SameObjectTermFactory.instance().createOrGetLiteral("value2"));

        Literal<Map<Literal<String>, Literal<String>>> mapLiteral = SameObjectTermFactory.instance().createOrGetLiteral(map);
        Literal<String> existingKey = SameObjectTermFactory.instance().createOrGetLiteral("key1");

        // Test that the key exists in the map
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.CONTAINS_KEY).invoke(mapLiteral, existingKey)
        );
    }

    @Test
    public void testContainsKeyWithInvalidKey() {
        // Create a valid dictionary (map) literal
        Map<Literal<String>, Literal<String>> map = new HashMap<>();
        map.put(SameObjectTermFactory.instance().createOrGetLiteral("key1"), SameObjectTermFactory.instance().createOrGetLiteral("value1"));
        map.put(SameObjectTermFactory.instance().createOrGetLiteral("key2"), SameObjectTermFactory.instance().createOrGetLiteral("value2"));

        Literal<Map<Literal<String>, Literal<String>>> mapLiteral = SameObjectTermFactory.instance().createOrGetLiteral(map);
        Literal<String> nonExistingKey = SameObjectTermFactory.instance().createOrGetLiteral("key3");

        // Test that the key does not exist in the map
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.CONTAINS_KEY).invoke(mapLiteral, nonExistingKey)
        );
    }

    @Test
    public void testContainsKeyWithInvalidArguments() {
        // Test with no arguments
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.CONTAINS_KEY).invoke();
        });

        // Test with a non-map first argument
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.CONTAINS_KEY).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("notAMap"),
                    SameObjectTermFactory.instance().createOrGetLiteral("key1")
            );
        });

        // Create a variable term using the factory
        Term invalidTerm = SameObjectTermFactory.instance().createOrGetVariable("invalidKey");

        // Test with the first argument being an invalid Term
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.CONTAINS_KEY).invoke(
                    invalidTerm,  // This term is not a literal map
                    SameObjectTermFactory.instance().createOrGetLiteral("key1")
            );
        });
    }

    @Test
    public void testContainsValue() {
        // Create a valid dictionary
        Literal<Map<Literal<?>, Literal<?>>> dictLiteral = SameObjectTermFactory.instance().createOrGetLiteral(
                Map.of(SameObjectTermFactory.instance().createOrGetLiteral("key1"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value1"),
                        SameObjectTermFactory.instance().createOrGetLiteral("key2"),
                        SameObjectTermFactory.instance().createOrGetLiteral("value2"))
        );

        // Valid value present in the map
        Literal<String> valueInMap = SameObjectTermFactory.instance().createOrGetLiteral("value1");
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.CONTAINS_VALUE).invoke(dictLiteral, valueInMap)
        );

        // Valid value not present in the map
        Literal<String> valueNotInMap = SameObjectTermFactory.instance().createOrGetLiteral("value3");
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.CONTAINS_VALUE).invoke(dictLiteral, valueNotInMap)
        );
    }

    @Test
    public void testContainsValueWithInvalidArguments() {
        // Test with no arguments
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.CONTAINS_VALUE).invoke();
        });

        // Test with a non-map first argument
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.CONTAINS_VALUE).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("notAMap"),
                    SameObjectTermFactory.instance().createOrGetLiteral("value1")
            );
        });

        // Create a variable term using the factory
        Term invalidTerm = SameObjectTermFactory.instance().createOrGetVariable("invalidValue");

        // Test with the first argument being an invalid Term
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.CONTAINS_VALUE).invoke(
                    invalidTerm,  // This term is not a literal map
                    SameObjectTermFactory.instance().createOrGetLiteral("value1")
            );
        });
    }

    @Test
    public void testIsEmpty() {
        // Test with an empty list
        Literal<List<String>> emptyListLiteral = SameObjectTermFactory.instance().createOrGetLiteral(new ArrayList<>());
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_EMPTY).invoke(emptyListLiteral)
        );

        // Test with a non-empty list
        Literal<List<String>> nonEmptyListLiteral = SameObjectTermFactory.instance().createOrGetLiteral(
                Arrays.asList("hello")
        );
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_EMPTY).invoke(nonEmptyListLiteral)
        );

        // Test with an empty string
        Literal<String> emptyStringLiteral = SameObjectTermFactory.instance().createOrGetLiteral("");
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_EMPTY).invoke(emptyStringLiteral)
        );

        // Test with a non-empty string
        Literal<String> nonEmptyStringLiteral = SameObjectTermFactory.instance().createOrGetLiteral("hello");
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_EMPTY).invoke(nonEmptyStringLiteral)
        );

        // Test with an empty set
        Literal<Set<String>> emptySetLiteral = SameObjectTermFactory.instance().createOrGetLiteral(new HashSet<>());
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_EMPTY).invoke(emptySetLiteral)
        );

        // Test with a non-empty set
        Literal<Set<String>> nonEmptySetLiteral = SameObjectTermFactory.instance().createOrGetLiteral(
                Set.of("hello")
        );
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_EMPTY).invoke(nonEmptySetLiteral)
        );
    }

    @Test
    public void testIsEmptyWithInvalidArguments() {
        // Test with no arguments
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_EMPTY).invoke();
        });

        // Test with a non-literal first argument
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_EMPTY).invoke(
                    SameObjectTermFactory.instance().createOrGetVariable("nonLiteral")
            );
        });

        // Test with a literal that is neither a collection nor a string
        Literal<Integer> integerLiteral = SameObjectTermFactory.instance().createOrGetLiteral(42);
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_EMPTY).invoke(integerLiteral);
        });

        // Test with a valid literal but not a collection or string
        Literal<Map<String, String>> invalidLiteral = SameObjectTermFactory.instance().createOrGetLiteral(new HashMap<>());
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_EMPTY).invoke(invalidLiteral);
        });
    }

    @Test
    public void testIsBlank() {
        // Test with a string that contains only spaces
        Literal<String> blankStringLiteral = SameObjectTermFactory.instance().createOrGetLiteral("   ");
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_BLANK).invoke(blankStringLiteral)
        );

        // Test with a string that contains only a newline
        Literal<String> newlineStringLiteral = SameObjectTermFactory.instance().createOrGetLiteral("\n");
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_BLANK).invoke(newlineStringLiteral)
        );

        // Test with an empty string
        Literal<String> emptyStringLiteral = SameObjectTermFactory.instance().createOrGetLiteral("");
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_BLANK).invoke(emptyStringLiteral)
        );

        // Test with a non-blank string
        Literal<String> nonBlankStringLiteral = SameObjectTermFactory.instance().createOrGetLiteral("hello");
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_BLANK).invoke(nonBlankStringLiteral)
        );

        // Test with a string that contains spaces and text
        Literal<String> mixedStringLiteral = SameObjectTermFactory.instance().createOrGetLiteral(" hello ");
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_BLANK).invoke(mixedStringLiteral)
        );
    }

    @Test
    public void testIsBlankWithInvalidArguments() {
        // Test with no arguments
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_BLANK).invoke();
        });

        // Test with a non-literal first argument
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_BLANK).invoke(
                    SameObjectTermFactory.instance().createOrGetVariable("nonLiteral")
            );
        });

        // Test with a literal that is not a string
        Literal<Integer> integerLiteral = SameObjectTermFactory.instance().createOrGetLiteral(42);
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_BLANK).invoke(integerLiteral);
        });

        // Test with a valid literal but not a string
        Literal<Map<String, String>> invalidLiteral = SameObjectTermFactory.instance().createOrGetLiteral(new HashMap<>());
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_BLANK).invoke(invalidLiteral);
        });
    }

    @Test
    public void testIsNumeric() {
        // Test with a numeric string
        Literal<String> numericStringLiteral = SameObjectTermFactory.instance().createOrGetLiteral("123");
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_NUMERIC).invoke(numericStringLiteral)
        );

        // Test with a negative numeric string
        Literal<String> negativeNumericStringLiteral = SameObjectTermFactory.instance().createOrGetLiteral("-123.45");
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_NUMERIC).invoke(negativeNumericStringLiteral)
        );

        // Test with a numeric value
        Literal<Integer> numericValueLiteral = SameObjectTermFactory.instance().createOrGetLiteral(456);
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_NUMERIC).invoke(numericValueLiteral)
        );

        // Test with a floating-point numeric value
        Literal<Double> floatValueLiteral = SameObjectTermFactory.instance().createOrGetLiteral(78.9);
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_NUMERIC).invoke(floatValueLiteral)
        );

        // Test with a non-numeric string
        Literal<String> nonNumericStringLiteral = SameObjectTermFactory.instance().createOrGetLiteral("abc");
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_NUMERIC).invoke(nonNumericStringLiteral)
        );

        // Test with an empty string
        Literal<String> emptyStringLiteral = SameObjectTermFactory.instance().createOrGetLiteral("");
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_NUMERIC).invoke(emptyStringLiteral)
        );

        // Test with a literal that is not a number or string
        Literal<List<?>> nonNumericListLiteral = SameObjectTermFactory.instance().createOrGetLiteral(new ArrayList<>());
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_NUMERIC).invoke(nonNumericListLiteral)
        );
    }

    @Test
    public void testIsNumericWithInvalidArguments() {
        // Test with no arguments
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_NUMERIC).invoke();
        });

        // Test with more than one argument
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_NUMERIC).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("123"),
                    SameObjectTermFactory.instance().createOrGetLiteral("456")
            );
        });

        // Test with a non-literal argument
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_NUMERIC).invoke(
                    SameObjectTermFactory.instance().createOrGetVariable("var")
            );
        });
    }

    @Test
    public void testToDouble() {
        // Test with a valid string representing a double
        Literal<String> stringDoubleLiteral = SameObjectTermFactory.instance().createOrGetLiteral("12.34");
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(12.34),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_DOUBLE).invoke(stringDoubleLiteral)
        );

        // Test with a valid integer
        Literal<Integer> integerLiteral = SameObjectTermFactory.instance().createOrGetLiteral(12);
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(12.0),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_DOUBLE).invoke(integerLiteral)
        );

        // Test with a valid long
        Literal<Long> longLiteral = SameObjectTermFactory.instance().createOrGetLiteral(12L);
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(12.0),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_DOUBLE).invoke(longLiteral)
        );

        // Test with a valid float
        Literal<Float> floatLiteral = SameObjectTermFactory.instance().createOrGetLiteral(12.0f);
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(12.0),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_DOUBLE).invoke(floatLiteral)
        );

        // Test with a valid double
        Literal<Double> doubleLiteral = SameObjectTermFactory.instance().createOrGetLiteral(12.34);
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(12.34),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_DOUBLE).invoke(doubleLiteral)
        );

        // Test with an invalid string
        Literal<String> invalidStringLiteral = SameObjectTermFactory.instance().createOrGetLiteral("invalid");
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_DOUBLE).invoke(invalidStringLiteral);
        });

        // Test with a non-literal argument (like a variable)
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_DOUBLE).invoke(
                    SameObjectTermFactory.instance().createOrGetVariable("X")
            );
        });

        // Test with multiple arguments
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_DOUBLE).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(1),
                    SameObjectTermFactory.instance().createOrGetLiteral(2)
            );
        });

        // Testing value instanceof String s not being true
        Literal<Object> nonNumericLiteral = SameObjectTermFactory.instance().createOrGetLiteral(new Object());
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_DOUBLE).invoke(nonNumericLiteral);
        });
    }

    @Test
    public void testToInt() {
        // Test with a valid string representing an integer
        Literal<String> stringIntegerLiteral = SameObjectTermFactory.instance().createOrGetLiteral("123");
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(123),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_INT).invoke(stringIntegerLiteral)
        );

        // Test with a valid float
        Literal<Float> floatLiteral = SameObjectTermFactory.instance().createOrGetLiteral(123.45f);
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(123),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_INT).invoke(floatLiteral)
        );

        // Test with a valid double
        Literal<Double> doubleLiteral = SameObjectTermFactory.instance().createOrGetLiteral(123.45);
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(123),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_INT).invoke(doubleLiteral)
        );

        // Test with a valid integer
        Literal<Integer> integerLiteral = SameObjectTermFactory.instance().createOrGetLiteral(123);
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(123),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_INT).invoke(integerLiteral)
        );

        // Test with a valid long
        Literal<Long> longLiteral = SameObjectTermFactory.instance().createOrGetLiteral(123L);
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(123),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_INT).invoke(longLiteral)
        );

        // Test with an invalid string
        Literal<String> invalidStringLiteral = SameObjectTermFactory.instance().createOrGetLiteral("invalid");
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_INT).invoke(invalidStringLiteral);
        });

        // Test with a non-literal argument (like a variable)
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_INT).invoke(
                    SameObjectTermFactory.instance().createOrGetVariable("X")
            );
        });

        // Test with multiple arguments
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_INT).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(1),
                    SameObjectTermFactory.instance().createOrGetLiteral(2)
            );
        });

        // Testing value instanceof String s not being true
        Literal<Object> nonNumericLiteral = SameObjectTermFactory.instance().createOrGetLiteral(new Object());
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_INT).invoke(nonNumericLiteral);
        });
    }

    @Test
    public void testToFloat() {
        // Test with a valid string representing a float
        Literal<String> stringFloatLiteral = SameObjectTermFactory.instance().createOrGetLiteral("12.34");
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(12.34f),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_FLOAT).invoke(stringFloatLiteral)
        );

        // Test with a valid integer
        Literal<Integer> integerLiteral = SameObjectTermFactory.instance().createOrGetLiteral(12);
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(12.0f),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_FLOAT).invoke(integerLiteral)
        );

        // Test with a valid long
        Literal<Long> longLiteral = SameObjectTermFactory.instance().createOrGetLiteral(12L);
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(12.0f),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_FLOAT).invoke(longLiteral)
        );

        // Test with a valid double
        Literal<Double> doubleLiteral = SameObjectTermFactory.instance().createOrGetLiteral(12.34);
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(12.34f),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_FLOAT).invoke(doubleLiteral)
        );

        // Test with an invalid string
        Literal<String> invalidStringLiteral = SameObjectTermFactory.instance().createOrGetLiteral("invalid");
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_FLOAT).invoke(invalidStringLiteral);
        });

        // Test with a non-literal argument (like a variable)
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_FLOAT).invoke(
                    SameObjectTermFactory.instance().createOrGetVariable("X")
            );
        });

        // Test with multiple arguments
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_FLOAT).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(1),
                    SameObjectTermFactory.instance().createOrGetLiteral(2)
            );
        });

        // Testing value instanceof String s not being true
        Literal<Object> nonNumericLiteral = SameObjectTermFactory.instance().createOrGetLiteral(new Object());
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_FLOAT).invoke(nonNumericLiteral);
        });
    }

    @Test
    public void testToLong() {
        // Test with a valid string representing a long
        Literal<String> stringLongLiteral = SameObjectTermFactory.instance().createOrGetLiteral("123456789");
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(123456789L),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_LONG).invoke(stringLongLiteral)
        );

        // Test with a valid integer
        Literal<Integer> integerLiteral = SameObjectTermFactory.instance().createOrGetLiteral(123);
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(123L),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_LONG).invoke(integerLiteral)
        );

        // Test with a valid float
        Literal<Float> floatLiteral = SameObjectTermFactory.instance().createOrGetLiteral(123.45f);
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(123L),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_LONG).invoke(floatLiteral)
        );

        // Test with a valid double
        Literal<Double> doubleLiteral = SameObjectTermFactory.instance().createOrGetLiteral(123456789.99);
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral(123456789L),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_LONG).invoke(doubleLiteral)
        );

        // Test with an invalid string
        Literal<String> invalidStringLiteral = SameObjectTermFactory.instance().createOrGetLiteral("invalid");
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_LONG).invoke(invalidStringLiteral);
        });

        // Test with a non-literal argument (like a variable)
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_LONG).invoke(
                    SameObjectTermFactory.instance().createOrGetVariable("X")
            );
        });

        // Test with multiple arguments
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_LONG).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(1),
                    SameObjectTermFactory.instance().createOrGetLiteral(2)
            );
        });

        // Testing value instanceof String s not being true
        Literal<Object> nonNumericLiteral = SameObjectTermFactory.instance().createOrGetLiteral(new Object());
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_LONG).invoke(nonNumericLiteral);
        });
    }

    @Test
    public void testToString() {
        // Test with a valid integer
        Literal<Integer> intLiteral = SameObjectTermFactory.instance().createOrGetLiteral(123);
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral("123"),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_STRING).invoke(intLiteral)
        );

        // Test with a valid double
        Literal<Double> doubleLiteral = SameObjectTermFactory.instance().createOrGetLiteral(12.34);
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral("12.34"),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_STRING).invoke(doubleLiteral)
        );

        // Test with a valid string
        Literal<String> stringLiteral = SameObjectTermFactory.instance().createOrGetLiteral("Hello");
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral("Hello"),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_STRING).invoke(stringLiteral)
        );

        // Test with a valid tuple (list)
        Literal<List<Term>> tupleLiteral = SameObjectTermFactory.instance().createOrGetLiteral(Arrays.asList(
                SameObjectTermFactory.instance().createOrGetLiteral(1),
                SameObjectTermFactory.instance().createOrGetLiteral(2),
                SameObjectTermFactory.instance().createOrGetLiteral(3)
        ));
        Assertions.assertEquals(
                SameObjectTermFactory.instance().createOrGetLiteral("[1, 2, 3]"),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_STRING).invoke(tupleLiteral)
        );

        // Test with multiple arguments
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_STRING).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("a"),
                    SameObjectTermFactory.instance().createOrGetLiteral("b")
            );
        });

        // Test with no arguments
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_STRING).invoke();
        });
    }

    @Test
    public void testToBooleanValidCases() {
        // Test numeric values
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_BOOLEAN).invoke(SameObjectTermFactory.instance().createOrGetLiteral(1)));

        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_BOOLEAN).invoke(SameObjectTermFactory.instance().createOrGetLiteral(0)));

        // Test string values
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_BOOLEAN).invoke(SameObjectTermFactory.instance().createOrGetLiteral("NonEmptyString")));

        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_BOOLEAN).invoke(SameObjectTermFactory.instance().createOrGetLiteral("False")));

        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_BOOLEAN).invoke(SameObjectTermFactory.instance().createOrGetLiteral("")));

        // Test collection (list)
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_BOOLEAN).invoke(SameObjectTermFactory.instance().createOrGetLiteral(Arrays.asList())));

        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_BOOLEAN).invoke(SameObjectTermFactory.instance().createOrGetLiteral(Arrays.asList(1, 2))));

        // Test set
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_BOOLEAN).invoke(SameObjectTermFactory.instance().createOrGetLiteral(new HashSet<>())));

        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_BOOLEAN).invoke(SameObjectTermFactory.instance().createOrGetLiteral(new HashSet<>(Arrays.asList(1)))));

        // Test map
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_BOOLEAN).invoke(SameObjectTermFactory.instance().createOrGetLiteral(new HashMap<>())));

        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_BOOLEAN).invoke(SameObjectTermFactory.instance().createOrGetLiteral(Map.of("key", "value"))));

        // Test cases for numeric string representation of zero
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_BOOLEAN).invoke(SameObjectTermFactory.instance().createOrGetLiteral("0")));

        // Test trimmed string of "0"
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_BOOLEAN).invoke(SameObjectTermFactory.instance().createOrGetLiteral(" 0 ")));

        // Test Map value case
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_BOOLEAN).invoke(SameObjectTermFactory.instance().createOrGetLiteral(Map.of())));

        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(false),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_BOOLEAN).invoke(SameObjectTermFactory.instance().createOrGetLiteral(null)));
    }

    @Test
    public void testToBooleanInvalidCases() {
        // Test multiple arguments
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_BOOLEAN).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("valid"),
                    SameObjectTermFactory.instance().createOrGetLiteral("extra")
            );
        });

        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_BOOLEAN).invoke(
                    SameObjectTermFactory.instance().createOrGetVariable("X")
            );
        });

        // Test empty input
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_BOOLEAN).invoke();
        });
    }

    @Test
    public void testIsTupleWithValidAndInvalidInputs() {
        // Valid tuples (lists of size 2)
        Literal<List<Term>> validTuple1 = SameObjectTermFactory.instance().createOrGetLiteral(
                Arrays.asList(SameObjectTermFactory.instance().createOrGetLiteral(2),
                        SameObjectTermFactory.instance().createOrGetLiteral(5)));

        Literal<List<Term>> validTuple2 = SameObjectTermFactory.instance().createOrGetLiteral(
                Arrays.asList(SameObjectTermFactory.instance().createOrGetLiteral(4),
                        SameObjectTermFactory.instance().createOrGetLiteral(5)));

        // Testing valid tuples through the weightedAverage invoker
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(3.0),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_AVERAGE).invoke(validTuple1, validTuple2));

        // Invalid tuple: not a List
        Term invalidTuple = SameObjectTermFactory.instance().createOrGetLiteral("notATuple");
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_AVERAGE).invoke(invalidTuple);
        });

        // Invalid tuple: empty list
        Literal<List<Term>> emptyTuple = SameObjectTermFactory.instance().createOrGetLiteral(List.of());
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_AVERAGE).invoke(emptyTuple);
        });

        // Invalid tuple: list with more than 2 elements
        Literal<List<Term>> largeTuple = SameObjectTermFactory.instance().createOrGetLiteral(
                Arrays.asList(SameObjectTermFactory.instance().createOrGetLiteral(1),
                        SameObjectTermFactory.instance().createOrGetLiteral(2),
                        SameObjectTermFactory.instance().createOrGetLiteral(3)));
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_AVERAGE).invoke(largeTuple);
        });

        // Invalid tuple: single element
        Literal<List<Term>> singleElementTuple = SameObjectTermFactory.instance().createOrGetLiteral(
                Collections.singletonList(SameObjectTermFactory.instance().createOrGetLiteral(1)));
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_AVERAGE).invoke(singleElementTuple);
        });

        // Invalid tuple: multiple single element
        Term multipleSingleElementTuple = SameObjectTermFactory.instance().createOrGetLiteral(List.of(
                SameObjectTermFactory.instance().createOrGetLiteral(List.of(
                        SameObjectTermFactory.instance().createOrGetLiteral(1))),
                        SameObjectTermFactory.instance().createOrGetLiteral(
                                List.of(SameObjectTermFactory.instance().createOrGetLiteral(1)))));
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_AVERAGE).invoke(multipleSingleElementTuple);
        });

        // Invalid tuple: not a tuple
        Literal<?> notTuple = SameObjectTermFactory.instance().createOrGetLiteral(
                Set.of(SameObjectTermFactory.instance().createOrGetLiteral(1)));
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_AVERAGE).invoke(notTuple);
        });

        // Invalid tuple: not a literal
        Term notLiteral = SameObjectTermFactory.instance().createOrGetVariable("X");
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.WEIGHTED_AVERAGE).invoke(notLiteral);
        });
    }

    @Test
    public void testParseNumbersWithValidAndInvalidInputs() {
        // Valid numbers in various forms
        Literal<Integer> validInt = SameObjectTermFactory.instance().createOrGetLiteral(10);
        Literal<Double> validDouble = SameObjectTermFactory.instance().createOrGetLiteral(20.5);
        Literal<Long> validLong = SameObjectTermFactory.instance().createOrGetLiteral(30L);
        Literal<String> validIntString = SameObjectTermFactory.instance().createOrGetLiteral("40");
        Literal<String> validDoubleString = SameObjectTermFactory.instance().createOrGetLiteral("50.5");

        // Testing valid numbers through the sum invoker
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(151.0),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.SUM).invoke(validInt, validDouble, validLong, validIntString, validDoubleString));

        // Invalid cases: Non-numeric strings
        Literal<String> invalidString = SameObjectTermFactory.instance().createOrGetLiteral("notANumber");
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.SUM).invoke(invalidString);
        });

        // Invalid case: Empty input
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.SUM).invoke();
        });

        // Invalid case: Null value
        Literal<?> nullValue = SameObjectTermFactory.instance().createOrGetLiteral(null);
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.SUM).invoke(nullValue);
        });

        // Testing a combination of valid and invalid inputs
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.SUM).invoke(validInt, invalidString);
        });
    }

    @Test
    public void testExtractLongValue() {
        // Valid long value
        Literal<Long> validLong = SameObjectTermFactory.instance().createOrGetLiteral(10L);
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_EVEN).invoke(validLong));

        // Valid integer value
        Literal<Integer> validInteger = SameObjectTermFactory.instance().createOrGetLiteral(10);
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_EVEN).invoke(validInteger));

        // Valid string representation of a long
        Literal<String> validString = SameObjectTermFactory.instance().createOrGetLiteral("10");
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(true),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_EVEN).invoke(validString));

        // Invalid string representation (non-numeric)
        Literal<String> invalidString = SameObjectTermFactory.instance().createOrGetLiteral("invalid");
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_EVEN).invoke(invalidString);
        });

        // Null literal
        Literal<?> nullLiteral = SameObjectTermFactory.instance().createOrGetLiteral(null);
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_EVEN).invoke(nullLiteral);
        });

        // Invalid case: not a number
        Literal<Set<?>> notANumber = SameObjectTermFactory.instance().createOrGetLiteral(Set.of());
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.IS_EVEN).invoke(notANumber);
        });
    }

    @Test
    public void testInvokerExceptionFormatting() {
        Term term1 = SameObjectTermFactory.instance().createOrGetLiteral(42);
        Term term2 = SameObjectTermFactory.instance().createOrGetLiteral("test");
        Term term3 = SameObjectTermFactory.instance().createOrGetLiteral(null); // Testing null term

        Term[] args = {term1, term2, term3};

        // Test the constructor with a cause
        Throwable cause = new RuntimeException("Original cause");
        InvokerException exceptionWithCause = new InvokerException("testFunction", args, cause);

        String expectedMessageWithCause = String.format(
                "[class fr.boreal.model.functions.IntegraalInvokers] Error when evaluating the computed function/predicate %s with arguments (%s): %s",
                "testFunction", "42, test, null", "Original cause");

        Assertions.assertEquals(expectedMessageWithCause, exceptionWithCause.getMessage());

        // Test the constructor with a custom message
        String customMessage = "A custom error occurred.";
        InvokerException exceptionWithMessage = new InvokerException("testFunction", args, customMessage);

        String expectedMessageWithMessage = String.format(
                "[class fr.boreal.model.functions.IntegraalInvokers] Error when evaluating the computed function/predicate %s with arguments (%s): %s",
                "testFunction", "42, test, null", customMessage);

        Assertions.assertEquals(expectedMessageWithMessage, exceptionWithMessage.getMessage());

        // Test the constructor with both message and cause
        InvokerException exceptionWithBoth = new InvokerException("testFunction", args, customMessage, cause);

        String expectedMessageWithBoth = String.format(
                "[class fr.boreal.model.functions.IntegraalInvokers] Error when evaluating the computed function/predicate %s with arguments (%s): %s",
                "testFunction", "42, test, null", String.format("Message: %s \n Cause: %s", customMessage, "Original cause"));

        Assertions.assertEquals(expectedMessageWithBoth, exceptionWithBoth.getMessage());
    }

    @Test
    public void testFormatArgs() {
        // Test with null args
        Term[] nullArgs = null;
        InvokerException exceptionWithNullArgs = new InvokerException("testFunction", nullArgs, "Some message");
        Assertions.assertEquals(
                "[class fr.boreal.model.functions.IntegraalInvokers] Error when evaluating the computed function/predicate testFunction with arguments (null): Some message",
                exceptionWithNullArgs.getMessage()
        );

        // Test with empty array
        Term[] emptyArgs = new Term[0];
        InvokerException exceptionWithEmptyArgs = new InvokerException("testFunction", emptyArgs, "Some message");
        Assertions.assertEquals(
                "[class fr.boreal.model.functions.IntegraalInvokers] Error when evaluating the computed function/predicate testFunction with arguments (): Some message",
                exceptionWithEmptyArgs.getMessage()
        );

        // Test with args containing literals
        Term term1 = SameObjectTermFactory.instance().createOrGetLiteral(42);
        Term term2 = SameObjectTermFactory.instance().createOrGetLiteral("test");
        Term term3 = SameObjectTermFactory.instance().createOrGetLiteral(null);

        Term[] validArgs = {term1, term2, term3};
        InvokerException exceptionWithValidArgs = new InvokerException("testFunction", validArgs, "Some message");
        Assertions.assertEquals(
                "[class fr.boreal.model.functions.IntegraalInvokers] Error when evaluating the computed function/predicate testFunction with arguments (42, test, null): Some message",
                exceptionWithValidArgs.getMessage()
        );

        // Test with a mixture of valid and null args
        Term[] mixedArgs = {term1, term3}; // term3 is a null literal
        InvokerException exceptionWithMixedArgs = new InvokerException("testFunction", mixedArgs, "Some message");
        Assertions.assertEquals(
                "[class fr.boreal.model.functions.IntegraalInvokers] Error when evaluating the computed function/predicate testFunction with arguments (42, null): Some message",
                exceptionWithMixedArgs.getMessage()
        );

        Term[] nullTerm = {null}; // term3 is a null literal
        InvokerException exceptionWithNullTerm = new InvokerException("testFunction", nullTerm, "Some message");
        Assertions.assertEquals(
                "[class fr.boreal.model.functions.IntegraalInvokers] Error when evaluating the computed function/predicate testFunction with arguments (null): Some message",
                exceptionWithNullTerm.getMessage()
        );
    }

    @Test
    public void testGetInvoker() {
        // Test each invoker by name
        for (IntegraalInvokers.StdInvoker invoker : IntegraalInvokers.StdInvoker.values()) {
            Invoker result = invokers.getInvoker(toCamelCase(invoker.name()));
            Assertions.assertNotNull(result, "Invoker should not be null for: " + invoker.name());
        }
    }

    @Test
    public void testGetInvokerInvalid() {
        // Test invalid invoker name
        Assertions.assertNull(invokers.getInvoker("invalidInvokerName"));
    }

    @Test
    public void testGetAllInvokerNames() {
        List<String> invokerNames = invokers.getAllInvokerNames();
        // Check that the size of the list is equal to the number of invokers
        Assertions.assertEquals(IntegraalInvokers.StdInvoker.values().length, invokerNames.size());

        // Check that all names match the expected camelCase format
        for (IntegraalInvokers.StdInvoker invoker : IntegraalInvokers.StdInvoker.values()) {
            Assertions.assertTrue(invokerNames.contains(toCamelCase(invoker.name())),
                    "Should contain invoker name: " + toCamelCase(invoker.name()));
        }
    }

    private String toCamelCase(String s) {
        String[] parts = s.toLowerCase().split("_");
        StringBuilder camelCaseString = new StringBuilder(parts[0]);
        for (int i = 1; i < parts.length; i++) {
            camelCaseString.append(parts[i].substring(0, 1).toUpperCase()).append(parts[i].substring(1));
        }
        return camelCaseString.toString();
    }
    @Test
    public void testToSetValidCases() {
        // Test with tuple containing duplicates
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2, 3)),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_SET).invoke(
                        SameObjectTermFactory.instance().createOrGetLiteral(Arrays.asList(1, 2, 2, 3))));

        // Test with empty tuple
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(Collections.emptySet()),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_SET).invoke(
                        SameObjectTermFactory.instance().createOrGetLiteral(Collections.emptyList())));

        // Test with set input
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(Set.of(1, 2)),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_SET).invoke(
                        SameObjectTermFactory.instance().createOrGetLiteral(new HashSet<>(Arrays.asList(1, 2)))));
    }

    @Test
    public void testToSetInvalidCases() {
        // Multiple arguments
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_SET).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(Arrays.asList(1, 2)),
                    SameObjectTermFactory.instance().createOrGetLiteral(3));
        });

        // Non-collection input
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_SET).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("not a collection"));
        });

        // No arguments
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_SET).invoke();
        });

        // Argument not a Literal instance
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_SET).invoke(
                    SameObjectTermFactory.instance().createOrGetVariable("X"));
        });
    }

    @Test
    public void testToTupleValidCases() {
        // Test with set input - order-independent comparison for tuple contents
        List<Integer> expectedTupleElements = Arrays.asList(1, 2, 3);
        Literal<List<Term>> resultTuple = (Literal<List<Term>>) invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_TUPLE).invoke(
                SameObjectTermFactory.instance().createOrGetLiteral(new HashSet<>(Arrays.asList(1, 2, 3))));

        Assertions.assertEquals(expectedTupleElements.size(), resultTuple.value().size());
        Assertions.assertTrue(resultTuple.value().containsAll(expectedTupleElements));

        // Test with empty set
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(Collections.emptyList()),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_TUPLE).invoke(
                        SameObjectTermFactory.instance().createOrGetLiteral(Collections.emptySet())));

        // Test with tuple input (order preserved)
        Assertions.assertEquals(SameObjectTermFactory.instance().createOrGetLiteral(Arrays.asList(1, 2, 2, 3)),
                invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_TUPLE).invoke(
                        SameObjectTermFactory.instance().createOrGetLiteral(Arrays.asList(1, 2, 2, 3))));
    }

    @Test
    public void testToTupleInvalidCases() {
        // Multiple arguments
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_TUPLE).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral(Arrays.asList(1, 2)),
                    SameObjectTermFactory.instance().createOrGetLiteral(3));
        });

        // Non-collection input
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_TUPLE).invoke(
                    SameObjectTermFactory.instance().createOrGetLiteral("not a collection"));
        });

        // No arguments
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_TUPLE).invoke();
        });

        // Argument not a Literal instance
        Assertions.assertThrows(InvokerException.class, () -> {
            invokers.getInvoker(IntegraalInvokers.StdInvoker.TO_TUPLE).invoke(
                    SameObjectTermFactory.instance().createOrGetVariable("Y"));
        });
    }
}