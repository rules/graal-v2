package fr.boreal.test.model.substitution;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.test.model.TestData;

@RunWith(Parameterized.class)
class SubstitutionTest extends TestData {

	@Parameters
	static Stream<Arguments> substitutionsAndResults() {
		return Stream.of(
				Arguments.of(pxy, Map.of(x, a), pay),
				Arguments.of(pxy, Map.of(y, a), pxa),
				Arguments.of(pxy, Map.of(u, a), pxy),
				Arguments.of(pxx, Map.of(x, a), paa),
				Arguments.of(pxy, Map.of(x, a, y, b), pab),
				Arguments.of(pxy, Map.of(x, y, y, x), pyx)
				);
	}

	@DisplayName("Apply substitution test")
	@ParameterizedTest(name = "{index} : {0} with {1} should be {2}")
	@MethodSource("substitutionsAndResults")
	public void applySubstitutionTest(Atom base, Map<Variable, Term> substitutionMap, Atom expected) {
		Substitution s = new SubstitutionImpl(substitutionMap);

		Atom image = s.createImageOf(base);
		Assert.assertEquals(expected, image);
	}

	@Parameters
	static Stream<Arguments> substitutionsAndLimiteds() {
		return Stream.of(
				Arguments.of(Map.of(x, a),
						Set.of(x),
						Map.of(x, a)),
				Arguments.of(Map.of(x, a),
						Set.of(y),
						Map.of()),
				Arguments.of(Map.of(x, y, y, x),
						Set.of(x),
						Map.of(x, y)),
				Arguments.of(Map.of(x, a, y, b),
						Set.of(x, y),
						Map.of(x, a, y, b)),
				Arguments.of(Map.of(),
						Set.of(x),
						Map.of())
				);
	}

	@DisplayName("Limit substitution test")
	@ParameterizedTest(name = "{index} : {0} limited to {1} should be {2}")
	@MethodSource("substitutionsAndLimiteds")
	public void limitSubstitutionTest(Map<Variable, Term> base, Collection<Variable> limit, Map<Variable, Term> expected) {
		Substitution baseSubstitution = new SubstitutionImpl(base);
		Substitution limitedSubstitution = baseSubstitution.limitedTo(limit);

		Substitution expectedSubstitution = new SubstitutionImpl(expected);

		Assert.assertEquals(expectedSubstitution, limitedSubstitution);
	}



	@Parameters
	static Stream<Arguments> substitutionsAndExtensions() {
		return Stream.of(
				Arguments.of(Map.of(x, a),
						Map.of(x, a)),
				Arguments.of(Map.of(x, a),
						Map.of()),
				Arguments.of(Map.of(x, y, y, x),
						Map.of(x, y)),
				Arguments.of(Map.of(x, a, y, b),
						Map.of(x, a, y, b)),
				Arguments.of(Map.of(),
						Map.of())
				);
	}

	@Parameters
	static Stream<Arguments> substitutionsAndFalseExtensions() {
		return Stream.of(
				Arguments.of(Map.of(x, a),
						Map.of(x, b)),
				Arguments.of(Map.of(),
						Map.of(x, a)),
				Arguments.of(Map.of(x, y),
						Map.of(x, y, y, x)),
				Arguments.of(Map.of(x, b, y, b),
						Map.of(x, a, y, b))
				);
	}

	@DisplayName("Extension substitution true test")
	@ParameterizedTest(name = "{index} : {0} should be an extension of {1}")
	@MethodSource("substitutionsAndExtensions")
	public void isExtensionsOfSubstitutionTest(Map<Variable, Term> extended, Map<Variable, Term> specialized) {
		Substitution extendedSubstitution = new SubstitutionImpl(extended);
		Substitution specializedSubstitution = new SubstitutionImpl(specialized);

		Assert.assertTrue(extendedSubstitution.isExtensionOf(specializedSubstitution));
	}

	@DisplayName("Extension substitution false test")
	@ParameterizedTest(name = "{index} : {0} should not be an extension of {1}")
	@MethodSource("substitutionsAndFalseExtensions")
	public void isNotExtensionsOfSubstitutionTest(Map<Variable, Term> extended, Map<Variable, Term> specialized) {
		Substitution extendedSubstitution = new SubstitutionImpl(extended);
		Substitution specializedSubstitution = new SubstitutionImpl(specialized);

		Assert.assertFalse(extendedSubstitution.isExtensionOf(specializedSubstitution));
	}



	@Parameters
	static Stream<Arguments> substitutionsAndMerge() {
		return Stream.of(
				Arguments.of(Map.of(x, a),
						Map.of(),
						Map.of(x, a)),
				Arguments.of(Map.of(),
						Map.of(x, a),
						Map.of(x, a)),
				Arguments.of(Map.of(x, y),
						Map.of(y, x),
						Map.of(x, y, y, x)),
				Arguments.of(Map.of(x, a, y, b),
						Map.of(x, a, z, a),
						Map.of(x, a, y, b, z, a))
				);
	}

	@Parameters
	static Stream<Arguments> substitutionsAndImpossibleMerge() {
		return Stream.of(
				Arguments.of(Map.of(x, a),
						Map.of(x, b)),
				Arguments.of(Map.of(x, y),
						Map.of(x, x)),
				Arguments.of(Map.of(x, a, y, b),
						Map.of(x, a, y, a))
				);
	}

	@DisplayName("Merged substitution test")
	@ParameterizedTest(name = "{index} : {0} merged with {1} should be {2}")
	@MethodSource("substitutionsAndMerge")
	public void mergedSubstitutionTest(Map<Variable, Term> base, Map<Variable, Term> extension, Map<Variable, Term> expected) {
		Substitution baseSubstitution = new SubstitutionImpl(base);
		Substitution extensionSubstitution = new SubstitutionImpl(extension);

		Optional<Substitution> mergedSubstitutionOpt = baseSubstitution.merged(extensionSubstitution);

		Assert.assertTrue(mergedSubstitutionOpt.isPresent());
		Substitution mergedSubstitution = mergedSubstitutionOpt.get();

		Substitution expectedSubstitution = new SubstitutionImpl(expected);
		Assert.assertEquals(expectedSubstitution, mergedSubstitution);
	}

	@DisplayName("Merged impossible substitution test")
	@ParameterizedTest(name = "{index} : {0} merged with {1} should not be possible")
	@MethodSource("substitutionsAndImpossibleMerge")
	public void mergedImpossibleSubstitutionTest(Map<Variable, Term> base, Map<Variable, Term> extension) {
		Substitution baseSubstitution = new SubstitutionImpl(base);
		Substitution extensionSubstitution = new SubstitutionImpl(extension);

		Optional<Substitution> mergedSubstitutionOpt = baseSubstitution.merged(extensionSubstitution);
		Assert.assertTrue(mergedSubstitutionOpt.isEmpty());
	}
}
