package fr.boreal.test.model.logicalElements.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import fr.boreal.model.logicalElements.api.*;
import fr.boreal.model.logicalElements.impl.functionalTerms.FunctionalTermHomomorphism;
import fr.boreal.model.logicalElements.impl.functionalTerms.GroundFunctionalTermImpl;
import fr.boreal.model.logicalElements.impl.functionalTerms.SpecializableLogicalFunctionalTermImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.boreal.model.functions.Invoker;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.functionalTerms.EvaluableFunctionImpl;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;

class FunctionalTermImplTest {

    private TermFactory termFactory;
    private Invoker invoker;

    @BeforeEach
    void setup() {
        termFactory = SameObjectTermFactory.instance();
        invoker = terms -> termFactory.createOrGetLiteral(true);  // Simple invoker for testing
    }


    @Test
    void testConstructor_NullInvokerThrowsException() {
        List<Term> terms = List.of(termFactory.createOrGetVariable("x"));
        assertThrows(NullPointerException.class, () -> new EvaluableFunctionImpl("testFunc", null, terms));
    }


    @Test
    void testEval_AllTermsAreLiterals_ReturnsInvokedTerm() {
        Term literalTerm = termFactory.createOrGetLiteral(true);
        List<Term> terms = List.of(literalTerm);
        EvaluableFunction term = new EvaluableFunctionImpl("testFunc", invoker, terms);

        Substitution substitution = new SubstitutionImpl();
        Term result = term.eval(substitution);

        assertEquals(literalTerm, result);
    }

    @Test
    void testEval_EmptySubstitution_ReturnsSameInstance() {
        Term variableTerm = termFactory.createOrGetVariable("x");
        List<Term> terms = List.of(variableTerm);
        EvaluableFunction term = new EvaluableFunctionImpl("testFunc", invoker, terms);

        Substitution emptySubstitution = new SubstitutionImpl();
        //evaluation must fail if the functional term is not fully instanciated
        assertThrows(RuntimeException.class, () -> term.eval(emptySubstitution));
    }


    @Test
    void testEval_NonEmptySubstitution_ReturnsEvaluatedLiteral() {
        Variable variableTerm = termFactory.createOrGetVariable("x");
        List<Term> terms = List.of(variableTerm);
        EvaluableFunction term = new EvaluableFunctionImpl("testFunc", invoker, terms);

        Literal<Boolean> literalValue = termFactory.createOrGetLiteral(true);
        Substitution substitution = new SubstitutionImpl();
        substitution.add(variableTerm, literalValue);

        Term result = term.eval(substitution);
        assertEquals(literalValue, result, "With a non-empty substitution, the evaluated term should match the literal in the substitution");
    }


    @Test
    void testHomomorphism_CompatibleTerms_ReturnsNonNullSubstitution() {
        Term varX = termFactory.createOrGetVariable("x");
        Term litTrue = termFactory.createOrGetLiteral(true);

        SpecializableLogicalFunctionalTerm term1 = new SpecializableLogicalFunctionalTermImpl("testFunc", List.of(varX));
        SpecializableLogicalFunctionalTerm term2 = new SpecializableLogicalFunctionalTermImpl("testFunc", List.of(litTrue));
        Substitution substitution = new SubstitutionImpl();

        Optional<Substitution> result = FunctionalTermHomomorphism.homomorphism(term1, term2, substitution);

        assertTrue(result.isPresent(), "Expected a non-empty substitution result.");
        assertEquals(litTrue, result.get().createImageOf(varX), "The substitution should map 'x' to true.");
    }


    @Test
    void testHomomorphism_IncompatibleTerms_ReturnsNull() {
        Term varX = termFactory.createOrGetVariable("x");
        Term litTrue = termFactory.createOrGetLiteral(true);

        SpecializableLogicalFunctionalTerm term1 = new SpecializableLogicalFunctionalTermImpl("testFunc", List.of(varX));
        SpecializableLogicalFunctionalTerm term2 = new SpecializableLogicalFunctionalTermImpl("differentFunc", List.of(litTrue));

        Substitution substitution = new SubstitutionImpl();
        Optional<Substitution> result = FunctionalTermHomomorphism.homomorphism(term1, term2, substitution);

        assertTrue(result.isEmpty(), "Expected an empty substitution result for incompatible terms.");
    }


    @Test
    void testGetVariables_ReturnsCorrectVariables() {
        Term varX = termFactory.createOrGetVariable("x");
        Term literalTerm = termFactory.createOrGetLiteral(true);

        SpecializableLogicalFunctionalTerm term = new SpecializableLogicalFunctionalTermImpl("testFunc", List.of(varX, literalTerm));
        Set<Variable> variables = term.getVariables();

        assertEquals(1, variables.size());
        assertTrue(variables.contains(varX));
    }


    @Test
    void testGetLiterals_ReturnsCorrectLiterals() {
        Term varX = termFactory.createOrGetVariable("x");
        Term literalTerm = termFactory.createOrGetLiteral(true);

        SpecializableLogicalFunctionalTerm term = new SpecializableLogicalFunctionalTermImpl("testFunc", List.of(varX, literalTerm));
        Set<Literal<?>> literals = term.getLiterals();

        assertEquals(1, literals.size());
        assertTrue(literals.contains(literalTerm));
    }

    @Test
    void testLabel_ThrowsIllegalArgumentException() {
        EvaluableFunction term = new EvaluableFunctionImpl("testFunc", invoker, List.of(termFactory.createOrGetVariable("x")));
        assertThrows(IllegalArgumentException.class, term::label);
    }

    @Test
    void testEquals_SameInstance() {
        EvaluableFunction term = new EvaluableFunctionImpl("testFunc", invoker, List.of(termFactory.createOrGetVariable("x")));
        assertEquals(term, term);
    }

    @Test
    void testEquals_DifferentInstancesSameValues() {
        Term varX = termFactory.createOrGetVariable("x");

        EvaluableFunction term1 = new EvaluableFunctionImpl("testFunc", invoker, List.of(varX));
        EvaluableFunction term2 = new EvaluableFunctionImpl("testFunc", invoker, List.of(varX));

        assertEquals(term1, term2);
        assertEquals(term1.hashCode(), term2.hashCode());
    }

    @Test
    void testEquals_DifferentFunctionNames() {
        Term varX = termFactory.createOrGetVariable("x");

        EvaluableFunction term1 = new EvaluableFunctionImpl("testFunc", invoker, List.of(varX));
        EvaluableFunction term2 = new EvaluableFunctionImpl("differentFunc", invoker, List.of(varX));

        assertNotEquals(term1, term2);
    }

    @Test
    void testEquals_DifferentTerms() {
        EvaluableFunction term1 = new EvaluableFunctionImpl("testFunc", invoker, List.of(termFactory.createOrGetVariable("x")));
        EvaluableFunction term2 = new EvaluableFunctionImpl("testFunc", invoker, List.of(termFactory.createOrGetVariable("y")));

        assertNotEquals(term1, term2);
    }

    @Test
    void testToString_ReturnsExpectedFormat() {
        Term varX = termFactory.createOrGetVariable("x");
        Term literalTerm = termFactory.createOrGetLiteral(true);

        EvaluableFunction term = new EvaluableFunctionImpl("testFunc", invoker, List.of(varX, literalTerm));
        String result = term.toString();

        assertEquals("testFunc(x, true)", result);
    }

    @Test
    void testEval_NonLiteralTerms_RaisesException() {
        // Initial setup: creating a FunctionalTermImpl with a variable and a literal
        Variable variableTerm = termFactory.createOrGetVariable("x");
        Term literalTerm = termFactory.createOrGetLiteral(42);
        List<Term> subTerms = List.of(variableTerm, literalTerm);
        EvaluableFunction functionalTerm = new EvaluableFunctionImpl("testFunc", invoker, subTerms);

        // Create a substitution where the variable is mapped to a non-literal term (another functional term)
        Term nonLiteralSubstitute = new EvaluableFunctionImpl("nonLiteralFunc", invoker, List.of(literalTerm));
        Substitution substitution = new SubstitutionImpl();
        substitution.add(variableTerm, nonLiteralSubstitute);

        Assertions.assertThrows(RuntimeException.class, () -> functionalTerm.eval(substitution));
    }

    @Test
    void testHomomorphism_DifferentFunctionNames_ReturnsEmptyOptional() {
        LogicalFunctionalTerm term1 = new SpecializableLogicalFunctionalTermImpl("functionA", List.of(termFactory.createOrGetVariable("x")));
        LogicalFunctionalTerm term2 = new SpecializableLogicalFunctionalTermImpl("functionB", List.of(termFactory.createOrGetVariable("x")));
        Substitution substitution = new SubstitutionImpl();

        // Test with different function names
        Optional<Substitution> result = FunctionalTermHomomorphism.homomorphism(term1, term2, substitution);
        assertTrue(result.isEmpty(), "Expected an empty Optional when function names differ.");
    }

    @Test
    void testHomomorphism_TermsNotMatching_ReturnsEmptyOptional() {
        LogicalFunctionalTerm term1 = new SpecializableLogicalFunctionalTermImpl("functionA", List.of(termFactory.createOrGetLiteral(42)));
        LogicalFunctionalTerm term3 = new GroundFunctionalTermImpl("functionA", List.of(termFactory.createOrGetLiteral(43)));
        LogicalFunctionalTerm term2 = new SpecializableLogicalFunctionalTermImpl("functionA", List.of(termFactory.createOrGetVariable("y")));
        Substitution substitution = new SubstitutionImpl();

        // Test with non-matching terms
        Optional<Substitution> result = FunctionalTermHomomorphism.homomorphism(term1, term2, substitution);
        assertTrue(result.isEmpty(), "Expected an empty Optional when terms do not match.");

        result = FunctionalTermHomomorphism.homomorphism(term3, term2, substitution);
        assertTrue(result.isEmpty(), "Expected an empty Optional when terms do not match.");

        // Test matching terms
        result = FunctionalTermHomomorphism.homomorphism(term2, term1, substitution);
        assertTrue(result.isPresent(), "Expected an empty Optional when terms do not match.");

        result = FunctionalTermHomomorphism.homomorphism(term2, term3, substitution);
        assertTrue(result.isPresent(), "Expected an empty Optional when terms do not match.");

    }

    @Test
    void testHomomorphism_FrozenTermMismatch_ReturnsEmptyOptional() {
        Variable variableX = termFactory.createOrGetVariable("x");
        LogicalFunctionalTerm term1 = new SpecializableLogicalFunctionalTermImpl("functionA", List.of(variableX));
        LogicalFunctionalTerm term2 = new SpecializableLogicalFunctionalTermImpl("functionA", List.of(termFactory.createOrGetLiteral(99)));
        Substitution substitution = new SubstitutionImpl();
        substitution.add(variableX, termFactory.createOrGetLiteral(42));  // Setting the substitution to 42

        // Test when frozen term in `s` does not match `targetTerm`
        Optional<Substitution> result = FunctionalTermHomomorphism.homomorphism(term1, term2, substitution);
        assertTrue(result.isEmpty(), "Expected an empty Optional when frozen term does not match substitution.");
    }


    @Test
    public void testHomomorphism_NestedFunctionalTermMismatch_ReturnsNoHomomorphism() {
        Variable x = SameObjectTermFactory.instance().createOrGetVariable("x");
        LogicalFunctionalTerm term1 = new SpecializableLogicalFunctionalTermImpl("testFunc", List.of(x));

        // Create a nested functional term that does not match
        LogicalFunctionalTerm nestedTerm = new SpecializableLogicalFunctionalTermImpl("nonMatchingFunc", List.of(SameObjectTermFactory.instance().createOrGetConstant("42")));

        // Create a substitution that maps the variable x to a Literal
        Substitution substitution = new SubstitutionImpl();
        substitution.add(x, SameObjectTermFactory.instance().createOrGetLiteral(99)); // Assuming createOrGetLiteral returns a Literal<T>

        // Create a second functional term that contains the nested term
        LogicalFunctionalTerm term2 = new SpecializableLogicalFunctionalTermImpl("testFunc", List.of(nestedTerm));

        // Test the homomorphism
        Assertions.assertTrue(FunctionalTermHomomorphism.homomorphism(term1, term2, substitution).isEmpty());

    }

    @Test
    void testHomomorphism_SuccessfulHomomorphism() {
        Variable variableX = termFactory.createOrGetVariable("x");
        LogicalFunctionalTerm term1 = new SpecializableLogicalFunctionalTermImpl("functionA", List.of(variableX));
        LogicalFunctionalTerm term2 = new SpecializableLogicalFunctionalTermImpl("functionA", List.of(termFactory.createOrGetLiteral(42)));
        Substitution substitution = new SubstitutionImpl();

        // Test for successful homomorphism
        Optional<Substitution> result = FunctionalTermHomomorphism.homomorphism(term1, term2, substitution);

        assertTrue(result.isPresent(), "Expected a non-empty result for a successful homomorphism.");
        assertEquals(termFactory.createOrGetLiteral(42), result.get().createImageOf(variableX), "The substitution should map 'x' to 42.");
    }

    @Test
    void testHomomorphism_ValidFunctionalTerm_ReturnsSubstitution() {
        TermFactory termFactory = SameObjectTermFactory.instance();

        // Create variable and literal using the factory
        Variable x = termFactory.createOrGetVariable("x");
        Literal<Integer> literal42 = termFactory.createOrGetLiteral(42);

        // Create functional terms
        LogicalFunctionalTerm term1 = new SpecializableLogicalFunctionalTermImpl("testFunc", List.of(x));
        LogicalFunctionalTerm term2 = new SpecializableLogicalFunctionalTermImpl("testFunc", List.of(literal42));

        // Create a substitution
        Substitution substitution = new SubstitutionImpl();
        substitution.add(x, literal42);

        // Perform homomorphism
        Optional<Substitution> result = FunctionalTermHomomorphism.homomorphism(term1, term2, substitution);


        // Check if the result contains the correct substitution
        assertTrue(result.isPresent());
        assertEquals(1, result.get().keys().size());
        assertEquals(literal42, result.get().createImageOf(x));

        // Perform homomorphism
        result = FunctionalTermHomomorphism.homomorphism(term1, term2, new SubstitutionImpl());

        // Check if the result contains the correct substitution
        assertTrue(result.isPresent());
        assertEquals(1, result.get().keys().size());
        assertEquals(literal42, result.get().createImageOf(x));
    }

    @Test
    public void testHomomorphism_MismatchedFunctionalTerm_ReturnsNull() {
        Variable x = SameObjectTermFactory.instance().createOrGetVariable("x");
        LogicalFunctionalTerm term1 = new SpecializableLogicalFunctionalTermImpl("testFunc", List.of(x));

        // Create a mismatched functional term
        LogicalFunctionalTerm term2 = new SpecializableLogicalFunctionalTermImpl("nonMatchingFunc", List.of(SameObjectTermFactory.instance().createOrGetLiteral(99)));

        // Create a substitution
        Substitution substitution = new SubstitutionImpl();
        substitution.add(x, SameObjectTermFactory.instance().createOrGetLiteral(42));

        // Call homomorphism
        Optional<Substitution> result = FunctionalTermHomomorphism.homomorphism(term1, term2, substitution);


        // Assert that the result is empty due to mismatched terms
        assertTrue(result.isEmpty());

        // Call homomorphism
        result = FunctionalTermHomomorphism.homomorphism(term1, term2, new SubstitutionImpl());

        // Assert that the result is empty due to mismatched terms
        assertTrue(result.isEmpty());
    }


    @Test
    void testHomomorphism_DifferentNumberOfSubTerms_ReturnsEmptyOptional() {
        // Create a functional term with one variable
        LogicalFunctionalTerm term1 = new SpecializableLogicalFunctionalTermImpl("functionA",
                List.of(termFactory.createOrGetVariable("x")));

        // Create another functional term with two variables
        LogicalFunctionalTerm term2 = new SpecializableLogicalFunctionalTermImpl("functionA",
                List.of(termFactory.createOrGetVariable("x"), termFactory.createOrGetVariable("y")));

        Substitution substitution = new SubstitutionImpl();

        // Test with different number of sub-terms
        Optional<Substitution> result = FunctionalTermHomomorphism.homomorphism(term1, term2, substitution);

        assertTrue(result.isEmpty(), "Expected an empty Optional when the number of sub-terms differs.");
    }

    @Test
    void testHomomorphism_FrozenTermMatches_ReturnsSubstitution() {
        // Create a variable and a literal using the factory
        Variable variableX = termFactory.createOrGetVariable("x");
        Literal<Integer> literal42 = termFactory.createOrGetLiteral(42);

        // Create a functional term that uses the variable
        LogicalFunctionalTerm term1 = new SpecializableLogicalFunctionalTermImpl("functionA", List.of(variableX));

        // Create a corresponding functional term with the literal
        LogicalFunctionalTerm term2 = new SpecializableLogicalFunctionalTermImpl("functionA", List.of(literal42));

        // Create a substitution and set variableX to literal42
        Substitution substitution = new SubstitutionImpl();

        // Call homomorphism
        Optional<Substitution> result = FunctionalTermHomomorphism.homomorphism(term1, term2, substitution);


        // Assert that the result is present and contains the expected mapping
        assertTrue(result.isPresent(), "Expected a non-empty Optional for matching frozen terms.");
        assertEquals(literal42, result.get().createImageOf(variableX), "The substitution should map 'x' to 42.");

        substitution.add(variableX, literal42);

        // Call homomorphism
        result = FunctionalTermHomomorphism.homomorphism(term1, term2, substitution);


        // Assert that the result is present and contains the expected mapping
        assertTrue(result.isPresent(), "Expected a non-empty Optional for matching frozen terms.");
        assertEquals(literal42, result.get().createImageOf(variableX), "The substitution should map 'x' to 42.");
    }

    @Test
    void testHomomorphism_NestedFunctionalTermMatches_ReturnsSubstitution() {
        // Create a variable and a literal using the factory
        Variable variableX = termFactory.createOrGetVariable("x");
        Literal<Integer> literal42 = termFactory.createOrGetLiteral(42);

        // Create a nested functional term
        LogicalFunctionalTerm nestedTerm1 = new SpecializableLogicalFunctionalTermImpl("nestedFunc", List.of(variableX));
        LogicalFunctionalTerm nestedTerm2 = new SpecializableLogicalFunctionalTermImpl("nestedFunc", List.of(literal42));

        // Create the outer functional terms
        LogicalFunctionalTerm term1 = new SpecializableLogicalFunctionalTermImpl("functionA", List.of(nestedTerm1));
        LogicalFunctionalTerm term2 = new SpecializableLogicalFunctionalTermImpl("functionA", List.of(nestedTerm2));

        // Create a substitution mapping
        Substitution substitution = new SubstitutionImpl();
        substitution.add(variableX, literal42);

        // Call homomorphism
        Optional<Substitution> result = FunctionalTermHomomorphism.homomorphism(term1, term2, substitution);


        // Assert that the result is present and contains the expected mapping
        assertTrue(result.isPresent(), "Expected a non-empty Optional for matching nested functional terms.");
        assertEquals(literal42, result.get().createImageOf(variableX), "The substitution should map 'x' to 42.");

        // Call homomorphism
        result = FunctionalTermHomomorphism.homomorphism(term1, term2, new SubstitutionImpl());

        // Assert that the result is present and contains the expected mapping
        assertTrue(result.isPresent(), "Expected a non-empty Optional for matching nested functional terms.");
        assertEquals(literal42, result.get().createImageOf(variableX), "The substitution should map 'x' to 42.");
    }

    @Test
    void testHomomorphism_NestedFunctionalTerms_ReturnsValidSubstitution() {
        Variable x = termFactory.createOrGetVariable("x");
        EvaluableFunction nestedTerm = new EvaluableFunctionImpl("nestedFunc", terms -> terms[0], List.of(termFactory.createOrGetLiteral(5)));
        EvaluableFunction term1 = new EvaluableFunctionImpl("outerFunc", invoker, List.of(x));
        EvaluableFunction term2 = new EvaluableFunctionImpl("outerFunc", invoker, List.of(nestedTerm));

        Substitution substitution = new SubstitutionImpl();

        // compare the terms by their structure
        Optional<Substitution> result = FunctionalTermHomomorphism.homomorphism(term1.asLogicalFunctionalTerm(), term2.asLogicalFunctionalTerm(), substitution);
        assertTrue(result.isPresent());
    }

    @Test
    void testHomomorphism_FrozenTermWithEvaluation_ReturnsValidSubstitution() {
        Variable x = termFactory.createOrGetVariable("x");
        EvaluableFunction term1 = new EvaluableFunctionImpl("evalFunc", invoker, List.of(x));
        EvaluableFunction term2 = new EvaluableFunctionImpl("evalFunc", invoker, List.of(termFactory.createOrGetLiteral(10)));

        Substitution substitution = new SubstitutionImpl();

        Optional<Substitution> result = FunctionalTermHomomorphism.homomorphism(term1.asLogicalFunctionalTerm(), term2.asLogicalFunctionalTerm(), substitution);


        assertTrue(result.isPresent());
        assertEquals(1, result.get().keys().size());
        assertEquals(termFactory.createOrGetLiteral(10), result.get().createImageOf(x));

        substitution.add(x, termFactory.createOrGetLiteral(10));

        result = FunctionalTermHomomorphism.homomorphism(term1.asLogicalFunctionalTerm(), term2.asLogicalFunctionalTerm(), substitution);


        assertTrue(result.isPresent());
        assertEquals(1, result.get().keys().size());
        assertEquals(termFactory.createOrGetLiteral(10), result.get().createImageOf(x));
    }


    @Test
    void testHomomorphism_WithMixedTerms_ReturnsNull() {
        Variable x = termFactory.createOrGetVariable("x");
        EvaluableFunction term1 = new EvaluableFunctionImpl("mixedFunc", invoker, List.of(x));
        EvaluableFunction term2 = new EvaluableFunctionImpl("mixedFunc", invoker, List.of(termFactory.createOrGetLiteral(20)));

        Substitution substitution = new SubstitutionImpl();
        substitution.add(x, termFactory.createOrGetLiteral(30)); // Mismatch

        Optional<Substitution> result = FunctionalTermHomomorphism.homomorphism(term1.asLogicalFunctionalTerm(), term2.asLogicalFunctionalTerm(), substitution);


        assertFalse(result.isPresent());
    }

    @Test
    void testHomomorphism_NestedFunctionalTerms_SuccessfulMatch() {
        TermFactory termFactory = SameObjectTermFactory.instance();

        // Create a variable and a nested functional term
        Variable x = termFactory.createOrGetVariable("x");
        EvaluableFunction innerFunc = new EvaluableFunctionImpl("innerFunc", invoker, List.of(termFactory.createOrGetLiteral(10)));

        // Create the outer functional term
        EvaluableFunction term1 = new EvaluableFunctionImpl("outerFunc", invoker, List.of(innerFunc));
        EvaluableFunction term2 = new EvaluableFunctionImpl("outerFunc", invoker, List.of(innerFunc));

        // Create a substitution
        Substitution substitution = new SubstitutionImpl();

        // Perform homomorphism
        Optional<Substitution> result = FunctionalTermHomomorphism.homomorphism(term1.asLogicalFunctionalTerm(), term2.asLogicalFunctionalTerm(), substitution);


        // Assert that the result is present and valid
        assertTrue(result.isPresent(), "Expected a non-empty result for nested functional terms.");
        assertTrue(result.get().isEmpty(), "Expected an empty substitution since there are no variable mappings.");
    }


    @Test
    void testHomomorphism_NestedFunctionalTerms_WithVariableMapping() {
        TermFactory termFactory = SameObjectTermFactory.instance();

        // Create variable x and a nested functional term
        Variable x = termFactory.createOrGetVariable("x");
        EvaluableFunction innerFunc = new EvaluableFunctionImpl("innerFunc", invoker, List.of(x));

        // Create the outer functional term with the nested functional term
        EvaluableFunction term1 = new EvaluableFunctionImpl("outerFunc", invoker, List.of(innerFunc));

        // Create a substitution that maps x to a literal
        Substitution substitution = new SubstitutionImpl();
        substitution.add(x, termFactory.createOrGetLiteral(99));

        // Create a matching outer term
        EvaluableFunction term2 = new EvaluableFunctionImpl("outerFunc",
                invoker, List.of(substitution.createImageOf((innerFunc))));

        // Perform homomorphism
        Optional<Substitution> result = FunctionalTermHomomorphism.homomorphism(term1.asLogicalFunctionalTerm(), term2.asLogicalFunctionalTerm(), substitution);


        // Assert that the result is present and that it maps x to 99
        assertTrue(result.isPresent(), "Expected a non-empty result for matching nested functional terms.");
        assertEquals(1, result.get().keys().size());
        assertEquals(termFactory.createOrGetLiteral(99), result.get().createImageOf(x), "Expected substitution to map x to 99.");
    }

    @Test
    void testHomomorphism_SourceFunctionalTermWithNestedFunctionalTerm_TargetNonFunctionalTerm() {
        TermFactory termFactory = SameObjectTermFactory.instance();

        // Create a variable
        Variable x = termFactory.createOrGetVariable("x");

        // Create a nested functional term
        EvaluableFunction nestedFunc = new EvaluableFunctionImpl("nestedFunc", invoker, List.of(x));

        // Create a source functional term that contains the nested functional term
        EvaluableFunction sourceTerm = new EvaluableFunctionImpl("sourceFunc", invoker, List.of(nestedFunc));

        // Create a target functional term that does not contain any functional terms
        EvaluableFunction targetTerm = new EvaluableFunctionImpl("sourceFunc", invoker, List.of(termFactory.createOrGetLiteral(99)));

        // Create a substitution that maps the variable x to a literal
        Substitution substitution = new SubstitutionImpl();

        // Perform homomorphism
        Optional<Substitution> result = FunctionalTermHomomorphism.homomorphism(sourceTerm.asLogicalFunctionalTerm(), targetTerm.asLogicalFunctionalTerm(), substitution);

        // Assert that the result is empty due to the mismatch in functional terms
        assertFalse(result.isPresent(), "Expected an empty result when the source term is functional but the target term is not.");
    }

    @Test
    void testHomomorphism_InnerFunctionalTermMismatch_ReturnsEmptyOptional() {
        TermFactory termFactory = SameObjectTermFactory.instance();

        // Create a variable
        Variable x = termFactory.createOrGetVariable("x");

        // Create a nested functional term that maps x
        EvaluableFunction nestedFunc = new EvaluableFunctionImpl("nestedFunc", invoker, List.of(x));

        // Create a source functional term with the nested functional term
        EvaluableFunction sourceTerm = new EvaluableFunctionImpl("sourceFunc", invoker, List.of(nestedFunc));

        // Create a target functional term that has a different nested functional term
        EvaluableFunction differentNestedFunc = new EvaluableFunctionImpl("differentNestedFunc", invoker, List.of(termFactory.createOrGetLiteral(99)));
        EvaluableFunction targetTerm = new EvaluableFunctionImpl("sourceFunc", invoker, List.of(differentNestedFunc));

        // Create a substitution that does not match the inner functional term
        Substitution substitution = new SubstitutionImpl();

        // Perform homomorphism
        Optional<Substitution> result = FunctionalTermHomomorphism.homomorphism(sourceTerm.asLogicalFunctionalTerm(), targetTerm.asLogicalFunctionalTerm(), substitution);

        // Assert that the result is empty due to the mismatch in nested functional terms
        assertFalse(result.isPresent(), "Expected an empty result due to a mismatch in nested functional terms.");
    }

    @Test
    void testHomomorphism_MergingFails_ReturnsEmptyOptional() {
        TermFactory termFactory = SameObjectTermFactory.instance();

        // Create variables
        Variable x = termFactory.createOrGetVariable("x");
        Variable y = termFactory.createOrGetVariable("y");
        Variable z = termFactory.createOrGetVariable("z");

        // Create a nested functional term that references x and y
        EvaluableFunction nestedFunc1 = new EvaluableFunctionImpl("g", invoker, List.of(x, y));

        // Create a source functional term that contains the nested functional term
        EvaluableFunction sourceTerm = new EvaluableFunctionImpl("f", invoker, List.of(x, nestedFunc1));

        // Create a target functional term with conflicting mappings
        EvaluableFunction nestedFunc2 = new EvaluableFunctionImpl("g", invoker, List.of(termFactory.createOrGetLiteral(41), z));
        EvaluableFunction targetTerm = new EvaluableFunctionImpl("f", invoker, List.of(termFactory.createOrGetLiteral(42), nestedFunc2));

        // Create a substitution that creates a conflict during merging
        Substitution substitution = new SubstitutionImpl();

        // Perform homomorphism
        Optional<Substitution> result = FunctionalTermHomomorphism.homomorphism(sourceTerm.asLogicalFunctionalTerm(), targetTerm.asLogicalFunctionalTerm(), substitution);


        // Assert that the result is empty due to the conflict during merging
        assertFalse(result.isPresent(), "Expected an empty result due to a conflict in substitution merging.");
    }

    @Test
    void testHomomorphism_VariableMapsCorrectly_ReturnsSubstitution() {
        TermFactory termFactory = SameObjectTermFactory.instance();

        // Create variables
        Variable x = termFactory.createOrGetVariable("x");
        Variable y = termFactory.createOrGetVariable("y");
        Variable z = termFactory.createOrGetVariable("z");

        // Create source and target functional terms
        EvaluableFunction sourceTerm = new EvaluableFunctionImpl("f", invoker, List.of(x, x));
        EvaluableFunction targetTerm = new EvaluableFunctionImpl("f", invoker, List.of(y, y));

        // Create a substitution that maps x to y
        Substitution substitution = new SubstitutionImpl();

        // Perform homomorphism
        Optional<Substitution> result = FunctionalTermHomomorphism.homomorphism(sourceTerm.asLogicalFunctionalTerm(), targetTerm.asLogicalFunctionalTerm(), substitution);


        // Assert that the result is present and maps x to y
        assertTrue(result.isPresent(), "Expected a non-empty result since x maps to y.");
        assertEquals(1, result.get().keys().size(), "Expected a single substitution to be present.");
        assertEquals(y, result.get().createImageOf(x), "Expected substitution to map x to y.");

        // Create source and target functional terms
        sourceTerm = new EvaluableFunctionImpl("f", invoker, List.of(x, x));
        targetTerm = new EvaluableFunctionImpl("f", invoker, List.of(y, z));

        // Perform homomorphism
        result = FunctionalTermHomomorphism.homomorphism(sourceTerm.asLogicalFunctionalTerm(), targetTerm.asLogicalFunctionalTerm(), substitution);


        // Assert that the result is present and maps x to y
        assertFalse(result.isPresent(), "Expected an empty result since f(x, x) cannot be mapped to f(y,z).");
    }

    @Test
    void testHomomorphism_TargetFunctionalTermWithNestedEvaluation() {
        TermFactory termFactory = SameObjectTermFactory.instance();

        // Create variables
        Variable x = termFactory.createOrGetVariable("x");
        Term y = termFactory.createOrGetLiteral(true);

        // Create a source functional term that contains the nested functional term
        EvaluableFunction sourceTerm = new EvaluableFunctionImpl("outerFunc", invoker, List.of(x, x));

        // Create a target functional term with a nested term that will be evaluated
        EvaluableFunction targetTerm = new EvaluableFunctionImpl("outerFunc", invoker, List.of(
                y, new EvaluableFunctionImpl("nestedFunc", invoker, List.of(termFactory.createOrGetLiteral(42)))
        ));

        Substitution substitution = new SubstitutionImpl();

        // Perform homomorphism
        Optional<Substitution> result = FunctionalTermHomomorphism.homomorphism(
                sourceTerm.asLogicalFunctionalTerm(),
                targetTerm.evaluateNestedFunctions(substitution).asLogicalFunctionalTerm(), substitution);


        // Assert that the result is present and that it correctly maps x to the evaluated value of targetTerm
        assertTrue(result.isPresent(), "Expected a non-empty result for matching nested functional terms.");
        assertEquals(1, result.get().keys().size(), "Expected a single substitution to be present.");
        assertEquals(termFactory.createOrGetLiteral(true), result.get().createImageOf(x), "Expected substitution to map x to the evaluated literal 42.");
    }

    @Test
    void testGetLiterals_WithNestedFunctionalTerms() {
        TermFactory termFactory = SameObjectTermFactory.instance();

        // Create literals
        Literal<Integer> literal1 = termFactory.createOrGetLiteral(42);
        Literal<Integer> literal2 = termFactory.createOrGetLiteral(99);

        // Create a functional term containing the literals
        EvaluableFunction innerFunctionalTerm = new EvaluableFunctionImpl("innerFunc", invoker, List.of(literal1));

        // Create a top-level functional term containing the inner functional term and another literal
        EvaluableFunction outerFunctionalTerm = new EvaluableFunctionImpl("outerFunc", invoker, List.of(innerFunctionalTerm, literal2));

        // Get the literals from the outer functional term
        Set<Literal<?>> resultLiterals = outerFunctionalTerm.asLogicalFunctionalTerm().getLiterals();

        // Assert that the correct literals are returned
        assertEquals(2, resultLiterals.size(), "Expected two literals to be collected.");
        assertTrue(resultLiterals.contains(literal1), "Expected to find literal1 in the result.");
        assertTrue(resultLiterals.contains(literal2), "Expected to find literal2 in the result.");
    }

    @Test
    void testEquals_NullObject_ReturnsFalse() {
        EvaluableFunction term = new EvaluableFunctionImpl("testFunc", invoker, List.of());
        assertFalse(term.equals(null), "Expected false when comparing with null.");
    }

    @Test
    void testEquals_SameNameDifferentNumberOfArguments_ReturnsFalse() {
        EvaluableFunction term1 = new EvaluableFunctionImpl("testFunc", invoker, List.of(termFactory.createOrGetVariable("x")));
        EvaluableFunction term2 = new EvaluableFunctionImpl("testFunc", invoker, List.of(termFactory.createOrGetVariable("x"), termFactory.createOrGetLiteral(42)));

        assertFalse(term1.equals(term2), "Expected false when the terms have the same name but different number of arguments.");
    }

    @Test
    void testEquals_DifferentName_ReturnsFalse() {
        EvaluableFunction term1 = new EvaluableFunctionImpl("testFunc", invoker, List.of(termFactory.createOrGetVariable("x")));
        EvaluableFunction term2 = new EvaluableFunctionImpl("otherFunc", invoker, List.of(termFactory.createOrGetVariable("x")));

        assertFalse(term1.equals(term2), "Expected false when the terms have different names.");
    }

    @Test
    void testEquals_SameNameAndSameNumberOfArguments_ReturnsTrue() {
        EvaluableFunction term1 = new EvaluableFunctionImpl("testFunc", invoker, List.of(termFactory.createOrGetVariable("x")));
        EvaluableFunction term2 = new EvaluableFunctionImpl("testFunc", invoker, List.of(termFactory.createOrGetVariable("x")));

        assertTrue(term1.equals(term2), "Expected true when both terms have the same name and the same arguments.");
    }

    @Test
    void testEquals_SameNameDifferentArgumentValues_ReturnsFalse() {
        EvaluableFunction term1 = new EvaluableFunctionImpl("testFunc", invoker, List.of(termFactory.createOrGetVariable("x")));
        EvaluableFunction term2 = new EvaluableFunctionImpl("testFunc", invoker, List.of(termFactory.createOrGetLiteral(99))); // Different argument

        assertFalse(term1.equals(term2), "Expected false when the terms have the same name but different argument values.");
    }

    @Test
    void testEquals_ExtraArgumentsInOtherTerm_ReturnsFalse() {
        EvaluableFunction term1 = new EvaluableFunctionImpl("testFunc", invoker, List.of(termFactory.createOrGetVariable("x")));
        EvaluableFunction term2 = new EvaluableFunctionImpl("testFunc", invoker, List.of(termFactory.createOrGetVariable("x"), termFactory.createOrGetLiteral(42)));

        assertFalse(term1.equals(term2), "Expected false when the other term has extra arguments.");

        term1 = new EvaluableFunctionImpl("testFunc", invoker, List.of(termFactory.createOrGetVariable("x"), termFactory.createOrGetLiteral(42)));
        term2 = new EvaluableFunctionImpl("testFunc", invoker, List.of(termFactory.createOrGetVariable("x")));

        assertFalse(term1.equals(term2), "Expected false when the other term has less arguments.");
    }

    @Test
    void testEquals_DifferentArguments_ReturnsFalse() {
        EvaluableFunction term1 = new EvaluableFunctionImpl("testFunc", invoker, List.of(termFactory.createOrGetVariable("x")));
        EvaluableFunction term2 = new EvaluableFunctionImpl("testFunc", invoker, List.of(termFactory.createOrGetVariable("y"))); // Different variable

        assertFalse(term1.equals(term2), "Expected false when the terms have the same name but different argument values.");
    }

    @Test
    void testEquals_SameArgumentsDifferentOrder_ReturnsFalse() {
        EvaluableFunction term1 = new EvaluableFunctionImpl("testFunc", invoker, List.of(termFactory.createOrGetVariable("x"), termFactory.createOrGetVariable("y")));
        EvaluableFunction term2 = new EvaluableFunctionImpl("testFunc", invoker, List.of(termFactory.createOrGetVariable("y"), termFactory.createOrGetVariable("x"))); // Same vars, different order

        assertFalse(term1.equals(term2), "Expected false when the terms have the same arguments in a different order.");
    }

    @Test
    void testEquals_SameArguments_ReturnsTrue() {
        EvaluableFunction term1 = new EvaluableFunctionImpl("testFunc", invoker, List.of(termFactory.createOrGetVariable("x"), termFactory.createOrGetLiteral(42)));
        EvaluableFunction term2 = new EvaluableFunctionImpl("testFunc", invoker, List.of(termFactory.createOrGetVariable("x"), termFactory.createOrGetLiteral(42))); // Same arguments

        assertTrue(term1.equals(term2), "Expected true when both terms have the same name and the same arguments.");
    }


}
