package fr.boreal.test.model.logicalElements.impl;

import fr.boreal.model.functions.Invoker;
import fr.boreal.model.logicalElements.api.*;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.logicalElements.impl.ComputedAtomImpl;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ComputedAtomImplTest {
    private TermFactory termFactory;
    private PredicateFactory predicateFactory;
    private Predicate predicate;
    private Substitution substitution;
    private Invoker invoker;

    @BeforeEach
    void setup() {
        termFactory = SameObjectTermFactory.instance();
        predicateFactory = SameObjectPredicateFactory.instance();
        predicate = predicateFactory.createOrGetPredicate("testPredicate", 2);
        substitution = new SubstitutionImpl();

        // Simple Invoker that always returns a Literal<Boolean> for testing purposes
        invoker = terms -> termFactory.createOrGetLiteral(true);
    }

    @Test
    void testConstructor_WithListOfTerms_Valid() {
        Term term1 = termFactory.createOrGetLiteral(true);
        Term term2 = termFactory.createOrGetLiteral(false);
        Invoker invoker = terms -> termFactory.createOrGetLiteral(true); // Simple Invoker

        ComputedAtomImpl atom = assertDoesNotThrow(() -> new ComputedAtomImpl(invoker, predicate, List.of(term1, term2)));
        assertNotNull(atom);
    }

    @Test
    void testConstructor_WithArrayOfTerms_Valid() {
        Term term1 = termFactory.createOrGetLiteral(true);
        Term term2 = termFactory.createOrGetLiteral(false);
        Invoker invoker = terms -> termFactory.createOrGetLiteral(true);

        ComputedAtomImpl atom = assertDoesNotThrow(() -> new ComputedAtomImpl(invoker, predicate, term1, term2));
        assertNotNull(atom);
    }

    @Test
    void testConstructor_NullInvoker_ThrowsException() {
        Term term1 = termFactory.createOrGetLiteral(true);
        Term term2 = termFactory.createOrGetLiteral(false);

        assertThrows(IllegalStateException.class, () -> new ComputedAtomImpl(null, predicate, term1, term2));
    }

    @Test
    void testEval_AllTermsEvaluable_ReturnsTop() {
        Variable var1 = termFactory.createOrGetVariable("x1");
        Variable var2 = termFactory.createOrGetVariable("x2");
        Term trueLiteral = termFactory.createOrGetLiteral(true);
        Term falseLiteral = termFactory.createOrGetLiteral(false);

        substitution.add(var1, trueLiteral);
        substitution.add(var2, falseLiteral);

        Invoker invoker = terms -> termFactory.createOrGetLiteral(true); // Simulates invoker returning true

        ComputedAtomImpl atom = new ComputedAtomImpl(invoker, predicate, trueLiteral, falseLiteral);
        Atom result = atom.eval(substitution);
        assertEquals(new AtomImpl(Predicate.TOP), result);
    }


    @Test
    void testEval_AllTermsEvaluable_ReturnsBottom() {
        Variable var1 = termFactory.createOrGetVariable("x1");
        Variable var2 = termFactory.createOrGetVariable("x2");
        Term trueLiteral = termFactory.createOrGetLiteral(true);
        Term falseLiteral = termFactory.createOrGetLiteral(false);

        // Map variables to literals in the substitution
        substitution.add(var1, trueLiteral);
        substitution.add(var2, falseLiteral);

        Invoker invoker = terms -> termFactory.createOrGetLiteral(false); // Simulates invoker returning false

        ComputedAtomImpl atom = new ComputedAtomImpl(invoker, predicate, List.of(var1, var2));
        Atom result = atom.eval(substitution);
        assertEquals(new AtomImpl(Predicate.BOTTOM), result);
    }

    @Test
    void testEval_NonLiteralTerm_ReturnsNewComputedAtomImpl() {
        Variable nonLiteralTerm1 = termFactory.createOrGetVariable("x1");
        Variable nonLiteralTerm2 = termFactory.createOrGetVariable("x2");

        Invoker invoker = terms -> termFactory.createOrGetLiteral(true);
        ComputedAtomImpl atom = new ComputedAtomImpl(invoker, predicate, nonLiteralTerm1, nonLiteralTerm2);

        Atom result = atom.eval(substitution);
        assertInstanceOf(ComputedAtomImpl.class, result);
        assertNotSame(atom, result);
    }


    @Test
    void testEqualsAndHashCode() {
        Term term1 = termFactory.createOrGetLiteral(true);
        Term term2 = termFactory.createOrGetLiteral(false);
        Invoker invoker = terms -> termFactory.createOrGetLiteral(true);

        ComputedAtomImpl atom1 = new ComputedAtomImpl(invoker, predicate, term1, term2);
        ComputedAtomImpl atom2 = new ComputedAtomImpl(invoker, predicate, term1, term2);

        assertEquals(atom1, atom2);
        assertEquals(atom1.hashCode(), atom2.hashCode());
    }

    @Test
    void testNotEquals() {
        Term term1 = termFactory.createOrGetLiteral(true);
        Term term2 = termFactory.createOrGetLiteral(false);
        Term term3 = termFactory.createOrGetLiteral(true);
        Invoker invoker = terms -> termFactory.createOrGetLiteral(true);

        ComputedAtomImpl atom1 = new ComputedAtomImpl(invoker, predicate, term1, term2);
        ComputedAtomImpl atom2 = new ComputedAtomImpl(invoker, predicate, term1, term3);

        assertNotEquals(atom1, atom2);
    }

    @Test
    void testEval_ResultNotLiteral_ThrowsRuntimeException() {
        Variable var1 = termFactory.createOrGetVariable("x1");
        Variable var2 = termFactory.createOrGetVariable("x2");

        substitution.add(var1, termFactory.createOrGetLiteral(true));
        substitution.add(var2, termFactory.createOrGetLiteral(false));

        Invoker invoker = terms -> termFactory.createOrGetConstant("notALiteral"); // Returns a Constant instead of Literal

        ComputedAtomImpl atom = new ComputedAtomImpl(invoker, predicate, List.of(var1, var2));

        RuntimeException exception = assertThrows(RuntimeException.class, () -> atom.eval(substitution));
        assertTrue(exception.getMessage().contains("Invoker result is not a Literal<Boolean>"));
    }

    @Test
    void testEval_ResultLiteralNotBoolean_ThrowsRuntimeException() {
        Variable var1 = termFactory.createOrGetVariable("x1");
        Variable var2 = termFactory.createOrGetVariable("x2");

        substitution.add(var1, termFactory.createOrGetLiteral(true));
        substitution.add(var2, termFactory.createOrGetLiteral(false));

        Invoker invoker = terms -> termFactory.createOrGetLiteral("NotABoolean"); // Literal with a non-Boolean value

        ComputedAtomImpl atom = new ComputedAtomImpl(invoker, predicate, List.of(var1, var2));

        RuntimeException exception = assertThrows(RuntimeException.class, () -> atom.eval(substitution));
        assertTrue(exception.getMessage().contains("Invoker result is not a Literal<Boolean>"));
    }

    @Test
    void testEval_ResultLiteralBoolean_Success() {
        Variable var1 = termFactory.createOrGetVariable("x1");
        Variable var2 = termFactory.createOrGetVariable("x2");

        substitution.add(var1, termFactory.createOrGetLiteral(true));
        substitution.add(var2, termFactory.createOrGetLiteral(false));

        Invoker invoker = terms -> termFactory.createOrGetLiteral(true); // Literal with Boolean value (valid case)

        ComputedAtomImpl atom = new ComputedAtomImpl(invoker, predicate, List.of(var1, var2));

        Atom result = atom.eval(substitution);
        assertEquals(new AtomImpl(Predicate.TOP), result);
    }


    @Test
    void testEquals_SameInstance() {
        Term term1 = termFactory.createOrGetLiteral(true);
        Term term2 = termFactory.createOrGetLiteral(false);
        ComputedAtomImpl atom = new ComputedAtomImpl(invoker, predicate, List.of(term1, term2));
        assertEquals(atom, atom);  // this == o, should return true
    }

    @Test
    void testEquals_NullObject() {
        Term term1 = termFactory.createOrGetLiteral(true);
        Term term2 = termFactory.createOrGetLiteral(false);
        ComputedAtomImpl atom = new ComputedAtomImpl(invoker, predicate, List.of(term1, term2));
        assertNotEquals(atom, null);  // o == null, should return false
    }

    @Test
    void testEquals_DifferentClass() {
        Term term1 = termFactory.createOrGetLiteral(true);
        Term term2 = termFactory.createOrGetLiteral(false);
        ComputedAtomImpl atom = new ComputedAtomImpl(invoker, predicate, List.of(term1, term2));
        String differentClassObject = "DifferentClassObject";
        assertNotEquals(atom, differentClassObject);  // o not instance of ComputedAtom, should return false
    }

    @Test
    void testEquals_DifferentPredicate() {
        Predicate otherPredicate = predicateFactory.createOrGetPredicate("differentPredicate", 2);
        Term term1 = termFactory.createOrGetLiteral(true);
        Term term2 = termFactory.createOrGetLiteral(false);
        ComputedAtomImpl atom1 = new ComputedAtomImpl(invoker, predicate, List.of(term1, term2));
        ComputedAtomImpl atom2 = new ComputedAtomImpl(invoker, otherPredicate, List.of(term1, term2));
        assertNotEquals(atom1, atom2);  // Different predicate, should return false
    }

    @Test
    void testEquals_DifferentTerms() {
        Term term1 = termFactory.createOrGetLiteral(true);
        Term term2 = termFactory.createOrGetLiteral(false);
        ComputedAtomImpl atom1 = new ComputedAtomImpl(invoker, predicate, List.of(term1, term1));
        ComputedAtomImpl atom2 = new ComputedAtomImpl(invoker, predicate, List.of(term1, term2));
        assertNotEquals(atom1, atom2);  // Different terms, should return false
    }

    @Test
    void testEquals_SamePredicateAndTerms() {
        Term term1 = termFactory.createOrGetLiteral(true);
        Term term2 = termFactory.createOrGetLiteral(false);
        ComputedAtomImpl atom1 = new ComputedAtomImpl(invoker, predicate, List.of(term1, term2));
        ComputedAtomImpl atom2 = new ComputedAtomImpl(invoker, predicate, List.of(term1, term2));
        assertEquals(atom1, atom2);  // Same predicate and terms, should return true
    }
}
