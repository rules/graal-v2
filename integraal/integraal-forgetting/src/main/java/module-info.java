/**
 * Module for forgetting elements of InteGraal
 *
 * @author Paul Fontaine
 *
 */
module fr.boreal.forgetting{

	requires transitive fr.boreal.model;

	requires fr.boreal.unifiers;
	requires fr.boreal.redundancy;
	requires fr.boreal.io;
    requires fr.boreal.backward_chaining;

	requires org.apache.commons.lang3;

    exports fr.boreal.forgetting;

}