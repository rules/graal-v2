package fr.boreal.test.forgetting;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import fr.boreal.forgetting.Forgetting;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;

class ForgettingTest extends TestClass{

	@Test
	void testClosing1() {
		RuleBase rb1 = new RuleBaseImpl(Stream.of(r1, r2).collect(Collectors.toList()));
		RuleBase rb1Closing = Forgetting.bodyUnfolding(rb1);
		testRbEq(new RuleBaseImpl(Set.of(r1, r2, r1r2)),rb1Closing);
	}

	@Test
	void testClosing2() throws IOException {
		testRbEq("src/test/resources/ruleBase.dlgp",
				Forgetting::bodyUnfolding,"src/test/resources/rbClosing.dlgp");
	}

	@Test
	void testForgetting1() throws IOException{
		testRbEq("src/test/resources/ruleBase.dlgp",
				rb -> Forgetting.naiveDatalogForget(rb, p -> p.label().equals("p") ||
						p.label().equals("t"))
				,"src/test/resources/forgettingPT.dlgp");
	}

	@Test
	void testForgetting2() throws IOException{
		testRbEq("src/test/resources/ruleBase.dlgp",
				rb -> Forgetting.naiveDatalogForget(rb, p -> p.label().equals("q"))
				,"src/test/resources/forgettingQ.dlgp");
	}

	@Test
	void testFastForgetting1() throws IOException{
		testRbEq("src/test/resources/ruleBase.dlgp",
				rb -> Forgetting.semiNaiveDatalogForget(rb, p -> p.label().equals("q"))
				,"src/test/resources/fastForgettingQ.dlgp");
	}
}
