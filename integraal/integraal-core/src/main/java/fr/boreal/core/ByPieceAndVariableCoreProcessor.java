package fr.boreal.core;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.storage.natives.FactBaseDelAtomsWrapper;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;
import fr.lirmm.boreal.util.PiecesSplitter;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Compute the core of a fact base with an algorithm that computes the core by
 * removing a variable in the atom set and try to retract the piece that contains
 * this variable into the fact base
 * Based on the algorithm presented in "Efficient Core Computation in Data Exchange",
 * p.9:41, by G. Gottlob and A. Nash
 *
 * @author Guillaume Pérution-Kihli
 *
 */


public class ByPieceAndVariableCoreProcessor implements CoreProcessor {
    private final FOQueryEvaluator<FOFormula> evaluator;

    public ByPieceAndVariableCoreProcessor() {
        this.evaluator = GenericFOQueryEvaluator.defaultInstance();
    }

    public ByPieceAndVariableCoreProcessor(FOQueryEvaluator<FOFormula> evaluator) {
        this.evaluator = evaluator;
    }

    @Override
    public void computeCore(FactBase fb, Set<Variable> frozenVariables) {
        // We need to compute the set of all variables that are not frozen
        // The pieces of "a" will be computed with these variables
        Set<Variable> notFrozenVariables = new HashSet<>(
                fb.getVariables().filter(var -> !frozenVariables.contains(var)).toList());

        // We compute the pieces induced by the variables that are not frozen
        // We don't want grounded atoms, so we put "false" in the first parameter
        PiecesSplitter splitter = new PiecesSplitter(false, notFrozenVariables);
        Collection<Collection<Atom>> pieces = splitter.split(fb.getAtomsInMemory());

        // We create a substitution to fix the frozen variables in the computing of the homomorphism
        Substitution frozenVariablesSubstitution = new SubstitutionImpl();
        for (Variable fv : frozenVariables) {
            frozenVariablesSubstitution.add(fv, fv);
        }

        // For each piece, we will test if we can remove one of the variable of the piece in the atom set
        // And we will try to retract the piece into the atom set without the variable
        for (Collection<Atom> piece : pieces) {
            SimpleInMemoryGraphStore p = new SimpleInMemoryGraphStore(piece);
            // Get the set of variables in the piece
            List<Variable> variables = p.getVariables().toList();

            for (Variable v : variables) {
                // We get all the atoms containing v
                Set<Atom> atomsUsingVar = p.getAtoms(v).collect(Collectors.toSet());

                // We virtually remove the atoms containing v
                FactBaseDelAtomsWrapper virtual_fact_base = new FactBaseDelAtomsWrapper(fb, atomsUsingVar);

                // We check the existence of a homomorphism from virtual_fact_base
                // If there exists such a homomorphism, we remove these atoms from fb
                FOQuery<FOFormulaConjunction> q = FOQueryFactory.instance().createOrGetQuery(
                        FOFormulaFactory.instance().createOrGetConjunction(p),
                        List.of());
                if (this.evaluator.existHomomorphism(q, virtual_fact_base, frozenVariablesSubstitution)) {
                    fb.removeAll(atomsUsingVar);
                }
            }
        }
    }
}
