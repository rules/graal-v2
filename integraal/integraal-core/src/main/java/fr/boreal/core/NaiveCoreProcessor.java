package fr.boreal.core;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.storage.natives.FactBaseDelAtomsWrapper;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class NaiveCoreProcessor implements CoreProcessor {
    private final FOQueryEvaluator<FOFormula> evaluator;

    public NaiveCoreProcessor() {
        this.evaluator = GenericFOQueryEvaluator.defaultInstance();
    }

    public NaiveCoreProcessor(FOQueryEvaluator<FOFormula> evaluator) {
        this.evaluator = evaluator;
    }

    @Override
    public void computeCore(FactBase fb, Set<Variable> frozenVariables) {
        // Get the set of variables in the fact base
        List<Variable> variables = fb.getVariables().filter(v -> !frozenVariables.contains(v)).toList();

        // We create a substitution to fix the answer variables in the computing of the homomorphism
        Substitution frozenVariablesSubstitution = new SubstitutionImpl();
        for (Variable fv : frozenVariables) {
            frozenVariablesSubstitution.add(fv, fv);
        }

        // For each variable v, we virtually remove v from the fact base, and
        // if it is equivalent to the initial fact base, we remove atoms containing v
        // This algorithm uses this property: a core of a graph G is an induced subgraph of G
        for (Variable v: variables) {
            // We get all the atoms containing v
            Set<Atom> atomsUsingVar = fb.getAtoms(v).collect(Collectors.toSet());

            // We virtually remove the atoms containing v
            FactBaseDelAtomsWrapper virtual_fact_base = new FactBaseDelAtomsWrapper(fb, atomsUsingVar);

            // We check the existence of a homomorphism from virtual_fact_base
            // If there exists such a homomorphism, we remove these atoms from fb
            FOQuery<FOFormulaConjunction> q = FOQueryFactory.instance().createOrGetQuery(
                    FOFormulaFactory.instance().createOrGetConjunction(fb), List.of());
            if (this.evaluator.existHomomorphism(q, virtual_fact_base, frozenVariablesSubstitution)) {
                fb.removeAll(atomsUsingVar);
            }
        }
    }
}
