package fr.boreal.core;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;
import fr.lirmm.boreal.util.PiecesSplitter;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Compute the core of a fact base with an algorithm that computes the core by
 * computing the retractions of each piece into the fact base.
 * In the "exhaustive" variant, for each piece, we search all the homomorphisms
 * that retract the piece into the fact base, and we keep the homomorphism that
 * minimizes the number of remaining variables in the piece.
 * In the "by specialization" variant, we specialize the piece when we find
 * a homomorphism h that reduces the number of the variables in the piece (the
 * specialization is done by using h on the piece and then search new
 * homomorphisms that retract more variables of the initial piece).
 * In the "by deletion" variant, when we found such a homomorphism, we remove
 * the retracted variables from the piece and the fact base and search new
 * homomorphisms on the result.
 *
 * @author Guillaume Pérution-Kihli
 *
 */

public class ByPieceCoreProcessor implements CoreProcessor {
    private final Variant variant; // Variant of the algorithm to use
    private final FOQueryEvaluator<FOFormula> evaluator;
    
    public enum Variant {
        EXHAUSTIVE,
        BY_SPECIALISATION,
        BY_DELETION
    }

    public ByPieceCoreProcessor(Variant variant, FOQueryEvaluator<FOFormula> evaluator) {
        this.variant = variant;
        this.evaluator = evaluator;
    }

    public ByPieceCoreProcessor(Variant variant) {
        this(variant, GenericFOQueryEvaluator.defaultInstance());
    }

    public ByPieceCoreProcessor(FOQueryEvaluator<FOFormula> evaluator) {
        this(Variant.BY_DELETION, evaluator);
    }

    public ByPieceCoreProcessor() {
        this(Variant.BY_DELETION, GenericFOQueryEvaluator.defaultInstance());
    }

    @Override
    public void computeCore(FactBase fb, Set<Variable> frozenVariables) {
        // We need to compute the set of all variables that are not frozen
        // The pieces of "a" will be computed with these variables
        Set<Variable> notFrozenVariables = new HashSet<>(
                fb.getVariables().filter(var -> !frozenVariables.contains(var)).toList());

        // We compute the pieces induced by the variables that are not frozen
        // We don't want grounded atoms, so we put "false" in the first parameter
        PiecesSplitter splitter = new PiecesSplitter(false, notFrozenVariables);
        Collection<Collection<Atom>> pieces = splitter.split(fb.getAtomsInMemory());

        // We create a substitution to fix the frozen variables in the computing of the homomorphism
        Substitution frozenVariablesSubstitution = new SubstitutionImpl();
        for (Variable fv : frozenVariables) {
            frozenVariablesSubstitution.add(fv, fv);
        }

        // For each piece, we will test if we can remove one of the variable of the piece in the fact base
        // And we will try to retract the piece into the fact base without the variable
        for (Collection<Atom> piece : pieces) {
            SimpleInMemoryGraphStore p = new SimpleInMemoryGraphStore(piece);

            switch (variant) {
                case BY_DELETION:
                    this.retractPiecesByDeletion(fb, p, frozenVariablesSubstitution, frozenVariables);
                    break;
                case BY_SPECIALISATION:
                    this.retractPiecesBySpecialisation(fb, p, frozenVariablesSubstitution, frozenVariables);
                    break;
                case EXHAUSTIVE:
                    this.retractPiecesExhaustive(fb, p, frozenVariablesSubstitution, frozenVariables);
                    break;
            }
        }
    }

    private void retractPiecesByDeletion (
            FactBase fb,
            SimpleInMemoryGraphStore p,
            Substitution frozenVariablesSubstitution,
            Set<Variable> frozenVariables) {
        FOQuery<FOFormulaConjunction> q = FOQueryFactory.instance().createOrGetQuery(FOFormulaFactory.instance().createOrGetConjunction(p), null);

        // Compute the retractions
        Iterator<Substitution> it = evaluator.homomorphism(q, fb, frozenVariablesSubstitution);

        // We count the number of variables that we can't retract
        long nbNotFrozenVariables = p.getVariables()
                .filter(v -> !frozenVariables.contains(v))
                .count();

        // While there is a retraction to treat
        while (it.hasNext()) {
            Substitution sub = it.next(); // Get the retraction
            sub.removeIdentity();
            if (sub.isEmpty()) {
                continue;
            }

            Set<Variable> deletedVars = new HashSet<>(sub.keys());
            long nbVariablesRetracted = deletedVars.size();

            // If we can remove more variables than a precedent retraction
            if (nbVariablesRetracted > 0) {
                Collection<Atom> toRemove = new ArrayList<>();
                for (Variable v : deletedVars) {
                    Iterator<Atom> it_var = p.getAtomsByTerm(v);
                    while (it_var.hasNext()) {
                        toRemove.add(it_var.next());
                    }
                }
                fb.removeAll(toRemove);
                p.removeAll(toRemove);

                // If we retracted all the variables, we can stop the while
                if (nbVariablesRetracted == nbNotFrozenVariables) {
                    break;
                }

                nbNotFrozenVariables = p.getVariables()
                        .filter(v -> !frozenVariables.contains(v))
                        .count();

                // We search the retractions with the deleted variables fixed to their new value
                q = FOQueryFactory.instance().createOrGetQuery(
                        FOFormulaFactory.instance().createOrGetConjunction(p),
                        null);
                it = evaluator.homomorphism(q, fb, frozenVariablesSubstitution);
            }
        }
    }

    private void retractPiecesBySpecialisation (
            FactBase fb,
            SimpleInMemoryGraphStore p,
            Substitution frozenVariablesSubstitution,
            Set<Variable> frozenVariables) {
        FOQuery<FOFormulaConjunction> q = FOQueryFactory.instance().createOrGetQuery(
                FOFormulaFactory.instance().createOrGetConjunction(p),
                null);

        // Compute the retractions
        Iterator<Substitution> it = evaluator.homomorphism(q, fb, frozenVariablesSubstitution);

        // Variables of p
        Set<Variable> p_vars = p.getVariables().collect(Collectors.toSet());

        // We count the number of variables that we can retract
        long nbNotFrozenVariables = p_vars.stream()
                .filter(v -> !frozenVariables.contains(v))
                .count();

        // At the beginning, no variable has been retracted
        long maxNbVariablesRetracted = 0;
        // We initialize the set of the variables that we have deleted
        Set<Variable> toRemove = new HashSet<>();

        // While there is a retraction to treat
        while (it.hasNext()) {
            Substitution sub = it.next(); // Get the retraction
            sub.removeIdentity();
            if (sub.isEmpty()) {
                continue;
            }

            Set<Variable> deletedVars = new HashSet<>(sub.keys());
            deletedVars.addAll(toRemove);
            long nbVariablesRetracted = deletedVars.size();

            // If we can remove more variables than a precedent retraction
            if (nbVariablesRetracted > maxNbVariablesRetracted) {
                // Update the set of variables to remove
                toRemove = deletedVars;
                // Update the maximum number of retracted variable
                maxNbVariablesRetracted = nbVariablesRetracted;

                // We fixed the deleted variables to their new value
                for (Variable v : toRemove) {
                    Term img = sub.createImageOf(v);
                    if (img instanceof Variable && !p_vars.contains(img)) {
                        frozenVariablesSubstitution.add(v, img);
                    }
                }

                // Update the list of variables of p
                p_vars = p.getVariables().collect(Collectors.toSet());

                // If we retracted all the variables, we can stop the while
                if (maxNbVariablesRetracted == nbNotFrozenVariables) {
                    break;
                }

                // We search the retractions with the deleted variables fixed to their new value
                q = FOQueryFactory.instance().createOrGetQuery(
                        FOFormulaFactory.instance().createOrGetConjunction(p),
                        null);
                it = evaluator.homomorphism(q, fb, frozenVariablesSubstitution);

            }
        }

        removeFromFactBase(toRemove, p, fb);
    }

    private void retractPiecesExhaustive (
            FactBase fb,
            SimpleInMemoryGraphStore p,
            Substitution frozenVariablesSubstitution,
            Set<Variable> frozenVariables) {
        FOQuery<FOFormulaConjunction> q = FOQueryFactory.instance().createOrGetQuery(
                FOFormulaFactory.instance().createOrGetConjunction(p),
                null);

        // Compute the retractions
        Iterator<Substitution> it = evaluator.homomorphism(q, fb, frozenVariablesSubstitution);

        // We count the number of variables that we can't retract
        long nbNotFrozenVariables = p.getVariables()
                .filter(v -> !frozenVariables.contains(v))
                .count();

        // At the beginning, no variable has been retracted
        long maxNbVariablesRetracted = 0;
        // We initialize the set of the variables that we have deleted
        Set<Variable> toRemove = new HashSet<>();

        // While there is a retraction to treat
        while (it.hasNext()) {
            Substitution sub = it.next(); // Get the retraction
            sub.removeIdentity();
            if (sub.isEmpty()) {
                continue;
            }

            Set<Variable> deletedVars = new HashSet<>(sub.keys());
            long nbVariablesRetracted = deletedVars.size();

            // If we can remove more variables than a precedent retraction
            if (nbVariablesRetracted > maxNbVariablesRetracted) {
                // Update the set of variables to remove
                toRemove = deletedVars;
                // Update the maximum number of retracted variable
                maxNbVariablesRetracted = nbVariablesRetracted;

                // If we retracted all the variables, we can stop the while
                if (maxNbVariablesRetracted == nbNotFrozenVariables) {
                    break;
                }
            }
        }

        removeFromFactBase(toRemove, p, fb);
    }

    private void removeFromFactBase(Set<Variable> toRemove, SimpleInMemoryGraphStore p, FactBase fb) {
        for (Variable v : toRemove) {
            Iterator<Atom> it_var = p.getAtomsByTerm(v);
            while (it_var.hasNext()) {
                fb.remove(it_var.next());
            }
        }
    }
}