/**
 * Module for computing core of conjunctions in InteGraal
 * 
 * @author Florent Tornil
 *
 */
module fr.boreal.core {

    requires transitive fr.boreal.model;

    requires fr.boreal.query_evaluation;
    requires fr.lirmm.boreal.util;
    requires fr.boreal.storage;
    requires fr.boreal.io;
    requires jsonld.java;

    exports fr.boreal.core;
}