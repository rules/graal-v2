package fr.boreal.test.core;

import fr.boreal.core.*;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.io.dlgp.ParserResult;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.query_evaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.storage.natives.SimpleInMemoryGraphStore;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

class CoreProcessorTest {
    private static final FOQueryEvaluator<FOFormula> evaluator = GenericFOQueryEvaluator.defaultInstance();

    static Stream<Object[]> provideData() {
        return Arrays.stream(new CoreProcessor[] {
                        new ByPieceCoreProcessor(ByPieceCoreProcessor.Variant.BY_SPECIALISATION),
                        new ByPieceCoreProcessor(ByPieceCoreProcessor.Variant.BY_DELETION),
                        new ByPieceCoreProcessor(ByPieceCoreProcessor.Variant.EXHAUSTIVE),
                        new MultiThreadsByPieceCoreProcessor(
                                16, MultiThreadsByPieceCoreProcessor.Variant.BY_SPECIALISATION),
                        new MultiThreadsByPieceCoreProcessor(
                                16, MultiThreadsByPieceCoreProcessor.Variant.BY_DELETION),
                        new MultiThreadsByPieceCoreProcessor(
                                16, MultiThreadsByPieceCoreProcessor.Variant.EXHAUSTIVE),
                        new NaiveCoreProcessor(),
                        new ByPieceAndVariableCoreProcessor()
                })
                .flatMap(processor -> provideTestCases(processor).stream());
    }

    private static Collection<Object[]> provideTestCases(CoreProcessor processor) {
        return Arrays.asList(new Object[][] {
               {"p(Z,Z), p(Z,X), p(Y,X), p(U,U), p(U,Z), p(X,a), p(a,a).", 1, 0, processor},
                {"p(T,X), p(X,Z), p(T,Y), p(Y,Y), p(Y,U).", 1, 1, processor},
                {"p(a,X),p(X,X),p(X,Y),p(a,U),p(U,U), p(b,U),p(a,Z),p(Z,Z),p(Z,T),p(b,S),p(S,V).", 3, 1, processor},
                {"p(a,X),p(Y,X),p(Y,Z),p(V,Z),p(V,U),p(b,U),p(a,J), p(J,c),p(b,J),p(a,I),p(I,c),p(b,I).",
                        3, 1, processor},
                {"r(X), t(X,a,b), s(a,z), r(Y), t(Y,a,b), relatedTo(Y,z).", 4, 1, processor},
                {"p(a,X), p(X,Z), p(X,Y), p(Y,Z).", 4, 3, processor},
                {"p(Z,b), p(a,Z), p(a,Y), p(Y,b).", 2, 1, processor},
                {"p(a,b), p(a,X), p(X,Y), p(Y,Z), p(a,U), p(U,V), p(V,W), "
                        + "p(a,I), p(I,J), p(J,K), p(K,L).", 5, 4, processor},
                {"p(a,X), p(X,Z), p(X,Y), p(Y,Z), p(a,a).", 1, 0, processor},
                {"p(Z0,V0), q(a, Z0), q(Z0,a), q(Z0,c), q(c,Z0), p(a,b), p(c,d), p(Z1,V1), q(a, Z1), q(Z1,a), " +
                        "q(Z1,c), q(c,Z1).", 7, 2, processor},
                {"@computed fct:<stdfct>\np(X,fct:sum(X,1)),p(Y,fct:sum(Y,1)).", 1, 1, processor}
        });
    }

    @ParameterizedTest
    @MethodSource("provideData")
    public void computeCoreTest(
            String stringAtomSet,
            int numberOfAtomsInCore,
            int numberOfVariablesInCore,
            CoreProcessor processor) {
        ParserResult it = DlgpParser.parseDLGPString(stringAtomSet);
        assert it != null;

        SimpleInMemoryGraphStore fact_base = new SimpleInMemoryGraphStore(it.atoms());
        SimpleInMemoryGraphStore core = new SimpleInMemoryGraphStore(fact_base.getAtomsInMemory());
        processor.computeCore(core);

        if(numberOfAtomsInCore!= core.size()){
            System.out.println("core "+core + " computed for " + stringAtomSet);
        }
        Assertions.assertEquals(numberOfAtomsInCore, core.size());
        Assertions.assertEquals(numberOfVariablesInCore, core.getVariables().distinct().count());
        Assertions.assertTrue(isACore(core));
        Assertions.assertTrue(isEquivalent(fact_base, core));
    }

    @ParameterizedTest
    @MethodSource("provideData")
    public void computeCoreFrozenTest(
            String stringAtomSet,
            int numberOfAtomsInCore,
            int numberOfVariablesInCore,
            CoreProcessor processor) {
        ParserResult it = DlgpParser.parseDLGPString(stringAtomSet);
        assert it != null;

        SimpleInMemoryGraphStore fact_base = new SimpleInMemoryGraphStore(it.atoms());

        for (Variable v: fact_base.getVariables().toList()) {
            SimpleInMemoryGraphStore core = new SimpleInMemoryGraphStore(fact_base.getAtomsInMemory());

            processor.computeCore(core, Set.of(v));

            Assertions.assertTrue(isACore(freeze(core, List.of(v))));
            Assertions.assertTrue(isEquivalent(freeze(fact_base, List.of(v)), freeze(core, List.of(v))));
        }
    }

    private static FactBase freeze (FactBase base, Collection<Variable> frozenVariables) {
        Substitution frozenVariablesSubstitution = new SubstitutionImpl();
        for (Variable fv : frozenVariables) {
            frozenVariablesSubstitution.add(
                    fv,
                    SameObjectTermFactory.instance().createOrGetConstant("c_" + fv.label()));
        }
        return new SimpleInMemoryGraphStore(base.getAtoms()
                .map(frozenVariablesSubstitution::createImageOf)
                .toList());
    }

    private static boolean isACore(FactBase fb) {
        // If a fact base is a core, then, all endomorphisms are isomorphisms
        FOQuery<?> q = FOQueryFactory.instance().createOrGetQuery(
                FOFormulaFactory.instance().createOrGetConjunction(fb),
                List.of(),
                null);
        Iterator<Substitution> it = evaluator.evaluate(q, fb);
        while (it.hasNext()) {
            Substitution s = it.next();
            if (fb.getAtomsInMemory().stream()
                    .map(s::createImageOf)
                    .distinct()
                    .count() != fb.size()) {
                return false;
            }
        }
        return true;
    }

    private static boolean isEquivalent(FactBase fb1, FactBase fb2) {
        FOQuery<?> q1 = FOQueryFactory.instance().createOrGetQuery(
                FOFormulaFactory.instance().createOrGetConjunction(fb1),
                List.of(),
                null);
        FOQuery<?> q2 = FOQueryFactory.instance().createOrGetQuery(
                FOFormulaFactory.instance().createOrGetConjunction(fb2),
                List.of(),
                null);

        return evaluator.existHomomorphism(q1, fb2) && evaluator.existHomomorphism(q2, fb1);
    }
}